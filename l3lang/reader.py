#
# Author:  Michael H. Hohn (mhhohn@lbl.gov)
#
# Copyright (c) 2006, The Regents of the University of California
# 
# See legal.txt and license.txt
#

#
#*    The reader (lexer and parser) for the data manipulation language.
#
from copy import deepcopy
import re, pdb, sys, pprint, os, yacc
from l3lang import ast
from l3lang.ast import                                                  \
     Function,                                                          \
     Macro,                                                             \
     Member,                                                            \
     Call,                                                              \
     Float,                                                             \
     If,                                                                \
     Immediate,                                                         \
     Inline,                                                            \
     Int,                                                               \
     Labeled,                                                           \
     Let,                                                               \
     List,                                                              \
     Map,                                                               \
     Marker,                                                            \
     MarkerTyped,                                                       \
     Program,                                                           \
     Return,                                                            \
     Set,                                                               \
     String,                                                            \
     Subdir,                                                            \
     Symbol,                                                            \
     Tuple,                                                             \
     viewList,                                                          \
     aList,                                                             \
     aNone,                                                             \
     row_adjust

from types import *


#*    lexer, using Python tokenize wrapper
#**        Python tokenizer
import sys, token, tokenize, pdb

def dump_tokens(generator):
    for i in generator:
        (type, string, begin, end, line) = i
        print "(%s, %r, %s, %s, %r)" % (token.tok_name[type], string,
                                        begin, end, line)  

#
#**        Wrapper for string input to tokenize.generate_tokens
class string_readline:
    pass
def __init__(self, stng):
    # HERE.  windows? 
    self._lines = map(lambda s: s + '\n', stng.split("\n"))
string_readline.__init__ = __init__

def __call__(self, *size):                     # readline-interface
    # ignore size
    if len(self._lines) > 0:
        line = self._lines[0]
        del self._lines[0]
        return line
    else:
        return ""
string_readline.__call__ = __call__

#
#**        Wrapper for file input to tokenize.generate_tokens
def file_readline(fname):
    file = open(fname, 'r')
    def readline(*size):
        return file.readline(*size)
    return readline


#
#**        yacc-compatible token type
# The lexer requires tokens to be defined as class instances with
# t.type, t.value, and t.lineno attributes. By default, tokens are
# created as instances of the LexToken class defined internally to
# lex.py. If desired, you can create new kinds of tokens provided that
# they have the three required attributes. However, in practice, it is
# probably safer to stick with the default. 
# E.g.:
#     LexToken(NUMBER,3,2)
#     LexToken(PLUS,'+',2)


class Pytoken:
    pass
def __init__(self, type, value, lineno, _meta = None):
    self.type   = type
    self.value  = value
    self.lineno = lineno - 1
    # meta is the (type, value, begin, end, line_text) tuple from tokenize
    self._meta  = _meta
Pytoken.__init__ = __init__

def char_column_range(self):
    return self._meta[2][1], self._meta[3][1]
Pytoken.char_column_range = char_column_range

def char_range(self):
    # Return ((line, col), (line, col)) range.
    return (self._meta[2]), (self._meta[3])
Pytoken.char_range = char_range

def __str__(self):
    return "Pytoken(%s,%r,%d)" % (self.type, self.value, self.lineno)
Pytoken.__str__ = __str__

#
#**        yacc-compatible lexer
# x must be a Lexer object that minimally has a x.token() method for
# retrieving the next token. If an input string is given to
# yacc.parse(), the lexer must also have an x.input() method.  
#
class Pylexer:
    pass

def __init__(self):
    self._previous = 0
Pylexer.__init__ = __init__

Pylexer.reserved = {
    'break'   	: 'BREAK',
    'def'   	: 'DEF',
    'if'    	: 'IF',
    'else'  	: 'ELSE',
    'elif'  	: 'ELIF',
    'for'       : 'FOR',
    'from'  	: 'FROM',
    'in'        : 'IN',
    'is'        : 'IS',
    'lambda'  	: 'LAMBDA',
    # # 'loop'  	: 'LOOP',
    # # 'list'  	: 'LIST',
    'program' 	: 'PROGRAM',
    'print' 	: 'PRINT',
    'while' 	: 'WHILE',
    'return'	: 'RETURN',
    'True'  	: 'TRUE',
    'False' 	: 'FALSE',
    'not' 		: 'NOT',
    'and' 		: 'AND',
    'or' 		: 'OR',
    # l3 additions
    'inline'    : 'INLINE',
    'subdir'    : 'SUBDIR',
	}
Pylexer.dedent_continuations = {
    'else'  : 'ELSE',
    'elif'  : 'ELIF',
	}

Pylexer.opers = {
    '&'   : 'AMPERSAND',
    '|'   : 'BAR',
    ':'   : 'COLON',
    ','   : 'COMMA',
    '.'   : 'DOT',
    '='   : 'EQUAL',
    '=='  : 'EQUAL_EQUAL',
    '!'   : 'EXCL',
    '!!'  : 'EXCL_EXCL',
    '!='  : 'EXCL_EQUAL',                
    '>'   : 'GREATER',
    '>='  : 'GREATEREQUAL',
    '{'   : 'LBRACE',
    '['   : 'LBRACKET',
    '<'   : 'LESS',
    '<='  : 'LESSEQUAL',
    '('   : 'LPAREN',
    '-'   : 'MINUS',
    '%'   : 'PERCENT',
    '+'   : 'PLUS',
    '}'   : 'RBRACE',
    ']'   : 'RBRACKET',
    ')'   : 'RPAREN',
    ';'   : 'SEMI',
    '/'   : 'SLASH',
    '//'   : 'SLASHSLASH',
    '**'  : 'STARSTAR',
    '*'   : 'STAR',
    }

def get_token(self):
    "Return the next token, as Pytoken."
    # This indirection (get_token / _real_token) is here to insert
    # fake semicolons between multiple dedents, to make the parser
    # happy. 
    #
    # Also, in case of
    #     Pytoken(SEMI,';',3)
    #     Pytoken(SEMI,'\n',3)
    # return only the first token.
    #
    # Also, the 'not in' and 'is not' sequences are combined.
    # 
    
    if self._previous:
        if self._previous.type == 'DEDENT':
            next_tok = self._real_token()
            if next_tok == None:
                return next_tok

            if next_tok.value in Pylexer.dedent_continuations:
                self._previous = 0
                return next_tok
            elif next_tok.type == 'DEDENT':
                # Multiple dedents in a row; no fake semicolon needed for
                # this one.  This also avoids the need to keep a full stack
                # of tokens...
                self._previous = next_tok
                return next_tok
            else:
                self._previous = next_tok
                return Pytoken('SEMI', '<fake;>', next_tok.lineno,
                               _meta = next_tok)

        elif self._previous.type == 'SEMI':
            next_tok = self._real_token()
            if self._previous.value == ';':
                self._previous = 0
                if next_tok.type == 'SEMI':
                    return self.token()
                else:
                    return next_tok
                    
            else:
                raise SyntaxError, \
                      "Internal error: unexpected token: " + str(next_tok)

        else:
            rv = self._previous         # Finish up. 
            self._previous = 0
            return rv
    else:
        return self._real_token()
Pylexer.token = get_token                 # avoid overlap w/ token module.


def _real_token(self):
    # Get tokenize token.
    try:
        tok = self._token_gen.next()
    except StopIteration:
        return None                     # EOF for yacc
    (type, value, begin, end, line_text) = tok
    type = token.tok_name[type]
    line = begin[0]

    def show_error_info():
        lbeg = begin[1]
        lend = end[1]            # character positions
        print "Syntax error:%d:%d-%d at '%s':" % \
              (line, lbeg, lend, value)
        print line_text
        print "%s%s" % (" " * lbeg, "^" * (lend-lbeg))
        
    # Map it.
    if type in ['COMMENT', 'NL']:
        return self.token()          # skip

    elif type == 'NAME':                # symbols and keywords
        if value == 'is':
            # Check for 'is not' sequence.
            tok_nxt = self._real_token()
            if tok_nxt.value == 'not':
                return Pytoken('ISNOT', 'is_not', line, _meta = tok)
            else:
                self._previous = tok_nxt
                type = Pylexer.reserved.get(value, 'SYMBOL')
                return Pytoken(type, 'is_', line, _meta = tok)

        elif value == 'not':
            # Check for 'not in' sequence.
            tok_nxt = self._real_token()
            if tok_nxt.value == 'in':
                return Pytoken('NOTIN', 'not_in', line, _meta = tok)
            else:
                self._previous = tok_nxt
                type = Pylexer.reserved.get(value, 'SYMBOL')
                return Pytoken(type, value, line, _meta = tok)

        else:
            type = Pylexer.reserved.get(value, 'SYMBOL')
            return Pytoken(type, value, line, _meta = tok)

    elif type == 'NUMBER':
        # ("1.0 + 2j") is number, op, number.
        # But numbers have no common 'number' base type -- only 'object'
        if ( (value.find('.') == -1) and # No period.
             (value.find('e') == -1)):   # No exponent.
            return Pytoken('INT', int(value), line, _meta = tok)
        else:
            return Pytoken('FLOAT', float(value), line, _meta = tok)

    elif type == 'STRING':
        # Convert raw string to internal form. 
        value = eval(value)
        return Pytoken(type, value, line, _meta = tok)

    # the tokenize module classifies many things as OP -- maybe use
    # plex instead.
    elif type == 'OP':                  
        type = Pylexer.opers.get(value)
        if type is None:
            show_error_info()
            raise SyntaxError, "Unexpected operator: " + str(value)
        elif type == 'SEMI':
            self._previous = Pytoken('SEMI', value, line, _meta = tok)
            return self._previous
        else:
            return Pytoken(type, value, line, _meta = tok)
    
    elif type == 'NEWLINE':
        return Pytoken('SEMI', value, line, _meta = tok)

    elif type == 'INDENT':
        return Pytoken('INDENT', value, line, _meta = tok)

    elif type == 'DEDENT':
        self._previous = Pytoken('DEDENT', value, line, _meta = tok)
        return self._previous

    elif type == 'ERRORTOKEN':
        # (ERRORTOKEN, !, (1, 0), (1, 1), '!! hello')
        if value == "!":
            return Pytoken('EXCL', value, line, _meta = tok)
        else:
            if value.isspace():
                return self.token()
            else:
                show_error_info()
                if value in ['"', "'"]:
                    print "Maybe use triple quotes?\n"
                raise SyntaxError, "Invalid token: " + repr(value)

    elif type == 'ENDMARKER':
        return None

    else:
        show_error_info()        
        raise SyntaxError, "Unknown token type: " + str(type)
Pylexer._real_token = _real_token 

def input(self, strng):
    "Reset lexer, using new string 'strng'."
    self._token_gen = tokenize.generate_tokens(string_readline(strng))
Pylexer.input = input

def input_readline(self, readline):
    "Lexer input from readline()-compatible callable."
    self._token_gen = tokenize.generate_tokens(readline)
Pylexer.input_readline = input_readline


#*    parser and code related
#**        Valid tokens
tokens = (
    'AMPERSAND',
	'AND',
    'BAR',
    'BREAK',
##    'CHAR',
    'COLON',
    'COMMA',
##    'COMMENT',
    'DEDENT',
    'DEF', 
    'DOT',
    'ELSE',
    # 'EOF' is not needed; the lexer returns None when done. 
    'EQUAL',
    'EQUAL_EQUAL',
    'EXCL',
    'EXCL_EXCL',
    'EXCL_EQUAL',                       # usually not equal
    'FALSE',
    'FLOAT',
    'FOR',
    'FROM',
    'GREATER',
    'GREATEREQUAL',
    'IF',
    'IN',
    'INT',
    'INLINE',
    'INDENT',
    'IS',
    'ISNOT',
    'LABEL',                   # graph id label
    'LAMBDA',
# #    'LIST',
# #    'LOOP',
    'LBRACE',
    'LBRACKET',
    'LESS',
    'LESSEQUAL',
    'LESSMINUS',
    'LPAREN',
    'MINUS',
##    'MINUSGREATER',
	'NOT',
	'NOTIN',
	'OR',
    'PERCENT',
    'PLUS',
    'PRINT',
    'PROGRAM',
    'RBRACE',
    'RBRACKET',
    'RETURN',
    'RPAREN',
    'SEMI',
##    'SEMISEMI',
    'SLASH',
    'SLASHSLASH',
    'STARSTAR',
    'STAR',
    'STRING',
    'SUBDIR',
    'SYMBOL',
##    'TILDE',
    'TRUE',
    'WHILE',
)
# if __name__ == '__main__':
if 0:
    for p in ['parsetab.py', 'parsetab.pyc', 'parser.out']:
        try:
            os.remove(p)
        except:
            pass
#
#**        precedence
precedence = (                          # lowest to highest
    ('nonassoc', 'EXPR_LABEL'),
    ('nonassoc', 'below_SEMI'),
    ('nonassoc', 'SEMI'),
    ('left', 'PRINT', 'PROGRAM'),
    ('nonassoc', 'ELSE'),               # (if ... then ... else ...)
	('left', 'OR'),                     # a or b
	('left', 'AND'),					# a and b
	('right', 'NOT'),					# not a
    ('nonassoc', 'below_COMMA'),
    ('left', 'COMMA'),                  # expr/expr_comma_list (e,e,e) 
    ('nonassoc', 'above_COMMA'),
    ('right', 'LESSMINUS', 'EQUAL'),    # a <- ( b = c )
    ('nonassoc', 'EQUAL_EQUAL',         
     'EXCL_EQUAL', 'LESSEQUAL',         
     'LESS', 'GREATEREQUAL',
     'GREATER', 'IS', 'ISNOT', 'IN',
     'NOTIN'),
    ('left','PLUS','MINUS'),
    ('left','TIMES','DIVIDE', 'STAR',
     'SLASH', 'SLASHSLASH', 'PERCENT'),
    ('right','POWER', 'STARSTAR'),
    ('right','UMINUS'),
    ('left','below_DOT', 'LPAREN', 'LBRACKET'), # f()[]
    ('left','DOT'),                     # (a.b).c
    ('nonassoc','EXCL'),                # a ** !b (matching)
    ('nonassoc','EXCL_EXCL'),           # a ** !! b exponent (matching)
    )

#
#**        Position tracking functions.

def first_char(token, index):
    # yacc internals hacking.  For Ply v. 1.3.1,
    # the .slice and .value attributes.
    obj = token.slice[index]
    if isinstance(obj, yacc.YaccSymbol):
        first = (obj.value._first_char)
    else:
        first, _ = obj.char_range()
    return first

def last_char(token, index):
    # yacc internals hacking.  For Ply v. 1.3.1, 
    # the .slice and .value attributes.
    obj = token.slice[index]
    if isinstance(obj, yacc.YaccSymbol):
        last = (obj.value._last_char)
    else:
        _, last = obj.char_range()
    return last

def zero_char():
    # Dummy character position.
    return (0,0)

def with_char_range(class_, token, index):
    # yacc internals hacking.  For Ply v. 1.3.1     
    first, last = token.slice[index].char_range()
    return class_(token[index]).set_char_range(first, last)

def astList(l):
    return aList([l])


#
#**        Rules
# In emacs, renumber these via
# (setq ii 0)
# (query-replace-regexp-eval "def p_\\w+" (quote (format "def p_%d" (incf ii))) nil ) 
def p_program(t):                          # main entry.
    ''' program : seq_expr opt_semi
    '''
    t[0] = Program(t[1]) \
           .set_char_range(first_char(t,1), last_char(t,2))
    t[0].llet_char_range(last_char(t,1), last_char(t,1))

def p_2(t):                          
    ''' program :  empty
    '''
    t[0] = Program(astList(aNone())) \
           .set_char_range(zero_char(), zero_char())

def p_3(t):
    ''' expr : PROGRAM COLON py_block
    '''
    t[0] = Program(t[3]) \
           .set_char_range(first_char(t,1), last_char(t,3))
    t[0].llet_char_range(first_char(t,1), last_char(t,2))


def p_4(t):         
    ''' expr :  simple_expr
    '''
    t[0] = t[1].set_char_range(first_char(t,1), last_char(t,1))

def p_5(t):
    ''' expr : expr LPAREN comma_list RPAREN %prec below_DOT '''
    # foo(a,b,...)
    t[0] = Call(t[1], t[3]) \
           .set_char_range(first_char(t,1), last_char(t,4))

def p_6(t):
    ''' expr : expr LBRACKET slice_list RBRACKET  %prec below_DOT'''
    # a[b:c, d:e, ...]
    # The long form
    #       operator.getitem(na, [slice(1,2), slice(3,4)] )
    # works, so here we use
    #       operator.getitem(na, SLICE_LIST )
    # From print parse('operator.getitem(na, SLICE_LIST )')
    t[0] = Call(Member(Symbol('operator'),
                       Symbol('getitem')),
                aList([t[1], (t[3])
                       ])).set_char_range(first_char(t,1), last_char(t,4))


def p_7(t):
    ''' slice : expr COLON expr 
    '''
    # a[from:to]
    # From print reader.parse('slice(1,2, None)')
    t[0] = Call(
        Symbol('slice'),
        aList([t[1], t[3], Symbol('None')]))\
        .set_char_range(first_char(t,1), last_char(t,3))


def p_7a(t):
    ''' slice : expr  
    '''
    t[0] = t[1].set_char_range(first_char(t,1), last_char(t,1))

def p_8(t):
    ''' slice : expr COLON expr COLON expr 
    '''
    # a[from:to:step]
    # From print reader.parse('slice(1,2,3)')
    t[0] = Call(
        Symbol('slice'),
        aList([t[1], t[3], t[5]])) \
        .set_char_range(first_char(t,1), last_char(t,5))

def p_9(t):
    ''' slice_list : slice %prec below_COMMA
    '''
    t[0] = astList(t[1])\
           .set_char_range(first_char(t,1), last_char(t,1)) 


def p_10(t):
    ''' slice_list : empty
    '''
    t[0] = aList([])\
           .set_char_range(first_char(t,1), last_char(t,1)) 

    
def p_11(t):
    ''' slice_list : slice_list COMMA slice
    '''
    t[0] = (t[1] + astList(t[3]))\
           .set_char_range(first_char(t,1), last_char(t,3))            


# def p_12(t):
#     ''' expr : expr LBRACKET expr RBRACKET  %prec below_DOT '''
#     # a[b]
#     # From print parse('operator.getitem(a,b)') 
#     t[0] = Call(
#         Member(Symbol('operator'),
#                Symbol('getitem')),
#         aList([t[1], t[3]])) \
#         .set_char_range(first_char(t,1), last_char(t,4))

def p_13(t):
    ''' expr : RETURN expr
    '''
    t[0] = Return(t[2]) \
           .set_char_range(first_char(t,1), last_char(t,2))

def p_14(t):
    ''' expr : RETURN empty
    '''
    t[2].llet_char_range(last_char(t,1), last_char(t,1))
    t[0] = Return(t[2]) \
           .set_char_range(first_char(t,1), last_char(t,1))

def p_15(t):
    ''' expr : BREAK empty
    '''
    t[2].llet_char_range(last_char(t,1), last_char(t,1))
    t[0] = Return(t[2]) \
           .set_char_range(first_char(t,1), last_char(t,1))

def p_16(t):
    ''' expr : INLINE expr
    '''
    t[0] = Inline(t[2]) \
           .set_char_range(first_char(t,1), last_char(t,2))

def p_17(t):
    ''' expr : PRINT comma_list 
    '''
    t[2].llet_char_range(last_char(t,1), last_char(t,1))
    t[0] = Call(Symbol('print_'),
                t[2]).set_char_range(first_char(t,1), last_char(t,2))


def p_18(t):
    ''' expr : WHILE expr COLON py_block
    '''
    # Expand
    # 
    #   while C:
    #       B
    # 
    # to 
    # 
    # print reader.parse('''
    # if "while":
    #     def "WLOOP"():
    #         if not C:           # force boolean evaluation via not
    #             return 
    #         else:
    #             B
    #         return WLOOP()
    #     WLOOP()
    # ''')
    #    
    # C and B are replaced below;
    # WLOOP is replaced in If.setup_if_while()
    # 
    #   Symbol('C') -> C
    #   aList([Symbol('B')]) -> B
    C = t[2]
    B = t[4]
    # 
    t[0] = If(String('while'), aList([Set(Symbol('WLOOP'), Macro(aList([]), aList([If(Call(Symbol('not'), aList([C])), aList([Return(aNone())]), B), Return(Call(Symbol('WLOOP'), aList([])))]))), Call(Symbol('WLOOP'), aList([]))]), aList([]))\
           .set_char_range(first_char(t,1), last_char(t,4))


def p_19(t):
    ''' expr : FOR expr IN expr COLON py_block
    '''
    # Expand
    # 
    # for V in SEQ:
    #     B
    # 
    # to
    # 
    # if "for":
    #     ITEMS = SEQ
    #     IDX = 0
    #     LEN = len(ITEMS)
    #     # orig. for
    #     def "LOOP"():
    #         if IDX < LEN: 
    #             IDX = IDX + 1
    #             # V in SEQ.
    #             V = ITEMS[ IDX - 1 ]
    #             # Body B
    #             [B]
    #             # Iterate.
    #             return LOOP()
    #     LOOP()
    # 
    # V, SEQ, and B are replaced below; 
    # uppercase identifiers (not V, SEQ, B) are replaced in If.setup()
    # 
    V = t[2]
    SEQ = t[4]
    B = t[6]
    # 
    t[0] = If(String('for'),
              aList([
        Set(Symbol('ITEMS'), SEQ),
        Set(Symbol('IDX'), Int(0)),
        Set(Symbol('LEN'), Call(Symbol('len'), aList([Symbol('ITEMS')]))),
        Set(Symbol('LOOP'),
            Macro(aList([]),
                  aList([If(Call(Symbol('<'),
                                 aList([Symbol('IDX'),
                                        Symbol('LEN')])),
                            aList([Set(Symbol('IDX'),
                                       Call(Symbol('+'),
                                            aList([Symbol('IDX'),
                                                   Int(1)]))),
                                   Set(V,
                                       Call(Member(Symbol('operator'),
                                                   Symbol('getitem')),
                                            aList([Symbol('ITEMS'),
                                                   Call(Symbol('-'),
                                                        aList([Symbol('IDX'),
                                                               Int(1)]))]))),
                                   List(B),
                                   Return(Call(Symbol('LOOP'),
                                               aList([])))]),
                            aList([]))]))),
        Call(Symbol('LOOP'), aList([]))]), aList([]))\
        .set_char_range(first_char(t,1), last_char(t,6))

def p_20(t):
    ''' expr : IF expr COLON py_block 
    '''
    t[0] = If(t[2], (t[4]), aList([])) \
           .set_char_range(first_char(t,1), last_char(t,4))

    # Outline?   E.g.  if ("outline",  "L3 version 0.3.0"): ...
    ma = ast.Matcher()
    if ma.match( t[0],
                 If(Tuple(aList([String('outline'),
                                 MarkerTyped(Symbol('TITLE'),
                                             String('_'))])),
                    MarkerTyped(Symbol('BODY'), aList([])),
                    aList([]))):
        # Add title.
        rv = viewList(ma['BODY']).set_char_range(
            first_char(t,1), last_char(t,4))
        rv.set_label(ma['TITLE'])
        t[0] = rv

    # Labeled program?   E.g.  if ("prog", "comment"): ...
    elif ma.match( t[0],
                   If(Tuple(aList([String('prog'),
                                   MarkerTyped(Symbol('TITLE'),
                                               String('_'))])),
                      MarkerTyped(Symbol('BODY'), aList([])),
                      aList([]))):
        # Add title.
        rv = Program(ma['BODY']).set_char_range(
            first_char(t,1), last_char(t,4))
        rv.set_label(ma['TITLE'])
        t[0] = rv


def p_21(t):
    ''' expr : IF expr COLON py_block ELSE COLON py_block
    '''
    t[0] = If(t[2], (t[4]), (t[7])) \
           .set_char_range(first_char(t,1), last_char(t,7))

def p_22(t):
    ''' expr : MINUS expr           %prec UMINUS
             '''
    t[0] = Call(Symbol('neg'), aList([ t[2] ])) \
           .set_char_range(first_char(t,1), last_char(t,2))

def p_23(t):
    ''' expr : NOT expr 
             '''
    t[0] = Call(Symbol(t[1]), aList([ t[2] ])) \
           .set_char_range(first_char(t,1), last_char(t,2))

def p_24(t):
    ''' expr : expr MINUS         expr      
             | expr PLUS          expr
             | expr PERCENT       expr
             | expr STAR          expr
             | expr SLASH         expr
             | expr SLASHSLASH    expr
             | expr EQUAL_EQUAL   expr
             | expr EXCL_EQUAL    expr
             | expr IS            expr
             | expr ISNOT         expr
             | expr LESS          expr
             | expr LESSEQUAL     expr
             | expr GREATER       expr
             | expr GREATEREQUAL  expr
             | expr AND			  expr
             | expr OR			  expr
             | expr STARSTAR      expr %prec POWER
             '''
    t[0] = Call(Symbol(t[2]).set_char_range(first_char(t,2),
                                            last_char(t,2)),
                aList([ t[1], t[3] ])) \
                .set_char_range(first_char(t,1), last_char(t,3))

def p_25(t):
    ''' expr : expr LESSMINUS expr 
             | expr EQUAL expr  
             '''

    # Default tree.
    t[0] = Set(t[1], t[3]) \
           .set_char_range(first_char(t,1), last_char(t,3))           

    # Special form "! AA[ ! II ] = ! RHS"
    ma = ast.Matcher()
    if ma.match( t[0],
                 Set(Call(Member(Symbol('operator'),
                                 Symbol('getitem')),
                          aList([Marker('AA'),
                                 Marker('II')])),
                     Marker('RHS'))  
                 ):
        t[0] = Call(Member(Symbol('operator'),
                           Symbol('setitem')),
                    aList([ma['AA'],
                           ma['II'],
                           ma['RHS']])).set_char_range(
            first_char(t,1), last_char(t,3))
        return

    # Special form "! AA[ ! II ] [ !JJ ] = ! RHS"
    if ma.match( t[0],
                 Set(Call(Member(Symbol('operator'),
                                 Symbol('getitem')),
                          aList([Call(Member(Symbol('operator'),
                                             Symbol('getitem')),
                                      aList([Marker('AA'),
                                             Marker('II')])),
                                 Marker('JJ')])),
                     Marker('RHS'))):
        t[0] = Call(Member(Symbol('operator'),
                           Symbol('setitem')),
                    aList([Call(Member(Symbol('operator'),
                                       Symbol('getitem')),
                                aList([ma['AA'],
                                       ma['II']])),
                           ma['JJ'],
                           ma['RHS']])).set_char_range(
            first_char(t,1), last_char(t,3))
        return


def p_26(t):
    ''' expr : expr IN expr '''
    # a in b -> contains(b, a)
    t[0] = Call(Symbol('contains').set_char_range(first_char(t,2),
                                                  last_char(t,2)),
                aList([ t[3], t[1] ])) \
                .set_char_range(first_char(t,1), last_char(t,3))

def p_27(t):
    ''' expr : expr NOTIN expr '''
    # a in b -> not_contains(b, a)
    t[0] = Call(Symbol('not_contains').set_char_range(first_char(t,2),
                                                      last_char(t,2)),
                aList([ t[3], t[1] ])) \
                .set_char_range(first_char(t,1), last_char(t,3))

def p_28(t):
    ''' expr : expr DOT expr '''
    # | block_member
    #     ''' block_member : simple_expr DOT simple_expr '''
    t[0] = Member(t[1], t[3]) \
           .set_char_range(first_char(t,1), last_char(t,3)) 



def p_29(t):
    ''' simple_expr : val_ident
                    | list_exp
                    | block
                    | mapping
                    | labeled_simple
                    | matcher
    '''
    t[0] = t[1]

# # def p_30(t):
# #     ''' simple_expr : LPAREN expr RPAREN %prec below_COMMA
# #     '''
# #     t[0] = t[2].set_char_range(first_char(t,1), last_char(t,3))

def p_31(t):
    ''' simple_expr : LPAREN comma_list RPAREN
    '''
    assert(isinstance(t[2], aList))
    t[2].llet_char_range(last_char(t,1), first_char(t,3))

    if len(t[2]) == 1:
        if t[2]._is_tuple:
            t[0] = Tuple(t[2]) \
                   .set_char_range(first_char(t,1), last_char(t,3))           
        else:
            t[0] = t[2][0].set_char_range(first_char(t,1), last_char(t,3))

    elif len(t[2]) == 0:
        t[0] = Tuple(t[2]) \
               .set_char_range(first_char(t,1), last_char(t,3))           
            
    else:
        t[0] = Tuple(t[2]) \
               .set_char_range(first_char(t,1), last_char(t,3))           

        

def p_32(t):
    ''' labeled_simple : LABEL simple_expr
    '''
    t[0] = Labeled(t[1], t[2]) \
           .set_char_range(first_char(t,1), last_char(t,2))

def p_33(t):
    ''' matcher : EXCL SYMBOL
    '''
    t[0] = Marker(t[2]) \
           .set_char_range(first_char(t,1), last_char(t,2))

def p_34(t):
    ''' matcher : EXCL EXCL SYMBOL simple_expr
    '''
    t[0] = MarkerTyped(String(t[3]), t[4]) \
           .set_char_range(first_char(t,1), last_char(t,4))

def p_35(t):
    ''' list_exp : LBRACKET comma_list  RBRACKET '''
    t[2].llet_char_range(last_char(t,1), first_char(t,3))
    t[0] = List(t[2]) \
           .set_char_range(first_char(t,1), last_char(t,3))

def p_36(t):
    ''' val_ident : SYMBOL
    '''
    t[0] = with_char_range(Symbol, t, 1)

def p_37(t):
    ''' val_ident : STRING '''
    t[0] = with_char_range(String, t, 1)

def p_38(t):
    ''' val_ident : FLOAT
    '''
    t[0] = with_char_range(Float, t, 1)

def p_39(t):
    ''' val_ident : INT
    '''
    t[0] = with_char_range(Int, t, 1)

def p_40(t):
    ''' val_ident : LPAREN operator RPAREN '''
    t[0] = Symbol(t[2]).set_char_range(first_char(t,1), last_char(t,3))

# 
# Convert True and False to 1 and 0.
# This avoids a new class Bool, which would have to deal with not
# being able to inherit from <type 'bool'>.
# 
def p_41(t):
    ''' val_ident : TRUE '''
    t[0] = Int(1).set_char_range(first_char(t,1), last_char(t,1))

def p_42(t):
    ''' val_ident : FALSE '''
    t[0] = Int(0).set_char_range(first_char(t,1), last_char(t,1))

def p_43(t):
    ''' operator : MINUS
                 | PLUS
                 | PERCENT
                 | SLASH
                 | SLASHSLASH
                 | STAR '''
    t[0] = with_char_range(String, t, 1)

def p_44(t):
    ''' block : DEF SYMBOL LPAREN comma_list RPAREN COLON py_block '''
    # def aa(bb, cc): dd
    # is a shortcut for ('aa = { |bb, cc| dd}')
    # 
    #     t[0] = Set(
    #                Symbol(t[2]),
    #                Function(t[4], t[7])) \
    #            .set_char_range(first_char(t,1), last_char(t,7))
    # 
    t[4].llet_char_range(last_char(t,3), first_char(t,5))
    t[0] = Set(
        with_char_range(Symbol, t, 2),
        Function(t[4], t[7]).set_char_range(first_char(t,1), last_char(t,7)),
        ).set_char_range(first_char(t,1), last_char(t,7))

def p_45(t):
    ''' block : DEF STRING LPAREN empty RPAREN COLON py_block '''
    # 
    # Macro definition.
    # def "aa"():
    #   body

    # Note the STRING -> SYMBOL switch.
    # t[4] is an aNone, and ignored.
    t[0] = Set(
        with_char_range(Symbol, t, 2),
        Macro(aList([]).set_char_range(last_char(t,3), first_char(t,5)),
              t[7]).set_char_range(first_char(t,1), last_char(t,7)),
        ).set_char_range(first_char(t,1), last_char(t,7))

def p_46(t):
    ''' block : LBRACE block_args seq_expr opt_semi RBRACE %prec below_COMMA
    '''
    #   | LBRACE  block_args seq_expr opt_semi error
    t[0] = Function(t[2], t[3]) \
           .set_char_range(first_char(t, 1), last_char(t,5))

def p_47(t):
    ''' block : LPAREN LAMBDA comma_list COLON comma_list RPAREN
    '''
    # lambda x: body
    t[0] = Function(t[3], t[5]) \
           .set_char_range(first_char(t, 1), last_char(t,6))

def p_48(t):
    ''' block : LAMBDA comma_list COLON py_block
    '''
    # lambda x:
    #   body
    t[0] = Function(t[2], t[4]) \
           .set_char_range(first_char(t, 1), last_char(t,4))

def p_49(t):
    ''' block : LBRACKET block_args seq_expr opt_semi RBRACKET %prec below_COMMA
    '''
    t[0] = Macro(t[2], t[3]) \
           .set_char_range(first_char(t, 1), last_char(t,5))

def p_50(t):
    ''' expr : SUBDIR COLON py_block '''
    #   | LBRACE seq_expr opt_semi error
    t[3].llet_char_range(last_char(t,1), first_char(t,3))
    t[0] = Subdir(t[3]) \
           .set_char_range(first_char(t,1), last_char(t,3))

def p_51(t):
    ''' mapping : LBRACE comma_list RBRACE '''
    #   | LBRACE seq_expr opt_semi error
    t[2].llet_char_range(last_char(t,1), first_char(t,3))
    t[0] = Map(t[2]) \
           .set_char_range(first_char(t,1), last_char(t,3))

# # Set semantics are incompatible here!
# #     In Python, 
# #         a = 1 
# #     'a' is quoted, while for
# #         {a : 1},
# #     'a' is evaluated.
# # The l3 Set is always the first (special) form, and a special case
# # for Set inside Map would reduce l3 to Python limitations...
# # 

# def p_52(t):
#     ''' mapping : LBRACE colon_b_list RBRACE '''
#     t[0] = Map(t[2]) \
#            .set_char_range(first_char(t,1), last_char(t,3))

# def p_53(t):
#     ''' colon_binding : expr COLON expr 
#     '''
#     t[0] = Set(t[1], t[3]) \
#            .set_char_range(first_char(t,1), last_char(t,3))

# def p_54(t):
#     ''' colon_b_list : colon_binding %prec below_COMMA
#     '''
#     t[0] = astList(t[1])\
#            .set_char_range(first_char(t,1), last_char(t,1)) 

# # def p_55(t):
# #     ''' colon_b_list : empty
# #     '''
# #     t[0] = aList([])\
# #            .set_char_range(first_char(t,1), last_char(t,1)) 

    
# def p_56(t):
#     ''' colon_b_list : colon_b_list COMMA colon_binding
#     '''
#     t[0] = (t[1]) + astList(t[3])\
#            .set_char_range(first_char(t,1), last_char(t,3))            

# def p_57(t):
#     ''' colon_b_list : colon_b_list COMMA       %prec COMMA
#     '''
#     t[0] = (t[1])\
#            .set_char_range(first_char(t,1), last_char(t,2)) 


def p_58(t):
    ''' empty :'''
    t[0] = aNone().set_char_range(None, None)           

def p_59(t):
    ''' py_block : SEMI INDENT seq_expr DEDENT '''
    t[0] = t[3].set_char_range(first_char(t,2), last_char(t,4))

# 
# Allowing this ::
# def p_60(t):
#     ''' py_block : seq_expr '''
#     t[0] = t[1].set_char_range(first_char(t,1), last_char(t,1))
# parses
#     def f(a,b, c = cos(22), d = 10): c
#     f(1,2)
#     f(1, 2, c = 22)
# as a single function.


def p_61(t):
    ''' block_args : BAR comma_list BAR 
    '''
    t[0] = t[2].set_char_range(first_char(t,1), last_char(t,3))

def p_62(t):
    ''' seq_expr : expr        %prec below_SEMI  
    '''
    t[0] = astList(t[1])\
           .set_char_range(first_char(t, 1), last_char(t,1))

def p_63(t):
    ''' seq_expr : expr SEMI   %prec SEMI
    '''
    t[0] = astList(t[1])\
           .set_char_range(first_char(t,1), last_char(t,2))

def p_64(t):
    ''' seq_expr : expr SEMI seq_expr
    '''
    t[0] = astList(t[1]) + t[3]\
           .set_char_range(first_char(t,1), last_char(t,3))           

def p_65(t):
    ''' comma_list : empty
    '''
    t[0] = aList([]).set_char_range(None, None)
    t[0]._is_tuple = False

def p_66(t):
    ''' comma_list : expr
    '''
    t[0] = astList(t[1])\
           .set_char_range(first_char(t,1), last_char(t,1)) 
    t[0]._is_tuple = False
    
def p_67(t):
    ''' comma_list : comma_list COMMA expr
    '''
    t[0] = (t[1]) + astList(t[3])\
           .set_char_range(first_char(t,1), last_char(t,3))            
    t[0]._is_tuple = True

def p_68(t):
    ''' comma_list : comma_list COMMA
    '''
    t[0] = (t[1]).set_char_range(first_char(t,1), last_char(t,2)) 
    t[0].llet_char_range(first_char(t,2), last_char(t,2)) 
    t[0]._is_tuple = True   # This hint is REQUIRED for the (1,) case.

# def p_69(t):
#     ''' comma_list :  expr COMMA comma_list 
#     '''
#     t[0] = astList(t[1]) + (t[3])\
#            .set_char_range(first_char(t,1), last_char(t,3))            
#     t[0]._is_tuple = True

# def p_70(t):
#     ''' comma_list : expr COMMA
#     '''
#     t[0] = astList(t[1])\
#            .set_char_range(first_char(t,1), last_char(t,2)) 
#     t[0]._is_tuple = True

def p_71(t):
    ''' opt_semi : SEMI
    '''
    t[0] = aNone().set_char_range(first_char(t,1), last_char(t,1))

def p_72(t):
    ''' opt_semi : empty
    '''
    t[0] = aNone().set_char_range(None, None)

def p_error(t):
    if t != None:
        beg, end = t.char_column_range()
        raise SyntaxError(
            "%d:%d-%d at '%s':\n" % (t.lineno, beg, end, t.value) +
            "%s" % t._meta[4] +
            "%s%s\n" % (" " * beg, "^" * (end-beg))
            )
    else:
        ## print "Unexpected end of input"
        raise EOFError
## reader.p_error = p_error
## reload(reader.yacc)
import yacc


#

#*    comment parsing and code association
#
# Expanded from l3gui/experimental.py, `comment attachment`
# 
#**        comment parsing
# Comment type identifier.
COMMENT, _ = filter(lambda (k,v): v == 'COMMENT', token.tok_name.items())[0]
NEWLINE, _ = filter(lambda (k,v): v == 'NEWLINE', token.tok_name.items())[0]
NL, _ = filter(lambda (k,v): v == 'NL', token.tok_name.items())[0]

# 
#***            Cluster successive (incl.separated) comment lines
def cluster_comments(code_s):
    """
    Combine blank-separated comment blocks in the input string
    and return comment blocks in the format
        ((last_row, first_col), string) list
    """
    gen = tokenize.generate_tokens(string_readline(code_s))
    st_accum = 0 
    va_cluster = []                         # (string, begin, end)  list
    va_accum = []                           # va_cluster list
    for ii in gen:
        (type, string, begin, end, line) = ii

        # Cluster successive comment lines, even those separated by blanks.
        if type in [COMMENT]:
            st_accum = 1
            va_cluster.append( (string, begin, end) )

        elif type in [NEWLINE, NL]:
            if st_accum:
                va_cluster.append( (string, begin, end) )
        else:
            if st_accum:
                # Remove trailing empty newlines.
                tl = ['\n']
                while tl[0] == '\n':
                    tl = va_cluster.pop()
                va_cluster.append(tl)
                # Keep cluster.
                va_accum.append(va_cluster)
                st_accum = 0
                va_cluster = []

    # 
    # Simplify comment structure.
    # Adjust lexeme row indexing, compact clusters, and remove unused
    # data. 
    # 
    va_comments = []            # ((last_row, first_col), string) list
    va_comments = [                             # 
        ( (clus[-1][2][0] - 1, clus[0][1][1]), # (last_row, first_col)
          "".join([line[0] for line in clus]))
        for clus in va_accum ]

    return va_comments

#
#***            extract a list of leading subnodes from the parse tree
def lead_nodes(tree):
    """ Extract a list of nodes from the tree.
    The returned list has form
        [(start_row, start_col), node] list
    and contains only the first (child)node for a given start_row.
    """
    lrow, lcol = -1, -1
    tr_lines = []                      # [(start_row, col), node] list

    for nd in [tree] + list(tree.subtrees()):
        (row, col), _ = nd.get_char_range()
        # Prune all but first expression on a line.
        if row != lrow:         
            tr_lines.append( [(row,col), nd] )
        lrow = row
    return (tr_lines)


# 
#**        Find expression to attach to.
def comment_to_expr(va_comments, tr_lines):
    """Match every comment block to the expression following it.

    Given a list of comments (from cluster_comments) and
    a list of node lines (from lead_nodes), return a
        (node, comment) list
    """
    import l3lang.globals as glbl
    com_it = iter(va_comments)
    node_it = iter(tr_lines)
    com_row = lambda com: com[0][0]
    nd_com_l = []                       # (node, comment) list
    while 1:
        try: com = com_it.next()
        except StopIteration: break
        try: nd = node_it.next()
        except StopIteration: break
        com_line = com[0][0]
        nd_line = nd[0][0]
        # Find first line after current comment, if any. 
        try: 
            while nd[0][0] <= com_line:
                nd = node_it.next()
        except StopIteration:
            pass
        else:
            if com[0][1] != nd[0][1]:
                glbl.logger.warn(
                    "Column mismatch between comment and expression.  "
                    "line %d:\n"
                    "%s" % (com[0][0], com[1])
                    )
            nd_com_l.append( (nd[1], com[1]) )
    return nd_com_l

#
#**        clean comment strings for display
stc_re = re.compile(r'^#\**', re.MULTILINE)
def strip_comment(str):
    return stc_re.sub("", str)

#*    external parser use.
def parse_comments(tree, full_src):
    """Return a
        (node, comment) list
    associating the node with the
    comment preceeding it.

    tree:
        The parsed tree
    
    full_src:
        the full source string; `tree` only retains its own source
        string; to retrieve all comments requires more.

    Note: Only the parse tree and associated source string is
    required; node ids are not.
    """
    va_comments = cluster_comments(full_src)
    tr_lines = lead_nodes(tree)
    return comment_to_expr(
        [(pos, strip_comment(string)) for (pos, string) in va_comments],
        tr_lines)


# raw string input
def raw_lexer(strng):
    gen = tokenize.generate_tokens(string_readline(strng))
    dump_tokens(gen)

# string py_lexer
def py_lexer(strng):
    lex = Pylexer()
    lex.input(strng)
    t = 1
    while t:
        t = lex.token()
        print t

def parse(strng, debug = 0):
    assert isinstance(strng, StringType)
    lex = Pylexer()
    lex.input(strng)
    parser = yacc.yacc(tabmodule = "parsetab")
    rv = row_adjust(parser.parse(lexer = lex, debug = debug)).set_source_string(strng)
    # The Program has no character range.  Patch.
    rv.set_char_range( (0, 0), # rv[0][0]._first_char
                       rv[0][-1]._last_char)
    return rv


def parse_file(name):
    lex = Pylexer()
    lex.input_readline(file_readline(name))
    parser = yacc.yacc(tabmodule = "parsetab")
    rv = row_adjust(parser.parse(lexer = lex)).set_source_file(name)
    rv.set_char_range( (0, 0), rv[0][-1]._last_char)
    # Wasteful, but needed elsewhere:
    rv.set_source_string("".join(open(name).readlines()))
    return rv

#*    Special cases
def empty_program():
    return Program(aList([])).set_char_range(zero_char(),
                                             zero_char()).set_source_string('')


 
#*    parse table setup
#       The yacc-generated (parser.out parsetab.py) files are put into
#       current working directory, which may not be in the PYTHONPATH
# To avoid this, pre-form the tables before distribution via
# 
#     cd l3lang
#     l3lang.python reader.py tables
# (or 
#     sparx.python reader.py tables
# )
# Expect 2 shift/reduce conflicts and some warnings.

def setup_tables():
    print "Producing parse tables." 
    for p in ['parsetab.py', 'parsetab.pyc', 'parser.out']:
        try:
            os.remove(p)
        except:
            pass
    tree = parse('"bar"')

# Enable during interactive development:
# if __name__ == '__main__': setup_tables()

if __name__ == '__main__' and len(sys.argv) == 2 and  \
       (sys.argv[1]) == 'tables': setup_tables()


#
#*    simple tests and parser construction
# After changes to the parser, use the following parse() call to
# create the parser in the current directory.  
if 0:
	tree = parse('"bar"')
#**        raw file input
if 0:
	gen = tokenize.generate_tokens(file_readline('test-calc.dml'))
	dump_tokens(gen)
#**        trivial tests 
if 0:
	tree = parse('"bar"')
	py_lexer('[1.1 ]')
# py_lexer('(  1.1 + 1)')
# py_lexer('1.1 ') 
# py_lexer('1.1')
# py_lexer('''{ 'a' : 1, 2 : 'b' }''')
# py_lexer('''{ a = 1, 2 = 'b' }''')

# print parse('[ a ]')
# print parse('1.1')
# print parse('{ amplitude = 1.1 }')
# print parse('[ 1.1 ]')

# py_lexer('[]')
# print parse('[ ]')
# print parse('[1,2, ]')
# print parse('{1,2, }')
# print parse('userdata = { amplitude = 1.1 }')
# py_lexer('userdata = { amplitude = 1.1 }')
# print parse('userdata (a,b)')
# print parse('operator.getslice(a,b,c)')

# print parse('a[1+2:2]')
# print parse('a[1]')
# print parse('a(1)')
# py_lexer('{ |bb, cc| dd}')
# print parse('{ |bb, cc| dd}')
# # raw_lexer(''' 
# # py_lexer(''' 
# print parse(''' 
# def aa(bb,cc):
#   dd
#   if ee:
#       gg
#   if hh:
#       ii
#   else:
#       jj
#   kk
# ''') 
# raw_lexer(''' 
# py_lexer(''' 
# print parse(''' 
# def refine_loop(extra_terms, current_sum):
#     if (abs_err > userdata.tolerance): 
#         if extra_terms:
#             refine_loop()
#         else:
#             message()
# refine_loop()
# ''') 
# #
# raw_lexer(''' 
# py_lexer(''' 
# print parse(''' 
# if aa:
#     bb
# ''') 

# raw_lexer(''' 
# a
# b
# [a+b] * 4
# ''') 

# tree = parse_file('test-calc.dml')

# raw_lexer('!! hello')
# py_lexer('!! hello')
# print parse('!! hello []')
# print parse('! hello ')

# print yacc.parse(" hello bar ")
# print yacc.parse(""" if a then { b } else c;   """)
# tokenize_string(""" if a then { b } else c;   """)
# pp('(a, b) <- userdata')

# dft = tree.traverse_depth()
# for i in dft:
#     print i, '\n'

# brft = tree.traverse_breadth()
# for i in brft:
#     print i, '\n'

# if tests
# print parse('''
# if 1:
#     1
# else:
#     0
# ''')

# # l3 only
# print parse('''
# if 1: 1 else: 0
# ''')

# print parse('''
# if 1: 1; 2; 3;
# else: 0
# ''')

# print parse('''
# if 1: 1; 2; 3;
# else:
#     0
# ''')

# # map tests
# print parse('{"bar" : foo, this : that}')
# parse('f(p, q, a = 0, c = 1)')

# print parse('f(p, q, a = 0, c = 1)')

# parse('def ali2d_c(stack, outdir, ir=1, ou=-1, rs=1): a')
# parse('ali2d_c(a,b, ir = 2, rs = 2)')

# _Function = parse('def ali2d_c(stack, outdir, ir=1, ou=-1, rs=1): a').body()[1]
# _Function.named_block_args()
# _Function.positional_block_args()

#*    View the grammar rules:
# (occur "^[ 	]+['\"]+.*:\\|^[ 	]+|" nil)

#* simple tests
# setup_tables()
# from l3lang.view import print_ast
# def prnt(str, debug = 0):
#     tree = parse(str, debug = debug)
#     print_ast(tree)
#     print tree
#     return tree

# prnt('''a[1:2, 3:4]''')
# prnt('''a[1,2,3]''')
# prnt('''a[1:2]''')
# prnt('''a[1:33:2]''')

# prnt('''(lambda x, y: x - y) ''')

# prnt('''lambda x, y:
#     print x,y
# ''')

# prnt('''for i in [1,2]:
#     print i
# ''')

# prnt('lambda x: x - 2')
# prnt('lambda x, y: x + y - 2')
# prnt('lambda (x, y): x + y - 2')

# prnt('filter(lambda x: x > 3, ed)')

# # For
# prnt('lambda x: x - 2, 3 * x')
# # get
# #     SyntaxError: 0:15-16 at ',':
# #     lambda x: x - 2, 3 * x
# # which seems to be consistent with Python:
# lambda x: x - 2, 3 * x
# # gives
# #   NameError: name 'x' is not defined


# body = prnt('''
# if ("outline", "sample"):
#     BODY
# ''')[0][0]
# print body

# prnt('''
# if "regular if":
#     a + b
# ''')

# prnt('')

# prnt('''program:
#     a - b''')

# prnt('print a')

# prnt('print a, b')
# prnt('(print a,c)')

# prnt('(a,c)')

# prnt('(a,c) ; b')

# prnt('(print a,c) ; b')
# prnt('print a,c ; b')
# prnt('print a; b')
# prnt('print a; ')
# prnt('a = b')
# prnt('(a = b)')
# prnt('a == b', debug = 0)
# # error (r439):
# prnt('(a == b)', debug = 0)
# prnt('(a <= b)')

# prnt('''
# def foo():
#     bar
# ''')

# prnt('''
# def "foo"():
#     bar
# ''')

# prnt('(a,b) = (c,d)')
# # error
# prnt('a,b')
# prnt('a,b = c,d')

