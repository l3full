#
# Author:  Michael H. Hohn (mhhohn@lbl.gov)
#
# Copyright (c) 2006, The Regents of the University of California
# 
# See legal.txt and license.txt
# 

"""
read-eval-print utilities needed to emulate Python's interactive
interpreter, for use with l3.   

Adapted from lib/python2.2/code.py.
"""
#
#*    Init & miscellaneous

import sys
import traceback
from types import *
from tokenize import TokenError
from l3lang.ast import *

class ReplError(Exception):
    def __str__(self):
        return repr(self.e_args)
    def __init__(self, e_args=None):
        self.e_args = e_args

def softspace(file, newvalue):
    oldvalue = 0
    try:
        oldvalue = file.softspace
    except AttributeError:
        pass
    try:
        file.softspace = newvalue
    except (AttributeError, TypeError):
        # "attribute-less object" or "read-only attributes"
        pass
    return oldvalue

#*    Interpreter front end
# 
  
#**        Core functions.
class TxtInterpret:
    '  Front end for l3 interpretation functions. '
    pass


def __init__(self, root_env, storage, interface):
    self._interface = interface         # TxtConsole or compatible
    self._storage = storage

    # Internal state for Program() positioning and evaluation.
    self._root_env = root_env
    self._current_env = root_env
    self._current_program = root_env._program
    self._insertion_point = self._current_program

    ma = Matcher()
    # Program(aList([...]))
    if ma.match(self._current_program,
                Program((MarkerTyped(String('list'), aList([]))))):
        self._insertion_list = ma['list']
        self._insertion_index = 0
    else:
        raise ReplError, "root_env has no Program()"
    self._insertion_mode = 'Insert'     
    self._eval_mode = 'Transient'       
TxtInterpret.__init__ = __init__


def compile(self, source, filename, symbol):
    " Produce a l3 AST. "
    import reader, ast
    try:
        tree = reader.parse(source)
        tree.setup(ast.empty_parent(), self._current_env, self._storage
                   )# no set_outl_edges(self.w_, None)
        return tree
    except TokenError:
        return None
    else:
        raise
TxtInterpret.compile = compile


def runcode(self, code, source):
    """Execute a l3 AST produced by .compile().

    When an exception occurs, self.showtraceback() is called to
    display a traceback.  All exceptions are caught except
    SystemExit, which is reraised.

    KeyboardInterrupt may occur elsewhere, and may not be caught.  The
    caller should be prepared to deal with it.
    """
    try:
        self._interface.associate_comments(code, source)
        result = code.interpret(self._current_env, self._storage)
        # Print ( <value>, <timestamp> ) pair.  Potentially LARGE.
        ## print result

    except SystemExit:
        raise
    except:
        self._interface.showtraceback()
    else:
        self._interface.maybe_softspace()
        self._interface.display_via(code)
TxtInterpret.runcode = runcode


def runsource(self, source, filename="<input>", symbol="single"):
    """Compile and run some source in the interpreter.

    One of several things can happen:

    1) The input is incorrect

    2) The input is incomplete, and more input is required

    3) The input is complete; compile() returned an ast
    """
    try:
        tree = self.compile(source, filename, symbol)
    except (OverflowError, SyntaxError, ValueError):
        # Case 1
        self.showsyntaxerror(filename)
        return 0
    except EOFError:
        # Case 2
        return 1

    if tree is None:
        # Case 2
        return 1

    # Case 3
    self.runcode(tree, source)
    return 0
TxtInterpret.runsource = runsource


def showsyntaxerror(self, filename=None):
    """Display the syntax error that just occurred.
    This doesn't display a stack trace because there isn't one.
    """
    type, value, sys.last_traceback = sys.exc_info()
    sys.last_type = type
    sys.last_value = value
    if filename and type is SyntaxError:
        # Work hard to stuff the correct filename in the exception
        # 
        # If a filename is given, it is stuffed in the exception instead
        # of what was there before (because Python's parser always uses
        # "<string>" when reading from a string).
        # 
        try:
            msg, (dummy_filename, lineno, offset, line) = value
        except:
            # Not the format we expect; leave it alone
            pass
        else:
            # Stuff in the right filename
            try:
                # Assume SyntaxError is a class exception
                value = SyntaxError(msg, (filename, lineno, offset, line))
            except:
                # If that failed, assume SyntaxError is a string
                value = msg, (filename, lineno, offset, line)
            sys.last_value = value
    list = traceback.format_exception_only(type, value)
    map(self._interface.se_write, list)
TxtInterpret.showsyntaxerror = showsyntaxerror

def disconnect_interface(self):
    self._interface = None       # HERE.  A log-only interface?
TxtInterpret.disconnect_interface = disconnect_interface

def connect_interface(self, interf):
    self._interface = interf
TxtInterpret.connect_interface = connect_interface

#
#**        Editing and interaction 
def pwd(self):
    print self._current_env
TxtInterpret.pwd = pwd

ii_indent = "    "

def ls(self):
    print "Parent: "
    print ii_indent, self._current_env._def_env_id
    print "Immediate sub-environments: "
    for env in self._current_env.children():
        print ii_indent, env
    print "Name bindings: "
    for key, val in self._current_env.all_bindings().items():
        print ii_indent, key
TxtInterpret.ls = ls

def cd(self, target):
    if isinstance(target, (IntType, LongType)):
        env = self._storage.load(target)
    elif isinstance(target, Env):
        env = target
    else:
        raise ReplError, \
              "Invalid argument to TxtInterpret.cd %s" % target
    
    self._current_env = env
    self._current_program = env._program
    self._insertion_point = self._current_program
    ma = Matcher()
    # Program(aList([...]))
    if ma.match(self._current_program,
                Program((MarkerTyped(String('list'), aList([]))))):
        self._insertion_list = ma['list']
        self._insertion_index = 0
    # Map(aList([...]))
    elif ma.match(self._current_program,
                  Map((MarkerTyped(String('list'), aList([]))))):
        self._insertion_list = ma['list']
        self._insertion_index = 0
    else:
        self._interface.se_write(
            "Warning: The target environment has no program.")
TxtInterpret.cd = cd


def lsast(self):
    # list current program, ast form
    print self._current_program
TxtInterpret.lsast = lsast


def lsp(self):
    # list current program, source
    p = self._current_program
    # 
    lines = p._source_string.split('\n')        # windows?
    for line, num in zip(lines, range(0, len(lines))):
        # actual code
        print line
        # "Highlight" line
        if num >= p._first_char[0]:
            if num > p._last_char[0]:    # outside row range
                continue
            # Find column range for this line 
            if num == p._first_char[0]:
                first_col = p._first_char[1]
            else:
                first_col = 0
            if num == p._last_char[0]:
                last_col = p._last_char[1] # one-past end
            else:
                last_col = len(line)
            # Print highlight
            print (" " * first_col + "^" * (last_col - first_col))
TxtInterpret.lsp = lsp

        
def select_tree(self, tree):
    self._current_program = tree
    self._insertion_point = self._current_program # redundant?
TxtInterpret.select_tree = select_tree


def select_insertion_pt(self, list, index):
    assert isinstance(list, aList) 
    self._insertion_list = list
    self._insertion_index = index
TxtInterpret.select_insertion_pt = select_insertion_pt


def insert_program(self, tree):
    if self._insertion_mode == "Insert":
        self._insertion_list.insert(self._insertion_index, tree)
        self._insertion_index += 1
    elif self._insertion_mode == "Replace":
        raise ReplError, "not implemented"
    else:
        raise ReplError
TxtInterpret.insert_program = insert_program    


'''
'
Relevant internal state for Program() positioning and evaluation

_current_env : Env()
    _current_program : astType() 
        _insertion_point : astType()
        _insertion_mode : Insert | Replace

_eval_mode : Transient | Persistent


For replacement of a subtree, 
        _insertion_mode ==  Replace
        _insertion_point 

For insertion in Program() list,
    _insertion_mode == Insert
    _insertion_list
    _insertion_index
'
'''

#
#*    Console
class TxtConsole:
    """Closely emulate the behavior of the interactive Python interpreter.

    This class builds on TxtInterpret and adds prompting
    using the familiar sys.ps1 and sys.ps2, and input buffering.
    """
    pass

def resetbuffer(self):
    """Reset the input buffer."""
    self.buffer = []
TxtConsole.resetbuffer = resetbuffer

def associate_comments(self, code, source):
    if self._associate_comments:
       self._associate_comments(code, source)
TxtConsole.associate_comments = associate_comments
        

def display_via(self, code):
    """Report successfully executed using display_via callback."""
    if self._display_via:
       self._display_via(code) 
TxtConsole.display_via = display_via

def interact(self, banner="==== l3 console, use EOF (Control-D) to exit ====\n",
             display_via = None,
             do_event = None,
             associate_comments = None,
             ):
    """Closely emulate the interactive Python console.

    The optional banner argument specify the banner to print
    before the first interaction; by default it prints a banner
    similar to the one printed by the real Python interpreter,
    followed by the current class name in parentheses (so as not
    to confuse this with the real interpreter -- since it's so
    close!).

    display_via
        specifies a function F(TREE) to call after successfull
        evaluation, with the l3 tree as argument.  

    associate_comments
        specifies F(tree, src) to call after successfull parsing.

    do_event
        specifies F() to call to form a local event loop.
    """
    self._display_via = display_via
    self._associate_comments = associate_comments
    self._do_event = do_event

    try:
        sys.ps1
    except AttributeError:
        sys.ps1 = ">>> "
    try:
        sys.ps2
    except AttributeError:
        sys.ps2 = "... "
    cprt = 'Type "copyright", "credits" or "license" for more information.'
    if banner is None:
        self.se_write("l3 %s on %s\n%s\n(%s)\n" %
                   (sys.version, sys.platform, cprt,
                    self.__class__.__name__))
    else:
        self.se_write("%s\n" % str(banner))
    more = 0
    while 1:
        try:
            if more:
                prompt = sys.ps2
            else:
                prompt = sys.ps1
            try:
                line = self._raw_input(prompt)
            except EOFError:
                self.se_write("\n==== l3 console exit. ====\n")
                break
            else:
                # Empty input finishes any indentation and displays
                # the next prompt.
                if line == '':
                    if len(self.buffer) > 0:
                        more = self.try_lines('\n')
                    else:
                        more = 0
                    continue

                # Comments at BOL.
                if line[0] == '#':
                    self.buffer.append(line)
                    more = 1
                    continue

                # Finishing unevaluated block?
                if (not line[0].isspace()) and (len(self.buffer) > 0):
                    if line.rstrip()[-1] == ':':
                        # Continuation of block statement (else: etc.)
                        self.buffer.append(line)
                        continue
                    elif self.buffer[-1][0] == '#':
                        # This line follows a comment; combine them.
                        self.buffer.append(line)
                        continue
                    else:
                        # Preceeding block is finished;
                        #       ... run it 
                        self.try_lines('\n') # finish buffer
                        #       ... and continue with current line
                        more = self.try_lines(line) 
                        continue

                # We must wait for dedents here, because the parser
                # considers end-of-string as EOF.
                if more:
                    self.buffer.append(line)
                else:
                    more = self.try_lines(line)
                continue

        except KeyboardInterrupt:
            self.se_write("\nKeyboardInterrupt\n")
            self.resetbuffer()
            more = 0
        except Exception, e:
            self.showtraceback()
            raise
TxtConsole.interact = interact

def try_lines(self, line):
    """Send a line to the interpreter to test expression for
    completeness. """ 
    self.buffer.append(line)
    source = "\n".join(self.buffer)
    more = self._interpreter.runsource(source, self.filename)
    if not more:                    # Command executed or was invalid. 
        self.resetbuffer()
    return more
TxtConsole.try_lines = try_lines

def _raw_input(self, prompt=""):
    """Write a prompt and read a line.

    The returned line does not include the trailing newline.
    When the user enters the EOF key sequence, EOFError is raised.

    """
    if self._do_event != None:
        import thread 
        from threading import Lock
        import time
        lck = Lock()
        value = []
        def _do():
            try:
                value.append(raw_input(prompt))
            except EOFError:
                value.append(EOFError())
            lck.release()
        lck.acquire()
        thread.start_new_thread(_do, ())
        # Manual main loop until input is ready.
        while not lck.acquire(False):
            self._do_event()
            time.sleep(0.01)
        if isinstance(value[-1], Exception):
            raise value[-1]
        return value[-1]

    else:        
        return raw_input(prompt)

TxtConsole._raw_input = _raw_input

def __init__(self, interpreter, filename="<console>"):
    """
    The optional filename argument should specify the (file)name
    of the input stream; it will show up in tracebacks.
    """
    self._display_via = None
    self._interpreter = interpreter
    self.filename = filename
    self.resetbuffer()
TxtConsole.__init__ = __init__

def connect_interpreter(self, interpreter):
    self._interpreter = interpreter
TxtConsole.connect_interpreter = connect_interpreter

def disconnect_interpreter(self, interpreter):
    self._interpreter = None            # HERE. error handling?
TxtConsole.disconnect_interpreter = disconnect_interpreter


def showtraceback(self):
    """Display the exception that just occurred.

    We remove the first stack item because it is our own code.
    """
    try:
        type, value, tb = sys.exc_info()
        sys.last_type = type
        sys.last_value = value
        sys.last_traceback = tb
        tblist = traceback.extract_tb(tb)
        del tblist[:1]
        list = traceback.format_list(tblist)
        if list:
            list.insert(0, "Traceback (most recent call last):\n")
        list[len(list):] = traceback.format_exception_only(type, value)
    finally:
        tblist = tb = None
    map(self.se_write, list)
TxtConsole.showtraceback = showtraceback

def se_write(self, data):
    # stderr write
    sys.stderr.write(data)
    sys.stderr.flush()
TxtConsole.se_write = se_write

def maybe_softspace(self):
    if softspace(sys.stdout, 0):
        print
TxtConsole.maybe_softspace = maybe_softspace


#
#*    common 

def get_console_interpreter_pair(root_env, storage):
    # TxtConsole and TxtInterpret are mutually
    # dependent.  This is the external constructor for the pair.
    interp               = TxtInterpret(root_env, storage, None)
    console              = TxtConsole(None)
    interp.connect_interface(console)
    console.connect_interpreter(interp)
    return (console, interp)
