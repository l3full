#
# Author:  Michael H. Hohn (mhhohn@lbl.gov)
#
# Copyright (c) 2006, The Regents of the University of California
# 
# See legal.txt and license.txt
#

"""
Extra tools for viewing l3 trees in various ways.
"""
import re, pdb, sys, pprint, os
from pprint import pprint
from l3lang import ast, utils
from l3lang.globals import logger
# AST types
from l3lang.ast import                                                  \
     Call,                                                              \
     Float,                                                             \
     Function,                                                          \
     If,                                                                \
     Immediate,                                                         \
     Inline,                                                            \
     Int,                                                               \
     Labeled,                                                           \
     Let,                                                               \
     List,                                                              \
     Macro,                                                             \
     Map,                                                               \
     Marker,                                                            \
     MarkerTyped,                                                       \
     Member,                                                            \
     Native, \
     Nested, \
     Program,                                                           \
     Return,                                                            \
     Set,                                                               \
     String,                                                            \
     Subdir,                                                            \
     Symbol,                                                            \
     Tuple,                                                             \
     aList,                                                             \
     aNone, \
     astType,\
     cls_viewList
#      viewList

# Others.
from l3lang.ast import TreeWork

from types import *

#*    Misc.
def strip_comment(st):
    # strip comments off a repr() string -- uses \\n etc.
    return (re.sub(r"#.*?\\n", "", st))

def combine_newlines(st):
    # combine trailing form-feeds off a repr() string -- uses \\l etc.
    return re.sub(r'(\\l){1,}$', r'\\l', st)

def leading_lines(str, num_lines = 4, max_len = 80):
    #     Return the first num_lines empty lines of str, each
    #     truncated to max_len
    #
    lines = str.split("\n")
    llen = len(lines)
    # Remove empty lines.
    lines = filter(lambda s: len(s) != 0 and (not s.isspace()), lines)
    # Use first num_lines
    lines = lines[0:num_lines]
    # Trim to max_len
    def shorten(ll):
        nl = ll[0:max_len]
        if len(ll) > max_len:
            return nl + "..."
        return nl
    lines = map(shorten, lines)
    if llen > num_lines:
        lines.append('...')
    return lines


#*    AST printing
#**        prefix tree dump
#
# Trivial And Ugly -- but useful.
# Use a stream with .write() method.
#
# Indentation is handled by Nested() types;  Immediates always print.
#
#***            immediates
def prefix_dump(self, stream, indent = 0):
    stream.write(str(self._primary[0]) + " ")
Immediate.prefix_dump = prefix_dump

def prefix_dump(self, stream, indent = 0):
    stream.write(repr(self._primary[0]) + " ")
String.prefix_dump = prefix_dump

def prefix_dump(self, stream, indent = 0):
    a, b = self._primary
    a.prefix_dump(stream, indent)
    stream.write( "." )
    b.prefix_dump(stream, indent)
Member.prefix_dump = prefix_dump
#
#***            nested
def prefix_dump(self, stream, indent = 0):
    # Output format is usually (name args)
    full_name = self.__class__.__name__
    # name -> print name
    name_map = {
        "Program" : "",
        "Map" : "map",
        "Set" : "set",
        "Call" : "call",
        "Call" : "",
        }
    # Head
    print_name = name_map.get(full_name, full_name)
    stream.write("(%s " % print_name )
    # Body
    indent += 1
    for sub in (self._primary):
        stream.write("\n" + "\t" * indent) # nl / indent
        sub.prefix_dump(stream, indent)
    # Tail
    indent -= 1
    stream.write(")")
    ## stream.write("\n" + "\t" * indent) # nl / indent
Nested.prefix_dump = prefix_dump

def prefix_dump(self, stream, indent = 0):
    # Head.
    stream.write( "(")
    # Body.
    indent += 1
    for sub in self:
        stream.write("\n" + "\t" * indent) # nl / indent
        sub.prefix_dump(stream, indent)
    # Tail
    indent -= 1
    stream.write(")" )
    ## stream.write("\n" + "\t" * indent) # nl / indent
aList.prefix_dump = prefix_dump


#**        infix tree formation (pretty-printing)
# 
# Ignores precedence for now;  that can be added in a separate pass
# providing annnotations.
#
# output format
# ----------------------
#     Output format defaults to Python syntax; this may lead to invalid
#     strings if l3's nesting abilities are used  heavily (like a
#     function definition inside an Environment, which in Python would be
#     a `def` inside a dict ({}), and is invalid.
#
#     A l3-syntax output formatter can be added later.
#
# Error handling 
# ----------------------
#       To find an error, a printout is needed.  Thus, raising
#       exceptions in printing routines is a bad idea.  Instead, errors are
#       inserted into the output as comments.

# Immediate.infix_string includes
#    Bool
#    Int
#    Float
#    Complex
#    Symbol

# Types that don't (yet) need .infix_string():
#   Macro
#   Native

# 
#***            pretty-printer imports
from l3lang.pretty import                                               \
    DocNil,                                                              \
    DocCons,                                                             \
    DocText,                                                             \
    DocNest,                                                             \
    DocBreak,                                                            \
    DocGroupFlat,                                                        \
    DocGroupBreak,                                                       \
    DocGroupFill,                                                        \
    DocGroupAuto

B = lambda : DocBreak(" ")
BW = lambda s : DocBreak(s)
C = lambda x,y: DocCons(x, y)
T = lambda t : DocText(t)
G = lambda x : DocGroupAuto(x)
GB = lambda x : DocGroupBreak(x)
GF = lambda x : DocGroupFill(x)
GFL = lambda x : DocGroupFlat(x)
I = lambda r, x : DocNest(r, x)

#
#***            main entry points
def get_infix_string(self, width, comment_dct = {}):
    ''' Return a pretty-printed representation of self.
    The string width is kept below `width` when possible.
    If the (tree id -> comment string) dict (comment_dct) is provided,
    include comments. 
    '''

    S = utils.Shared()
    S._comment = comment_dct

    # Comment printing requires a .setup() tree.
    if comment_dct:
        if not hasattr(self, '_id'):
            logger.warn("get_infix_string: argument is not set up and "
                        "cannot be printed with comments.")
            S._comment = {}
    # Statement / expression distinction.
    S._in_statement = [True]
    S.es = lambda : S._in_statement.append(False) # expression start
    S.ee = lambda : S._in_statement.pop()
    S.ss = lambda : S._in_statement.append(True) # statement start
    S.se = lambda : S._in_statement.pop()
    S.in_statement = lambda : S._in_statement[-1]

    return self.infix_string(S).toString(width)
astType.get_infix_string = get_infix_string
aList.get_infix_string = get_infix_string
aNone.get_infix_string = get_infix_string
Native.get_infix_string = get_infix_string

def tree_difference(self, self_tbl, pptree, pptree_tbl):
    ''' Compares trees (including comments) and returns a difference description.
    For eql trees, returns "".
    '''
    o_it = self.top_down()
    pp_it = pptree.top_down()
    while 1:
        try: o_nd = o_it.next()
        except StopIteration: break
        pp_nd = pp_it.next()

        # Trees equal?
        if o_nd.eql_1(pp_nd):
            if self_tbl.has_key(o_nd._id):
                if self_tbl[o_nd._id].eql_1(pptree_tbl[pp_nd._id]):
                    continue
                else:
                    return ("Tree comment difference from line %d, col %d\n"
                            "                     to line %d, col %d\n" 
                            % (o_nd._first_char[0], o_nd._first_char[1],
                               pp_nd._first_char[0], pp_nd._first_char[1]))
            else:
                continue
        else:
            return ("Tree difference: line %d, col %d\n    original:\n%s\n"
                    "    new:\n%s\n"
                    %
                    (pp_nd._first_char[0], pp_nd._first_char[1],
                     str(o_nd), str(pp_nd)))
    return ""
astType.tree_difference = tree_difference

def tree_difference(self, self_tbl, pptree, pptree_tbl):
    return ""
Native.tree_difference = tree_difference

#
#***            comment functions
def with_comment(self, S, body):
    if S._comment:
        try:
            if S._comment.has_key(self._id):
                # Multi-line comments must be properly indented.
                # Form every line separately and assemble the group.
                comm_lines = S._comment[self._id].split('\n') # windows?
                while comm_lines[-1] == '': # Strip trailing blanks
                    comm_lines.pop()
                comment = DocNil()
                for line in comm_lines:
                    comment |= T("#" + line) | B()
                return GB(comment | body)
        except AttributeError:
            return body                 # Ignore missing self._id
    return body
astType.with_comment = with_comment
aList.with_comment = with_comment
Native.with_comment = with_comment

#
#***            immediates
def infix_string(self, S):
    return self.with_comment(S, T(str(self._primary[0])))
Immediate.infix_string = infix_string

def infix_string(self, S):
    # Using repr() introduces escaped escapes ('\\n' instead of '\n')
    # so that `print repr(a)` == a.
    # But using repr() inside another string without print is wrong
    # when PARSING the contents: 
    #     aa = "a\nb"
    #     repr(aa)
    #     -> "'a\\nb'"
    # Parsing this will read \\n instead of \n.  The \\n is correct
    # only within a string-conversion pass.
    # 
    # str(aa)    converts unprintables into ascii
    # repr(aa)   converts unprintables into ascii and escapes when needed
    #
    # Here, we only want unprintables -> ascii plus correct quotes.

    got = lambda sub : self._primary[0].find(sub) != -1
    if got('\n'):
        if got("'''"):
            if got('"""'):
                raise Exception("String too complicated for infix_string.")
            else:
                pstr = '"""%s"""' % self._primary[0]        
        else:
            pstr = "'''%s'''" % self._primary[0]

        return self.with_comment(S, T(pstr))
    
    if got('"'):
        if got("'"):
            if got("'''"):
                if got('"""'):
                    raise Exception("String too complicated for infix_string.")
                else:
                    pstr = '"""%s"""' % self._primary[0]        
            else:
                pstr = "'''%s'''" % self._primary[0]
        else:
            pstr = "'%s'" % self._primary[0]
    else:
        pstr = '"%s"' % self._primary[0]
        
    return self.with_comment(S, T(pstr))
String.infix_string = infix_string

#
#***            nested
def infix_string(self, S):
    return self.with_comment(S, GB(T("program:") |
                                   I(4, (B() |
                                         self[0].infix_string(S) ))))
Program.infix_string = infix_string

def infix_string(self, S):
    return self.with_comment(S, GB(T("subdir:") |
                                   I(4, (B() |
                                         self[0].infix_string(S) ))))
Subdir.infix_string = infix_string

def infix_string(self, S):
    #
    #    return
    #      foo
    # 
    # is invalid; requires \ as break character:
    # 
    #     return \
    #       foo
    # Or use GroupFill to force first element onto same line.
    return self.with_comment(S, GF( T('return ') |
               self[0].infix_string(S)))
Return.infix_string = infix_string

def infix_string(self, S):
    # {|| }
    S.es()
    body = (T('{|') | self[0].infix_string(S) |   # args
            T('|') |
            I(4, (B() |
                  self[1].infix_string(S))) |  # body
            B() |
            T('}')
            )
    S.ee()
    return self.with_comment(S, G(body))
Function.infix_string = infix_string

def infix_string(self, S):
    fname = self.calltree_str()    # function name. 
    if fname[0].isalpha():
        # Standard prefix form f(a,b); ignore special cases for f.
        body = T(fname) | T('(') 
        S.es()
        body |= (I(4, self[1].infix_string(S)) |  # arguments.
                 T(')'))
        S.ee()
        return self.with_comment(S, G(body))

    else:
        if self.nargs() == 1:
            # Assume unary prefix operator.
            S.es()
            body = GF( T('(') |
                       T(fname) |  
                       I(4, self[1].infix_string(S)) |  # arguments.
                       T(')'))
            S.ee()
            return self.with_comment(S, body)

        else:
            # Assume binary infix operator.
            left, right = self.positional_args()
            S.es()
            body = GF( T('(') |
                       left.infix_string(S) |
                       B() |
                       T(fname) | 
                       B() |
                       right.infix_string(S) |
                       T(')'))
            S.ee()
            return self.with_comment(S, body)
Call.infix_string = infix_string

def infix_string(self, S):
    # a.b
    a, b = self._primary
    return self.with_comment(S, G(a.infix_string(S) |
             T(".") |
             b.infix_string(S)))
Member.infix_string = infix_string

def infix_string(self, S):
    # if cond:
    #    yes
    # else:
    #    no
    cond, yes, no = self._primary
    if len(no) > 0:
        S.ss()
        the_else = (B()|
                    T("else:") |
                    I(4, B() | no.infix_string(S)))
        S.se()
    else:
        the_else = DocNil()

    if not S.in_statement():
        body = T("# ERROR: For printing, If must not be in an expression.") | B()
    else:
        body = DocNil()

    body |= T("if ")
    S.es()
    body |= cond.infix_string(S)
    S.ee()
    S.ss()
    body |= T(":") | I(4, B() | yes.infix_string(S)) | the_else
    S.se()    
    return self.with_comment(S, GB(body))
If.infix_string = infix_string

def infix_string(self, S):
    # lhs = rhs
    # Special case for `def foo(a,b): body`
    #         def LNAME(LARGS):
    #             LBODY
    ma = ast.Matcher()
    if ma.match(self,
                Set(MarkerTyped(Symbol('LNAME'), Symbol('symbol')),
                    Function(MarkerTyped(Symbol('LARGS'), aList([])),
                             MarkerTyped(Symbol('LBODY'), aList([]))))):
        ma['LNAME']
        body = T('def ') | ma['LNAME'].infix_string(S) | T('(')
        S.es()
        body |= ma['LARGS'].infix_string(S) | T('):') 
        S.ee()
        body |= I(4, B() | ma['LBODY'].infix_string(S))
        return self.with_comment(S, GB(body))

    else:
        lhs, rhs = self._primary
        return self.with_comment(S, GF(lhs.infix_string(S) |
                  T('=') |
                  rhs.infix_string(S)))
Set.infix_string = infix_string

def infix_string(self, S):
    # [ body ]
    S.es()
    body = G( T('[') |
              I(4, (B() |
                    self[0].infix_string(S)))|
              B() |
              T(']'))
    S.ee()
    return self.with_comment(S, body)
List.infix_string = infix_string


def infix_string(self, S):
    body = GF(T('if ("outline",') | B() |
              T(repr(self.get_label())) | 
              B() | T("):"))
    S.ss()
    body |=  I(4, B() | self[0].infix_string(S))
    S.se()    
    return self.with_comment(S, GB(body))
cls_viewList.infix_string = infix_string


def infix_string(self, S):
    # { body }
    S.es()
    body = G( T('{') |
              I(4, (B() |
                    self[0].infix_string(S)))|
              B() |
              T('}'))
    S.ee()
    return self.with_comment(S, body)
Map.infix_string = infix_string

def infix_string(self, S):
    # ( body )
    S.es()
    body = GF( T('(') |
              I(1, self[0].infix_string(S)) |
              T(')'))
    S.ee()
    return self.with_comment(S, body)
Tuple.infix_string = infix_string

#
#***            Specials
def infix_string(self, S):
    return DocNil()
aNone.infix_string = infix_string

def infix_string(self, S):
    return self.with_comment(S, T(repr(self._primary[0])))
Native.infix_string = infix_string


def infix_string(self, S):
    # inline string
    return self.with_comment(S, GFL( T('inline') | B() | self[0].infix_string(S)))
Inline.infix_string = infix_string

def infix_string(self, S):
    body = DocNil()
    if len(self) > 0:
        if S.in_statement():
            # inside program, def: ';'      '\n'    (no ',')
            for sub in self:
                body = body | sub.infix_string(S) | BW(';')
            body = body.head()              # strip BW(';')
        else:
            # inside list, dict:   ' '      '\n' (always with ',')
            for sub in self:
                body = body | sub.infix_string(S) | T(',') | B()
            body = body.head().head()       # strip T(',') | B()
        
    return self.with_comment(S, G( body ))
aList.infix_string = infix_string


#*    AST information
#**        print_references
# As of [Fri Sep 17 11:35:26 2004],
# A tree's ._id is used in
#     (astType) self._id
#               self._primary_ids
#               child._parent
#     (RamMem)  storage.store / load
#               storage.get_attribute / set_attributes
#     (IncEval) storage.ie_.touch_setup / touch / get_timestamp
#               has_clone
#     (Env)     env.bind_id / lookup_symbol_id
#
def print_references(storage, tree_id, indent = 0):
    # Print information about references to tree_id, from various
    # sources.

    # Use via
    #     print_references(storage, 140163)
    # and together with
    #     pprint(user_env.dump())
    #     print_info(storage, 140163)
    #
    tree = storage.load(tree_id)
    print ' '*indent, 'id / string:', tree_id, tree.source_substring()
    print ' '*indent, 'tree:', tree

    # Parent referring to tree.
    parent_id = tree.parent_id()
    if parent_id is None:
        print ' '*indent, "no parent"
    else:
        parent = storage.load(parent_id)
        print ' '*indent, "index %d of parent %d" % (parent.find_child_index(tree_id),
                                         parent_id,
                                         )
    # Children referring to tree
    child_id_l = tree.childrens_ids()
    for ch in child_id_l:
        print ' '*indent, "has child %d" % ch
        if storage.load(ch).parent_id() != tree_id:
            print ' '*indent, "warning: child has other parent"

    # Tree's Env's
    for cmd_s in ['tree._arg_env',      # block
                  'tree._block_env',    # block
                  'tree._def_env',      # block
                  'tree._eval_env',     # some Nested()s
                  'storage.get_attribute(tree_id, "interp_env")',
                  ]:
        try:
            env = eval(cmd_s)

            if env._program == tree:
                print ' '*indent, cmd_s, '._program'

            # name -> value, tree may be value
            # (name, 'ie_status') -> timestamp
            the_name = None
            if isinstance(tree, Symbol):
                the_name = tree.as_index()
            for nm, val in env._bindings.iteritems():
                if val == tree:
                    print ' '*indent, cmd_s, '._bindings[', repr(nm), ']'
                if nm == the_name:
                    print ' '*indent, cmd_s, '._bindings[', repr(nm), ']'
                if nm == (the_name, 'ie_status'):
                    print ' '*indent, cmd_s, '._bindings[', \
                          repr((the_name, 'ie_status')), ']'

            # name -> id, tree_id may == id
            for nm, id in env._bindings_ids.iteritems():
                if id == tree_id:
                    print ' '*indent, cmd_s, '._bindings_ids[', repr(nm), ']'

        except AttributeError:
            print ' '*indent, 'no ', cmd_s, 'attribute'
            continue

def print_all_references(storage, tree_id, indent = 0):
    # Traverse all clones (recursively) and call print_references()
    # for each.
    #
    print ' '*indent, "---------------------- original data"
    print_references(storage, tree_id, indent = indent)

    clone_l = storage.get_attribute(tree_id, "interp_clone")
    if clone_l != None:
        print ' '*indent, "---------------------- clone data"
        for clone in clone_l:
            print_all_references(storage, clone, indent = indent + 4)


#
#**        print_info: prefix format tabular dump
def print_info(storage, tree,
               show_macros = False,
               lead_indent = 0,
               node_width = 50,
               max_node_len = 20,
               per_level_indent = 4,
               space = ' ',
               custom = "nd._id",       
               ):
    #
    # `custom` is a caller-chosen expression; may use `nd` for the
    # current node.
    # 
    # Print id & timestamp with leading parts of the ast, in a prefix
    # format.
    # This prefix dump ignores clones.
    #
    lead = space * lead_indent * per_level_indent
    # columns are
    # lead  id  time [custom] indent  node-string
    print "%s%5s %6s %8s %s %s" % (lead, 'id', 'time', custom, '', 'node')
    print "%s--------------------------------------------" % lead
    for nd, indent in tree.top_down_indented():
        # id = nd._id fails w/o ._id attribute.
        id = nd.__dict__.get('_id')

        # Print main tree.
        nd_str = str(nd)             # Inefficient string formation...

        # Prune nested trees
        if isinstance(nd, (Nested, aList)):
            nd_str = nd_str[0:nd_str.find('(')]

        # Prune long items
        if isinstance(nd, (String, Symbol)):
            if len(nd_str) + indent * per_level_indent > node_width and \
                   len(nd_str) > max_node_len:
                off = nd_str.find('(')
                cutoff = max(off + 8,
                             abs(node_width - indent * per_level_indent - off))
                nd_str = nd_str[0 : cutoff] + " ...')"

        # Print source macro.
        if show_macros and hasattr(nd, '_macro_id'):
            macro = storage.load(nd._macro_id)
            print "\n%s[ From macro:" % lead
            print_info(storage, macro, show_macros = show_macros,
                       lead_indent = indent, custom = custom)
            print "%s]" % lead

        if id is None:
            print "%s----- ---------- %s %s" % (
                lead,
                space * per_level_indent * indent,
                nd_str,
                )
        else:
            try:
                cus_val = eval(custom)
            except:
                cus_val = '--------'
            print "%s%5d %6s %8s %s %s" % (
                lead,
                id,
                storage.ie_.get_timestamp(id),
                cus_val,
                space * per_level_indent * indent,
                nd_str,
                )
#
#**        print_ast: prefix format dump, needs only ast.
def print_ast(tree,
              lead_indent = 0,
              node_width = 50,
              max_node_len = 20,
              per_level_indent = 4,
              space = ' ',
              ):
    # Print leading parts of the ast, in a prefix format.
    # 
    lead = space * lead_indent * per_level_indent
    print "%s%5s %10s %s %s" % (lead, 'id', 'time', '', 'node')
    print "%s--------------------------------------------" % lead
    for nd, indent in tree.top_down_indented():
        # Print main tree.
        nd_str = str(nd)             # Inefficient string formation...

        # Prune nested trees
        if isinstance(nd, (Nested, aList)):
            nd_str = nd_str[0:nd_str.find('(')]

        # Prune long items
        if isinstance(nd, (String, Symbol)):
            if len(nd_str) + indent * per_level_indent > node_width and \
                   len(nd_str) > max_node_len:
                off = nd_str.find('(')
                cutoff = max(off + 8,
                             abs(node_width - indent * per_level_indent - off))
                nd_str = nd_str[0 : cutoff] + " ...')"

        print "%s----- ---------- %s %s" % (
            lead,
            space * per_level_indent * indent,
            nd_str,
            )


#
#**        print_all_info: prefix format tabular dump with clones.
def print_all_info(storage, tree,
                   per_level_indent = 4,
                   values = False,
                   ):
    # print id, timestamp and clone level information,
    # with leading parts of the ast.
    # This provides a long dump of all tree nodes and their clones;
    # useful for comparing state between runs.
    #
    printed = {}
    space = ' '
    print "%5s %10s %5s %s %s" % ('id', 'time', 'clone', '', 'node')
    print "--------------------------------------------"
    geta = storage.get_attribute
    for nd, clone_lev, indent in tree.prefix_all(storage):
        id = nd._id
        # print a clone's nested elements only once, as part of the
        # clone.
        if printed.has_key(id):     continue
        printed[id] = 1

        # Inefficient string formation...
        nd_str = str(nd)
        if isinstance(nd, (Nested, aList)):
            nd_str = nd_str[0:nd_str.find('(')]

        print "%5d %10s %.5d %s %s" % (
            id,
            storage.ie_.get_timestamp(id),
            clone_lev,
            space * per_level_indent * indent,
            nd_str,
            )
        if values:
            val = geta(id, 'interp_result')
            if val != None:
                val_str = str(val)
                print "%5s %10s %5s %s `...... %.30s" % (
                    " ",
                    " ",
                    " ",
                    space * per_level_indent * indent,
                    val_str)


#*    Call context
#**        print_calling_context
def print_calling_context(self, context, level = 0):
    geta = self._storage.get_attribute
    load = self._storage.load
    gts = self._storage.ie_.get_timestamp
    #
    for path in context:
        src, clone = path[0]
        print "    "*level,
        print src, gts(src), load(src).source_string(), " -> ", \
              clone, gts(clone), load(clone).source_string()
        ## print geta(clone, "interp_result")
        if len(path) > 1:
            self.print_calling_context( path[1:], level + 1)
TreeWork.print_calling_context = print_calling_context

#
#**        print_call_ctxt
def print_call_ctxt(self, ctxt):
    geta = self._storage.get_attribute
    load = self._storage.load
    gts = self._storage.ie_.get_timestamp
    #
    def _show_path(path, level):
        cc = path[0]
        ## print "    "*level,
        # Calls.
        if cc.J:
            print cc.J, gts(cc.J), load(cc.J).source_string(), " -> ", \
                  cc.K, gts(cc.K), load(cc.K).source_string()
        else:
            print "program source",  " -> ", \
                  cc.K, gts(cc.K), load(cc.K).source_string()

        # Sources
        if cc.O:
            print cc.O, gts(cc.O), '[['
            print load(cc.O).source_string()
            print ']]'

        ## print geta(clone, "interp_result")
        if len(path) > 1:
            _show_path( path[1:], level + 1)
    #
    for path in ctxt:
        _show_path(path, 0)
        print
TreeWork.print_call_ctxt = print_call_ctxt

#
#**        print_call_ctxt_dot
def print_call_ctxt_dot(self, ctxt,
                        title = "print_call_ctxt_dot",
                        out = sys.stdout,
                        unique = False):
    #
    # Produce O -> J -> C locally, and recurse.  This omits the
    # lexical source of O, which is important for nested loops.
    #
    # K is contained in C, so substitute
    #     C[i] -> J[i+1]
    # for
    #     K[i] -> J[i+1]
    #
    # Path leading to goal call            goal call
    #                                 |
    #                                 |
    #      +----+                     |
    #      |  O |                     |
    #      +----+                     |
    #         |                       |
    #         V                       |
    #      +----+                     |
    #      |  J |                     |
    #      +----+          +----+     |
    #         |            |  O |     |
    #         V            +----+     |
    #      +----+             |       |
    #      |  C |             V       |
    #      |    |          +----+     |     +---+
    #      |  K----------> |  J |  . .|. .  | C |
    #      +----+          +----+     |     +---+
    #                         |       |       |
    #                         V       |       V
    #                      +----+     |     +---+
    #                      |  C |     |     | K |
    #                      |    |     |     +---+
    #                      |  K |     |
    #                      +----+     |
    #
    # Flags:
    # unique
    #    ALL calling paths are shown, so for a target T reached
    #    via N paths passing through P, P will have N incoming and (N-1)
    #    outgoing edges.  Use unique = True to get only one.
    #

    uniq_t = {}

    geta = self._storage.get_attribute
    load = self._storage.load
    gts = self._storage.ie_.get_timestamp
    #
    def dotify(id):
        # Get a dot-usable string from the node id.
        return combine_newlines(
            strip_comment(repr(load(id).source_string()))[1:-1].\
            replace(r"\n", "\l") + "\l")
    #
    def _show_path(path, J_from, level):
        cc = path[0]

        # Sources.
        if cc.O:
            print >> out, cc.O, '[label="%s"];' % dotify(cc.O)
        if cc.J:
            print >> out, cc.J, \
                  '[label="call %d\l%s"];' % (cc.J, dotify(cc.J))
        if cc.C:
            # K is in C
            if level == 0:
                # not a clone
                print >> out, cc.C, \
                      '[label="%s"];' % (dotify(cc.C))
            else:
                print >> out, cc.C, \
                      '[label="clone %d\l%s"];' % (cc.C, dotify(cc.C))

        # Calls.
        if cc.J:
            if not uniq_t.has_key((cc.O, cc.J)):
                print >> out, cc.O, '->', cc.J, '[label="used by"];'
                print >> out, cc.J, '->', cc.C, '[label="produces"];'
            if unique:
                uniq_t[(cc.O, cc.J)] = 1
                uniq_t[(cc.J, cc.C)] = 1
        if J_from:
            if not uniq_t.has_key((J_from, cc.J)):
                print >> out, J_from, '->', cc.J, '[label="contains"];'
            if unique:
                uniq_t[(J_from, cc.J)] = 1

        if len(path) > 1:
            # Continue down the call chain.
            _show_path( path[1:], cc.C, level + 1)
        else:
            # Show the goal call K.
            print >> out, cc.K, \
                  '[label="call %d\l%s"];' % (cc.K, dotify(cc.K))
            print >> out, cc.C, '->', cc.K, '[label="contains"];'
    # Header.
    print >> out, '''
    digraph foo {
            page = "8.5,11.0";            /* size of single physical page */
            size="7.5,10.0";              /* graph size */
            /* rotate=90; */
            /* ratio=fill; */
            /* rankdir=LR; */
            margin="0.5,0.5";
            fontsize=24;
            edge [fontname = "Helvetica", fontsize=24];
            node [style=filled, color=grey90, shape=plaintext,
                  fontname="Helvetica", fontsize=24];
    '''
    if title:
        print >> out, '''
        title_node [label="%s", fontname="Helvetica", fontsize=20];
        ''' % title
    # Edges and nodes.
    for path in ctxt:
        _show_path(path, None, 0)
        print >> out

    # Rank alignment for all target clones.
    print >> out, "{ rank=same;"
    for path in ctxt:
        print >> out, path[-1].K, ';'
    print >> out, "}"

    # Footer.
    print >> out, "}"
    out.flush()
TreeWork.print_call_ctxt_dot = print_call_ctxt_dot


#
#**        print_call_chains
def print_call_chains(self, ctxt):
    #
    # Show only the dynamic call chains; subset of print_call_ctxt.
    #
    geta = self._storage.get_attribute
    load = self._storage.load
    gts = self._storage.ie_.get_timestamp
    #
    def _show_path(path, level):
        cc = path[0]
        # Calls.
        if cc.J:
            print cc.J, gts(cc.J), load(cc.J).source_string(), " -> ", \
                  cc.K, gts(cc.K), load(cc.K).source_string()
        else:
            print "program source",  " -> ", \
                  cc.K, gts(cc.K), load(cc.K).source_string()

        if len(path) > 1:
            _show_path( path[1:], level + 1)
    #
    for path in ctxt:
        _show_path(path, 0)
        print
TreeWork.print_call_chains = print_call_chains

#*    interpretation-generated environments
def print_env(env):
    ol = 0
    for (k,v, env, lev) in env.all_bindings_recursive():
        if lev > ol:
            print " "* 4 * ol, "{" * (lev - ol)
            ol = lev
        if lev < ol:
            print " "* 4 * lev, "}" * (ol - lev)
            ol = lev
        print "%s%s = %s\n" % (" "* 4 * lev, k, v)
    if lev > 0:
        print " "* 4 * lev, "}" * (lev - 0)



#*    Decoration (title / label / comment) support 
#**        Find appropriate decoration text 
def deco_title_text(self):
    return self.__class__.__name__
Nested.deco_title_text = deco_title_text

def deco_title_text(self):
    return str(self)
Immediate.deco_title_text = deco_title_text

def deco_title_text(self):
    return str(self)
Native.deco_title_text = deco_title_text

def deco_title_text(self):
    ma = ast.Matcher()
    if ma.match(self,
                Call(Marker('func_name'), Marker('rhs'))):
        return ma['func_name'].get_infix_string(23)
    return ""
Call.deco_title_text = deco_title_text

def deco_title_text(self):
    ma = ast.Matcher()
    if ma.match(self,
                Set(Marker('lhs'), Marker('rhs')) ):
        return "define " + ma['lhs'].get_infix_string(23)
    return ""
Set.deco_title_text = deco_title_text

def deco_title_text(self):
    if self.l_label is None:
        return "List"
    else:
        lbl = self.l_label
        if hasattr(lbl, 'get_infix_string'):
            return lbl.get_infix_string(23)
        else:
            return str(lbl)
List.deco_title_text = deco_title_text

def deco_title_text(self):
    if self.m_label is None:
        if isinstance(self, Subdir):
            return "Subdir"
        else:
            return "Map"
    else:
        lbl = self.m_label
        if hasattr(lbl, 'get_infix_string'):
            return lbl.get_infix_string(23)
        else:
            return str(lbl)
Map.deco_title_text = deco_title_text

def deco_title_text(self):
    if self.p_label is None:
        return "Program"
    else:
        lbl = self.p_label
        if hasattr(lbl, 'get_infix_string'):
            return lbl.get_infix_string(23)
        else:
            return str(lbl)
Program.deco_title_text = deco_title_text

def deco_title_text(self):
    return "Function"
Function.deco_title_text = deco_title_text

def deco_title_text(self):
    return "Raw Python"
Inline.deco_title_text = deco_title_text

