#
# Author:  Michael H. Hohn (mhhohn@lbl.gov)
#
# Copyright (c) 2006, The Regents of the University of California
# 
# See legal.txt and license.txt
#

# 
#*    Interpreter construction utilities.
# For actual interpreters, see l3.py, l3gwidgets.py, main.py (old)
#
import pdb, pickle, sys, traceback, os
import operator, math, string
from pprint import pprint
from types import *

from l3lang import reader, ast
from l3lang.ast import RamMem, Env, empty_parent

#*    l3 infrastructure 
#**        message, parallel, parallel_apply, call_kwd_func
def message(str):
    print str
#
def print_(*args, **kwds):
    for arg in args:
        print arg,
    if kwds.get('trailing_comma', False):
        return
    print 
#
def parallel(*blocks):
    return tuple([bl() for bl in blocks])
#
def parallel_apply(func, seq):
    return [func(item) for item in seq]
#
def read_file(file):
    return "".join(open(file, 'r').readlines())
#
#**        boolean FUNCTIONS
# 	Difference: In python, 'or' and 'and' are shortcut operators,
#				a.k.a. special forms.  Here, both arguments are
#				evaluated. 
def and__(a,b):
	return a and b

def or__(a,b):
	return a or b

def not__(a):
	return not a

def not_contains(a,b):
	return not operator.contains(a,b)

#*    New sub-environment
def get_child_env(root_env, storage):
    """
    return a new, empty child environment.  System setup (via
    setup_envs) should have been done for the root_env.
    """
    tree = reader.parse("1")   #  The Program() for insertion in user_env.
    def_env = root_env.new_child(tree)
    tree.setup(empty_parent(), def_env, storage) # no set_outl_edges
    ## setup_envs(storage, root_env, def_env)
    return def_env


#*  iterator wrapper
class l3iter:
    # Wrap iterators so that
    # 1. time stamps are returned along with the value
    # 2. re-iterating the wrapped iterator produces the same timestamps
    # 3. instances of this class can be pickled and restored.

    # UNFINISHED.

    def __init__(self, seq, storage):
        self._iter = iter(seq)
        self._index = 0
        self._timestamp = []

        self._next = None
        self._next_ready = False

        storage.new_id

    def __getstate__(self):
        rv = {}
        rv.update(self.__dict__)
        rv['_iter'] = None
        return rv

    def __setstate__(self, stuff):
        self.__dict__.update(stuff)

    def more(self):
        if self._next_ready:
            return True
        try:
            self._next = self._iter.next()
            self._next_ready = True
            return True
        except StopIteration:
            self._next_ready = False
            return False

    def next(self):
        if self.more():
            self._next_ready = False
            return self._next
        raise InterpreterError("Request for elements from finished iterator.")
            

#*    Additions to Environments
def setup_envs(storage, def_env, user_env):
    # 
    # The def_env should hold system definitions, and others that need
    # not be persistent.  In particular, references to storage.
    # 
    # Non-atomic functions must not be bound; in particular:
    #     open, close
    # 
    #**        operators, python __builtins__ 
    for symb , oper in [
        ('+'   	, operator.add),
        ('-'   	, operator.sub),
        ('*'   	, operator.mul),
        ('/'   	, operator.div),
        ('//'   , operator.floordiv), # floordiv(a, b) -- Same as a // b.
        ('**'  	, math.pow),
        ('and'  , and__),
        ('or'  , or__),
        ('not'  , not__),
        ## ('neg' 	, operator.neg),       # - a, via import_all_names
        ('%'    , operator.mod),
        #
        ('=='  	, operator.eq),
        ('!='  	, operator.ne),
        ('>'   	, operator.gt),
        ('>='  	, operator.ge),
        ('<'   	, operator.lt),
        ('<='  	, operator.le),
        # 
        ('is_'  	, operator.is_),
        ('is_not'  	, operator.is_not),
        ('contains' , operator.contains),
        ('not_contains', not_contains),
        # 
        # Different semantics!!
        # 
        ('&'   	, operator.__and__),
        ## no: ('or'  	, operator.__or__),
        ('|'   	, operator.__or__),
        ('not'	, operator.__not__),
        #
        ]:
        def_env.import_external(symb, oper)

    #
    #**        modules
    for name in ['operator', 'string', 'reader', 'pdb',]:
        def_env.import_module(name)

    def_env.import_all_names('operator')

    # 
    #**        l3 builtin functions
    # Blanket binding of (contents of) 'locals', 'globals', ] in
    # Env()s causes problems with pickling lateron, as did
    #
    #     def_env.import_all_names(__builtins__)
    # 
    # Only import needed functions.
    for names in ['parallel', 'parallel_apply', 'message', 'read_file',
                  'None', 'True', 'False',
                  'len', 'reduce', 'apply', 'max', 'min', 'pprint',
                  'xrange', 'range', 'print_', 'filter', 'map', 'zip',
                  'l3iter', 'int', 'float', 'dict', 'dir', 'globals', 'locals',
                  'slice', 'help',
                  ]:
        def_env.import_def(names, globals())

    # 
    #**        l3 execfile for emacs/toplevel interaction.
    def fep(file):
        '''
        file-eval-print
        '''
        str = "".join(open(file, 'r').readlines())
        tree = reader.parse(str)
        tree.setup(empty_parent(), user_env, storage
                   ) # no set_outl_edges(self.w_, None)
        result = tree.interpret(user_env, storage)
        # storage.add_program(tree)
        return tree, result
    def_env.import_external('execfile', fep)


    #
    #**        l3 eval for run-time evaluation.
    def sep(str):
        # string-eval-print
        # 
        tree = reader.parse(str)
        tree.setup(empty_parent(), user_env, storage
                   ) # no set_outl_edges(self.w_, None)
        result = tree.interpret(user_env, storage)
        return tree, result
    def_env.import_external('eval', sep)

    #
    #**        user_env / l3 itself
    # Make the user_env visible in l3 itself, including some member
    # functions.
    # 
    # The member functions thus look and behave like globals in the repl;
    # this is for convenience, but has to be tested for interference before
    # making it a default.
    # 
    l3 = user_env
    user_env.import_def("user_env", locals())
    user_env.import_def("l3", locals())

    user_env.import_external("l3ls"          , user_env.all_bindings) 
    ## user_env.import_external("import_names" , user_env.import_names)
    ## user_env.import_external("import_module" , user_env.import_module)

    def_env.import_external("pm"           , pdb.pm)
    # @@ indirect link to storage.  Not ideal...
    def_env.import_external("new_id"       , storage.new_id)
    def_env.import_external("new_name"       , storage.new_name)
    def_env.import_external("l3pwd"          , os.getcwd)
    def_env.import_external("l3cd"           , os.chdir)
    def_env.import_external("l3chdir"        , os.chdir)
    ## def_env.import_external("None"         , None)


#*    Environments as keyword arguments
#**        Filter time stamps
def clean_dict(dct):
    """ Remove all non-string keyed entries from dct. """
    new = {}
    for k,v in dct.iteritems():
        if isinstance(k, StringType):
            new[k] = v
    return new
