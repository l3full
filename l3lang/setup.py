#
# Author:  Michael H. Hohn (mhhohn@lbl.gov)
#
# Copyright (c) 2006, The Regents of the University of California
# 
# See legal.txt and license.txt
#

from distutils.core import setup
setup(
    author='Michael Hohn',
    author_email='mhhohn@lbl.gov',
    description='The l3 language interpreter',
    name='l3lang',
    url='http://cci.lbl.gov/~hohn/l3/',
    version='0.3.1',

    package_dir = {'l3lang': ''},
    packages=['l3lang', 'l3lang.external'],

    data_files = [('man/man1', ['doc/man/man1/l3.1']),
                  ('man/mann', ['doc/man/mann/l3lang.n']),
                  ],
    scripts=['bin/l3'],
    )


