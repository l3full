#
# Author:  Michael H. Hohn (mhhohn@lbl.gov)
#
# Copyright (c) 2006, The Regents of the University of California
# 
# See legal.txt and license.txt
#

#*    init
import pdb, pickle, sys, traceback, os
import operator, math, string
from types import *
from math import *

from l3lang import reader, ast, interp, repl, utils
from l3lang.ast import RamMem, Env, empty_parent
from pprint import pprint
from l3lang.interp import parallel, parallel_apply, message, \
     read_file, and__, or__, not__
import l3lang.globals as glbl
pm = pdb.pm

#
#*    bare env 
storage = ast.RamMem('root_memory', 30000)  # Use lexically distinct values.
root_env = ast.Env('builtins', None, None, storage)
#
#*    Provide the Program() for insertion in def_env
tree = reader.parse("1") 
def_env = root_env.new_child(tree)
tree.setup(empty_parent(), def_env, storage)# no set_outl_edges(self.w_, None)
### result = tree.interpret_start(def_env, storage)

interp.setup_envs(storage, root_env, def_env)

#*    Interaction console / interpreter
console, interpreter = repl.get_console_interpreter_pair(def_env, storage)
def_env.import_external('console', console)
def_env.import_external('interp', interpreter)

#*    Set world state.
w_ = utils.Shared()                     # (w_)orld parameters
w_.state_ = utils.Shared()
w_.state_.def_env = def_env
w_.state_.storage = storage
glbl.l3_set_text(w_)

#*    main I/O entry points.
#
#**        load script
def load_script(w_, filename):
    # Tree setup.
    text = "".join(open(filename, "r").readlines())
    tm_tree = reader.parse(text)
    tm_play_env = interp.get_child_env(w_.state_.def_env,
                                       w_.state_.storage)
    tm_tree.setup(ast.empty_parent(), tm_play_env, w_.state_.storage)
    tm_tree.set_outl_edges(w_, None)
    
    # Comment association.

    # Node.

    # Run
    return tm_tree.interpret(w_.state_.def_env, w_.state_.storage)
    

#*    Entry point.
def run():
    console.interact()

