#
# Author:  Michael H. Hohn (mhhohn@lbl.gov)
#
# Copyright (c) 2006, The Regents of the University of California
# 
# See legal.txt and license.txt
#

# 
# Global parameters and functions; these may be imported via 'from
# l3lang.globals import *'
#
# FIXME:  use explicit import/reference.
__all__ = ['L3_TRACE',
           'L3_TRACE_BIND',
           'debug_on_error',
           'l3_running_in_gui',
           'l3_state_w',
           'l3_set_gui',
           'shell_cmds',
           'logger',
           'Logger',
           'ShellCmds',
           ]# '',

#* imports
import sys, os, imp
from popen2 import popen3
from subprocess import Popen, PIPE


#* Control parameters.
global L3_TRACE, L3_TRACE_BIND
L3_TRACE = 0                            # Execution tracing. 
L3_TRACE_BIND = 0
L3_PICKLE_IMMEDIATELY = 1
L3_PICKLE_SIZE = True                 # Print sizes of pickled values.

debug_on_error = True                # Enter debugger on error.

#* Modes
l3_running_in_gui = False
l3_state_w = None
def l3_set_gui(w_):
    global l3_running_in_gui, l3_state_w
    l3_running_in_gui = True
    l3_state_w = w_

def l3_set_text(w_):
    global l3_running_in_gui, l3_state_w
    l3_running_in_gui = False
    l3_state_w = w_

#* Tools.
write = sys.stderr.write
def l3_trace(*argv):
    if L3_TRACE:
        for ii in argv:
            write(str(ii))
            write(": ")
        write('\n')

#* Logging.
class Logger:
    ''' Report information to the user.  In order of increasing priority:
    debug()::   
    info()::    Internals information, for development
    message():: Potentially useful information not part of regular
                program execution.
    warn()::    An unexpected occurrence that may cause problems.  The
                rest of the Program will execute unchanged.
    error()::   Serious problem in user script.  Will usually stop the
                script, but l3 will continue.
    fatal()::   Unhandled error in l3 execution.  Save state if
                possible, and terminate.

    Simple message control is done via
    set_threshold('info' | 'warn' | ...)::
                Messages at or above this level will be printed to
                stderr.

    The default is to print warnings, errors, and fatal messages to
    stderr. 

    The actual messages can be routed by replacing the member
    functions as needed.  
    '''
    pass

def __init__(self):
    self._levels = ['debug', 'info', 'message', 'warning', 'error', 'fatal']
    self._threshold = self._levels.index('warning')
Logger.__init__ = __init__

def set_threshold(self, name):
    '''
    set_threshold('debug' | 'info' | 'message' |
                  'warning' | 'error' | 'fatal')::
                Messages at or above this level will be printed to
                stderr.
    '''
    assert name in self._levels, "Invalid threshold name."
    self._threshold = self._levels.index(name)
Logger.set_threshold = set_threshold

def debug(self, strng):
    if self._threshold <= 0:
        for line in strng.split('\n'):
            sys.stderr.write("DEBUG: %s\n" % line)
Logger.debug = debug

def info(self, strng):
    if self._threshold <= 1:
        for line in strng.split('\n'):
            sys.stderr.write("Info: %s\n" % line)
Logger.info = info

def message(self, strng):
    if self._threshold <= 2:
        for line in strng.split('\n'):
            sys.stderr.write("Message: %s\n" % line)
Logger.message = message

def warn(self, strng):
    if self._threshold <= 3:
        for line in strng.split('\n'):
            sys.stderr.write("WARNING: %s\n" % line)
Logger.warn = warn

def error(self, strng):
    import l3lang.globals as glbl
    if glbl.debug_on_error:
        for line in strng.split('\n'):
            sys.stderr.write("ERROR: %s\n" % line)
        import pdb
        pdb.set_trace()                 # enter debugger.

    if self._threshold <= 4:
        for line in strng.split('\n'):
            sys.stderr.write("ERROR: %s\n" % line)
Logger.error = error

def fatal(self, strng):
    if self._threshold <= 5:
        for line in strng.split('\n'):
            sys.stderr.write("FATAL: %s\n" % line)
Logger.fatal = fatal

#* Shell command searching & caching
class ShellCmds:
    pass

def __init__(self):
    self._found = {}                    # name -> bool
ShellCmds.__init__ = __init__

def system_wrap(self, cmd, *args, **kwd):
    # Provide a standard Python interface to the .system() call.
    self.system(cmd, list(args), kwd.items())
ShellCmds.system_wrap = system_wrap

def system(self, cmd, pos_arg_l, name_val_l):
    # Wrapper for calling os.system with a specific argument syntax.
    # Here,
    #   - keywords have form '-name'
    #   - keywords with arguments have form '-name val'
    #   - the argument order is keyword, positional.
    #   
    cmd_str = " ".join([cmd] + 
                       ["-%s %s" % (name, val) for name, val in name_val_l] +
                       pos_arg_l)
    logger.info("running: %r\n" % cmd_str)
    status = os.system(cmd_str)
    if status != 0:
        raise Exception("System command `%s` failed." % cmd_str)
ShellCmds.system = system

def exists(self, name):
    # Use the system's `which` command to find `name` (unix only).
    # A full PATH search is a possibility, but would miss, e.g., the
    # user's shell functions.

    # Check cache
    if self._found.has_key(name):
        return self._found[name]
    # Is `name` an executable on the PATH?
    path = Popen(['which', name], stdout=PIPE).communicate()[0].strip()
    if path == '':
        self._found[name] = False
        return False
    else:
        self._found[name] = True
        return True
ShellCmds.exists = exists

#* The singletons
shell_cmds = ShellCmds()
logger = Logger()

#* Shortcuts
message = logger.message

#* l3 paths
l3_home = imp.find_module("l3lang")[1]  # Absolute path as string.

