"""
Pretty printer based on Wadler's pretty printer in a strict language,
http://www.st.cs.uni-sb.de/~lindig/papers/pretty/strictly-pretty.html

Translated from tony-0.9/mylib/pp.ml, taken from
http://www.st.cs.uni-sb.de/~lindig/src/ocaml-tony/tony-0.9.tar.gz

----------------------

Usage
-----------
Documents use the constructors

    DocNil
    DocCons
    DocText
    DocNest
    DocBreak
    DocGroupFlat    
    DocGroupBreak
    DocGroupFill
    DocGroupAuto

The `DocGroup*` operators provide nesting facilities; within a group,
either everything fits on one line, or is broken at every DocBreak.

The `DocNest(indent, doc)` operator determines the indentation within `doc`.
When an optional line break inside `doc` gets printed as a newline, it
is followed by `indent` spaces to indent the next line. If the line
break comes out as a space, no additional spaces are added.

The DocBreak(str) either results in insertion of `str` at the current
position, or insertion of newline + indentation.


----------------------

Author:  Michael H. Hohn (mhhohn@lbl.gov)

Copyright (c) 2006, The Regents of the University of California

See legal.txt and license.txt

"""

# debuging makes groups visible 
debug = False

# definitions 
nl = "\n"

# A type for the different kind of (user visible) groups.
# 
# gmode = {
#     "GFlat"  : 20,
#     "GBreak" : 21,
#     "GFill"  : 22,
#     "GAuto"  : 23,
# }
# This is incorporated into DocGroup* classes, below.

# Users build (complex) documents of this type.
class Doc:
    # Note that >> has higher precedence than |.

    def __or__(self, r):
        # Use "a | b" for DocCons(a,b)
        return DocCons(self, r)

    def __rshift__(self, r):
        # Use "doc >> r" for DocNest(r, doc)
        return DocNest(r, self)

class DocNil(Doc):
    pass

class DocCons(Doc):
    def __init__(self, x, y):
        # doc * doc
        # Note: construction must associate to the left: (a.b).c, not a.(b.c)
        self._x, self._y = x, y
    
def head(self):
    return self._x
DocCons.head = head

class DocText(Doc):
    def __init__(self, s):
        # string
        self._s = s
    
class DocNest(Doc):
    def __init__(self, j, x):
        # int * doc
        self._j, self._x = j, x
    
class DocBreak(Doc):
    def __init__(self, s):
        # string
        self._s = s
    
class DocGroup(Doc):
    def __init__(self, doc):
        pass
    
class DocGroupFlat(DocGroup):
    def __init__(self, x):
        # doc
        self._x = x
    
class DocGroupBreak(DocGroup):
    def __init__(self, x):
        # doc
        self._x = x
    
class DocGroupFill(DocGroup):
    def __init__(self, x):
        # doc
        self._x = x
    
class DocGroupAuto(DocGroup):
    def __init__(self, x):
        # doc
        self._x = x
    
# constructor functions for documents
def agrp(d):
    if debug:
        return DocGroupAuto(DocText("[") |  d | DocText("]"))
    else:
        return DocGroupAuto(d)

# Formatting turns a complex [doc] document into a simpler
#  [sdoc] document which 
class SDoc:
    pass

class SNil(SDoc):
    def __init__(self):
        pass

class SText(SDoc):
    def __init__(self, s, d):
        # string * sdoc
        self._s, self._d = s, d
    
class SLine(SDoc):
    def __init__(self, i, d):
        # int * sdoc
        self._i, self._d = i, d
    
# [sdocToString] formats a simple document into a string: [SLIne]
# line breaks are expanded into a newline followed by spaces 
# 
# For Python, I split the single 
#    let rec sdocToString = function
#        | SNil              -> ""
#        | SText(s,d)        -> s ^ sdocToString d
#        | SLine(i,d)        -> let prefix = String.make i ' ' 
#                               in  nl ^ prefix ^ sdocToString d
# across classes.
# 
#        | SNil              -> ""
def toString(self):
    return ""
SNil.toString = toString
# 
#        | SText(s,d)        -> s ^ sdocToString d
def toString(self):
    return self._s + self._d.toString()
SText.toString = toString
# 
#        | SLine(i,d)        -> let prefix = String.make i ' ' 
def toString(self):
    # newline + indent + string
    return nl + (" " * self._i) + self._d.toString()
SLine.toString = toString


# [sdocToFile oc doc] formats [doc] into output channel [oc] 
#
# todo.
#
# let sdocToFile oc doc = 
#     let pstr = output_string oc in
#     let rec loop = function
#         | SNil          -> () 
#         | SText(s,d)    -> pstr s; loop d
#         | SLine(i,d)    -> let prefix = String.make i ' ' 
#                            in  pstr nl;
#                                pstr prefix;
#                                loop d
#     in
#         loop doc

# [agrp]s are turned into [Flat] or [Break] groups - so there are
# only 3 different modes internally. 

# mode = {
#     "Flat"  : 30,
#     "Break" : 31,
#     "Fill"  : 32,
# }

# [fits] checks whether a documents up to the next [break] fits into w
#        characters. All kind of groups are considered flat: their breaks count
#        as spaces. This means the [break] this function looks for must not be
#        inside sub-groups 

#     let rec fits w = function
#         | _ when w < 0                   -> false
#         | []                             -> true
def fits_f(w, lis):
    if w < 0:   return False
    if len(lis) == 0: return True
    return lis[0][2].fits(w, lis)

#         | (i,m,DocNil)              :: z -> fits w z
def fits(self, w, lis):
    return fits_f(w, lis[1:None])
DocNil.fits = fits

#         | (i,m,DocCons(x,y))        :: z -> fits w ((i,m,x)::(i,m,y)::z)
def fits(self, w, lis):
    i_, m_, z_ = lis[0][0], lis[0][1], lis[1:None]
    return fits_f(w, [(i_, m_, self._x), (i_, m_, self._y)] + z_)
DocCons.fits = fits

#         | (i,m,DocNest(j,x))        :: z -> fits w ((i+j,m,x)::z)
def fits(self, w, lis):
    i_, m_, z_ = lis[0][0], lis[0][1], lis[1:None]
    return fits_f(w, [(i_ + self._j, m_, self._x)] + z_)
DocNest.fits = fits

#         | (i,m,DocText(s))          :: z -> fits (w - strlen s) z
def fits(self, w, lis):
    i_, m_, z_ = lis[0][0], lis[0][1], lis[1:None]
    return fits_f(w - len(self._s), z_)
DocText.fits = fits

#         | (i,Flat, DocBreak(s))     :: z -> fits (w - strlen s) z
#         | (i,Fill, DocBreak(_))     :: z -> true 
#         | (i,Break,DocBreak(_))     :: z -> true
def fits(self, w, lis):
    i_, m_, z_ = lis[0][0], lis[0][1], lis[1:None]
    if m_ == "Flat":
        return fits_f(w - len(self._s), z_)
    else:
        return True
DocBreak.fits = fits

#         | (i,m,DocGroup(_,x))       :: z -> fits w ((i,Flat,x)::z)
def fits(self, w, lis):
    i_, m_, z_ = lis[0][0], lis[0][1], lis[1:None]
    return fits_f(w, [(i_, "Flat", self._x)] + z_)
DocGroup.fits = fits


# [format] does the actual pretty printing. It turns a [doc] document
# into a simple [sdoc] document 
#     let rec format w k = function
#         | []                             -> SNil
def format_f(w, k, lis):
    if len(lis) == 0:
        return SNil()
    else:
        return lis[0][2].format(w, k, lis)

#         | (i,m,DocNil)              :: z -> format w k z
def format(self, w, k, lis):
    i_, m_, z_ = lis[0][0], lis[0][1], lis[1:None]    
    return format_f(w, k, z_)
DocNil.format = format
       
#         | (i,m,DocCons(x,y))        :: z -> format w k ((i,m,x)::(i,m,y)::z)
def format(self, w, k, lis):
    i_, m_, z_ = lis[0][0], lis[0][1], lis[1:None]    
    return format_f(w, k, [(i_, m_, self._x), (i_, m_, self._y)] + z_)
DocCons.format = format

#         | (i,m,DocNest(j,x))        :: z -> format w k ((i+j,m,x)::z)
def format(self, w, k, lis):
    i_, m_, z_ = lis[0][0], lis[0][1], lis[1:None]    
    return format_f(w, k, [(i_ + self._j, m_, self._x)] + z_)
DocNest.format = format

#         | (i,m,DocText(s))          :: z -> SText(s ,format w (k + strlen s) z)
def format(self, w, k, lis):
    i_, m_, z_ = lis[0][0], lis[0][1], lis[1:None]    
    return SText(self._s, format_f(w, k + len(self._s), z_))
DocText.format = format

#         | (i,Flat, DocBreak(s))     :: z -> SText(s ,format w (k + strlen s) z)
#         | (i,Fill, DocBreak(s))     :: z -> let l = strlen s in
#                                                 if   fits (w - k - l) z 
#                                                 then SText(s, format w (k+l) z)
#                                                 else SLine(i, format w  i    z)
#         | (i,Break,DocBreak(s))     :: z -> SLine(i,format w i z)
def format(self, w, k, lis):
    i_, m_, z_ = lis[0][0], lis[0][1], lis[1:None]    
    if m_ == "Flat":
        return SText(self._s, format_f(w, k + len(self._s), z_))
    elif m_ == "Fill":
        l_ = len(self._s)
        if fits_f(w - k - l_, z_):
            return SText(self._s, format_f(w, k + l_, z_))
        else:
            return SLine(i_, format_f(w, i_, z_))
    elif m_ == "Break":
        return SLine(i_, format_f(w, i_, z_))
    else:
        raise Exception("Invalid docbreak format")
DocBreak.format = format

#         | (i,m,DocGroup(GFlat ,x))  :: z -> format w k ((i,Flat ,x)::z)
def format(self, w, k, lis):
    i_, m_, z_ = lis[0][0], lis[0][1], lis[1:None]
    return format_f(w, k, [(i_, "Flat", self._x)] + z_)
DocGroupFlat.format = format    

#         | (i,m,DocGroup(GFill ,x))  :: z -> format w k ((i,Fill ,x)::z)
def format(self, w, k, lis):
    i_, m_, z_ = lis[0][0], lis[0][1], lis[1:None]
    return format_f(w, k, [(i_, "Fill", self._x)] + z_)
DocGroupFill.format = format    

#         | (i,m,DocGroup(GBreak,x))  :: z -> format w k ((i,Break,x)::z)
def format(self, w, k, lis):
    i_, m_, z_ = lis[0][0], lis[0][1], lis[1:None]
    return format_f(w, k, [(i_, "Break", self._x)] + z_)
DocGroupBreak.format = format    

#         | (i,m,DocGroup(GAuto, x))  :: z -> if fits (w-k) ((i,Flat,x)::z)
#                                             then format w k ((i,Flat ,x)::z)
#                                             else format w k ((i,Break,x)::z)
def format(self, w, k, lis):
    i_, m_, z_ = lis[0][0], lis[0][1], lis[1:None]
    if fits_f(w - k, [(i_, "Flat", self._x)] + z_):
        return format_f(w, k, [(i_, "Flat", self._x)] + z_)
    else:
        return format_f(w, k, [(i_, "Break", self._x)] + z_)
DocGroupAuto.format = format    

def toString(self, w):
    return (format_f(w, 0, [(0, "Flat", agrp(self))])).toString()
Doc.toString = toString


# todo.
# let ppToFile oc w doc =
#     sdocToFile oc (format w 0 [0,Flat,agrp(doc)])

""" Simple tests.
doc = DocText("hello,") | DocBreak(" ") | DocText("world")
print doc.toString(40)
print doc.toString(4)
# ---------------------------------
B = lambda : DocBreak(" ")
T = lambda t : DocText(t)
G = lambda x : DocGroupAuto(x)
GB = lambda x : DocGroupBreak(x)
GF = lambda x : DocGroupFill(x)
doc = G(T("begin") | B() |
         G(T("stmt1;") | B() |
           T("stmt3;"))
         | B() |
         T("end;"))
# 
print doc.toString(40)                  # no breaks
print doc.toString(20)                  # break outer group
print doc.toString(4)                   # break all
"""
