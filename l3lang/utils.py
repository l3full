#
# Author:  Michael H. Hohn (mhhohn@lbl.gov)
#
# Copyright (c) 2006, The Regents of the University of California
# 
# See legal.txt and license.txt
#

from  types import *
import exceptions, os, pickle, string, sys
from l3lang.globals import *

#
#*    misc
def print_(strng):
    print strng
    sys.stdout.flush()
    return strng

#
#*    In dict, find key corresponding to value.
def key_of(value, dict):
    'Find key corresponding to value in dict, or None.'
    for key, val in dict.iteritems():
        if val == value: return key
    return None

#
#*    dict of lists, key -> value list
class ListDict(dict):
    def __init__(self, *dict):
        # (key -> value list) dict
        if len(dict) == 1:
            self.update(dict[0])

def push(self, key, val):
    "Push val onto the end of the dict's key entry. "
    if self.has_key(key):
        self[key].append(val)
    else:
        self[key] = [val]
    return self
ListDict.push = push

def peek(self, key):
    "Return the most recently pushed value for key."
    if self.has_key(key):
        return self[key][-1]
    else:
        raise Error, "No values."
ListDict.peek = peek

def iter_items(self):
    "yield all key,value pairs."
    for k, val_l in self.items():
        for v1 in val_l:
            yield k, v1
ListDict.iter_items = iter_items

#*    base36 integer
# Following hex notation, go from 0-9, a-v
_b36alphabet = {
    0: '0',  9: '9', 18: 'i', 27: 'r',
    1: '1', 10: 'a', 19: 'j', 28: 's',
    2: '2', 11: 'b', 20: 'k', 29: 't',
    3: '3', 12: 'c', 21: 'l', 30: 'u',
    4: '4', 13: 'd', 22: 'm', 31: 'v',
    5: '5', 14: 'e', 23: 'n', 32: 'w',
    6: '6', 15: 'f', 24: 'o', 33: 'x',
    7: '7', 16: 'g', 25: 'p', 34: 'y',
    8: '8', 17: 'h', 26: 'q', 35: 'z',
    }
_b36tab = [v for v in _b36alphabet.values()]
_b36rev = dict([(v, long(k)) for k, v in _b36alphabet.items()])

def int36(val):
    def _do(vv):
        next = ( vv / 36 )
        if next:
            return _do(next) + _b36alphabet[ vv % 36 ]
        return _b36alphabet[ vv % 36 ]

    if val < 0:
        return '-' + _do(-val)
    return _do(val)



#*    indexed iteration
def iter_indexed(lst):
    for item, idx in zip(lst, xrange(0, len(lst))):
        yield item, idx



#*    shared resources / parameters mechanism
# For now, state is kept in a single dict and is 'global'.  Nesting /
# chaining may be added as necessary.
# Note:
#    This class puposely does not inherit from dict, to provide full
#    choice in behavior.

# This is an interface-based approach.  All data is added dynamically;
# functions operating on an instance S have interface (member)
# requirements, and may add to the existing interfaces.
#
# Effectively, all functions are mix-ins.

# @@ chaining state additions and tagging the links would provide useful
# debugging info.

class RepeatedBinding(exceptions.Exception):
    def __str__(self):
        return repr(self.e_args)

    def __init__(self, e_args=None):
        self.e_args = e_args

class DynamicScopeError(exceptions.Exception):
    def __str__(self):
        return repr(self.e_args)

    def __init__(self, e_args=None):
        self.e_args = e_args

class Shared:
    pass

def __init__(self, **kwds):
    self.setup(**kwds)
    self.__fluids = {}
Shared.__init__ = __init__

def setup(self, **kwds):
    self.__dict__.update(kwds)
    self._non_pickling_keys = []        # name list
Shared.setup = setup

def keys(self):
    return self.__dict__.keys()
Shared.keys = keys

def __getattr__(self, key):
    try:
        return self.__dict__[key]
    except KeyError:
        raise AttributeError("No attribute named '%s'." % key)
Shared.__getattr__ = __getattr__

def __getitem__(self, key):
    return self.__dict__[key]
Shared.__getitem__ = __getitem__

def __setitem__(self, key, val):
    return self.__setattr__(key, val)
Shared.__setitem__ = __setitem__

def __setattr__(self, key, val):
    if key in ['_non_pickling_keys']:   # Use general pattern instead?
        pass
    else:
        if self.__dict__.has_key(key):
            logger.info("Warning: Shared(): Repeated binding for " + key)
            # raise RepeatedBinding, key
    self.__dict__[key] = val
    return self
Shared.__setattr__ = __setattr__

#
#**        content handling
def update(self, other):
    ''' Add entries from `other` to self.  Overrides existing entries.'''
    self.__dict__.update(other)
    return self
Shared.update = update

def iteritems(self):
    for kv in self.__dict__.iteritems():
        yield kv
Shared.iteritems = iteritems

def pull(self, other):
    ''' Merge entries with those from 'other', recursively.
    - Values from `other` override self.
    - Keys keys matching __* are excluded
    - Shared() instances are referenced if absent, or their contents
      copied, thus preserving all existing entries in self and
      nested Shared()s.
    '''
    for key, val in other.iteritems():
        if key.__class__ == StringType:
            if key.startswith('__'):
                continue
        if isinstance(val, Shared):
            if self.__dict__.has_key(key):
                s_val = self.__dict__[key]
                assert (isinstance(s_val, Shared),
                        "Attempt to merge incompatible structures.")
                s_val.pull(val)
            else:
                self.__dict__[key] = val
        else:
            self.__dict__[key] = val
    return self
Shared.pull = pull


def update_except(self, other, except_l):
    '''
    Update entries from 'other', excluding keys found in except_l,
    and names matching __*. 
    '''
    for key, val in other.iteritems():
        if key.__class__ == StringType:
            if key.startswith('__'):
                continue
        if key in except_l:
            continue
        self.__dict__[key] = val
    return self
Shared.update_except = update_except

def add_bindings(self, dct, names):
    # Add NAMES from DCT to self.
    for key in names:
        if self.__dict__.has_key(key):
            logger.info("Warning: Shared(): Repeated binding for " + key)
        self.__dict__[key] = dct[key] # Let keyerror propagate.
    return self
Shared.add_bindings = add_bindings

def add_bindings_uniquely_except(self, other, except_l):
    # Update entries from 'other' not found in self,  excluding keys
    # found in except_l and __* names.
    for key, val in other.iteritems():
        if key.__class__ == StringType:
            if key.startswith('__'):
                continue
        if key in except_l:
            continue
        if self.__dict__.has_key(key):
            logger.info("Warning: Shared(): Repeated binding for " + key)
            ## raise RepeatedBinding, key
        self.__dict__[key] = val
    return self
Shared.add_bindings_uniquely_except = add_bindings_uniquely_except

def add_named_bindings_uniquely(self, other, name_l):
    # Update entries from 'other' with keys in name_l; raise
    # RepeatedBinding error on duplicate key.
    for key in name_l:
        if self.__dict__.has_key(key):
            raise RepeatedBinding, key
        self.__dict__[key] = other[key]
    return self
Shared.add_named_bindings_uniquely = add_named_bindings_uniquely

def name_collisions(self, other):
    kl = self.keys() + other.keys()
    kl.sort()
    collisions = []
    if len(kl) > 1:
        for ii in xrange(1, len(kl)):
            if kl[ii] == kl[ii-1]:
                collisions.append(kl[ii-1])
    self.__dict__['_collisions_'] = collisions
    return collisions
Shared.name_collisions = name_collisions

def has_key(self, key):
    return self.__dict__.has_key(key)
Shared.has_key = has_key

#
#**        persistence
def __getstate__(self):
    # from copy import copy
    dct = {}
    dct.update(self.__dict__)
    for key in self._non_pickling_keys:
        try:
            del dct[key]
        except:
            pass
    return dct
Shared.__getstate__ = __getstate__

def __setstate__(self, stuff):
    self.__dict__.update(stuff)
Shared.__setstate__ = __setstate__

def mark_not_pickleable(self, name_l):
    assert isinstance(name_l, ListType)
    self._non_pickling_keys += name_l
Shared.mark_not_pickleable = mark_not_pickleable

# Nested Env()-style functionality?
# def new_subenv():
#     pass

# def foo(a, b):
#     return locals()

'''
Usage:
    foo = Shared()
    foo.setup(a=1, b = 2, c = 'd')

    print foo.a, foo.b

    foo.name_collisions({})
    foo.name_collisions(Shared(a = 'other'))
    foo.name_collisions(Shared(not_yet = 'aha'))
'''

#
#**        convert to list
def as_list(self):
    return [ (k, as_list(v)) for k, v in self.__dict__.iteritems()]
Shared.as_list = as_list

#

#*    dynamic scoping
def with_fluids(self, function, **kwds):
    # Usage:
    #     - In the caller::
    #     
    #         def body():
    #             ...
    #         with_fluids(body,
    #                     detach_l3_tree = False,
    #                     other = "this", ...)
    #     
    #     - Somewhere down the call chain::
    #
    #         if fluid_ref(detach_l3_tree = True):
    #             ...
    #
    # Description:
    # The key/value pairs in KWDS are used to in the current dynamic
    # scope,  and FUNCTION is called without arguments.  After
    # FUNCTION returns, the values are reset.
    # 
    orig = {}
    dyn = self.__fluids

    def _restore():
        for key, val in kwds.items():
            if orig.has_key(key):
                dyn[key] = orig[key]    # restore prior value or ...
            else:
                del dyn[key]            # ... remove binding

    for key, val in kwds.items():
        if dyn.has_key(key):
            orig[key] = dyn[key]
        dyn[key] = val
    try:
        rv = function()
    except:
        _restore()
        raise
    else:
        _restore()
        return rv
Shared.with_fluids = with_fluids

def fluid_ref(self, **args):
    # If args is of the form NAME = DEFAULT (dict containing one pair):
    #     For this NAME, its value in the dynamic scope used if available,
    #     the default otherwise. 
    # 
    assert isinstance(args, dict) 
    assert len(args) == 1
    
    for name, val in args.items():
        if self.__fluids.has_key(name):
            return self.__fluids[name]
        else:
            return val
            ## raise DynamicScopeError("Unknown name: " + name)
    # @@ Message like:
    # function FOO
    # requires dynamic parameter BAR
Shared.fluid_ref = fluid_ref





#*    convert to list, generic.
def as_list(obj):
    # Generic conversion;  any type one of the tested attributes
    # will be expanded.

    if hasattr(obj, 'as_list'):
        return obj.as_list()

    elif hasattr(obj, 'iteritems'):
        return [(k1, as_list(v1))
                for k1, v1 in obj.iteritems()]

    # elif hasattr(obj, '__iter__'):      # Too general (tuples included)...
    elif isinstance(obj, ListType):
        return [as_list(val)
                for val in obj.__iter__()]

    else:
        return obj


#*    Persistence prep
#
#**        pickle customization
#***            modules
import copy_reg

def module_restore(name):
    '''
    test:
        import l3lang.utils as utils
        import l3lang.external.xmipp_wrap as xw
        utils.file_pickle(xw)
        loaded = utils.file_unpickle('/tmp/foo')
        print loaded
    '''
    # Note: this puts nothing into ll if the module is imported elsewhere:
    ## exec('import %s' % name, ll)
    ll = locals()
    exec('import %s as _imp_foo' % name, ll)
    return ll.get('_imp_foo')

# __reduce__() method analog
def module_pickle(mm):
    name = mm.__name__
    return module_restore, (name,)

copy_reg.pickle(type(copy_reg), module_pickle, module_restore)

#
#***            members
def member_restore_first(name):
    type, id, member = name.split('--')
    if type == 'instance-member':
        return name                     # Done later.
    elif type ==  'class-member':
        omit = '__main__'
        if id.startswith(omit):
            id = id[len(omit) + 1:]
        return eval('%s.%s' % (id, member))
    else:
        raise Exception, "Invalid member unpickling."

def member_restore_full(name, storage):
    # Instance restoration from instance-member--*--*
    type, id, member = name.split('--')
    if type == 'instance-member':
        # e.g., storage.load(30001).import_names
        return eval('storage.load(%s).%s' % (id, member))
    else:
        raise Exception, "Invalid member unpickling."

def member_pickle(memb):
    if memb.im_self is None:
        # class
        name = 'class-member--%s.%s--%s' % (memb.im_class.__module__,
                                            memb.im_class.__name__,
                                            memb.__name__)
    else:
        # instance
        if hasattr(memb.im_self, "_id"):
            name = 'instance-member--%d--%s' % (memb.im_self._id,
                                                memb.__name__)
        else:
            
            logger.warn("Ignoring non-pickleable member: " + str(memb))
            name = 'instance-member--None--%s' % (memb.__name__)
    return member_restore_first, (name,)

copy_reg.pickle(MethodType, member_pickle, member_restore_first)

#
#***            instance restoration, step 2
def restore_non_persistent(state):
    # Check attributes.
    for nd in state.tree.top_down():
        val = state.storage.get_attribute(nd._id, "interp_result")
        if val is None:
            continue
        if isinstance(val, StringType) and \
               val.startswith('instance-member'):
            state.storage.set_attributes(
                nd._id,
                "interp_result", member_restore_full(val, state.storage))

    # Check Env() values.
    for key, val, env in state.def_env.all_bindings_recursive():
        if isinstance(val, StringType) and \
               val.startswith('instance-member'):
            env.bind(key, member_restore_full(val, state.storage))

## utils.restore_non_persistent = restore_non_persistent

#
#**        pickle funcs.
def file_pickle(obj, filename = "/tmp/foo"):
    f = open(filename, 'w')
    pickle.dump(obj, f, protocol=2)
    f.close()
    logger.info("file size: %d" % os.path.getsize(filename))
    return filename

def file_cPickle(obj, filename = "/tmp/foo"):
    import cPickle as pickle
    f = open(filename, 'w')
    pickle.dump(obj, f, protocol=2)
    f.close()
    logger.info("file size: %d" % os.path.getsize(filename))
    return filename

def file_unpickle(filename):
    f = open(filename, 'r')
    val = pickle.load(f)
    f.close()
    return val

def test_pickle_full(storage):
    file_pickle(storage)
    really =  file_unpickle()
    # pprint(storage._store)
    # pprint(really._store)

#*    callchain
def callchain():
    import sys, traceback
    tbl = traceback.extract_stack()
    for (filen, ln, funn, text) in tbl[:-1]:
        sys.stderr.write("%s:%d: In %s: %s\n" % (filen, ln, funn, text))


#*    docstring work

