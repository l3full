.\"     Title: l3lang
.\"    Author: 
.\" Generator: DocBook XSL Stylesheets v1.72.0 <http://docbook.sf.net/>
.\"      Date: 01/16/2008
.\"    Manual: 
.\"    Source: 
.\"
.TH "L3LANG" "n" "01/16/2008" "" ""
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.SH "NAME"
l3lang \- the l3 data handling and programming language
.SH "SYNOPSIS"
.SH "LANGUAGE OVERVIEW"
The l3 language is an interpreted, lexically scoped language designed to automate much of the parameter and data handling of the type often encountered in experimental (scientific) computing. To accomplish this, l3 programs and all their data are persistent, and can be inspected at any time; further, the language maps one\-to\-one onto its graphical interface to make data traversal and inspection practical. See l3gui(1) for the interface description.
.sp
The l3 language is implemented as an interpreter written in Python; all Python modules can be directly imported and used from l3. Thus, algorithms without interesting intermediate data can be written in Python and called from l3 to provide a data interface for applications, and existing Python code can be used directly.
.sp
The l3 syntax follows Python's when possible, with only a few syntactic extensions for parameter, module, and documentation handling. As result, most l3 scripts are valid Python scripts and can be moved to Python if desired.
.sp
Only a subset of Python is repeated in l3; in particular, there are no classes.
.sp
The semantics of l3 are closer to a purely functional language. Everything in l3 is an expression, including computed values. l3 is designed as a single\-assignment language to permit reliable data tracking; overwriting of existing names is \fBstrongly\fR discouraged (but allowed for compatibility with existing Python scripts).
.sp
l3 introduces several new language features to support direct access to expressions and their values from a random\-access interface (see l3gui(1)); they are
.sp
.RS 4
\h'-04'\(bu\h'+03'Every expression is turned into an object by assignment of a permanent, unique id. Both the expression and the value(s) computed by the expression can be accessed via this id (using the gui, through the script's text).
.RE
.RS 4
\h'-04'\(bu\h'+03'The data flow graph of l3 scripts is a stacked dag, and can be generated from a script. In this view, named data are emphasized, and data interdependencies shown. Loops cause no \(lqexplosion\(rq of the data flow graph.
.RE
.RS 4
\h'-04'\(bu\h'+03'l3 scripts are lexically scoped, allowing for arbitrarily nested (static) namespaces and avoiding name collisions. Also, every function call introduces a new (dynamic) scope, so multiple calls to the same function do not produce naming conflicts, even when file names are used.
.RE
.RS 4
\h'-04'\(bu\h'+03'All scripts and their computed data are persistent. l3 is most useful when most data is should be kept. By keeping
\fIall\fR
data, file I/O need not be part of a script, keeping it simpler and cleaner. Exporting data to other programs
\fIafter\fR
script execution is simple and can be done interactively or by adding I/O commands where desired.
.RE
.RS 4
\h'-04'\(bu\h'+03'Evaluation of scripts is incremental (need\-based). An expression is only executed if it is out\-of\-date or newly inserted. This allows l3 scripts to
\fIevolve\fR; they can be added to without re\-running existing code, and references to all previouly computed data are available.
.RE
.RS 4
\h'-04'\(bu\h'+03'Values and scripts are the same. An l3 value is a valid l3 script. Therefore, computed values from prior scripts can be directly inserted into new scripts.
.RE
.SH "SYNTAX"
grammar.
.sp
.SH "ENVIRONMENTS"
Names are bound in environments. Environments are lexically nested and dynamically formed on function entry. Scoping is lexical.
.sp
.SH "SEMANTICS"
from paper.
.sp
.PP
\fBprinted representation of values\fR. Most application\-specific values are too large to print or not recognized by L3 at all. These are represented as text, using their Python |str()| form (which in turn is provided by the library implementing the value). They are treated as terminal values by l3.
.PP
\fBinline\fR. Unlike other expressions in L3, |inline| is always evaluated, so its contents must be constant \(em modules and functions usually are.
.SH "PYTHON COMPATIBILITY"
The following Python(1) constructs are not available in l3. The workarounds are straightforward and are valid in Python and l3.
.sp
.PP
\fBUngrouped tuple assignment\fR. Instead of
.sp
.RS 4
.nf
    psi, sx, sy, scale = compose_()
.fi
.RE
use
.sp
.sp
.RS 4
.nf
   (psi, sx, sy, scale) = compose_()
.fi
.RE
.PP
\fBlist assignments\fR. Instead of
.sp
.RS 4
.nf
    [psin, sxn, syn, mn] = combine_params2( ...)
.fi
.RE
use
.sp
.sp
.RS 4
.nf
    (psin, sxn, syn, mn) = combine_params2( ...)
.fi
.RE
.PP
\fBdictionary syntax\fR. The
{ key : val }
syntax is not available. Instead of
.sp
.RS 4
.nf
    {"negative":0, "mask":mask}
.fi
.RE
use
.sp
.sp
.RS 4
.nf
    dict(negative = 0, mask = mask)
.fi
.RE
.PP
\fBNo inline blocks\fR. The shortcuts
if 1: do_this
and
def f(a,b): return a+b
are not available. Use the long forms
.sp
.RS 4
.nf
    if 1:
        do_this
.fi
.RE
and
.sp
.sp
.RS 4
.nf
    def f(a,b):
        return a+b
.fi
.RE
instead.
.sp
.PP
\fBValues returned to l3 must pickle properly.\fR. There are several Python constructs that cannot be pickled and must not be used in l3 code.
Most generators will not pickle. In particular avoid xrange and use range instead.
.sp
Avoid using from foo import * in inline code. Importing this way is often a problem for pickling. For example, using
.sp
.sp
.RS 4
.nf
    from numpy import *
.fi
.RE
causes the error
.sp
.sp
.RS 4
.nf
    pickle.PicklingError: Can't pickle <type 'frame'>: it's not
    found as __builtin__.frame
.fi
.RE
and the session state will no longer save.
.sp
.SH "ERRORS"
Errors encountered during batch\-mode execution (\-b) can be propagated in the appropriate ways using the \-\-error flags.
.sp
.SH "WARNINGS"
none yet.
.sp
.SH "EXAMPLES"
See l3gui(1).
.sp
.SH "ENVIRONMENT VARIABLES"
L3HOME
.sp
.SH "SEE ALSO"
l3(1), l3gui(1)
.sp
.SH "BUGS"
None known.
.sp
.SH "AUTHOR"
Michael Hohn, mhhohn@users.sf.net
.sp
.SH "COPYING"
Copyright \(co 2004\-8 Lawrence Berkeley National Laboratory. l3 is released under the BSD license. See license.txt for details.
.sp
