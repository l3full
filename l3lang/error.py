#
# Author:  Michael H. Hohn (mhhohn@lbl.gov)
#
# Copyright (c) 2006, The Regents of the University of California
# 
# See legal.txt and license.txt
#

# 
# l3 error handling.
# 
from l3lang.ast import                                                  \
     aList,                                                             \
     aNone,                                                             \
     Function,                                                          \
     Macro,                                                             \
     Member,                                                            \
     Call,                                                              \
     Float,                                                             \
     If,                                                                \
     Immediate,                                                         \
     Inline,                                                            \
     Int,                                                               \
     Labeled,                                                           \
     Let,                                                               \
     List,                                                              \
     Map,                                                               \
     Marker,                                                            \
     MarkerTyped,                                                       \
     Nested,                                                            \
     Program,                                                           \
     Return,                                                            \
     Set,                                                               \
     String,                                                            \
     Symbol,                                                            \
     Tuple


def show_error_info():
    lbeg = begin[1]
    lend = end[1]            # character positions
    print "Syntax error:%d:%d-%d at '%s':" % \
          (line, lbeg, lend, value)
    print line_text
    print "%s%s" % (" " * lbeg, "^" * (lend-lbeg))

def context(self):
    self._source_file
    lines = self._source_string.split()
    

astType.context = context
