#!/usr/bin/env python
#
# $Id: //projects/empy/em.py#180 $ $Date: 2008/03/20 $

"""
A system for processing Python as markup embedded in text.
"""


__program__ = 'empy'
__version__ = '4.0x3'
__url__ = 'http://www.alcyone.com/software/empy/'
__author__ = 'Erik Max Francis <max@alcyone.com>'
__copyright__ = 'Copyright (C) 2002-2008 Erik Max Francis'
__license__ = 'LGPL'


import copy
import getopt
import os
import re
import sys
import types

import codecs
try:
    import unicodedata
except ImportError:
    unicodedata = None # Jython doesn't have unicodedata

# Do the equivalent of: import cStringIO as StringIO.
try:
    import cStringIO
    StringIO = cStringIO
    del cStringIO
except ImportError:
    import StringIO

# For backward compatibility, we can't assume these are defined.
try:
    False, True
except NameError:
    False, True = 0, 1

# Environment variable names.
OPTIONS_ENV = 'EMPY_OPTIONS'
PREFIX_ENV = 'EMPY_PREFIX'
PSEUDO_ENV = 'EMPY_PSEUDO'
FLATTEN_ENV = 'EMPY_FLATTEN'
RAW_ENV = 'EMPY_RAW_ERRORS'
INTERACTIVE_ENV = 'EMPY_INTERACTIVE'
DELETE_ON_ERROR_ENV = 'EMPY_DELETE_ON_ERROR'
NO_OVERRIDE_ENV = 'EMPY_NO_OVERRIDE'
INPUT_ENCODING_ENV = 'EMPY_UNICODE_INPUT_ENCODING'
OUTPUT_ENCODING_ENV = 'EMPY_UNICODE_OUTPUT_ENCODING'
INPUT_ERRORS_ENV = 'EMPY_UNICODE_INPUT_ERRORS'
OUTPUT_ERRORS_ENV = 'EMPY_UNICODE_OUTPUT_ERRORS'

# Usage info.
OPTION_INFO = [
("-V --version", "Print version and exit"),
("-h --help", "Print usage and exit"),
("-H --extended-help", "Print extended usage and exit"),
("-k --suppress-errors", "Do not exit on errors; go interactive"),
("-p --prefix=<char>", "Change prefix to something other than @"),
("   --no-prefix", "Do not do any markup processing at all"),
("-q --no-output", "Suppress all output"),
("-m --module=<name>", "Change the internal pseudomodule name"),
("-f --flatten", "Flatten the members of pseudmodule to start"),
("-r --raw-errors", "Show raw Python errors"),
("-i --interactive", "Go into interactive mode after processing"),
("-n --no-override-stdout", "Do not override sys.stdout with proxy"),
("-o --output=<filename>", "Specify file for output as write"),
("-a --append=<filename>", "Specify file for output as append"),
("   --output-binary=<filename>", "Specify file for binary output as write"),
("   --append-binary=<filename>", "Specify file for binary output as append"),
("-d --delete-on-error", "Delete output file (-o, -a) on error"),
("   --binary", "Treat the file as a binary"),
("   --chunk-size=<chunk>", "Use this chunk size for reading binaries"),
("-P --preprocess=<filename>", "Interpret EmPy file before main processing"),
("-Q --postprocess=<filename>", "Interpret EmPy file after main processing"),
("-I --import=<modules>", "Import Python modules before processing"),
("-D --define=<definition>", "Execute Python assignment statement"),
("-S --define-string=<string>", "Assign string variable directly"),
("-E --execute=<statement>", "Execute Python statement before processing"),
("-F --execute-file=<filename>", "Execute Python file before processing"),
("   --pause-at-end", "Prompt at the ending of processing"),
("   --relative-path", "Add path of EmPy script to sys.path"),
("   --no-callback-error", "Custom markup without callback is error"),
("   --no-bangpath-processing", "Suppress bangpaths as comments"),
("-u --unicode", "Enable Unicode subsystem"),
("   --unicode-encoding=<e>", "Set both input and output encodings"),
("   --unicode-input-encoding=<e>", "Set input encoding"),
("   --unicode-output-encoding=<e>", "Set output encoding"),
("   --unicode-errors=<err>", "Set both input and output error handler"),
("   --unicode-input-errors=<err>", "Set input error handler"),
("   --unicode-output-errors=<err>", "Set output error handler"),
]

BANNER_TEXT = """\
Usage: %s [options] [<filename, or '-' for stdin> [<argument>...]]
Welcome to EmPy version %s."""

USAGE_TEXT = """\
Notes: Whitespace immediately inside parentheses of @(...) are
ignored.  Whitespace immediately inside braces of @{...} are ignored,
unless ... spans multiple lines.  Use @{ ... }@ to suppress newline
following expansion.  Simple expressions ignore trailing dots; `@x.'
means `@(x).'.  A #! at the start of a file is treated as a @#
comment."""

MARKUP_INFO = [
("@# ... NL", "Line comment; remove everything up to newline"),
("@* ... *", "Inline comment; remove everything in between"),
("@? NAME NL", "Set the current context name"),
("@! INTEGER NL", "Set the current context line number"),
("@ WS", "Remove following whitespace character"),
("@\\ ESCAPE_CODE", "A C-style escape sequence"),
("@@", "Literal @; @ is escaped (duplicated prefix)"),
("@), @], @>, @}", "Literal close parenthese, brackets, brace"),
("@ STRING_LITERAL", "Replace with string literal contents"),
("@( EXPRESSION )", "Evaluate expression and substitute with str"),
("@( TEST [? THEN [! ELSE]] )", "If test, evaluate then, otherwise else"),
("@( TRY $ CATCH )", "Expand try expression, or catch if it raises"),
("@ SIMPLE_EXPRESSION", "Evaluate simple expression and substitute;\n"
                        "e.g., @x, @x.y, @f(a, b), @l[i], etc."),
("@` EXPRESSION `", "Evaluate expression and substitute with repr"),
("@: EXPRESSION : [DUMMY] :", "Evaluates to @:...:expansion:"),
("@{ STATEMENTS }", "Statements are executed for side effects"),
("@[ CONTROL ]", "Control markups: if E; elif E; for N in E;\n"
                 "while E; try; except E, N; finally; continue;\n"
                 "break; end if; end for; end while; end try"),
("@%[!] KEY WS VALUE NL", "Significator"),
("@%%[!] KEY WS VALUE %% NL", "Multiline significator form"),
("@& NAME : ARGUMENTS NL", "Declarator"),
("@&& NAME : ARGUMENTS && NL", "Multiline declarator form"),
("@< CONTENTS >", "Custom markup; meaning provided by user"),
]

ESCAPE_INFO = [
("@\\0", "NUL, null"),
("@\\a", "BEL, bell"),
("@\\b", "BS, backspace"),
("@\\dDDD", "three-digit decimal code DDD"),
("@\\e", "ESC, escape"),
("@\\f", "FF, form feed"),
("@\\h", "DEL, delete"),
("@\\n", "LF, linefeed, newline"),
("@\\N{NAME}", "Unicode character named NAME"),
("@\\oOOO", "three-digit octal code OOO"),
("@\\qQQQQ", "four-digit quaternary code QQQQ"),
("@\\r", "CR, carriage return"),
("@\\s", "SP, space"),
("@\\t", "HT, horizontal tab"),
("@\\uHHHH", "16-bit hexadecimal Unicode HHHH"),
("@\\UHHHHHHHH", "32-bit hexadecimal Unicode HHHHHHHH"),
("@\\v", "VT, vertical tab"),
("@\\xHH", "two-digit hexadecimal code HH"),
("@\\z", "EOT, end of transmission"),
("@\\( ... and )[]<>\'\"\\", "Literal (, ), etc."),
]

PSEUDOMODULE_INFO = [
("VERSION", "String representing EmPy version"),
("argv", "The EmPy script name and command line arguments"),
("args", "The command line arguments only"),
("identify()", "Identify top context as name, line"),
("setContextName(name)", "Set the name of the current context"),
("setContextLine(line)", "Set the line number of the current context"),
("atExit(callable)", "Invoke no-argument function at shutdown"),
("getGlobals()", "Retrieve this interpreter's globals"),
("setGlobals(dict)", "Set this interpreter's globals"),
("updateGlobals(dict)", "Merge dictionary into interpreter's globals"),
("clearGlobals()", "Start globals over anew"),
("saveGlobals([deep])", "Save a copy of the globals"),
("restoreGlobals([pop])", "Restore the most recently saved globals"),
("defined(name, [loc])", "Find if the name is defined"),
("evaluate(expression, [loc])", "Evaluate the expression"),
("serialize(expression, [loc])", "Evaluate and serialize the expression"),
("execute(statements, [loc])", "Execute the statements"),
("single(source, [loc])", "Execute the 'single' object"),
("atomic(name, value, [loc])", "Perform an atomic assignment"),
("assign(name, value, [loc])", "Perform an arbitrary assignment"),
("significate(key, [value])", "Significate the given key, value pair"),
("include(file, [loc])", "Include filename or file-like object"),
("expand(string, [loc])", "Explicitly expand string and return"),
("string(data, [name], [loc])", "Process string-like object"),
("quote(string)", "Quote prefixes in provided string and return"),
("flatten([keys])", "Flatten module contents into globals namespace"),
("getPrefix()", "Get current prefix"),
("setPrefix(char)", "Set new prefix"),
("stopDiverting()", "Stop diverting; data sent directly to output"),
("createDiversion(name)", "Create a diversion but do not divert to it"),
("retrieveDiversion(name)", "Retrieve the actual named diversion object"),
("startDiversion(name)", "Start diverting to given diversion"),
("playDiversion(name)", "Recall diversion and then eliminate it"),
("replayDiversion(name)", "Recall diversion but retain it"),
("purgeDiversion(name)", "Erase diversion"),
("playAllDiversions()", "Stop diverting and play all diversions in order"),
("replayAllDiversions()", "Stop diverting and replay all diversions"),
("purgeAllDiversions()", "Stop diverting and purge all diversions"),
("getFilter()", "Get current filter"),
("resetFilter()", "Reset filter; no filtering"),
("setFilter(filter)", "Install new filter"),
("addFilter(filter)", "Add a single filter to end of current chain"),
("setFilterChain(fitlers)", "Install a filter chain"),
("areHooksEnabled()", "Return whether or not hooks are enabled"),
("enableHooks()", "Enable hooks (default)"),
("disableHooks()", "Disable hook invocation"),
("getHooks()", "Get all the hooks"),
("clearHooks()", "Clear all hooks"),
("addHook(hook, [i])", "Register the hook (optionally insert)"),
("removeHook(hook)", "Remove an already-registered hook from name"),
("invokeHook(name_, ...)", "Manually invoke hook"),
("getCallback()", "Get interpreter callback"),
("registerCallback(callback)", "Register callback with interpreter"),
("deregisterCallback()", "Deregister callback from interpreter"),
("invokeCallback(contents)", "Invoke the callback directly"),
("Interpreter", "The interpreter class"),
("Filter", "The base class for custom filters"),
("Hook", "The base class for hooks"),
("VerboseHook", "The base class for the verbose hook (-v)"),
]

ENVIRONMENT_INFO = [
(OPTIONS_ENV, "Specified options will be included"),
(PREFIX_ENV, "Specify the default prefix: -p <value>"),
(PSEUDO_ENV, "Specify name of pseudomodule: -m <value>"),
(FLATTEN_ENV, "Flatten empy pseudomodule if defined: -f"),
(RAW_ENV, "Show raw errors if defined: -r"),
(INTERACTIVE_ENV, "Enter interactive mode if defined: -i"),
(DELETE_ON_ERROR_ENV, "Delete output file on error: -d"),
(NO_OVERRIDE_ENV, "Do not override sys.stdout if defined: -n"),
(INPUT_ENCODING_ENV, "Unicode input encoding"),
(OUTPUT_ENCODING_ENV, "Unicode output encoding"),
(INPUT_ERRORS_ENV, "Unicode input error handler"),
(OUTPUT_ERRORS_ENV, "Unicode output error handler"),
]


class Error(Exception):
    """The base class for all EmPy errors."""
    pass

class ConfigurationError(Error):
    """An error associated with a configuration."""
    pass

class ConsistencyError(Error):
    """An expected implementation constrant was violated."""
    pass

class DiversionError(Error):
    """An error related to diversions."""
    pass

class StackUnderflowError(Error):
    """A stack underflow."""
    pass

class FlowError(Error):
    """An exception related to control flow."""
    pass

class ContinueFlow(FlowError):
    """A continue control flow."""
    pass

class BreakFlow(FlowError):
    """A break control flow."""
    pass

class CustomError(Error):
    """An error concerning custom markups occurred."""
    pass

class ParseError(Error):
    """A parse error occurred."""
    pass

class TransientParseError(ParseError):
    """A parse error occurred which may be resolved by feeding more data.
    Such an error reaching the toplevel is an unexpected EOF error."""
    pass


class MetaError(Exception):

    """A wrapper around a real Python exception for including a copy of
    the context."""
    
    def __init__(self, contexts, exc):
        Exception.__init__(self, exc)
        self.contexts = contexts
        self.exc = exc

    def __str__(self):
        backtrace = map(lambda x: str(x), self.contexts)
        return "%s: %s (%s)" % (self.exc.__class__, self.exc, 
                                (', '.join(backtrace)))


class Configuration:

    """The configuration encapsulates all the defaults and standard
    behavior of an interpreter.  When created, an interpreter is
    assigned a configuration; multiple configurations can be shared
    between different interpreters.  To override the defaults of an
    interpreter, create a Configuration instance and then modify its
    attributes."""

    defaultPrefix = '@'
    defaultPseudomoduleName = 'empy'

    bangpath = '#!'
    unknownScriptName = '?'
    defaultChunkSize = 8192
    successCode = 0
    failureCode = 1
    significatorPrefix = significatorSuffix = '__'

    isProxyInstalled = False

    def __init__(self):
        # Defaults.
        self.prefix = self.environment(PREFIX_ENV, self.defaultPrefix)
        self.pseudomoduleName = self.environment(PSEUDO_ENV, 
                                                 self.defaultPseudomoduleName)
        self.processBangpaths = True
        self.rawErrors = self.hasEnvironment(RAW_ENV)
        self.exitOnError = True
        self.doInteractive = self.hasEnvironment(INTERACTIVE_ENV)
        self.deleteOnError = self.hasEnvironment(DELETE_ON_ERROR_ENV)
        self.doFlatten = self.hasEnvironment(FLATTEN_ENV)
        self.overrideProxy = not self.hasEnvironment(NO_OVERRIDE_ENV)
        self.noCallbackIsError = True
        self.pauseAtEnd = False
        self.defaultEncoding = self.getDefaultEncoding()
        self.defaultErrors = self.getDefaultErrors()
        self.useUnicode = False
        self.contextLineIncrement = 1
        self.unicodeInputEncoding = self.environment(INPUT_ENCODING_ENV, 
                                                     self.defaultEncoding)
        self.unicodeOutputEncoding = self.environment(OUTPUT_ENCODING_ENV, 
                                                      self.defaultEncoding)
        self.unicodeInputErrors = self.environment(INPUT_ERRORS_ENV, 
                                                   self.defaultErrors)
        self.unicodeOutputErrors = self.environment(OUTPUT_ERRORS_ENV, 
                                                    self.defaultErrors)

    def shutdown(self):
        """Shutdown the configuration system."""
        self.pauseIfRequired()

    def requireUnicode(self):
        """require Unicode for this configuration."""
        self.useUnicode = True

    def hasEnvironment(self, name):
        """Is the current environment defined in the environment?"""
        if os.environ.has_key(name):
            return True
        else:
            return False

    def environment(self, name, default=None):
        """Get data from the current environment."""
        try:
            return os.environ[name]
        except KeyError:
            return default
        if os.environ.has_key(name):
            # Do the True/False test by value for future compatibility.
            if default == False or default == True:
                return True
            else:
                return os.environ[name]
        else:
            return default

    def open(self, name, mode=None):
        """Open a new file, depending on whether Unicode is required or not."""
        if self.useUnicode:
            if mode is None:
                mode = 'rb'
            if mode.find('w') >= 0 or mode.find('a') >= 0:
                encoding = self.unicodeOutputEncoding
                errors = self.unicodeOutputErrors
            else:
                encoding = self.unicodeInputEncoding
                errors = self.unicodeInputErrors
            return codecs.open(name, mode, encoding, errors)
        else:
            if mode is None:
                mode = 'r'
            return open(name, mode)

    def getDefaultEncoding(self, default='unknown'):
        """What is the default encoding?"""
        try:
            return sys.getdefaultencoding()
        except AttributeError:
            return default

    def getDefaultErrors(self):
        return 'strict'

    def unwantedGlobalsKeys(self):
        """When unfixing globals, these are the keys that should be removed."""
        return (self.pseudomoduleName, '__builtins__')

    def isStringType(self, data):
        """Is this is a string (normal or Unicode) type?"""
        return (type(data) is types.StringType or 
                type(data) is types.UnicodeType)

    def toStringType(self, data):
        """Convert this object to a string type as appropriate."""
        if self.useUnicode:
            return unicode(data)
        else:
            return str(data)

    def installProxy(self):
        """Install a proxy if necessary."""
        if not self.overrideProxy:
            return
        # Unfortunately, there's no surefire way to make sure that installing
        # a sys.stdout proxy is idempotent, what with different interpreters
        # running from different modules.  The best we can do here is to try
        # manipulating the proxy's test function ...
        try:
            sys.stdout._testProxy()
        except AttributeError:
            # ... if the current stdout object doesn't have one, then check
            # to see if we think _this_ particularly Interpreter class has
            # installed it before ...
            if Configuration.isProxyInstalled:
                # ... and if so, we have a proxy problem.
                raise ConsistencyError, "interpreter stdout proxy lost"
            else:
                # Otherwise, install the proxy and set the flag.
                sys.stdout = ProxyFile(sys.stdout)
                Configuration.isProxyInstalled = True

    def pauseIfRequired(self):
        """Do any finalization that needs to be done."""
        if self.pauseAtEnd:
            try:
                raw_input()
            except EOFError:
                pass


class Stack:
    
    """A simple stack that behaves as a sequence (with 0 being the top
    of the stack, not the bottom)."""

    def __init__(self, seq=None):
        if seq is None:
            seq = []
        self.data = seq

    def top(self):
        """Access the top element on the stack."""
        try:
            return self.data[-1]
        except IndexError:
            raise StackUnderflowError, "stack is empty for top"
        
    def pop(self):
        """Pop the top element off the stack and return it."""
        try:
            return self.data.pop()
        except IndexError:
            raise StackUnderflowError, "stack is empty for pop"
        
    def push(self, object):
        """Push an element onto the top of the stack."""
        self.data.append(object)

    def filter(self, function):
        """Filter the elements of the stack through the function."""
        self.data = filter(function, self.data)

    def purge(self):
        """Purge the stack."""
        self.data = []

    def clone(self):
        """Create a duplicate of this stack."""
        return self.__class__(self.data[:])

    def __nonzero__(self): return len(self.data) != 0
    def __len__(self): return len(self.data)
    def __getitem__(self, index): return self.data[-(index + 1)]

    def __repr__(self):
        return '<%s instance at 0x%x [%s]>' % \
               (self.__class__, id(self), 
                ', '.join(map(repr, self.data)))


class Diversion:

    """The representation of an active diversion.  Diversions act as
    (writable) file objects, and then can be recalled either as pure
    strings or (readable) file objects."""

    def __init__(self):
        self.file = StringIO.StringIO()

    # These methods define the writable file-like interface for the
    # diversion.

    def write(self, data):
        self.file.write(data)

    def writelines(self, lines):
        for line in lines:
            self.write(line)

    def flush(self):
        self.file.flush()

    def close(self):
        self.file.close()

    # These methods are specific to diversions.

    def preferFile(self):
        """Would this particular diversion prefer to be treated as a
        file (true) or a string (false)?  This allows future
        optimization of diversions into streams if they get overly
        large."""
        return False

    def asString(self):
        """Return the diversion as a string."""
        return self.file.getvalue()

    def asFile(self):
        """Return the diversion as a file."""
        return StringIO.StringIO(self.file.getvalue())

    def spool(self, output, chunkSize=Configuration.defaultChunkSize):
        """Spool the diversion to the given output file."""
        if self.preferFile():
            # Either write it a chunk at a time ...
            input = self.asFile()
            while True:
                chunk = input.read(chunkSize)
                if not chunk:
                    break
                output.write(chunk)
        else:
            # ... or write it all at once.
            output.write(self.asString())


class Stream:
    
    """A wrapper around an (output) file object which supports
    diversions and filtering."""
    
    def __init__(self, file):
        self.file = file
        self.currentDiversion = None
        self.diversions = {}
        self.filter = file
        self.done = False

    def write(self, data):
        if self.currentDiversion is None:
            self.filter.write(data)
        else:
            self.diversions[self.currentDiversion].write(data)
    
    def writelines(self, lines):
        for line in lines:
            self.write(line)

    def flush(self):
        self.filter.flush()

    def close(self):
        if not self.done:
            self.undivertAll(True)
            self.filter.close()
            self.done = True

    def last(self):
        """Find the last filter in the current filter chain, or None if
        there are no filters installed."""
        if self.filter is None:
            return None
        thisFilter, lastFilter = self.filter, None
        while thisFilter is not None and thisFilter is not self.file:
            lastFilter = thisFilter
            thisFilter = thisFilter.next()
        return lastFilter

    def install(self, filters=None):
        """Install a sequence of filters.  None or an empty sequence means
        no filter."""
        # Before starting, execute a flush.
        self.filter.flush()
        if filters is None:
            filters = []
        if len(filters) == 0:
            # An empty sequence means no filter.
            self.filter = self.file
        elif len(filters) == 1:
            # If there's only one filter, assume that it's alone or it's
            # part of a chain that has already been manually chained;
            # just find the end.
            filter = filters[0]
            lastFilter = filter.last()
            lastFilter.attach(self.file)
            self.filter = filter
        else:
            # If there's more than one filter provided, chain them
            # together.
            lastFilter = None
            for filter in filters:
                if lastFilter is not None:
                    lastFilter.attach(filter)
                lastFilter = filter
            lastFilter.attach(self.file)
            self.filter = filters[0]

    def attach(self, filter):
        """Attached a solitary filter (no sequences allowed here) at the
        end of the current filter chain."""
        self.filter.flush()
        lastFilter = self.last()
        if lastFilter is None:
            # Just install it from scratch if there is no active filter.
            self.install([filter])
        else:
            # Attach the last filter to this one, and this one to the file.
            lastFilter.attach(filter)
            filter.attach(self.file)

    def revert(self):
        """Reset any current diversions."""
        self.currentDiversion = None

    def create(self, name):
        """Create a diversion if one does not already exist, but do not
        divert to it yet."""
        if name is None:
            raise DiversionError, "diversion name must be non-None"
        if not self.diversions.has_key(name):
            self.diversions[name] = Diversion()

    def retrieve(self, name):
        """Retrieve the given diversion."""
        if name is None:
            raise DiversionError, "diversion name must be non-None"
        if self.diversions.has_key(name):
            return self.diversions[name]
        else:
            raise DiversionError, "nonexistent diversion: %s" % name

    def divert(self, name):
        """Start diverting."""
        if name is None:
            raise DiversionError, "diversion name must be non-None"
        self.create(name)
        self.currentDiversion = name

    def undivert(self, name, purgeAfterwards=False):
        """Undivert a particular diversion."""
        if name is None:
            raise DiversionError, "diversion name must be non-None"
        if self.diversions.has_key(name):
            diversion = self.diversions[name]
            diversion.spool(self.filter)
            if purgeAfterwards:
                self.purge(name)
        else:
            raise DiversionError, "nonexistent diversion: %s" % name

    def purge(self, name):
        """Purge the specified diversion."""
        if name is None:
            raise DiversionError, "diversion name must be non-None"
        if self.diversions.has_key(name):
            del self.diversions[name]
            if self.currentDiversion == name:
                self.currentDiversion = None

    def undivertAll(self, purgeAfterwards=True):
        """Undivert all pending diversions."""
        if self.diversions:
            self.revert() # revert before undiverting!
            names = self.diversions.keys()
            names.sort()
            for name in names:
                self.undivert(name)
                if purgeAfterwards:
                    self.purge(name)
            
    def purgeAll(self):
        """Eliminate all existing diversions."""
        if self.diversions:
            self.diversions = {}
        self.currentDiversion = None


class NullFile:

    """A simple class that supports all the file-like object methods
    but simply does nothing at all."""

    def __init__(self): pass
    def write(self, data): pass
    def writelines(self, lines): pass
    def flush(self): pass
    def close(self): pass


class UncloseableFile:

    """A simple class which wraps around a delegate file-like object
    and lets everything through except close calls."""

    def __init__(self, delegate):
        self.delegate = delegate

    def write(self, data):
        self.delegate.write(data)

    def writelines(self, lines):
        self.delegate.writelines(data)

    def flush(self):
        self.delegate.flush()

    def close(self):
        """Eat this one."""
        pass


class ProxyFile:

    """The proxy file object that is intended to take the place of
    sys.stdout.  The proxy can manage a stack of file objects it is
    writing to, and an underlying raw file object."""

    def __init__(self, bottom):
        self.stack = Stack()
        self.bottom = bottom

    def current(self):
        """Get the current stream to write to."""
        if self.stack:
            return self.stack[-1][1]
        else:
            return self.bottom

    def push(self, interpreter):
        self.stack.push((interpreter, interpreter.stream()))

    def pop(self, interpreter):
        result = self.stack.pop()
        assert interpreter is result[0]

    def clear(self, interpreter):
        self.stack.filter(lambda x, i=interpreter: x[0] is not i)

    def write(self, data):
        self.current().write(data)

    def writelines(self, lines):
        self.current().writelines(lines)

    def flush(self):
        self.current().flush()

    def close(self):
        """Close the current file.  If the current file is the bottom, then
        close it and dispose of it."""
        current = self.current()
        if current is self.bottom:
            self.bottom = None
        current.close()

    def _testProxy(self): pass


class Filter:

    """An abstract filter."""

    def __init__(self):
        if self.__class__ is Filter:
            raise NotImplementedError
        self.sink = None

    def next(self):
        """Return the next filter/file-like object in the sequence, or None."""
        return self.sink

    def write(self, data):
        """The standard write method; this must be overridden in subclasses."""
        raise NotImplementedError

    def writelines(self, lines):
        """Standard writelines wrapper."""
        for line in lines:
            self.write(line)

    def _flush(self):
        """The _flush method should always flush the sink and should not
        be overridden."""
        self.sink.flush()

    def flush(self):
        """The flush method can be overridden."""
        self._flush()

    def close(self):
        """Close the filter.  Do an explicit flush first, then close the
        sink."""
        self.flush()
        self.sink.close()

    def attach(self, filter):
        """Attach a filter to this one."""
        if self.sink is not None:
            # If it's already attached, detach it first.
            self.detach()
        self.sink = filter

    def detach(self):
        """Detach a filter from its sink."""
        self.flush()
        self._flush() # do a guaranteed flush to just to be safe
        self.sink = None

    def last(self):
        """Find the last filter in this chain."""
        this, last = self, self
        while this is not None:
            last = this
            this = this.next()
        return last

class Context:
    
    """An interpreter context, which encapsulates a name, an input
    file object, and a parser object."""

    DEFAULT_UNIT = 'lines'

    def __init__(self, name, line=0, units=DEFAULT_UNIT):
        self.name = name
        self.line = line
        self.units = units
        self.pause = False

    def bump(self, quantity=1):
        if self.pause:
            self.pause = False
        else:
            self.line += quantity

    def identify(self):
        return self.name, self.line

    def __str__(self):
        if self.units == self.DEFAULT_UNIT:
            return "%s:%s" % (self.name, self.line)
        else:
            return "%s:%s[%s]" % (self.name, self.line, self.units)


class Hook:

    """The base class for implementing hooks."""

    def __init__(self):
        self.interpreter = None

    def register(self, interpreter):
        self.interpreter = interpreter

    def deregister(self, interpreter):
        if interpreter is not self.interpreter:
            raise ConsistencyError, "hook not associated with this interpreter"
        self.interpreter = None

    def push(self):
        self.interpreter.push()

    def pop(self):
        self.interpreter.pop()

    def null(self): pass

    def atStartup(self): pass
    def atReady(self): pass
    def atFinalize(self): pass
    def atShutdown(self): pass
    def atParse(self, scanner, locals): pass
    def atToken(self, token): pass
    def atHandle(self, meta): pass
    def atInteract(self): pass

    def beforeInclude(self, name, file, locals): pass
    def afterInclude(self): pass

    def beforeExpand(self, string, locals): pass
    def afterExpand(self, result): pass

    def beforeRun(self, tokens, locals): pass
    def afterRun(self, result): pass

    def beforeFile(self, name, file, locals): pass
    def afterFile(self): pass

    def beforeBinary(self, name, file, chunkSize, locals): pass
    def afterBinary(self): pass

    def beforeString(self, name, string, locals): pass
    def afterString(self): pass

    def beforeQuote(self, string): pass
    def afterQuote(self, result): pass

    def beforeEscape(self, string, more): pass
    def afterEscape(self, result): pass

    def beforeControl(self, type, rest, locals): pass
    def afterControl(self): pass

    def beforeSignificate(self, key, value, locals): pass
    def afterSignificate(self): pass

    def beforeDeclare(self, name, type, keywords, locals): pass
    def afterDeclare(self, name): pass

    def beforeAtomic(self, name, value, locals): pass
    def afterAtomic(self): pass

    def beforeMulti(self, name, values, locals): pass
    def afterMulti(self): pass

    def beforeImport(self, name, locals): pass
    def afterImport(self): pass

    def beforeClause(self, catch, locals): pass
    def afterClause(self, exception, variable): pass

    def beforeDictionary(self, code): pass
    def afterDictionary(self, result): pass

    def beforeSerialize(self, expression, locals): pass
    def afterSerialize(self): pass

    def beforeDefined(self, name, locals): pass
    def afterDefined(self, result): pass

    def beforeLiteral(self, text): pass
    def afterLiteral(self): pass

    def beforeEvaluate(self, expression, locals): pass
    def afterEvaluate(self, result): pass

    def beforeExecute(self, statements, locals): pass
    def afterExecute(self): pass

    def beforeSingle(self, source, locals): pass
    def afterSingle(self): pass

class VerboseHook(Hook):

    """A verbose hook that reports all information received by the
    hook interface.  This class dynamically scans the Hook base class
    to ensure that all hook methods are properly represented."""

    EXEMPT_ATTRIBUTES = ['register', 'deregister', 'push', 'pop']

    def __init__(self, output=sys.stderr):
        Hook.__init__(self)
        self.output = output
        self.indent = 0

        class FakeMethod:
            """This is a proxy method-like object."""
            def __init__(self, hook, name):
                self.hook = hook
                self.name = name

            def __call__(self, **keywords):
                self.hook.output.write("%s%s: %s\n" % \
                                       (' ' * self.hook.indent, 
                                        self.name, repr(keywords)))

        for attribute in dir(Hook):
            if attribute[:1] != '_' and \
                   attribute not in self.EXEMPT_ATTRIBUTES:
                self.__dict__[attribute] = FakeMethod(self, attribute)


class Resource:

    """An individual resource."""

    def __init__(self, **args):
        self.update(args)


class Resolver:

    """An entity for resolving resources."""

    def __init__(self, factory=Resource):
        self.factory = factory
        self.resources = {}

    def add(self, namespace, name, type, keywords=None):
        """Add a new resource to the resolver."""
        if keywords is None:
            keywords = {}
        if not keywords.has_key('type'):
            keywords['type'] = type
        resource = self.factory(**keywords)
        self.resources[namespace, name] = resource

    def has(self, namespace, name):
        """Does this resolver already have a resource?"""
        return self.resource.has_key((namespace, name))

    def resolve(self, namespace, name):
        """Resolve an individual resource."""
        raise NotImplementedError

    def resolveAll(self):
        """Resolve all registered resources."""
        for identity in self.resources.keys():
            self.resolve(identity)


class Token:

    """An element of expansion."""

    def run(self, interpreter, locals):
        raise NotImplementedError

    def string(self):
        raise NotImplementedError

    def __str__(self): return self.string()

    def __repr__(self):
        return '<%s @ 0x%x %s>' % \
               (self.__class__.__name__, id(self), str(self))

class NullToken(Token):
    """A chunk of data not containing markups."""
    def __init__(self, data):
        self.data = data

    def run(self, interpreter, locals):
        interpreter.write(self.data)

    def string(self):
        return self.data

class ExpansionToken(Token):
    """A token that involves an expansion."""
    def __init__(self, prefix, first):
        self.prefix = prefix
        self.first = first

    def scan(self, scanner):
        pass

    def run(self, interpreter, locals):
        pass

class WhitespaceToken(ExpansionToken):
    """A whitespace markup."""
    def string(self):
        return '%s%s' % (self.prefix, self.first)

class LiteralToken(ExpansionToken):
    """A literal markup."""
    def run(self, interpreter, locals):
        interpreter.write(self.first)

    def string(self):
        return '%s%s' % (self.prefix, self.first)

class PrefixToken(ExpansionToken):
    """A prefix markup."""
    def run(self, interpreter, locals):
        interpreter.write(interpreter.config.prefix)

    def string(self):
        return self.prefix * 2
        
class CommentLineToken(ExpansionToken):
    """A full line comment markup."""
    def scan(self, scanner):
        loc = scanner.find('\n')
        if loc >= 0:
            self.comment = scanner.chop(loc, 1)
        else:
            raise TransientParseError, "comment expects newline"

    def string(self):
        return '%s#%s\n' % (self.prefix, self.comment)

class CommentInlineToken(ExpansionToken):
    """An inline comment markup."""
    def scan(self, scanner):
        loc = scanner.find('*')
        if loc >= 0:
            self.comment = scanner.chop(loc, 1)
        else:
            raise TransientParseError, "inline comment expects asterisk"

    def string(self):
        return '%s*%s*' % (self.prefix, self.comment)

class ContextNameToken(ExpansionToken):
    """A context name change markup."""
    def scan(self, scanner):
        loc = scanner.find('\n')
        if loc >= 0:
            self.name = scanner.chop(loc, 1).strip()
        else:
            raise TransientParseError, "context name expects newline"

    def run(self, interpreter, locals):
        context = interpreter.context()
        context.name = self.name

class ContextLineToken(ExpansionToken):
    """A context line change markup."""
    def scan(self, scanner):
        loc = scanner.find('\n')
        if loc >= 0:
            try:
                self.line = int(scanner.chop(loc, 1))
            except ValueError:
                raise ParseError, "context line requires integer"
        else:
            raise TransientParseError, "context line expects newline"

    def run(self, interpreter, locals):
        context = interpreter.context()
        context.line = self.line
        context.pause = True

class EscapeToken(ExpansionToken):
    """An escape markup."""
    def scan(self, scanner):
        try:
            code = scanner.chop(1)
            result = None
            if code in '()[]<>{}\'\"\\': # literals
                result = code
            elif code == '0': # NUL
                result = '\x00'
            elif code == 'a': # BEL
                result = '\x07'
            elif code == 'b': # BS
                result = '\x08'
            elif code == 'd': # decimal code
                decimalCode = scanner.chop(3)
                result = chr(int(decimalCode, 10))
            elif code == 'e': # ESC
                result = '\x1b'
            elif code == 'f': # FF
                result = '\x0c'
            elif code == 'h': # DEL
                result = '\x7f'
            elif code == 'n': # LF (newline)
                result = '\x0a'
            elif code == 'N': # Unicode character name
                scanner.config.requireUnicode()
                if scanner.chop(1) != '{':
                    raise ParseError, "Unicode name escape should be \\N{...}"
                i = scanner.find('}')
                name = scanner.chop(i, 1)
                try:
                    result = unicodedata.lookup(name)
                except AttributeError:
                    raise ConfigurationError, \
                          "\\N{...} only works where unicodedata is available"
                except KeyError:
                    raise ConfigurationError, \
                          "unknown Unicode character name: %s" % name
            elif code == 'o': # octal code
                octalCode = scanner.chop(3)
                result = chr(int(octalCode, 8))
            elif code == 'q': # quaternary code
                quaternaryCode = scanner.chop(4)
                result = chr(int(quaternaryCode, 4))
            elif code == 'r': # CR
                result = '\x0d'
            elif code in 's ': # SP
                result = ' '
            elif code == 't': # HT
                result = '\x09'
            elif code == 'u': # Unicode 16-bit hex literal
                scanner.config.requireUnicode()
                hexCode = scanner.chop(4)
                result = unichr(int(hexCode, 16))
            elif code == 'U': # Unicode 32-bit hex literal
                scanner.config.requireUnicode()
                hexCode = scanner.chop(8)
                result = unichr(int(hexCode, 16))
            elif code == 'v': # VT
                result = '\x0b'
            elif code == 'x': # hexadecimal code
                hexCode = scanner.chop(2)
                result = chr(int(hexCode, 16))
            elif code == 'z': # EOT
                result = '\x04'
            elif code == '^': # control character
                controlCode = scanner.chop(1).upper()
                if controlCode >= '@' and controlCode <= '`':
                    result = chr(ord(controlCode) - ord('@'))
                elif controlCode == '?':
                    result = '\x7f'
                else:
                    raise ParseError, \
                          "invalid escape control code: ^%s" % controlCode
            else:
                raise ParseError, "unrecognized escape code"
            assert result is not None
            self.code = result
        except ValueError:
            raise ParseError, "invalid numeric escape code"

    def run(self, interpreter, locals):
        interpreter.write(self.code)

    def string(self):
        return '%s\\x%02x' % (self.prefix, ord(self.code))

class SignificatorToken(ExpansionToken):
    """A significator markup."""

    ENDS = ['\n', '%%\n']
    
    def scan(self, scanner):
        self.multiline = self.stringized = False
        peek = scanner.read()
        if peek == '%':
            self.multiline = True
            scanner.advance(1)
            anotherPeek = scanner.read()
            if anotherPeek == '!':
                self.stringized = True
                scanner.advance(1)
        elif peek == '!':
            self.stringized = True
            scanner.advance(1)
        loc = scanner.find(self.ENDS[self.multiline])
        if loc >= 0:
            contents = scanner.chop(loc, len(self.ENDS[self.multiline]))
            if not contents:
                raise ParseError, "significator must have nonblank key"
            if contents[0] in ' \t\v\n':
                raise ParseError, "no whitespace between % and key"
            # Work around a subtle CPython-Jython difference by stripping
            # the string before splitting it: 'a '.split(None, 1) has two
            # elements in Jython 2.1).
            fields = contents.strip().split(None, 1)
            if len(fields) == 2 and fields[1] == '':
                fields.pop()
            self.key = fields[0]
            if len(fields) < 2:
                fields.append(None)
            self.key, self.valueCode = fields
        else:
            if self.multiline:
                raise TransientParseError, "significator expects %% newline"
            else:
                raise TransientParseError, "significator expects newline"

    def run(self, interpreter, locals):
        value = self.valueCode
        if value is not None:
            if not self.stringized:
                value = interpreter.evaluate(value.strip(), locals)
            else:
                value = value.strip()
        interpreter.significate(self.key, value, locals)

    def string(self):
        if self.valueCode is None:
            return '%s%%%s%s\n' % (self.prefix, ('', '!')[self.stringized],
                                   self.key)
        else:
            if self.multiline:
                return '%s%%%s%s \\\n%s\n' % (self.prefix, 
                                            ('', '!')[self.stringized],
                                            self.key, self.valueCode)
            else:
                return '%s%%%s%s %s\n' % (self.prefix, self.key,
                                          ('', '!')[self.stringized],
                                          self.valueCode)

class DeclaratorToken(ExpansionToken):
    """A declarator markup."""

    ENDS = ['\n', '&&\n']
    
    def scan(self, scanner):
        peek = scanner.read()
        if peek == '&':
            self.multiline = True
            scanner.advance(1)
        else:
            self.multiline = False
        loc = scanner.find(self.ENDS[self.multiline])
        if loc >= 0:
            contents = scanner.chop(loc, len(self.ENDS[self.multiline]))
            if not contents:
                raise ParseError, "significator must have nonblank key"
            if contents[0] in ' \t\v\n':
                raise ParseError, "no whitespace between % and key"
            if contents.find(':') < 0:
                raise ParseError, "declarator requires : separator"
            names, self.argumentsCode = \
                   contents.strip().split(':', 1)
            self.names = [x.strip() for x in names.split(',')]
            argumentsCodes = self.argumentsCode.split(',', 1)
            if len(argumentsCodes) < 2:
                self.typeCode, self.keywordsCode = argumentsCodes[0], ''
            else:
                self.typeCode, self.keywordsCode = argumentsCodes
        else:
            if self.multiline:
                raise TransientParseError, "declarator expects && newline"
            else:
                raise TransientParseError, "declarator expects newline"

    def run(self, interpreter, locals):
        type = interpreter.evaluate(self.typeCode, locals)
        keywords = interpreter.dictionary(self.keywordsCode, locals)
        for name in self.names:
            interpreter.declare(name, type, keywords, locals)

    def string(self):
        namesCode = ', '.join(self.names)
        if self.multiline:
            return '%s&%s:\n%s\n' % (namesCode, self.argumentsCode)
        else:
            return '%s&%s: %s\n' % (namesCode, self.argumentsCode)

class ExpressionToken(ExpansionToken):
    """An expression markup."""
    def scan(self, scanner):
        z = scanner.complex('(', ')', 0)
        try:
            q = scanner.next('$', 0, z, True)
        except ParseError:
            q = z
        try:
            i = scanner.next('?', 0, q, True)
            try:
                j = scanner.next('!', i, q, True)
            except ParseError:
                j = q
        except ParseError:
            i = j = q
        code = scanner.chop(z, 1)
        self.testCode = code[:i]
        self.thenCode = code[i + 1:j]
        self.elseCode = code[j + 1:q]
        self.exceptCode = code[q + 1:z]

    def run(self, interpreter, locals):
        try:
            result = interpreter.evaluate(self.testCode, locals)
            if self.thenCode:
                if result:
                    result = interpreter.evaluate(self.thenCode, locals)
                else:
                    if self.elseCode:
                        result = interpreter.evaluate(self.elseCode, locals)
                    else:
                        result = None
        except SyntaxError:
            # Don't catch syntax errors; let them through.
            raise
        except:
            if self.exceptCode:
                result = interpreter.evaluate(self.exceptCode, locals)
            else:
                raise
        if result is not None:
            interpreter.write(interpreter.config.toStringType(result))

    def string(self):
        result = self.testCode
        if self.thenCode:
            result += '?' + self.thenCode
        if self.elseCode:
            result += '!' + self.elseCode
        if self.exceptCode:
            result += '$' + self.exceptCode
        return '%s(%s)' % (self.prefix, result)

class StringLiteralToken(ExpansionToken):
    """A string token markup."""
    def scan(self, scanner):
        scanner.retreat()
        assert scanner[0] == self.first
        i = scanner.quote()
        self.literal = scanner.chop(i)

    def run(self, interpreter, locals):
        interpreter.literal(self.literal)

    def string(self):
        return '%s%s' % (self.prefix, self.literal)

class SimpleExpressionToken(ExpansionToken):
    """A simple expression markup."""
    def scan(self, scanner):
        i = scanner.simple()
        self.code = self.first + scanner.chop(i)
        # Now scan ahead for functional expressions.
        self.subtokens = []
        scanner.acquire()
        while scanner and scanner[0] == '{':
            scanner.chop(1)
            self.subtokens.append([])
            level = 0
            while True:
                peek = scanner.read()
                if peek == '{':
                    level += 1
                    scanner.chop(1)
                    self.subtokens[-1].append(NullToken('{'))
                elif peek == '}':
                    level -= 1
                    scanner.chop(1)
                    if level < 0:
                        break
                    self.subtokens[-1].append(NullToken('}'))
                token = scanner.one('{}')
                self.subtokens[-1].append(token)
        scanner.release()

    def run(self, interpreter, locals):
        if self.subtokens:
            interpreter.functional(self.code, self.subtokens, locals)
        else:
            interpreter.serialize(self.code, locals)

    def string(self):
        s = '%s%s' % (self.prefix, self.code)
        for tokens in self.subtokens:
            s += '{%s}' % ''.join(map(str, tokens))
        return s

class ReprToken(ExpansionToken):
    """A repr markup."""
    def scan(self, scanner):
        i = scanner.next('`', 0)
        self.code = scanner.chop(i, 1)

    def run(self, interpreter, locals):
        interpreter.write(repr(interpreter.evaluate(self.code, locals)))

    def string(self):
        return '%s`%s`' % (self.prefix, self.code)
    
class InPlaceToken(ExpansionToken):
    """An in-place markup."""
    def scan(self, scanner):
        i = scanner.next(':', 0)
        j = scanner.next(':', i + 1)
        self.code = scanner.chop(i, j - i + 1)

    def run(self, interpreter, locals):
        interpreter.write("%s:%s:" % (interpreter.config.prefix, self.code))
        try:
            interpreter.serialize(self.code, locals)
        finally:
            interpreter.write(":")

    def string(self):
        return '%s:%s::' % (self.prefix, self.code)

class StatementToken(ExpansionToken):
    """A statement markup."""
    def scan(self, scanner):
        i = scanner.complex('{', '}', 0)
        self.code = scanner.chop(i, 1)

    def run(self, interpreter, locals):
        interpreter.execute(self.code, locals)

    def string(self):
        return '%s{%s}' % (self.prefix, self.code)

class CustomToken(ExpansionToken):
    """A custom markup."""
    def scan(self, scanner):
        i = scanner.complex('<', '>', 0)
        self.contents = scanner.chop(i, 1)

    def run(self, interpreter, locals):
        result = interpreter.invokeCallback(self.contents)
        if result is not None:
            interpreter.write(interpreter.config.toStringType((result)))

    def string(self):
        return '%s<%s>' % (self.prefix, self.contents)

class ControlToken(ExpansionToken):

    """A control token."""

    PRIMARY_TYPES = ['if', 'for', 'while', 'try', 'def']
    SECONDARY_TYPES = ['elif', 'else', 'except', 'finally']
    TERTIARY_TYPES = ['continue', 'break']
    GREEDY_TYPES = ['if', 'elif', 'for', 'while', 'def', 'end']
    END_TYPES = ['end']

    IN_RE = re.compile(r"\bin\b")
    
    def scan(self, scanner):
        scanner.acquire()
        i = scanner.complex('[', ']', 0)
        self.contents = scanner.chop(i, 1)
        fields = self.contents.strip().split(' ', 1)
        if len(fields) > 1:
            self.type, self.rest = fields
        else:
            self.type = fields[0]
            self.rest = None
        self.subtokens = []
        if self.type in self.GREEDY_TYPES and self.rest is None:
            raise ParseError, "control '%s' needs arguments" % self.type
        if self.type in self.PRIMARY_TYPES:
            self.subscan(scanner, self.type)
            self.kind = 'primary'
        elif self.type in self.SECONDARY_TYPES:
            self.kind = 'secondary'
        elif self.type in self.TERTIARY_TYPES:
            self.kind = 'tertiary'
        elif self.type in self.END_TYPES:
            self.kind = 'end'
        else:
            raise ParseError, "unknown control markup: '%s'" % self.type
        scanner.release()

    def subscan(self, scanner, primary):
        """Do a subscan for contained tokens."""
        while True:
            token = scanner.one()
            if token is None:
                raise TransientParseError, \
                      "control '%s' needs more tokens" % primary
            if isinstance(token, ControlToken) and \
                   token.type in self.END_TYPES:
                if token.rest != primary:
                    raise ParseError, \
                          "control must end with 'end %s'" % primary
                break
            self.subtokens.append(token)

    def build(self, allowed=None):
        """Process the list of subtokens and divide it into a list of
        2-tuples, consisting of the dividing tokens and the list of
        subtokens that follow them.  If allowed is specified, it will
        represent the list of the only secondary markup types which
        are allowed."""
        if allowed is None:
            allowed = self.SECONDARY_TYPES
        result = []
        latest = []
        result.append((self, latest))
        for subtoken in self.subtokens:
            if (isinstance(subtoken, ControlToken) and 
                subtoken.kind == 'secondary'):
                if subtoken.type not in allowed:
                    raise ParseError, \
                          "control unexpected secondary: '%s'" % subtoken.type
                latest = []
                result.append((subtoken, latest))
            else:
                latest.append(subtoken)
        return result

    def run(self, interpreter, locals):
        interpreter.invoke('beforeControl', type=self.type, rest=self.rest, 
                           locals=locals)
        if self.type == 'if':
            info = self.build(['elif', 'else'])
            elseTokens = None
            if info[-1][0].type == 'else':
                elseTokens = info.pop()[1]
            for secondary, subtokens in info:
                if secondary.type not in ('if', 'elif'):
                    raise ParseError, \
                          "control 'if' unexpected secondary: '%s'" % secondary.type
                if interpreter.evaluate(secondary.rest, locals):
                    self.subrun(subtokens, interpreter, locals)
                    break
            else:
                if elseTokens:
                    self.subrun(elseTokens, interpreter, locals)
        elif self.type == 'for':
            sides = self.IN_RE.split(self.rest, 1)
            if len(sides) != 2:
                raise ParseError, "control expected 'for x in seq'"
            iterator, sequenceCode = sides
            info = self.build(['else'])
            elseTokens = None
            if info[-1][0].type == 'else':
                elseTokens = info.pop()[1]
            if len(info) != 1:
                raise ParseError, "control 'for' expects at most one 'else'"
            sequence = interpreter.evaluate(sequenceCode, locals)
            for element in sequence:
                try:
                    interpreter.assign(iterator, element, locals)
                    self.subrun(info[0][1], interpreter, locals)
                except ContinueFlow:
                    continue
                except BreakFlow:
                    break
            else:
                if elseTokens:
                    self.subrun(elseTokens, interpreter, locals)
        elif self.type == 'while':
            testCode = self.rest
            info = self.build(['else'])
            elseTokens = None
            if info[-1][0].type == 'else':
                elseTokens = info.pop()[1]
            if len(info) != 1:
                raise ParseError, "control 'while' expects at most one 'else'"
            exitedNormally = False
            while True:
                try:
                    if not interpreter.evaluate(testCode, locals):
                        exitedNormally = True
                        break
                    self.subrun(info[0][1], interpreter, locals)
                except ContinueFlow:
                    continue
                except BreakFlow:
                    break
            if exitedNormally and elseTokens:
                self.subrun(elseTokens, interpreter, locals)
        elif self.type == 'try':
            info = self.build(['except', 'finally', 'else'])
            if len(info) == 1:
                raise ParseError, "control 'try' needs 'except' or 'finally'"
            elseTokens = None
            if info[-1][0].type == 'else':
                elseTokens = info.pop()[1]
            type = info[-1][0].type
            if type == 'except':
                for secondary, _tokens in info[1:]:
                    if secondary.type != 'except':
                        raise ParseError, \
                              "control 'try' cannot have 'except' and 'finally'"
            else:
                assert type == 'finally'
                if elseTokens is not None:
                    raise ParseError, "'try'/'finally' cannot have 'else'"
                if len(info) != 2:
                    raise ParseError, \
                          "control 'try' can only have one 'finally'"
            if type == 'except':
                try:
                    self.subrun(info[0][1], interpreter, locals)
                except FlowError:
                    raise
                except Exception, e:
                    for secondary, tokens in info[1:]:
                        exception, variable = interpreter.clause(secondary.rest)
                        if variable is not None:
                            interpreter.assign(variable, e)
                        if isinstance(e, exception):
                            self.subrun(tokens, interpreter, locals)
                            break
                    else:
                        raise
                else:
                    if elseTokens is not None:
                        self.subrun(elseTokens, interpreter, locals)
            else:
                try:
                    self.subrun(info[0][1], interpreter, locals)
                finally:
                    self.subrun(info[1][1], interpreter, locals)
        elif self.type == 'continue':
            raise ContinueFlow, "control 'continue' without 'for', 'while'"
        elif self.type == 'break':
            raise BreakFlow, "control 'break' without 'for', 'while'"
        elif self.type == 'def':
            signature = self.rest
            definition = self.substring()
            code = ('def %s:\n'
                    ' r"""%s"""\n'
                    ' return %s.expand(r"""%s""", locals())\n') % \
                   (signature, definition, 
                    interpreter.config.pseudomoduleName, definition)
            interpreter.execute(code, locals)
        elif self.type == 'end':
            raise ParseError, "control 'end' requires primary markup"
        else:
            raise ParseError, \
                  "control '%s' cannot be at this level" % self.type
        interpreter.invoke('afterControl')

    def subrun(self, tokens, interpreter, locals):
        """Execute a sequence of tokens."""
        for token in tokens:
            token.run(interpreter, locals)

    def substring(self):
        return ''.join(map(str, self.subtokens))

    def string(self):
        if self.kind == 'primary':
            return '%s[%s]%s%s[end %s]' % \
                   (self.prefix, self.contents, self.substring(), 
                    self.prefix, self.type)
        else:
            return '%s[%s]' % (self.prefix, self.contents)


class Scanner:

    """A scanner holds a buffer for lookahead parsing and has the
    ability to scan for special symbols and indicators in that
    buffer."""

    IDENTIFIER_FIRST_CHARS = '_abcdefghijklmnopqrstuvwxyz' \
                             'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    IDENTIFIER_CHARS = IDENTIFIER_FIRST_CHARS + '0123456789.'
    ENDING_CHARS = {'(': ')', '[': ']', '{': '}'}

    # This is the token mapping table that maps first characters to
    # token classes.
    TOKEN_MAP = (
        (None,                   PrefixToken),
        (' \t\v\r\n',            WhitespaceToken),
        (')]>}',                 LiteralToken),
        ('\\',                   EscapeToken),
        ('#',                    CommentLineToken),
        ('*',                    CommentInlineToken),
        ('?',                    ContextNameToken),
        ('!',                    ContextLineToken),
        ('%',                    SignificatorToken),
        ('&',                    DeclaratorToken),
        ('(',                    ExpressionToken),
        (IDENTIFIER_FIRST_CHARS, SimpleExpressionToken),
        ('\'\"',                 StringLiteralToken),
        ('`',                    ReprToken),
        (':',                    InPlaceToken),
        ('[',                    ControlToken),
        ('{',                    StatementToken),
        ('<',                    CustomToken),
    )

    def __init__(self, config, data=''):
        self.config = config
        self.pointer = 0
        self.buffer = data
        self.lock = 0

    def __nonzero__(self): return self.pointer < len(self.buffer)
    def __len__(self): return len(self.buffer) - self.pointer
    def __getitem__(self, index): return self.buffer[self.pointer + index]

    def __getslice__(self, start, stop):
        if stop > len(self):
            stop = len(self)
        return self.buffer[self.pointer + start:self.pointer + stop]

    def advance(self, count=1):
        """Advance the pointer count characters."""
        self.pointer += count

    def retreat(self, count=1):
        self.pointer -= count
        if self.pointer < 0:
            raise ParseError, "can't retreat back over synced out chars"

    def set(self, data):
        """Start the scanner digesting a new batch of data; start the pointer
        over from scratch."""
        self.pointer = 0
        self.buffer = data

    def feed(self, data):
        """Feed some more data to the scanner."""
        self.buffer += data

    def chop(self, count=None, slop=0):
        """Chop the first count + slop characters off the front, and return
        the first count.  If count is not specified, then return
        everything."""
        if count is None:
            assert slop == 0
            count = len(self)
        if count > len(self):
            raise TransientParseError, "not enough data to read"
        result = self[:count]
        self.advance(count + slop)
        return result

    def acquire(self):
        """Lock the scanner so it doesn't destroy data on sync."""
        self.lock += 1

    def release(self):
        """Unlock the scanner."""
        self.lock -= 1

    def sync(self):
        """Sync up the buffer with the read head."""
        if self.lock == 0 and self.pointer != 0:
            self.buffer = self.buffer[self.pointer:]
            self.pointer = 0

    def unsync(self):
        """Undo changes; reset the read head."""
        if self.pointer != 0:
            self.lock = 0
            self.pointer = 0

    def rest(self):
        """Get the remainder of the buffer."""
        return self[:]

    def read(self, i=0, count=1):
        """Read count chars starting from i; raise a transient error if
        there aren't enough characters remaining."""
        if len(self) < i + count:
            raise TransientParseError, "need more data to read"
        else:
            return self[i:i + count]

    def check(self, i, archetype=None):
        """Scan for the next single or triple quote, with the specified
        archetype.  Return the found quote or None."""
        quote = None
        if self[i] in '\'\"':
            quote = self[i]
            if len(self) - i < 3:
                for j in range(i, len(self)):
                    if self[i] == quote:
                        return quote
                else:
                    raise TransientParseError, "need to scan for rest of quote"
            if self[i + 1] == self[i + 2] == quote:
                quote = quote * 3
        if quote is not None:
            if archetype is None:
                return quote
            else:
                if archetype == quote:
                    return quote
                elif len(archetype) < len(quote) and archetype[0] == quote[0]:
                    return archetype
                else:
                    return None
        else:
            return None

    def find(self, sub, start=0, end=None):
        """Find the next occurrence of the character, or return -1."""
        if end is not None:
            return self.rest().find(sub, start, end)
        else:
            return self.rest().find(sub, start)

    def last(self, char, start=0, end=None):
        """Find the first character that is _not_ the specified character."""
        if end is None:
            end = len(self)
        i = start
        while i < end:
            if self[i] != char:
                return i
            i += 1
        else:
            raise TransientParseError, "expecting other than %s" % char

    def next(self, target, start=0, end=None, mandatory=False):
        """Scan for the next occurrence of one of the characters in
        the target string; optionally, make the scan mandatory."""
        if mandatory:
            assert end is not None
        quote = None
        if end is None:
            end = len(self)
        i = start
        while i < end:
            newQuote = self.check(i, quote)
            if newQuote:
                if newQuote == quote:
                    quote = None
                else:
                    quote = newQuote
                i += len(newQuote)
            else:
                c = self[i]
                if quote:
                    if c == '\\':
                        i += 1
                else:
                    if c in target:
                        return i
                i += 1
        else:
            if mandatory:
                raise ParseError, "expecting %s, not found" % target
            else:
                raise TransientParseError, "expecting ending character"

    def quote(self, start=0, end=None, mandatory=False):
        """Scan for the end of the next quote."""
        assert self[start] in '\'\"'
        quote = self.check(start)
        if end is None:
            end = len(self)
        i = start + len(quote)
        while i < end:
            newQuote = self.check(i, quote)
            if newQuote:
                i += len(newQuote)
                if newQuote == quote:
                    return i
            else:
                c = self[i]
                if c == '\\':
                    i += 1
                i += 1
        else:
            if mandatory:
                raise ParseError, "expecting end of string literal"
            else:
                raise TransientParseError, "expecting end of string literal"

    def nested(self, enter, exit, start=0, end=None):
        """Scan from i for an ending sequence, respecting entries and exits
        only."""
        depth = 0
        if end is None:
            end = len(self)
        i = start
        while i < end:
            c = self[i]
            if c == enter:
                depth += 1
            elif c == exit:
                depth += 1
                if depth < 0:
                    return i
            i += 1
        else:
            raise TransientParseError, "expecting end of complex expression"

    def complex(self, enter, exit, start=0, end=None, skip=None):
        """Scan from i for an ending sequence, respecting quotes,
        entries and exits."""
        quote = None
        depth = 0
        if end is None:
            end = len(self)
        lastNonQuote = None
        i = start
        while i < end:
            newQuote = self.check(i, quote)
            if newQuote:
                if newQuote == quote:
                    quote = None
                else:
                    quote = newQuote
                i += len(newQuote)
            else:
                c = self[i]
                if quote:
                    if c == '\\':
                        i += 1
                else:
                    if skip is None or lastNonQuote != skip:
                        if c == enter:
                            depth += 1
                        elif c == exit:
                            depth -= 1
                            if depth < 0:
                                return i
                lastNonQuote = c
                i += 1
        else:
            raise TransientParseError, "expecting end of complex expression"

    def word(self, start=0):
        """Scan from i for a simple word."""
        length = len(self)
        i = start
        while i < length:
            if not self[i] in Scanner.IDENTIFIER_CHARS:
                return i
            i += 1
        else:
            raise TransientParseError, "expecting end of word"

    def phrase(self, start=0):
        """Scan from i for a phrase (e.g., 'word', 'f(a, b, c)', 'a[i]', or
        combinations like 'x[i](a)'."""
        # Find the word.
        i = self.word(start)
        while i < len(self) and self[i] in '([':
            enter = self[i]
            exit = Scanner.ENDING_CHARS[enter]
            i = self.complex(enter, exit, i + 1) + 1
        return i
    
    def simple(self, start=0):
        """Scan from i for a simple expression, which consists of one
        more phrases separated by dots.  Return a tuple giving the end
        of the expression and a list of tuple pairs consisting of the
        simple expression extensions found, if any."""
        i = self.phrase(start)
        pairs = []
        length = len(self)
        while i < length and self[i] == '.':
            i = self.phrase(i)
        # Make sure we don't end with a trailing dot.
        while i > 0 and self[i - 1] == '.':
            i -= 1
        return i

    def one(self, firebreaks=None):
        """Parse and return one token, or None if the scanner is empty.
        If the firebreaks argument is supplied, chop up null tokens before a
        character in that string."""
        if not self:
            return None
        if not self.config.prefix:
            loc = -1
        else:
            loc = self.find(self.config.prefix)
        if loc < 0:
            # If there's no prefix in the buffer, then set the location to
            # the end so the whole thing gets processed.
            loc = len(self)
        if loc == 0:
            # If there's a prefix at the beginning of the buffer, process
            # an expansion.
            prefix = self.chop(1)
            assert prefix == self.config.prefix
            first = self.chop(1)
            if first == self.config.prefix:
                first = None
            for firsts, factory in self.TOKEN_MAP:
                if firsts is None:
                    if first is None:
                        break
                elif first in firsts:
                    break
            else:
                raise ParseError, "unknown markup: %s%s" % (self.config.prefix, first)
            token = factory(self.config.prefix, first)
            try:
                token.scan(self)
            except TransientParseError:
                # If a transient parse error occurs, reset the buffer pointer
                # so we can (conceivably) try again later.
                self.unsync()
                raise
        else:
            # Process everything up to loc as a null token, unless there
            # are characters in breaks before loc.
            if firebreaks:
                for firebreak in firebreaks:
                    i = self.find(firebreak, 0, loc)
                    if i >= 0 and i < loc:
                        loc = i
            data = self.chop(loc)
            token = NullToken(data)
        self.sync()
        return token


class Interpreter:
    
    """An interpreter can process chunks of EmPy code."""

    # Constants.

    VERSION = __version__

    # Types.

    Interpreter = None # define this below to prevent a circular reference
    Configuration = Configuration
    Hook = Hook; VerboseHook = VerboseHook
    Filter = Filter
    Resource = Resource
    Resolver = Resolver

    # Tables.

    ESCAPE_CODES = {0x00: '0', 0x07: 'a', 0x08: 'b', 0x1b: 'e', 0x0c: 'f',
                    0x7f: 'h', 0x0a: 'n', 0x0d: 'r', 0x09: 't', 0x0b: 'v',
                    0x04: 'z'}

    ASSIGN_TOKEN_RE = re.compile(r"[_a-zA-Z][_a-zA-Z0-9]*|\(|\)|,")

    # Construction, initialization, destruction.

    def __init__(self, output=None, globals=None, arguments=None, config=None,
                 filespec=None):
        self.ok = True
        # Set up the stream.
        if output is None:
            output = UncloseableFile(sys.__stdout__)
        self.output = output
        # Set up the configuration.
        if config is None:
            config = Configuration()
        self.config = config
        self.filespec = filespec
        # Hooks, finalizers, significators, callbacks.
        self.hooksEnabled = None # special sentinel meaning "false until added"
        self.hooks = []
        self.finals = []
        self.significators = {}
        self.callback = None
        # The interpreter stacks and globals.
        self.contexts = Stack()
        self.streams = Stack()
        self.globals = globals
        self.fixGlobals()
        self.globalsHistory = Stack()
        # Handle the arguments.
        if arguments is None:
            arguments = [self.config.unknownScriptName]
        self.argv = arguments[:]
        self.args = arguments[1:]
        # Install a proxy stdout if one hasn't been already.
        self.config.installProxy()
        # Finally, reset the state of all the stacks.
        self.reset()
        # Okay, now flatten the namespaces if that option has been set.
        if self.config.doFlatten:
            self.flatten()
        # Set up old pseudomodule attributes.
        self.Interpreter = self.__class__
        # Done.  Now state we're done.
        self.invoke('atStartup')

    def __del__(self):
        self.shutdown()

    def __repr__(self):
        return '<%s pseudomodule/interpreter at 0x%x>' % \
               (self.config.pseudomoduleName, id(self))

    def ready(self):
        """Declare the interpreter ready for normal operations."""
        self.invoke('atReady')

    def shutdown(self):
        """Declare this interpreting session over; close the stream file
        object.  This method is idempotent."""
        if self.streams is not None:
            try:
                self.finalize()
                self.invoke('atShutdown')
                while self.streams:
                    stream = self.streams.pop()
                    stream.close()
            finally:
                self.streams = None

    def ok(self):
        """Is the interpreter still active?"""
        return self.streams is not None

    # Writeable file-like methods.

    def write(self, data):
        self.stream().write(data)

    def writelines(self, stuff):
        self.stream().writelines(stuff)

    def flush(self):
        self.stream().flush()

    def close(self):
        self.shutdown()

    # Stack-related activity.

    def context(self):
        return self.contexts.top()

    def stream(self):
        return self.streams.top()

    def reset(self):
        self.contexts.purge()
        self.streams.purge()
        self.streams.push(Stream(self.output))
        if self.config.overrideProxy:
            sys.stdout.clear(self)

    def push(self):
        if self.config.overrideProxy:
            sys.stdout.push(self)

    def pop(self):
        if self.config.overrideProxy:
            sys.stdout.pop(self)

    # Higher-level operations.

    def include(self, filename, locals=None):
        """Do an include pass on a file."""
        if not self.config.isStringType(filename):
            raise TypeError, "include requires filename"
        file = self.config.open(filename, 'r')
        self.invoke('beforeInclude', name=filename, file=file, locals=locals)
        self.file(file, filename, locals)
        self.invoke('afterInclude')

    def expand(self, data, locals=None):
        """Do an explicit expansion on a subordinate stream."""
        outFile = StringIO.StringIO()
        stream = Stream(outFile)
        self.invoke('beforeExpand', string=data, locals=locals)
        self.streams.push(stream)
        try:
            self.string(data, '<expand>', locals)
            stream.flush()
            expansion = outFile.getvalue()
            self.invoke('afterExpand', result=expansion)
            return expansion
        finally:
            self.streams.pop()

    def run(self, tokens, name='<run>', locals=None):
        """Do an explicit expansion on a sequence of tokens."""
        outFile = StringIO.StringIO()
        stream = Stream(outFile)
        self.invoke('beforeRun', tokens=tokens, locals=locals)
        self.streams.push(stream)
        try:
            context = Context(name)
            self.contexts.push(context)
            for token in tokens:
                token.run(self, locals)
            stream.flush()
            expansion = outFile.getvalue()
            self.invoke('afterRun', result=expansion)
            self.contexts.pop()
            return expansion
        finally:
            self.streams.pop()

    def quote(self, data):
        """Quote the given string so that if it were expanded it would
        evaluate to the original."""
        self.invoke('beforeQuote', string=data)
        scanner = Scanner(self.config, data)
        result = []
        i = 0
        try:
            j = scanner.next(self.config.prefix, i)
            result.append(data[i:j])
            result.append(self.config.prefix * 2)
            i = j + 1
        except TransientParseError:
            pass
        result.append(data[i:])
        result = ''.join(result)
        self.invoke('afterQuote', result=result)
        return result

    def escape(self, data, more=''):
        """Escape a string so that nonprintable characters are replaced
        with compatible EmPy expansions."""
        self.invoke('beforeEscape', string=data, more=more)
        result = []
        for char in data:
            if char < ' ' or char > '~':
                charOrd = ord(char)
                if Interpreter.ESCAPE_CODES.has_key(charOrd):
                    result.append(self.config.prefix + '\\' + 
                                  Interpreter.ESCAPE_CODES[charOrd])
                else:
                    result.append(self.config.prefix + '\\x%02x' % charOrd)
            elif char in more:
                result.append(self.config.prefix + '\\' + char)
            else:
                result.append(char)
        result = ''.join(result)
        self.invoke('afterEscape', result=result)
        return result

    # Processing.

    def wrap(self, callable, args):
        """Wrap around an application of a callable and handle errors.
        Return whether no error occurred."""
        try:
            callable(*args)
            self.reset()
            return True
        except KeyboardInterrupt, e:
            # Handle keyboard interrupts specially: we should always exit
            # from these.
            self.fail(e, True)
        except Exception, e:
            # A standard exception (other than a keyboard interrupt).
            self.fail(e)
        except:
            # If we get here, then either it's an exception not
            # derived from Exception or a string exception, so get the
            # error type from the sys module.
            e = sys.exc_type
            self.fail(e)
        # An error occurred if we leak through to here, so do cleanup.
        self.reset()
        return False

    def interact(self):
        """Perform interaction."""
        self.invoke('atInteract')
        done = False
        while not done:
            result = self.wrap(self.file, (sys.stdin, '<interact>'))
            if self.config.exitOnError:
                done = True
            else:
                if result:
                    done = True
                else:
                    self.reset()

    def fail(self, error, fatal=False):
        """Handle an actual error that occurred."""
        self.ok = False
        meta = self.meta(error)
        self.handle(meta)
        if self.config.rawErrors:
            raise
        self.shutdown()
        # If we are supposed to delete the file on error, do it.
        if self.filespec is not None and self.config.deleteOnError:
            os.remove(self.filespec[0])
        # Exit.
        if fatal or self.config.exitOnError:
            sys.exit(self.config.failureCode)

    def file(self, file, name='<file>', locals=None):
        """Parse the entire contents of a file-like object, line by line."""
        context = Context(name)
        self.contexts.push(context)
        self.invoke('beforeFile', name=name, file=file, locals=locals)
        scanner = Scanner(self.config)
        first = True
        done = False
        while not done:
            self.context().bump(self.config.contextLineIncrement)
            line = file.readline()
            if first:
                if self.config.processBangpaths and self.config.prefix:
                    # Replace a bangpath at the beginning of the first line
                    # with an EmPy comment.
                    if line.find(self.config.bangpath) == 0:
                        line = self.config.prefix + '#' + line[2:]
                first = False
            if line:
                scanner.feed(line)
            else:
                done = True
            self.safe(scanner, done, locals)
        self.invoke('afterFile')
        self.contexts.pop()

    def binary(self, file, name='<binary>', chunkSize=0, locals=None):
        """Parse the entire contents of a file-like object, in chunks."""
        if chunkSize <= 0:
            chunkSize = self.config.defaultChunkSize
        context = Context(name, units='bytes')
        self.contexts.push(context)
        self.invoke('beforeBinary', name=name, file=file, 
                    chunkSize=chunkSize, locals=locals)
        scanner = Scanner(self.config)
        done = False
        while not done:
            chunk = file.read(chunkSize)
            if chunk:
                scanner.feed(chunk)
            else:
                done = True
            self.safe(scanner, done, locals)
            self.context().bump(len(chunk))
        self.invoke('afterBinary')
        self.contexts.pop()

    def string(self, data, name='<string>', locals=None):
        """Parse a string."""
        context = Context(name)
        self.contexts.push(context)
        self.invoke('beforeString', name=name, string=data, locals=locals)
        context.bump(self.config.contextLineIncrement)
        scanner = Scanner(self.config, data)
        self.safe(scanner, True, locals)
        self.invoke('afterString')
        self.contexts.pop()

    def safe(self, scanner, final=False, locals=None):
        """Do a protected parse.  Catch transient parse errors; if
        final is true, then make a final pass with a terminator,
        otherwise ignore the transient parse error (more data is
        pending)."""
        try:
            self.parse(scanner, locals)
        except TransientParseError:
            if final:
                # If the buffer doesn't end with a newline, try tacking on
                # a dummy terminator.
                buffer = scanner.rest()
                if buffer and buffer[-1] != '\n':
                    scanner.feed(self.config.prefix + '\n')
                # A TransientParseError thrown from here is a real parse
                # error.
                self.parse(scanner, locals)

    def parse(self, scanner, locals=None):
        """Parse and run as much from this scanner as possible."""
        self.invoke('atParse', scanner=scanner, locals=locals)
        while True:
            token = scanner.one()
            if token is None:
                break
            self.invoke('atToken', token=token)
            token.run(self, locals)

    def process(self, commands):
        """Handle some main-level processing of metacommands."""
        i = 0
        for which, thing in commands:
            if which == 'document':
                command = self.file
                target = self.config.open(thing, 'r')
                name = thing
            elif which == 'define':
                command = self.string
                if thing.find('=') >= 0:
                    target = '%s{%s}' % (self.config.prefix, thing)
                else:
                    target = '%s{%s = None}' % (self.config.prefix, thing)
                name = '<define:%d>' % i
            elif which == 'define-string':
                command = None
                if thing.find('=') >= 0:
                    key, value = thing.split('=', 1)
                    key = key.strip()
                    value = value.strip()
                else:
                    key, value = key.strip(), None
                self.atomic(key, value)
            elif which == 'exec':
                command = self.string
                target = '%s{%s}' % (self.config.prefix, thing)
                name = '<exec:%d>' % i
            elif which == 'file':
                command = self.string
                name = '<file:%d (%s)>' % (i, thing)
                target = '%s{execfile("""%s""")}' % (self.config.prefix, thing)
            elif which == 'import':
                command = self.string
                name = '<import:%d>' % i
                target = '%s{import %s}' % (self.config.prefix, thing)
            else:
                assert False
            if command is not None:
                self.wrap(command, (target, name))
            i += 1

    # Medium-level evaluation and execution.

    def tokenize(self, name):
        """Take an lvalue string and return a name or a (possibly recursive)
        list of names."""
        result = []
        stack = [result]
        for garbage in self.ASSIGN_TOKEN_RE.split(name):
            garbage = garbage.strip()
            if garbage:
                raise ParseError, "unexpected assignment token: '%s'" % garbage
        tokens = self.ASSIGN_TOKEN_RE.findall(name)
        # While processing, put a None token at the start of any list in which
        # commas actually appear.
        for token in tokens:
            if token == '(':
                stack.append([])
            elif token == ')':
                top = stack.pop()
                if len(top) == 1:
                    top = top[0] # no None token means that it's not a 1-tuple
                elif top[0] is None:
                    del top[0] # remove the None token for real tuples
                stack[-1].append(top)
            elif token == ',':
                if len(stack[-1]) == 1:
                    stack[-1].insert(0, None)
            else:
                stack[-1].append(token)
        # If it's a 1-tuple at the top level, turn it into a real subsequence.
        if result and result[0] is None:
            result = [result[1:]]
        if len(result) == 1:
            return result[0]
        else:
            return result

    def significate(self, key, value=None, locals=None):
        """Declare a significator."""
        self.invoke('beforeSignificate', key=key, value=value, locals=locals)
        name = (self.config.significatorPrefix + key + 
                self.config.significatorSuffix)
        self.atomic(name, value, locals)
        self.significators[key] = value
        self.invoke('afterSignificate')

    def declare(self, name, type, keywords=None, locals=None):
        """Declare a resource."""
        if keywords is None:
            keywords = {}
        self.invoke('beforeDeclare', 
                    name=name, type=type, keywords=keywords, locals=locals)
        self.invoke('afterDeclare', name=name)

    def atomic(self, name, value, locals=None):
        """Do an atomic assignment."""
        self.invoke('beforeAtomic', name=name, value=value, locals=locals)
        if locals is None:
            self.globals[name] = value
        else:
            locals[name] = value
        self.invoke('afterAtomic')

    def multi(self, names, values, locals=None):
        """Do a (potentially recursive) assignment."""
        self.invoke('beforeMulti', names=names, values=values, locals=locals)
        values = tuple(values) # to force an exception if not a sequence
        if len(names) != len(values):
            raise ValueError, "unpack tuple of wrong size"
        for name, value in zip(names, values):
            if self.config.isStringType(name):
                self.atomic(name, value, locals)
            else:
                self.multi(name, value, locals)
        self.invoke('afterMulti')

    def assign(self, name, value, locals=None):
        """Do a potentially complex (including tuple unpacking) assignment."""
        left = self.tokenize(name)
        # The return value of tokenize can either be a string or a list of
        # (lists of) strings.
        if self.config.isStringType(left):
            self.atomic(left, value, locals)
        else:
            self.multi(left, value, locals)

    def import_(self, name, locals=None):
        """Do an import."""
        self.invoke('beforeImport', name=name, locals=locals)
        self.execute('import %s' % name, locals)
        self.invoke('afterImport')

    def clause(self, catch, locals=None):
        """Given the string representation of an except clause, turn it into
        a 2-tuple consisting of the class name, and either a variable name
        or None."""
        self.invoke('beforeClause', catch=catch, locals=locals)
        if catch is None:
            exceptionCode, variable = None, None
        elif catch.find(',') >= 0:
            exceptionCode, variable = catch.strip().split(',', 1)
            variable = variable.strip()
        else:
            exceptionCode, variable = catch.strip(), None
        if not exceptionCode:
            exception = Exception
        else:
            exception = self.evaluate(exceptionCode, locals)
        self.invoke('afterClause', exception=exception, variable=variable)
        return exception, variable

    def dictionary(self, code, locals=None):
        """Given a string represent a keyword argument list, turn it into a
        dictionary."""
        code = code.strip()
        self.push()
        try:
            self.invoke('beforeDictionary', code=code)
            if code:
                result = self.evaluate('dict(%s)' % code, locals)
            else:
                result = {}
            self.invoke('afterDictionary', result=result)
            return result
        finally:
            self.pop()

    def defined(self, name, locals=None):
        """Return a Boolean indicating whether or not the name is
        defined either in the locals or the globals."""
        self.invoke('beforeDefined', name=name, local=local)
        if locals is not None:
            if locals.has_key(name):
                result = True
            else:
                result = False
        elif self.globals.has_key(name):
            result = True
        else:
            result = False
        self.invoke('afterDefined', result=result)

    def literal(self, text):
        """Process a string literal."""
        self.invoke('beforeLiteral', text=text)
        self.serialize(text)
        self.invoke('afterLiteral')

    def functional(self, code, tokensList, locals=None):
        """Handle a functional expression like @f{x}.  tokensList is a list
        of list of tokens."""
        self.push()
        try:
            function = self.evaluate(code, locals)
            arguments = []
            for tokensSublist in tokensList:
                arguments.append(self.run(tokensSublist, locals=locals))
            result = function(*tuple(arguments))
            if result is not None:
                self.write(self.config.toStringType(result))
        finally:
            self.pop()

    # Low-level evaluation and execution.

    def evaluate(self, expression, locals=None):
        """Evaluate an expression."""
        if expression in ('1', 'True'): return True
        if expression in ('0', 'False'): return False
        self.push()
        try:
            self.invoke('beforeEvaluate', 
                        expression=expression, locals=locals)
            if locals is not None:
                result = eval(expression, self.globals, locals)
            else:
                result = eval(expression, self.globals)
            self.invoke('afterEvaluate', result=result)
            return result
        finally:
            self.pop()

    def execute(self, statements, locals=None):
        """Execute a statement."""
        # If there are any carriage returns (as opposed to linefeeds/newlines)
        # in the statements code, then remove them.  Even on DOS/Windows
        # platforms, this will work in the Python interpreter.
        if statements.find('\r') >= 0:
            statements = statements.replace('\r', '')
        # If there are no newlines in the statements code, then strip any
        # leading or trailing whitespace.
        if statements.find('\n') < 0:
            statements = statements.strip()
        self.push()
        try:
            self.invoke('beforeExecute', 
                        statements=statements, locals=locals)
            if locals is not None:
                exec statements in self.globals, locals
            else:
                exec statements in self.globals
            self.invoke('afterExecute')
        finally:
            self.pop()

    def single(self, source, locals=None):
        """Execute an expression or statement, just as if it were
        entered into the Python interactive interpreter."""
        self.push()
        try:
            self.invoke('beforeSingle', 
                        source=source, locals=locals)
            code = compile(source, '<single>', 'single')
            if locals is not None:
                exec code in self.globals, locals
            else:
                exec code in self.globals
            self.invoke('afterSingle')
        finally:
            self.pop()

    def serialize(self, expression, locals=None):
        """Do an expansion, involving evaluating an expression, then
        converting it to a string and writing that string to the
        output if the evaluation is not None."""
        self.invoke('beforeSerialize', expression=expression, locals=locals)
        result = self.evaluate(expression, locals)
        if result is not None:
            self.write(self.config.toStringType(result))
        self.invoke('afterSerialize')

    # Hooks.

    def register(self, hook, prepend=False):
        """Register the provided hook."""
        hook.register(self)
        if self.hooksEnabled is None:
            # A special optimization so that hooks can be effectively
            # disabled until one is added or they are explicitly turned on.
            self.hooksEnabled = True
        if prepend:
            self.hooks.insert(0, hook)
        else:
            self.hooks.append(hook)

    def deregister(self, hook):
        """Remove an already registered hook."""
        hook.deregister(self)
        self.hooks.remove(hook)

    def invoke(self, _name, **keywords):
        """Invoke the hook(s) associated with the hook name, should they
        exist."""
        if self.hooksEnabled:
            for hook in self.hooks:
                hook.push()
                try:
                    method = getattr(hook, _name)
                    method(**keywords)
                finally:
                    hook.pop()

    def finalize(self):
        """Execute any remaining final routines."""
        self.push()
        self.invoke('atFinalize')
        try:
            # Pop them off one at a time so they get executed in reverse
            # order and we remove them as they're executed in case something
            # bad happens.
            while self.finals:
                final = self.finals.pop()
                final()
        finally:
            self.pop()

    # Error handling.

    def meta(self, exc=None):
        """Construct a MetaError for the interpreter's current state."""
        return MetaError(self.contexts.clone(), exc)

    def handle(self, meta):
        """Handle a MetaError."""
        first = True
        self.invoke('atHandle', meta=meta)
        for context in meta.contexts:
            if first:
                if meta.exc is not None:
                    desc = "error: %s: %s" % (meta.exc.__class__, meta.exc)
                else:
                    desc = "error"
            else:
                desc = "from this context"
            first = False
            sys.stderr.write('%s: %s\n' % (context, desc))

    #
    # Pseudomodule routines.
    #

    # Identification.

    def identify(self):
        """Identify the topmost context with a 2-tuple of the name and
        line number."""
        return self.context().identify()

    def atExit(self, callable):
        """Register a function to be called at exit."""
        self.finals.append(callable)

    # Context manipulation.

    def pushContext(self, name='<unnamed>', line=0):
        """Create a new context and push it."""
        self.contexts.push(Context(name, line))

    def popContext(self):
        """Pop the top context."""
        self.contexts.pop()

    def setContextName(self, name):
        """Set the name of the topmost context."""
        context = self.context()
        context.name = name
        
    def setContextLine(self, line):
        """Set the name of the topmost context."""
        context = self.context()
        context.line = line

    # Globals manipulation.

    def fixGlobals(self):
        """Reset the globals, stamping in the pseudomodule."""
        if self.globals is None:
            self.globals = {}
        # Make sure that there is no collision between two interpreters'
        # globals.
        if self.globals.has_key(self.config.pseudomoduleName):
            if self.globals[self.config.pseudomoduleName] is not self:
                raise ConsistencyError, "interpreter globals collision"
        self.globals[self.config.pseudomoduleName] = self

    def unfixGlobals(self):
        """Remove the pseudomodule (if present) from the globals."""
        for unwantedKey in self.config.unwantedGlobalsKeys():
            if self.globals.has_key(unwantedKey):
                del self.globals[unwantedKey]

    def getGlobals(self):
        """Retrieve the globals."""
        return self.globals

    def setGlobals(self, globals):
        """Set the globals to the specified dictionary."""
        self.globals = globals
        self.fixGlobals()

    def updateGlobals(self, otherGlobals):
        """Merge another mapping object into this interpreter's globals."""
        self.globals.update(otherGlobals)
        self.fixGlobals()

    def clearGlobals(self):
        """Clear out the globals with a brand new dictionary."""
        self.globals = {}
        self.fixGlobals()

    def saveGlobals(self, deep=True):
        """Save a copy of the globals off onto the history stack."""
        if deep:
            copyMethod = copy.deepcopy
        else:
            copyMethod = copy.copy
        self.unfixGlobals()
        self.globalsHistory.push(copyMethod(self.globals))
        self.fixGlobals()

    def restoreGlobals(self, destructive=True):
        """Restore the most recently saved copy of the globals."""
        if destructive:
            fetchMethod = self.globalsHistory.pop
        else:
            fetchMethod = self.globalsHistory.top
        self.unfixGlobals()
        self.globals = fetchMethod()
        self.fixGlobals()

    # Hook support.

    def areHooksEnabled(self):
        """Return whether or not hooks are presently enabled."""
        if self.hooksEnabled is None:
            return True
        else:
            return self.hooksEnabled

    def enableHooks(self):
        """Enable hooks."""
        self.hooksEnabled = True

    def disableHooks(self):
        """Disable hooks."""
        self.hooksEnabled = False

    def getHooks(self):
        """Get the current hooks."""
        return self.hooks[:]

    def clearHooks(self):
        """Clear all hooks."""
        self.hooks = []

    def addHook(self, hook, prepend=False):
        """Add a new hook; optionally insert it rather than appending it."""
        self.register(hook)

    def removeHook(self, hook):
        """Remove a preexisting hook."""
        self.deregister(hook)

    def invokeHook(self, _name, **keywords):
        """Manually invoke a hook."""
        self.invoke(_name, **keywords)

    # Callbacks.

    def getCallback(self):
        """Get the callback registered with this interpreter, or None."""
        return self.callback

    def registerCallback(self, callback):
        """Register a custom markup callback with this interpreter."""
        self.callback = callback

    def deregisterCallback(self):
        """Remove any previously registered callback with this interpreter."""
        self.callback = None

    def invokeCallback(self, contents):
        """Invoke the callback."""
        if self.callback is None:
            if self.config.noCallbackIsError:
                raise CustomError, \
                      "custom markup invoked with no defined callback"
            else:
                return None
        else:
            return self.callback(contents)

    # Pseudomodule manipulation.

    def flatten(self, keys=None):
        """Flatten the contents of the pseudo-module into the globals
        namespace."""
        if keys is None:
            keys = self.__dict__.keys() + self.__class__.__dict__.keys()
        dict = {}
        for key in keys:
            # The pseudomodule is really a class instance, so we need to
            # fumble use getattr instead of simply fumbling through the
            # instance's __dict__.
            dict[key] = getattr(self, key)
        # Stomp everything into the globals namespace.
        self.globals.update(dict)

    # Prefix.

    def getPrefix(self):
        """Get the current prefix."""
        return self.config.prefix

    def setPrefix(self, prefix):
        """Set the prefix."""
        if len(prefix) != 1:
            raise ValueError
        self.config.prefix = prefix

    # Diversions.

    def stopDiverting(self):
        """Stop any diverting."""
        self.stream().revert()

    def createDiversion(self, name):
        """Create a diversion (but do not divert to it) if it does not
        already exist."""
        self.stream().create(name)

    def retrieveDiversion(self, name):
        """Retrieve the diversion object associated with the name."""
        return self.stream().retrieve(name)

    def startDiversion(self, name):
        """Start diverting to the given diversion name."""
        self.stream().divert(name)

    def playDiversion(self, name):
        """Play the given diversion and then purge it."""
        self.stream().undivert(name, True)

    def replayDiversion(self, name):
        """Replay the diversion without purging it."""
        self.stream().undivert(name, False)

    def purgeDiversion(self, name):
        """Eliminate the given diversion."""
        self.stream().purge(name)

    def playAllDiversions(self):
        """Play all existing diversions and then purge them."""
        self.stream().undivertAll(True)

    def replayAllDiversions(self):
        """Replay all existing diversions without purging them."""
        self.stream().undivertAll(False)

    def purgeAllDiversions(self):
        """Purge all existing diversions."""
        self.stream().purgeAll()

    def getCurrentDiversion(self):
        """Get the name of the current diversion."""
        return self.stream().currentDiversion

    def getAllDiversions(self):
        """Get the names of all existing diversions."""
        names = self.stream().diversions.keys()
        names.sort()
        return names
    
    # Filter.

    def resetFilter(self):
        """Reset the filter so that it does no filtering."""
        self.stream().install(None)

    def getFilter(self):
        """Get the current filter."""
        filter = self.stream().filter
        if filter is self.stream().file:
            return None
        else:
            return filter

    def setFilter(self, filter):
        """Set the filter."""
        self.stream().install([filter])

    def setFilterChain(self, filters):
        """Set the filter."""
        self.stream().install(filters)

    def addFilter(self, filter):
        """Attach a single filter to the end of the current filter chain."""
        self.stream().attach(filter)


def expand(_data, _globals=None, _arguments=None, _config=None, **_locals):
    """Do an atomic expansion of the given source data, creating and
    shutting down an interpreter dedicated to the task.  The sys.stdout
    object is saved off and then replaced before this function
    returns."""
    if len(_locals) == 0:
        # If there were no keyword arguments specified, don't use a locals
        # dictionary at all.
        _locals = None
    output = NullFile()
    interpreter = Interpreter(output, globals=_globals, arguments=_arguments, 
                              config=_config)
    interpreter.ready()
    if interpreter.config.overrideProxy:
        oldStdout = sys.stdout
    try:
        result = interpreter.expand(_data, _locals)
    finally:
        interpreter.shutdown()
        if _globals is not None:
            interpreter.unfixGlobals() # remove pseudomodule to prevent clashes
        if interpreter.config.overrideProxy:
            sys.stdout = oldStdout
    return result

def info(table):
    DEFAULT_LEFT = 28
    maxLeft = 0
    maxRight = 0
    for left, right in table:
        maxLeft = max(maxLeft, len(left))
        maxRight = max(maxRight, len(right))
    FORMAT = '  %%-%ds  %%s\n' % max(maxLeft, DEFAULT_LEFT)
    for left, right in table:
        if right.find('\n') >= 0:
            for right in right.split('\n'):
                line = FORMAT % (left, right)
                assert len(line) <= 80, line
                sys.stderr.write(line)
                left = ''
        else:
            line = FORMAT % (left, right)
            assert len(line) <= 80, line
            sys.stderr.write(line)

def usage(verbose=True):
    """Print usage information."""
    programName = sys.argv[0]
    def warn(line=''):
        sys.stderr.write("%s\n" % line)
    warn(BANNER_TEXT % (programName, __version__))
    warn()
    warn("Valid options:")
    info(OPTION_INFO)
    if verbose:
        warn()
        warn("The following markups are supported:")
        info(MARKUP_INFO)
        warn()
        warn("Valid escape sequences are:")
        info(ESCAPE_INFO)
        warn()
        warn("The %s pseudomodule contains the following attributes:" % \
             Configuration.defaultPseudomoduleName)
        info(PSEUDOMODULE_INFO)
        warn()
        warn("The following environment variables are recognized:")
        info(ENVIRONMENT_INFO)
        warn()
        warn(USAGE_TEXT)
    else:
        warn()
        warn("Type %s -H for more extensive help." % programName)

def invoke(args):
    """Run a standalone instance of an EmPy interpeter."""
    # Initialize the options.
    _config = Configuration()
    _output = None
    _filespec = None
    _globals = None
    _preprocessing = []
    _postprocessing = []
    _binary = -1 # negative for not, 0 for default size, positive for size
    _hooks = []
    _relativePath = False
    _extraArguments = _config.environment(OPTIONS_ENV)
    if _extraArguments is not None:
        _extraArguments = _extraArguments.split()
        args = _extraArguments + args
    # Parse the command line arguments.
    pairs, remainder = getopt.getopt(args, 'VhHvkp:qm:frino:a:duBP:Q:I:D:S:E:F:', ['version', 'help', 'extended-help', 'verbose', 'null-hook', 'suppress-errors', 'prefix=', 'no-output', 'no-prefix', 'module=', 'flatten', 'raw-errors', 'interactive', 'no-override-stdout', 'binary', 'chunk-size=', 'output=' 'append=', 'output-binary=', 'append-binary=', 'preprocess=', 'postprocess=', 'import=', 'define=', 'define-string=', 'execute=', 'execute-file=', 'delete-on-error', 'pause-at-end', 'relative-path', 'no-callback-error', 'no-bangpath-processing', 'unicode', 'unicode-encoding=', 'unicode-input-encoding=', 'unicode-output-encoding=', 'unicode-errors=', 'unicode-input-errors=', 'unicode-output-errors='])
    for option, argument in pairs:
        if option in ('-V', '--version'):
            sys.stderr.write("%s version %s\n" % (__program__, __version__))
            return
        elif option in ('-h', '--help'):
            usage(False)
            return
        elif option in ('-H', '--extended-help'):
            usage(True)
            return
        elif option in ('-v', '--verbose'):
            _hooks.append(VerboseHook())
        elif option in ('--null-hook',):
            _hooks.append(Hook())
        elif option in ('-k', '--suppress-errors'):
            _config.exitOnError = False
            _config.doInteractive = True # suppress errors implies interactive
        elif option in ('-m', '--module'):
            _config.pseudomoduleName = argument
        elif option in ('-f', '--flatten'):
            _config.doFlatten = True
        elif option in ('-p', '--prefix'):
            _config.prefix = argument
        elif option in ('-q', '--no-output'):
            _output = NullFile()
        elif option in ('--no-prefix',):
            _config.prefix = None
        elif option in ('-r', '--raw-errors'):
            _config.rawErrors = True
        elif option in ('-i', '--interactive'):
            _config.doInteractive = True
        elif option in ('-n', '--no-override-stdout'):
            _config.overrideProxy = False
        elif option in ('-o', '--output'):
            _filespec = argument, 'w'
        elif option in ('-a', '--append'):
            _filespec = argument, 'a'
        elif option in ('--output-binary',):
            _filespec = argument, 'wb'
        elif option in ('--append-binary',):
            _filespec = argument, 'ab'
        elif option in ('-d', '--delete-on-error'):
            _config.deleteOnError = True
        elif option in ('--binary',):
            _binary = 0
        elif option in ('--chunk-size',):
            _binary = int(argument)
        elif option in ('-P', '--preprocess'):
            _preprocessing.append(('document', argument))
        elif option in ('-Q', '--postprocess'):
            _postprocessing.append(('document', argument))
        elif option in ('-I', '--import'):
            for module in argument.split(','):
                module = module.strip()
                _preprocessing.append(('import', module))
        elif option in ('-D', '--define'):
            _preprocessing.append(('define', argument))
        elif option in ('-S', '--define-string'):
            _preprocessing.append(('define-string', argument))
        elif option in ('-E', '--execute'):
            _preprocessing.append(('exec', argument))
        elif option in ('-F', '--execute-file'):
            _preprocessing.append(('file', argument))
        elif option in ('--pause-at-end',):
            _config.pauseAtEnd = True
        elif option in ('--relative-path',):
            _relativePath = True
        elif option in ('--no-callback-error',):
            _config.noCallbackIsError = False
        elif option in ('--no-bangpath-processing',):
            _config.processBangpaths = False
        elif option in ('-u', '--unicode'):
            _config.requireUnicode()
        elif option in ('--unicode-encoding',):
            _config.requireUnicode()
            _config.unicodeInputEncoding = _config.unicodeOutputEncoding = argument
        elif option in ('--unicode-input-encoding',):
            _config.requireUnicode()
            _config.unicodeInputEncoding = argument
        elif option in ('--unicode-output-encoding',):
            _config.requireUnicode()
            _config.unicodeOutputEncoding = argument
        elif option in ('--unicode-errors',):
            _config.requireUnicode()
            _config.unicodeInputErrors = _config.unicodeOutputErrors = argument
        elif option in ('--unicode-input-errors',):
            _config.requireUnicode()
            _config.unicodeInputErrors = argument
        elif option in ('--unicode-output-errors',):
            _config.requireUnicode()
            _config.unicodeInputErrors = argument
    # Now initialize the output file if something has already been selected.
    if _filespec is not None:
        if _output is not None:
            raise Error, "can't specify two outputs"
        _output = _config.open(*_filespec)
    # Set up the main filename and the argument.
    if not remainder:
        remainder.append('-')
    filename, arguments = remainder[0], remainder[1:]
    # Set up the interpreter.
    if _config.deleteOnError and _output is None:
        raise ValueError, "-d only makes sense with -o or -a arguments"
    if _config.prefix == 'None':
        _config.prefix = None
    if (_config.prefix and _config.isStringType(_config.prefix) and 
        len(_config.prefix) != 1):
        raise Error, "prefix must be single-character string"
    interpreter = Interpreter(output=_output, 
                              globals=_globals,
                              arguments=args,
                              config=_config,
                              filespec=_filespec)
    for hook in _hooks:
        interpreter.addHook(hook)
    try:
        # Execute any preprocessing commands.
        interpreter.process(_preprocessing)
        # Now process the primary file.
        interpreter.ready()
        if filename == '-':
            if not _config.doInteractive:
                name = '<stdin>'
                path = ''
                file = sys.stdin
            else:
                name, file = None, None
        else:
            name = filename
            file = _config.open(filename, 'r')
            path = os.path.split(filename)[0]
            if _relativePath:
                sys.path.insert(0, path)
        if file is not None:
            if _binary < 0:
                interpreter.wrap(interpreter.file, (file, name))
            else:
                chunkSize = _binary
                interpreter.wrap(interpreter.binary, (file, name, chunkSize))
        # Execute any postprocessing commands.
        interpreter.process(_postprocessing)
        # If we're supposed to go interactive afterwards, do it.
        if _config.doInteractive:
            interpreter.interact()
    finally:
        interpreter.shutdown()
    # Finally, handle any cleanup.
    _config.shutdown()
    sys.exit(_config.successCode)

def main():
    invoke(sys.argv[1:])

if __name__ == '__main__': main()
