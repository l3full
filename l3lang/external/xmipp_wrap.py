"""
Connection between external programs and l3.  Arguments to all
commands must be l3 values or None.

Externals are run as separate processes, and communication is
shell-style: files and command-line arguments.  

todo: External process wrappers are imported via import *, so new ones can
be added by moving them into this directory.

"""

from subprocess import Popen, PIPE, STDOUT
from l3lang.globals import l3_state_w, l3_running_in_gui, logger
from l3lang.ast import astType
import re, sys, select
from types import *

import os
ev = os.path.expandvars

#* Common functions.
def options_for_sh(options_d):
    '''
    Form xmipp-style single-dash options  (-h, -B <val>, etc.) from
    the `options_d` dict and return the string.
    '''
    opt_l = []
    for key, val in options_d.iteritems():
        opt_l.append("-%s %s" % (key, val))
    return " ".join(opt_l)



#*    Interactive commands
# These commands are run inside their own terminal, so they behave
# exactly as expected.
# 
# Here, use `xterm -e`, the simplest possible approach.
# The process sequence is thus gui -> xterm -> xmipp
# 
# For more control, and logging of output, could start a vte widget in
# the main gui, and only 
# spawn xmipp.

# 
#**        common functions
def run_cmd_g(name, cmd, ask_status = True):
    # 
    # The terminal command, run as separate process.
    # 
    # A full command sample is
    #       xterm -e /bin/sh -c '$SPXROOT/bin/xmipp_mark -i taguA.tot
    #       -tilted tagtA.tot ;  echo "Press enter to finish..." ; read ' 
    # Note the quoting for the shell; in the commands below, the ''
    # part is combined into `shell_arg`.
    # 
    # This command MUST be run from the l3gui because it uses
    #   - pop-ups
    #   - a terminal
    # and the command itself is graphical & interactive.
    #
    # ask_status:
    #       Because not all programs return proper exit status, the user is
    #       asked whether the program succeeded.
    #       
    if l3_running_in_gui:
        w_ = l3_state_w
    else:
        raise Exception("%s must be run from the graphical interface."
                        % name)

    vte_cmd = ["$SPXROOT/bin/sparx.python", "$SPXROOT/l3gui/bin/l3-vte", "-c"]
    vte_cmd = ["xterm", "-e", "/bin/sh", "-c"]

    # Wait for user exit confirmation after execution. This is
    # critical for handling failing programs. 
    post_cmd = ['; echo "Press enter to finish..."',
                "; read "]

    # Expand command and any environment variables.
    shell_arg = [(" ".join(cmd + post_cmd))]
    full_cmd = vte_cmd + shell_arg
    exp_cmd = map(ev, full_cmd)
    logger.info("Running command %s" % exp_cmd)

    # Run full command.
    p = Popen(exp_cmd, shell = False,
              stdin = PIPE,
              stdout = PIPE,
              stderr = STDOUT,
              close_fds = False)
    p.stdin.close()
    status = p.poll()                   
    while status is None:
        # Echo stdout/err in immediately.
        _, out, _ = select.select([], [p.stdout], [], 0.001) # windows?
        got_output = len(out) > 0
        if got_output:
            out_err = p.stdout.readline()
            print out_err
            sys.stdout.flush()

        # Update other events.
        w_.api.flush_events()

        # Continue.
        status = p.poll()

    # Collect stdout/err and status information.
    if status != 0:
        print "xterm exited with status ", status

    for line in p.stdout:
        print line,
    sys.stdout.flush()
    p.stdout.close()

    #       For external terminal.
    if status != 0:
        print "Original command (some quotes absent)`%s`" % (" ".join(full_cmd))
        print "Expanded command `%s`" % (" ".join(exp_cmd))
        sys.stdout.flush()
        raise Exception("External command failed.")

    #       For end-user command.
    if ask_status:
        answer = []
        if 1:
            import l3gui.widgets as W
            W.ExtProcConfirm(w_, w_.canvas.view, answer,
                             text = "Did the program finish successfully?")
        while len(answer) == 0:
            w_.api.flush_events()
        (status, log) = answer[0]

        if status != 0:
            raise Exception("%s failed." % name)
    else:
        log = "Completed."

    return (status, log)

# 
#**        xmipp_mark
def xmipp_mark(image, tilted = None):
    assert isinstance(image, astType)

    # The full application command line.
    if tilted is None:
        cmd = ['$SPXROOT/bin/xmipp_mark', '-i', image.py_string()]
    else:
        cmd = ['$SPXROOT/bin/xmipp_mark', '-i', image.py_string(),
               '-tilted', tilted.py_string()]

    return run_cmd_g('xmipp_mark', cmd)

# 
#**        xmipp_show
def show(**options):
    '''
    Usage:
        xw.show(vol = 'foo.vol')
    or
        xw.show(sel = 'foo.sel')
    '''

    # The full application command line.
    cmd = ['$SPXROOT/bin/xmipp_show', options_for_sh(options)]
    return run_cmd_g('xmipp_show', cmd, ask_status = False)


#*    Batch commands.
#**        Common functions.
def run_cmd(name, cmd, outfiles):
    '''
    Run external command `name` using command line `cmd`.  `outfiles`
    enumerates names of files generated by `cmd`.   
    '''

    # Run command.
    print "Running `%s`" % cmd
    sys.stdout.flush()
    p = Popen(cmd, shell = True,
              stdin = PIPE, 
              stdout = PIPE, stderr = STDOUT,
              close_fds = False) 
    p.stdin.close()
    # Regex for error messages in stderr 
    err_re = re.compile(r'^\d+:.*:', re.MULTILINE)
    status = p.poll()                   
    while status is None:
        # Echo stdout/err in immediately, IF the external command  flushes.
        _, out, _ = select.select([], [p.stdout], [], 0.001) # windows?
        got_output = len(out) > 0
        if got_output:
            out_err = p.stdout.readline()
            print out_err,
            sys.stdout.flush()

        # Update other events.
        if l3_running_in_gui:
            w_ = l3_state_w
            w_.api.flush_events()

        # Look for generic errors
        if got_output:
            error = err_re.search(out_err)
            if error != None:
                raise Exception(
                    "%s returned error without error status. "
                    "\nFull command was:\n%s"
                    "\nMessage:\n" "%s" % (name, cmd, out_err))
        status = p.poll()

    for line in p.stdout.readlines():
        print line,
    sys.stdout.flush()
    p.stdout.close()

    # Propagate non-zero status as exception.
    if status != 0:
        # Clean up generated files
        for fname in outfiles:
            try: os.remove(fname)
            except OSError: pass
        raise Exception("%s returned exit status %d." % (name, status))
    

def require_kwds(fname, optdict, keylist):
    '''Check that `optdict` contains all keys in `keylist`.'''
    missing = []
    for key in keylist:
        if optdict.has_key(key):
            continue
        missing.append(key)
    if missing:
        raise Exception("%s: missing required keyword argument(s): %s"
                        % (fname, missing))

def seq_to_docfile(dct):
    '''For the dictionary `dct`, convert sequence values to docfile
    sequences.  Return a new dict with the same keys.

    E.g. the entry foo : (1,2,'a') -> foo : "1 2 a"
    '''
    new = {}
    for key, val in dct.iteritems():
        if isinstance(val, (TupleType, ListType)):
            new[key] = " ".join(map(str, val))
        else:
            new[key] = val
    return new


# 
#**        xmipp_do_selfile
def xmipp_do_selfile(glob_pattern):
    '''
    Python usage:

        selfile = xmipp_do_selfile(glob_pattern)

    Original shell form: 

        xmipp_do_selfile GLOB_PATTERN > SELFILE

    '''
    # Form command
    w_ = l3_state_w

    selfile = "_l3_do_selfile-%d" % w_.state_.storage.new_id()
    cmd = 'xmipp_do_selfile "%(glob_pattern)s" > %(selfile)s' % locals()
    run_cmd('xmipp_do_selfile', cmd, [selfile])

    return selfile

do_selfile = xmipp_do_selfile

# 
#**        pdbphantom
def pdbphantom(pdb, **options):
    '''
        (vol, fourpr) = xw.pdbphantom("input-data/1P30.pdb",
                                      sampling_rate = 1.6,
                                      high_sampling_rate = 0.8,
                                      )
    '''
    # Form command.
    sh_opt_string = options_for_sh(options)
    pref = "_l3_pdbph-%d" % l3_state_w.state_.storage.new_id()
    cmd = 'xmipp_pdbphantom -i %(pdb)s -o %(pref)s ' \
          '%(sh_opt_string)s'% locals()
    (out_vol, out_fourier) = pref + '.vol', pref + '_Fourier_profile.txt'
    run_cmd('xmipp_pdbphantom', cmd, [out_vol, out_fourier])
    return (out_vol, out_fourier)

# 
#**        fourierfilter
def fourierfilter(vol, **options):
    '''
        filtvol = xw.fourierfilter(vol, 
                                   sampling = 1.6,
                                   low_pass = 10,
                                   fourier_mask = "raised_cosine 0.1",
                                   )
    '''
    # Form command.
    sh_opt_string = options_for_sh(options)
    out_vol = "_l3_fouri-%d" % l3_state_w.state_.storage.new_id()
    cmd = 'xmipp_fourierfilter -i %(vol)s -o %(out_vol)s ' \
          '%(sh_opt_string)s'% locals()
    run_cmd('xmipp_fourierfilter', cmd, [out_vol])
    return (out_vol)

# 
#**        normalize_vol
def normalize_vol(vol, **options):
    '''
    l3:
        normvol = xw.normalize_vol(vol)

    shell original:
        xmipp_normalize -vol -i out-ff.vol -o out-ff-n.vol
    '''
    # Form command.
    sh_opt_string = options_for_sh(options)
    out_vol = "_l3_normalize-%d" % l3_state_w.state_.storage.new_id()
    cmd = 'xmipp_normalize -vol -i %(vol)s -o %(out_vol)s ' \
          '%(sh_opt_string)s'% locals()
    run_cmd('xmipp_normalize', cmd, [out_vol])
    return (out_vol)

# 
#**        project_vol
def project_vol(**kwds):
    '''
    l3:
    (projsel,
     angledoc,
     projdesc) = xw.project_vol( volume = normvol,
                                 proj_dims = (65, 65),
                                 rot_range = (0, 360, 4, "random"),
                                 rot_noise = (10, 3),
                                 tilt_range = (55, 65, 4, "random_group"),
                                 tilt_noise = (10, 3),
                                 psi_range = (0, 360, 4, "random"),
                                 psi_noise = (10, 3),
                                 pix_noise = 20,
                                 cent_noise = 5,
                                 )

    shell original:
        cat > proj-desc <<EOF
        # Input volume description file or volume file
        ...
        EOF

        xmipp_project -i proj-desc -o proj.sel 

    '''
    require_kwds('project_vol', kwds, [
        "proj_dims",
        "rot_range",
        "rot_noise",
        "tilt_range",
        "tilt_noise",
        "psi_range",
        "psi_noise",
        "pix_noise",
        "cent_noise",
        ])
    # Convert tuples to docfile sequence, e.g. (1,2,'a') -> "1 2 a"
    kwds = seq_to_docfile(kwds)

    # Form prefix for projections.
    projpre = "_l3_proj-%d.d/" % l3_state_w.state_.storage.new_id()
    os.mkdir(projpre)

    # Form projection description file.
    projdesc = "_l3_projdesc-%d" % l3_state_w.state_.storage.new_id()
    locals_ = locals()
    locals_.update(kwds)
    pdf = open(projdesc, 'w')
    print >> pdf, '''
# Input volume description file or volume file
%(volume)s

# Projection prefix, first projection number (by default, 1) and extension
# (the two last are optional)
%(projpre)s 1 spi

# Y and X projection dimensions
%(proj_dims)s 

# Angle Definitions ------------------------------------------
# Externally generated angles
NULL

# Internally generated angles
# Rotational range: <init> <final> <samples> random
%(rot_range)s

# Tilting range: <init> <final> <samples> random_group
%(tilt_range)s

# Psi range: <init> <final> <samples> random
%(psi_range)s

# Noise description ------------------------------------------
# Noise (and bias) applied to rotational angle
%(rot_noise)s

# Noise (and bias) applied to tilting angle
%(tilt_noise)s

# Noise (and bias) applied to psi angle
%(psi_noise)s

# Noise (and bias) applied to pixels
%(pix_noise)s

# Noise (and bias) applied to particle center coordenates
%(cent_noise)s
    ''' % locals_
    pdf.close()

    # Form command.
    projsel = "_l3_proj-%d.sel" % l3_state_w.state_.storage.new_id()
    angledoc = "%s_movements.txt" % projpre

    cmd = 'xmipp_project -i %(projdesc)s -o %(projsel)s' % locals()
    run_cmd('xmipp_project', cmd, [projsel, angledoc, projdesc])

    return (projsel, angledoc, projdesc)
