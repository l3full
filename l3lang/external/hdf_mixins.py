# Additional Python functions to simplify the pytables hdf5 interface.
#
# These functions assume a very simple use of hdf5, with files holding
# only an array of images (as done by eman2).
# 
# Author:  Michael H. Hohn (mhhohn@lbl.gov)
#
# Copyright (c) 2006, The Regents of the University of California
# 
# See legal.txt and license.txt
# 

#*  Simplified hdf interface for l3, using pytables
try:
    import tables
except:
    l3_have_pytables = False
else:
    l3_have_pytables = True

    def l3_len(self):
        'Return the number of images.'
        return self.getNode('/MDF/images')._v_nchildren
    tables.file.File.l3_len = l3_len


    def l3_getimg(self, index):
        'Return the image at `index`.'
        return self.getNode('/MDF/images')._f_getChild(str(index))\
               ._f_getChild('image')
    tables.file.File.l3_getimg = l3_getimg


    def l3_getatts(self, index):
        ''' Return a (name, value) list holding all the attributes of
        image `index`.
        '''
        ind = str(index)
        attset = self.root.MDF.images._f_getChild(ind)._v_attrs
        return [ (att, attset.__getattr__(att)) for att in attset._f_list()]
    tables.file.File.l3_getatts = l3_getatts


    def l3_getatt(self, index, name):
        ''' Return the `name` attribute of the image at `index`.'''
        ind = str(index)
        attset = self.root.MDF.images._f_getChild(ind)._v_attrs 
        return attset.__getattr__(str(name))
    tables.file.File.l3_getatt = l3_getatt

