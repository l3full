"""
Connection between external programs and l3.  Arguments to all
commands must be l3 values or None.

Externals are run as separate processes, and communication is
shell-style: files and command-line arguments.  

External process wrappers are imported via import *, so new ones can
be added by moving them into this directory.

"""

from subprocess import Popen, PIPE, STDOUT
from l3lang.globals import l3_state_w
from l3lang.ast import astType
import re, sys, select

import os
ev = os.path.expandvars

#* Interactive commands
# These commands are run inside their own terminal, so they behave
# exactly as expected.


#* Batch commands.
#** Common functions.
def options_for_sh(options_d):
    opt_l = []
    for key, val in options_d.iteritems():
        if len(key) == 1:
            # Single-character, single-dash option (-h, -B<val>, etc.)
            opt_l.append("-%s%s" % (key, val))
        else:
            # Multi-character long option (--chains=ABE etc.)
            opt_l.append("--%s=%s" % (key, val))
    return " ".join(opt_l)

#** e2pdb2em
def e2pdb2em(pdb_in, **options):
    '''
    Python usage:
    out_mrc = e2pdb2em(input_pdb, **options)

    Original shell form: 
    e2pdb2em.py [options] input.pdb output.mrc
          --version            show program's version number and exit
          -h, --help           show this help message and exit
          -AAPIX, --apix=APIX  A/voxel
          -RRES, --res=RES     Resolution in A, equivalent to Gaussian
                               lowpass with 
                               1/e width at 1/res
          -BBOX, --box=BOX     Box size in pixels, <xyz> or <x>,<y>,<z>
          --het                Include HET atoms in the map
          --chains=CHAINS      String list of chain identifiers to include, eg
                               'ABEFG'
          --center=CENTER      center: c - coordinates, a - center of
                               gravity, n - no 
          --O                  use O system of coordinates
          --quiet              Verbose is the default
    '''                                 # '
    w_ = l3_state_w

    # Form command.
    sh_opt_string = options_for_sh(options)
    out_mrc = "e2pdb2em-out-%d" % w_.state_.storage.new_id()
    cmd = 'e2pdb2em.py %(sh_opt_string)s %(pdb_in)s %(out_mrc)s' % locals()

    # Run command.
    print "Running `%s`" % cmd
    sys.stdout.flush()
    p = Popen(cmd, shell = True,
              stdin = PIPE, 
              stdout = PIPE, stderr = STDOUT,
              close_fds = False) 
    p.stdin.close()
    status = p.poll()                   
    while status is None:
        # Echo stdout/err in immediately, IF the external command
        # doesn't buffer.  
        _, out, _ = select.select([], [p.stdout], [], 0.001) # windows?
        got_output = len(out) > 0
        if got_output:
            out_err = p.stdout.readline()
            print out_err,
            sys.stdout.flush()

        # Update other events.
        if l3_running_in_gui:
            w_.api.flush_events()

        status = p.poll()

    for line in p.stdout.readlines():
        print line,
    sys.stdout.flush()
    p.stdout.close()

    # Propagate non-zero status as exception.
    if status != 0:
        try: os.remove(out_mrc)
        except OSError: pass
        raise Exception("e2pdb2em returned exit status %d." % status)

    return out_mrc


    
