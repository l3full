#
# Author:  Michael H. Hohn (mhhohn@lbl.gov)
#
# Copyright (c) 2006, The Regents of the University of California
# 
# See legal.txt and license.txt
# 
import numpy
def movavg(vec, n):
    # A[t] = (V[t] + V[t-1] + ... + V[ t-n+1 ]) / n
    # t = N .. N-n
    assert len(vec.shape) == 1, 'Expecting vector argument.'
    assert n > 1, 'Averaging over less than 2 elements.'
    (N, ) = vec.shape
    assert N > n, 'Not enough samples for averaging.'
    # 
    ma = numpy.copy(vec) * numpy.nan
    for t in xrange(n - 1, N):
        ma[t] = 1.0 * sum(vec[t-n+1: t + 1]) / n
    return ma
