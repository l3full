# Additional Python functions to simplify the sparx python interface.
#
# Author:  Michael H. Hohn (mhhohn@lbl.gov)
#
# Copyright (c) 2006, The Regents of the University of California
# 
# See legal.txt and license.txt
# 
from EMAN2  import *

#* emdata.set
def set(self, **dct):
    '''Convenience interface function.
    Instead of
        proj.set_attr('ctf_applied', 0)
    use 
        proj.set(ctf_applied = 0, phi = angles[ii][0], ...)

    Instead of 
        ddata.set_attr_dict({'alpha': alpha, 'sx':sx, 'sy':sy, 'mirror': mirror})
    use
        ddata.set(alpha = alpha, ...)
    '''        
    self.set_attr_dict(dct)
EMData.set = set


#* Member lookup (emdata.foo)
def __getattr__(self, name):
    try:
        return self.get_attr(name)
    except:
        # Avoid interference with pickling, resulting in
        #     NotExistingObjectException at
        #     .../eman2/libEM/emdata_metadata.cpp:709: error with
        #     '__getinitargs__': 'Not exist' caught   
        raise AttributeError
EMData.__getattr__ = __getattr__

#* Suppress log output.
Log.logger().set_logfile('/dev/null')
