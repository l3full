;; Batch script to convert %.org to %.tex
;;
;; Used via     emacs --batch -q $< -l ./org-export-cust-tex.el
;; 
(load-library "~/emacs/lisp/org-export-latex.el")
(org-export-as-latex 3 'hidden)
