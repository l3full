;; Batch script to convert %.org to %.html, with external style sheet
;; and other options.
;;
;; Used via     emacs --batch -q $< -l ./org-export-cust.el
;; 
(load-library "~/emacs/lisp/org.elc")

(setq org-export-html-style
      (concat
       "<link rel=stylesheet href=\"l3style.css\" type=\"text/css\" media=\"screen\">\n"
       "<link rel=stylesheet href=\"l3style-print.css\" type=\"text/css\" media=\"print\">"
       ))

(org-export-as-html 3 'hidden)
