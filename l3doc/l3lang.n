.\"     Title: l3lang
.\"    Author: 
.\" Generator: DocBook XSL Stylesheets v1.72.0 <http://docbook.sf.net/>
.\"      Date: 04/09/2008
.\"    Manual: 
.\"    Source: 
.\"
.TH "L3LANG" "n" "04/09/2008" "" ""
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.SH "NAME"
l3lang \- the l3 data handling and programming language
.SH "SYNOPSIS"
The l3 language is an interpreted, lexically scoped language designed to automate much of the parameter and data handling of the type often encountered in experimental (scientific) computing. To accomplish this, l3 programs and all their data are persistent, and can be inspected at any time; further, the language maps one\-to\-one onto its graphical interface to make data traversal and inspection practical. See l3gui(1) for the interface description.
.sp
The l3 language is implemented as an interpreter written in Python; all Python modules can be directly imported and used from l3. Thus, algorithms without interesting intermediate data can be written in Python and called from l3 to provide a data interface for applications, and existing Python code can be used directly.
.sp
The l3 syntax follows Python's when possible, with only a few syntactic extensions for parameter, module, and documentation handling. As result, most l3 scripts are valid Python scripts and can be moved to Python if desired.
.sp
Only a subset of Python is repeated in l3; in particular, there are no classes.
.sp
The semantics of l3 are closer to a purely functional language. Everything in l3 is an expression, including computed values. l3 is designed as a single\-assignment language to permit reliable data tracking; overwriting of existing names is \fBstrongly\fR discouraged (but allowed for compatibility with existing Python scripts).
.sp
l3 introduces several new language features to support direct access to expressions and their values from a random\-access interface (see l3gui(1)); they are
.sp
.RS 4
\h'-04'\(bu\h'+03'Every expression is turned into an object by assignment of a permanent, unique id. Both the expression and the value(s) computed by the expression can be accessed via this id (using the gui, through the script's text).
.RE
.RS 4
\h'-04'\(bu\h'+03'The data flow graph of l3 scripts is a stacked dag, and can be generated from a script. In this view, named data are emphasized, and data interdependencies shown. Loops cause no \(lqexplosion\(rq of the data flow graph.
.RE
.RS 4
\h'-04'\(bu\h'+03'l3 scripts are lexically scoped, allowing for arbitrarily nested (static) namespaces and avoiding name collisions. Also, every function call introduces a new (dynamic) scope, so multiple calls to the same function do not produce naming conflicts, even when file names are used.
.RE
.RS 4
\h'-04'\(bu\h'+03'All scripts and their computed data are persistent. l3 is most useful when most data is should be kept. By keeping
\fIall\fR
data, file I/O need not be part of a script, keeping it simpler and cleaner. Exporting data to other programs
\fIafter\fR
script execution is simple and can be done interactively or by adding I/O commands where desired.
.RE
.RS 4
\h'-04'\(bu\h'+03'Evaluation of scripts is incremental (need\-based). An expression is only executed if it is out\-of\-date or newly inserted. This allows l3 scripts to
\fIevolve\fR; they can be added to without re\-running existing code, and references to all previouly computed data are available.
.RE
.RS 4
\h'-04'\(bu\h'+03'Values and scripts are the same. An l3 value is a valid l3 script. Therefore, computed values from prior scripts can be directly inserted into new scripts.
.RE
.SH "ENVIRONMENTS"
Names are bound in environments. Environments are lexically nested and dynamically formed on function entry. Scoping is lexical.
.sp
.SH "SEMANTICS"
The L3 semantics deal with the special cases of persistence and need\-based evaluation to make the language convenient to \fBuse\fR, at the cost of complicating the language.
.sp
When a new L3 program is executed the special cases do not affect evaluation, so a simple description of those semantics are given first, in GENERAL RULES.
.sp
Persistence and need\-based evaluation introduce retention and retrieval of computed values, and a mechanism based on timestamps to decide whether to interpret to get a new value or retrieve a previously computed value. The additional rules introduced by evaluation are covered in Timestamps and IDs.
.sp
Other special cases are listed after.
.sp
.PP
\fBGeneral rules\fR. Numbers and strings evaluate to their internal representation and return that. Nested expressions evaluate children, then self, and return this value. Lists evaluate from first to last element, and a list is returned. Programs evaluate from first to last entry, and the last computed value is returned.
Assignments are made in the enclosing environment, typically the enclosing Program, Map, or Function.
.sp
Name (Symbol) lookup is lexical, starting in the current environment and searching enclosing defining environments until a binding is found, if any.
.sp
A Map (dictionary) is a Program inside a nested environment; the Program is evaluated with bindings made in this environment, and the environment itself is returned.
.sp
.PP
\fBTimestamps and IDs\fR. Every expression has an associated numeric id and time stamp. The id is assigned once and never changes; every expression is uniquely identified via its id. There are three logical ages an expression may have: SETUP, EXTERN, and INTEGER. Corresponding to these are the actual stamps "SETUP", "EXTERN" and any positive integer. The ordering of these is SETUP < EXTERN < INTEGER.
During evaluation, timestamps increase monotonically; they are independent of system time. An expression with time SETUP is evaluated and its time updated to the current time (a positive integer).
.sp
An expression with time EXTERN is evaluated and no time stamp change is made.
.sp
A terminal expression with an integer time stamp is not evaluated at all; its prior value is retrieved and returned. The timestamp remains unchanged.
.sp
A nested expression E with an intger time stamp first evaluates its children. If any child's value is newer than E, it is evaluated again using the new values, and E's timestamp updated.
.sp
An expression of EXTERN age is assumed constant, and its time stamp never changes.
.sp
.PP
\fBSpecial forms\fR. Unlike other expressions in L3,
inline
is always evaluated, so its contents must be constant. For example
.PP
\fBExternal values\fR. Most application\-specific values are not recognized by L3 at all. They are treated as simple values by l3 and represented as text, using their Python
str()
form (which in turn is provided by the library implementing the value).
.SH "PYTHON COMPATIBILITY"
The following Python(1) constructs are not available in l3. The workarounds are straightforward and are valid in Python and l3.
.sp
.PP
\fBUngrouped tuple assignment\fR. Instead of
.sp
.RS 4
.nf
    psi, sx, sy, scale = compose_()
.fi
.RE
use
.sp
.sp
.RS 4
.nf
   (psi, sx, sy, scale) = compose_()
.fi
.RE
.PP
\fBlist assignments\fR. Instead of
.sp
.RS 4
.nf
    [psin, sxn, syn, mn] = combine_params2( ...)
.fi
.RE
use
.sp
.sp
.RS 4
.nf
    (psin, sxn, syn, mn) = combine_params2( ...)
.fi
.RE
.PP
\fBdictionary syntax\fR. The
{ key : val }
syntax is not available. Instead of
.sp
.RS 4
.nf
    {"negative":0, "mask":mask}
.fi
.RE
use
.sp
.sp
.RS 4
.nf
    dict(negative = 0, mask = mask)
.fi
.RE
.PP
\fBNo inline blocks\fR. The shortcuts
if 1: do_this
and
def f(a,b): return a+b
are not available. Use the long forms
.sp
.RS 4
.nf
    if 1:
        do_this
.fi
.RE
and
.sp
.sp
.RS 4
.nf
    def f(a,b):
        return a+b
.fi
.RE
instead.
.sp
.PP
\fBValues returned to l3 must pickle properly.\fR. There are several Python constructs that cannot be pickled and must not be used in l3 code.
Most generators will not pickle. In particular avoid xrange and use range instead.
.sp
Avoid using from foo import * in inline code. Importing this way is often a problem for pickling. For example, using
.sp
.sp
.RS 4
.nf
    from numpy import *
.fi
.RE
causes the error
.sp
.sp
.RS 4
.nf
    pickle.PicklingError: Can't pickle <type 'frame'>: it's not
    found as __builtin__.frame
.fi
.RE
and the session state will no longer save.
.sp
.SH "SEE ALSO"
l3(1), l3gui(1)
.sp
.SH "AUTHOR"
Michael Hohn, mhhohn@users.sf.net
.sp
.SH "COPYING"
Copyright \(co 2004\-8 Lawrence Berkeley National Laboratory. l3 is released under the BSD license. See license.txt for details.
.sp
