#
# Author:  Michael H. Hohn (mhhohn@lbl.gov)
#
# Copyright (c) 2006, The Regents of the University of California
# 
# See legal.txt and license.txt
#

from distutils.core import setup

# 
#* Work around package_data chopping bug (python-Bugs-1668596)
# 
import distutils.command.build_py as bpy
if bpy.__revision__ == "$Id: build_py.py 37828 2004-11-10 22:23:15Z loewis $":
    import os.path
    def get_data_files (self):
        """Generate list of '(package,src_dir,build_dir,filenames)' tuples"""
        data = []
        if not self.packages:
            return data
        for package in self.packages:
            # Locate package source directory
            src_dir = self.get_package_dir(package)

            # Compute package build directory
            build_dir = os.path.join(*([self.build_lib] + package.split('.')))

            # Length of path to strip from found files
            plen = len(src_dir)+1
            if plen == 1: plen = 0

            # Strip directory from globbed filenames
            filenames = [
                file[plen:] for file in self.find_data_files(package, src_dir)
                ]
            data.append((package, src_dir, build_dir, filenames))
        return data
    bpy.build_py.get_data_files = get_data_files

#* Setup 
setup(
    author='Michael Hohn',
    author_email='mhhohn@lbl.gov',
    description='The l3 graphical interface',
    name='l3gui',
    url='http://cci.lbl.gov/~hohn/l3/',
    version='0.3.1',

    package_dir = {'l3gui' : ''},
    packages=['l3gui', 'l3gui.cruft'],
    package_data={'l3gui': ['library.l3']},

    data_files = [('man/man1', ['doc/man/man1/l3gui.1']),
                  ],
    scripts=['bin/l3gui'],

    )


