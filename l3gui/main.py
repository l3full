#
# Author:  Michael H. Hohn (mhhohn@lbl.gov)
#
# Copyright (c) 2006, The Regents of the University of California
# 
# See legal.txt and license.txt
#

"""
Primary entry point for the l3 interface.

The main window is set up here, and all layout parameters are set. 

"""
import sys, operator, string, time, os
import thread
from types import *
from math import *
from copy import deepcopy, copy
from pprint import pprint
from  pdb import pm

from l3gui.cruft.pylab import Console, MessageLog

import pygtk
import pango
import gtk
import gobject
try:
    import gnomecanvas as canvas
    from gnomecanvas import MOVETO_OPEN, MOVETO, LINETO, CURVETO
except:
    # Try backported version used in sparx.
    import canvas
    from canvas import MOVETO_OPEN, MOVETO, LINETO, CURVETO

from l3lang import utils, reader, ast, view, interp
from l3lang.ast import empty_parent
from l3gui.misc import Selector, file_selection, save_state, load_state, \
     load_script, StdInOutErr, ConsoleNtbk
from l3gui import l3canvas, misc


#*    Data and window construction
w_ = utils.Shared()              # (w_)orld parameters
state_ = utils.Shared()          # l3 and pGraph state -- non-graphical.
w_.state_ = state_

#
#**        menu
# top-down construction mess:
# w_.menubar = gtk.MenuBar()
# w_.mb_ = utils.Shared()
# w_.mb_.file = gtk.MenuItem('File')
# w_.mb_.file.set_submenu()

w_.menubar             = gtk.MenuBar()
w_.menubar_            = utils.Shared()

#
#***            File
w_.menubar_.file_head  = gtk.MenuItem("File")
w_.menubar_.file       = gtk.Menu()
w_.menubar_.file_      = utils.Shared()

w_.menubar_.file_.scr_load = scr_load = gtk.MenuItem("import script")
scr_load.connect("activate",
                 lambda *args : file_selection(lambda name:
                                               load_script(w_, name)))

w_.menubar_.file_.save = save = gtk.MenuItem("save session to")
save.connect("activate",
             lambda *args : file_selection(lambda name: save_state(w_, name)))

w_.menubar_.file_.load = load = gtk.MenuItem("load session")
load.connect("activate",
             lambda *args : file_selection(lambda name: load_state(w_, name)))

def exit_to_console(*args):
    ''' Return to the calling Python process from the main loop.
    If started via `python -i`, this gives a toplevel.
    '''
    w_.stdinouterr.push()
    w_.stdinouterr.std_connection()
    gtk.main_quit()
w_.menubar_.file_.exit = ex = gtk.MenuItem("exit")
ex.connect("activate", exit_to_console)


w_.menubar_.file.add( w_.menubar_.file_.scr_load )
w_.menubar_.file.add( w_.menubar_.file_.load )
w_.menubar_.file.add( w_.menubar_.file_.save )
w_.menubar_.file.add( w_.menubar_.file_.exit )
w_.menubar_.file_head.set_submenu( w_.menubar_.file )
w_.menubar.add( w_.menubar_.file_head )

#
#***            Edit
w_.menubar_.edit_head  = gtk.MenuItem("Edit")
w_.menubar_.edit       = gtk.Menu()
w_.menubar_.edit_      = utils.Shared()
# 
w_.menubar_.edit_.insert_l3_console = tmp = gtk.MenuItem('l3 gui console')
tmp.connect("activate", (lambda *args: misc.add_l3appconsole(w_)) )
# 
w_.menubar_.edit_.insert_l3_console_term = tmp = gtk.MenuItem('l3 terminal console, one program/expr')
tmp.connect("activate", (lambda *args: l3cli_gui(w_)))
# 
w_.menubar_.edit_.i_consterm = tmp = gtk.MenuItem('l3 terminal console')
tmp.connect("activate", (lambda *args: misc.new_termconsole(w_)))
# 
w_.menubar_.edit.add( w_.menubar_.edit_.i_consterm )
w_.menubar_.edit.add( w_.menubar_.edit_.insert_l3_console )
w_.menubar_.edit.add( w_.menubar_.edit_.insert_l3_console_term )


w_.menubar_.edit_head.set_submenu( w_.menubar_.edit )
w_.menubar.add( w_.menubar_.edit_head )

#
#***            View
w_.menubar_.view_head  = gtk.MenuItem("View")
w_.menubar_.view       = gtk.Menu()
w_.menubar_.view_      = utils.Shared()
# 
w_.menubar_.view_.show_hide_console = gtk.MenuItem('show / hide python console')
w_.menubar_.view_.show_hide_console.connect(
    "activate",
    (lambda *args: misc.show_hide_console(w_)) )
w_.menubar_.view.add( w_.menubar_.view_.show_hide_console )
# 
w_.menubar_.view_.show_program_only = gtk.MenuItem('show program only')
w_.menubar_.view_.show_program_only.connect(
    "activate",
    (lambda *args: misc.show_program_only(w_)) )
w_.menubar_.view.add( w_.menubar_.view_.show_program_only )
# 
w_.menubar_.view_head.set_submenu( w_.menubar_.view )
w_.menubar.add( w_.menubar_.view_head )

# 
#***            Help
w_.menubar_.help_head  = gtk.MenuItem("Help")
w_.menubar_.help       = gtk.Menu()
w_.menubar_.help_      = utils.Shared()

w_.menubar_.help_.about_l3 = gtk.MenuItem('About l3')
def about_l3(*args):
    dialog = gtk.MessageDialog(
        None,
        gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
        gtk.MESSAGE_INFO,
        gtk.BUTTONS_OK,
        """L3 version 0.3.1 (alpha).
See http://l3lang.sf.net""")
    dialog.run()
    dialog.destroy()
w_.menubar_.help_.about_l3.connect("activate", about_l3)
w_.menubar_.help.add( w_.menubar_.help_.about_l3 )


w_.menubar_.help_.manual_l3 = gtk.MenuItem('l3 manual')
def manual_l3(*args):
    dialog = gtk.MessageDialog(
        None,
        gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
        gtk.MESSAGE_INFO,
        gtk.BUTTONS_OK,
        """Manual pages are available via
    man l3
and
    man l3gui
All documentation can be found at http://l3lang.sf.net, including HTML \
and PDF versions of the manual pages.
""")
    dialog.run()
    dialog.destroy()
w_.menubar_.help_.manual_l3.connect("activate", manual_l3)
w_.menubar_.help.add( w_.menubar_.help_.manual_l3 )


w_.menubar_.help_.license_l3 = gtk.MenuItem('l3 License')
def license_l3(*args):
    dialog = gtk.MessageDialog(
        None,
        gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
        gtk.MESSAGE_INFO,
        gtk.BUTTONS_OK,
        """Copyright (c) 2006, The Regents of the University of California, through Lawrence Berkeley National Laboratory.  All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

(1) Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

(2) Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

(3) Neither the name of the University of California, Lawrence Berkeley National Laboratory, U.S. Dept. of Energy nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

You are under no obligation whatsoever to provide any bug fixes, patches, or upgrades to the features, functionality or performance of the source code ("Enhancements") to anyone; however, if you choose to make your Enhancements available either publicly, or directly to Lawrence Berkeley National Laboratory, without imposing a separate written license agreement for such Enhancements, then you hereby grant the following license: a non-exclusive, royalty-free perpetual license to install, use, modify, prepare derivative works, incorporate into other computer software, distribute, and sublicense such enhancements or derivative works thereof, in binary and source code form.

""")
    dialog.run()
    dialog.destroy()
w_.menubar_.help_.license_l3.connect("activate", license_l3)
w_.menubar_.help.add( w_.menubar_.help_.license_l3 )


w_.menubar_.help_head.set_submenu( w_.menubar_.help )
w_.menubar.add( w_.menubar_.help_head )

#
#**        l3 init
#
#***            storage / root_env
storage = ast.RamMem('root_memory', 30000)  # Use lexically distinct values.
root_env = ast.Env('builtins', None, None, storage)

#
#***            def_env
tree = reader.parse("1")   #  The Program() for insertion in def_env.
def_env = root_env.new_child(tree)
tree.setup(empty_parent(), def_env, storage)
interp.setup_envs(storage, root_env, def_env)
state_.add_bindings(locals(), ['storage', 'def_env', 'root_env'])

#
#***            console (toplevel env)
# See also l3lang/l3.py.
from l3lang import repl
console, interpreter = repl.get_console_interpreter_pair(def_env, storage)
def_env.import_external('console', console)
def_env.import_external('interp', interpreter)

# 
#***            Console entry point, no threads
def l3cli():
    console.interact()

# 
#***            Console entry point, threaded
def root_display(program):
    def _do(*args):
        # Don't add display functions to TxtConsole (as done in
        # l3Console), but use a separate class / function.

        # Extract body from Program. 
        #     body = program.body()
        #     program._primary[0].detach_child(body._id, self._storage)
        #     del program

        # Display node.
        body_l3pic = w_.canvas.view.start_add_l3tree(program)
    gobject.idle_add(_do, ())

def l3cli_gui(w_):
    '''Produce one program per entered expression. '''
    w_.stdinouterr.push()
    w_.stdinouterr.std_connection()

    gtk.threads_enter()
    thread.start_new_thread(console.interact, (), {"display_via": root_display})
    gtk.threads_leave()
    # After thread finishes: 
    ## w.stdinouterr.pop()
    # >> stdinouterr needs a thread lock...
    # >> only one console at a time...

#
#**        external access
from l3lang.globals import *
l3_set_gui(w_)
#**        Control logging
logger.set_threshold('debug')
logger.set_threshold('warning')         # default
logger.set_threshold('message')


w_.api = utils.Shared()
w_.api.flush_events = misc.flush_events

#
#**        options flags
w_.opts = utils.Shared()
w_.opts.lang = utils.Shared()
w_.opts.gui = utils.Shared()
w_.opts.gui.native_console = True       # For developer use / debugging

# # #
# # #**        comment handling
# # w_.comment_table_ = {}                  # tree id -> Comment
w_.pprint_width = 80             # Line width goal for pretty-printer.

#
#**        decoration (header / comment)  handling
w_.deco_table_ = {}                     # tree id -> Comment

#
#**        display choice table
w_.layout_table_ = {}                   # (tree id -> direction) map
                                        # direction is 'h' or 'v'
#
#**        selection handling
w_.selector = Selector(w_)


#
#**        main canvas (right)
#***            display parameters
cp_ = w_.cp_ = utils.Shared()           # canvas parameters

cp_.drag_state = (None, ())             # (drag state, drag start)
                                        # starting position (x,y) when
                                        # drags cross event handlers

cp_.display_file_content = True         # Try to display file content
                                        # instead of names.   

cp_.off_x = 1000     # logical origin = real origin + offset (world units)
cp_.off_y = 50
cp_.paper = utils.Shared()
cp_.paper.width = 3 * 8.5                       # inches (units)
cp_.paper.height = 9 * 11
cp_.margin = 2.5                        # testing
cp_.margin = 0.0                        # inches
cp_.margin = 0.05                        # inches

cp_.width = 400                         # MINIMUM pixel sizes for viewport. 
cp_.height = 400


# Font size is in points (1/72 inch); pixel size depends on resolution.
# cp_.screen_dpi = 300                    # jsb paper
cp_.screen_dpi = 96
cp_.view_dpi = 300
cp_.print_dpi = 72
cp_.font_size = 10                      # ps printer points (always 72 dpi)

if sys.platform == 'win32':
    cp_.viewlist_head_font = "Verdana"
    cp_.comment_font = "Verdana Italic"
    cp_.comment_font = "Verdana"
    cp_.canvas_font_family = "Lucida Console"
    cp_.rawtext_font = "Lucida Console"
    cp_.label_font = "Lucida Console"
    cp_.label_font_desc = "Verdana Italic"
    cp_.py_console_font = "Lucida Console 9"

else:
    cp_.viewlist_head_font = "Sans"
    cp_.comment_font = "Sans Italic"
    cp_.comment_font = "Serif"
    cp_.canvas_font_family = "Monospace"
    cp_.rawtext_font = "Monospace"
    cp_.label_font = "Monospace"
    cp_.label_font_desc = "Sans Italic"
    cp_.py_console_font = "Monospace 9"
    
# testing
am = "Andale Mono"
cp_.canvas_font_family = am
cp_.rawtext_font = am
cp_.label_font = am
cp_.label_font_desc = "Verdana Italic"
cp_.py_console_font = am + "-9"


cp_.font_size_comments = 14        # ps printer points (always 72 dpi)
cp_.font_size_labels = 10           # ps printer points (always 72 dpi)
cp_.font_height = 17                    # line height
cp_.font_em = 10                        # width of em, points
cp_.font_ex = 10                        # height of ex, points
cp_.font_hex = 1.0 * cp_.font_height / cp_.font_ex

cp_.deco = utils.Shared()
# cp_.deco.outline_color = 0xA3A35C00     # jsb paper
cp_.deco.outline_color = 0xE3E38C00     # beige
# cp_.deco.outline_width = 1.0/cp_.font_size * 5 # jsb paper
cp_.deco.outline_width = 1.0/cp_.font_size * 2 # approximate stem width

cp_.deco.foo = 'test'

cp_.deco.emph_color = utils.Shared()
cp_.deco.emph_color.valuelist = 0x0F9EF300 # light blue
cp_.deco.emph_color.filelist = 0x0F9EF300

cp_.deco.background_color = cp_.deco.outline_color

cp_.outline_width_normal = 1            # pixels
cp_.outline_width_emph = 3              # pixels
cp_.edge_width = 3                      # pixels
cp_.colors = ("red",
              "yellow",
              "green",
              "cyan",
              "blue",
              "magenta")

## cp_.marker_width = 22                      # too wide
cp_.marker_width = 11                      # units
## cp_.marker_height = 1                     # units
cp_.marker_height = 0.0                     # units
cp_.marker_thickness = 0.2                # units
cp_.marker_vgap = 0.5                     # units
cp_.marker_color = "grey80"
cp_.marker_padding = 0.5                # units 

cp_.tuple_marker_width = 0.0            # units
cp_.tuple_marker_height = 6.0           # units

# list / placeholder expander parameters
cp_.symbol_width = 0.7                  # units
cp_.symbol_padding = 0.4                # units

cp_.loop_cond_sep = 1                    # units

cp_.label_indent = 4                    # units
# cp_.marker_width = cp_.label_indent - 0.5 # clearly separate markers
#                                         # -- quite ugly  
# cp_.label_fill_color = "#ceceff"        # bluish
# cp_.label_fill_color = "#f0e0e0"        # brown / reddish
cp_.label_fill_color = "white"
cp_.label_outline_color = "white"

cp_.exp_marker_width = 0.6              # units
cp_.exp_marker_height = 0.6             # units
cp_.exp_marker_hoff = 0.3               # units
cp_.exp_marker_voff = 0.3               # units

cp_.placeholder = utils.Shared()
cp_.placeholder.dimen = 1.0                          # units
cp_.placeholder.color = "brown"                      # units
cp_.placeholder.padding = 0.5                          # units

cp_.textview = utils.Shared()
cp_.textview.width = 10                 # units
### cp_.textview.height = 1.3               # units
cp_.textview.height = 1.0 * cp_.font_height / cp_.font_ex # world units
cp_.textview.outline_padding = 1.0      # pixels

cp_.inline = utils.Shared()
cp_.inline.width = 20                   # units
cp_.inline.height = 1.0 * cp_.font_height / cp_.font_ex * 5 # world units
cp_.inline.outline_padding = 3.0        # pixels

cp_.image_natural_size  = True
cp_.image_width  = 15                   # image goal and maximum size,
cp_.image_height = 15                   # world units      
cp_.image_scale = 1.0

cp_.text_display = utils.Shared()
cp_.text_display.width = 10                   # units
cp_.text_display.height = 1                   # units
cp_.text_display.outline_padding = 3.0        # pixels

cp_.highlight_thickness = 0.2           # world units
# cp_.highlight_padding = 0.0             # jsb paper
cp_.highlight_padding = 0.5             # units

cp_.hover_magnification = 4             # factor

cp_.item_detach_distance = 2       # units

cp_.macro_call = utils.Shared()
cp_.macro_call.width = 10               # Box dimensions, in characters (em)
cp_.macro_call.height = 3               #

cp_.macro_group = utils.Shared()
cp_.macro_special = utils.Shared()

cp_.header_outline_width = 1            # pixels

#
#***            instance setup
w_.canvas = utils.Shared()
from l3gui.l3canvas import l3Canvas
w_.canvas.view = l3Canvas(w_)


#**        tree selection canvas (left)
#
w_.lib_canvas = utils.Shared()
w_.lib_canvas.view = nc = l3Canvas(w_)
import imp
w_.gui_home = imp.find_module("l3gui")[1]  # Absolute path as string.
gobject.idle_add(lambda : misc.load_library(
    w_, 
    os.path.join(w_.gui_home, "library.l3"),
    maxdepth = 2))


#**        toolbar
#***            zoom spinner
w_.toolbar = utils.Shared()
def spinner_for(widget):
    spinner_adj = gtk.Adjustment(1.0,       # default
                                 0.1, 5.0,  # min, max
                                 0.1,       # step increment
                                 1.0, 1.0)  # page increment, page size
    spinner = gtk.SpinButton(spinner_adj, 0.0, 1)

    def zoom_canvas(spinbutton, view):
        factor = spinbutton.get_value()

        # Use middle of window as anchor point.
        anchor_x = view.get_allocation().width / 2    # pixels (canvas coords.)
        anchor_y = view.get_allocation().height / 2

        view.centered_zoom(factor)

    spinner.connect("value-changed", lambda a: zoom_canvas(a, widget))
    return spinner

w_.toolbar.zoom_work = spinner_for(w_.canvas.view)

w_.toolbar.zoom_reference = spinner_for(w_.lib_canvas.view)

## spinner.disconnect(scb)

#
#***            actual toolbar
w_.toolbar.toolbar = tb = gtk.Toolbar()

zm_ref = gtk.ToolItem()
zm_ref.add(w_.toolbar.zoom_reference)
tb.insert(zm_ref, -1)

zm_item = gtk.ToolItem()
zm_item.add(w_.toolbar.zoom_work)
tb.insert(zm_item, -1)

#
#**        status (mode) line
w_.statusbar = gtk.Statusbar()

def ten_second_message(text):
    w_.statusbar.push(0, text)
    def clean_statusbar():
        w_.statusbar.pop(0)
        return False
    gobject.timeout_add(10000, clean_statusbar)
w_.ten_second_message = ten_second_message


#**        Consoles.
w_.notebk_consoles = ConsoleNtbk(w_)
#
#***            parameters
w_.consoles = utils.Shared()
w_.consoles.proportion_y = 0.33    # Proportion of the available area.


#
#***            console
w_.console = Console(w_, globals(),
                     quit_handler = 
                     lambda *args:
                     w_.notebk_consoles.destroy_page("Python Console")
                     )
w_.console.banner()
#
#***            message log
if not w_.opts.gui.native_console:
    w_.message = MessageLog(w_)
    w_.message.banner()

#
#***            combine
w_.notebk_consoles.add_page(w_.console, "Python Console")
if not w_.opts.gui.native_console:
    w_.notebk_consoles.add_page(w_.message, "Messages")

    cs = w_.notebk_consoles
    cs.set_current_page(cs.page_index("Messages"))

#**  boost pickle preparation; preliminary imports
# sparx writes logfiles on import, so we don't import it
# # Preload boost functions required to restore eman2 pickles.
try:
    import sparx, EMAN2
except:
    pass

#**        stdin/out/err
w_.stdinouterr = StdInOutErr(w_)

# 
#***            Redirect to w_.message
if not w_.opts.gui.native_console:
    w_.stdinouterr.push()
    w_.message.std_to_widget()


#*    Stack the widgets
# canvases.
w_.hpane = gtk.HPaned()

w_.lib_canvas.scroll_bin = gtk.ScrolledWindow()
w_.lib_canvas.scroll_bin.add(w_.lib_canvas.view)
w_.hpane.pack1(w_.lib_canvas.scroll_bin, resize = False)

w_.canvas.scroll_bin = gtk.ScrolledWindow()
w_.canvas.scroll_bin.add(w_.canvas.view)
w_.hpane.pack2(w_.canvas.scroll_bin, resize = True)


# # if 1:
# #     label = gtk.Label("Python Console")
# #     label.set_padding(2, 2)
# #     w_.notebk_consoles.append_page(w_.console, label)
# # if 1:
# #     label = gtk.Label("Messages")
# #     label.set_padding(2, 2)
# #     w_.notebk_consoles.append_page(w_.message, label)

w_.vpane = gtk.VPaned()
w_.vpane.pack1(w_.hpane, resize = True)
w_.vpane.pack2(w_.notebk_consoles, resize = False)

w_.vbox = gtk.VBox()
w_.vbox.pack_start(w_.menubar, expand = False)
w_.vbox.pack_start(w_.toolbar.toolbar, expand = False)
w_.vbox.pack_start(w_.vpane)
w_.vbox.pack_start(w_.statusbar, expand = False)

#
#*    and put them in the main window.
w_.window = gtk.Window()
w_.window.set_property("title", "L3 interface")
w_.window.connect("delete-event", gtk.main_quit)
w_.window.set_border_width(0)
w_.window.add(w_.vbox)

#*    Run
w_.window.show_all()

#*    Post-display adjustments.
#**        w_.hpane
left = w_.hpane.get_property("min-position")
right = w_.hpane.get_property("max-position")
# Set pane position as fraction.  
w_.hpane.set_property("position",
                      ((3 - 1) * left + right)/3)
#**        w_.vpane
if w_.opts.gui.native_console:
    # no consoles:
    # top = w_.vpane.get_property("min-position")
    bot = w_.vpane.get_property("max-position")
    w_.vpane.set_property("position", bot)
else:
    misc.show_consoles(w_)


def start():
    gobject.threads_init()
    gtk.main()
    # sys.exit(0)

if __name__ == "__main__":
    start()
