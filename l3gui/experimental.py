#
# Author:  Michael H. Hohn (mhhohn@lbl.gov)
#
# Copyright (c) 2006, The Regents of the University of California
# 
# See legal.txt and license.txt
#

import l3gui.main

#*    Experimenting:
from l3lang.ast import *
from l3lang.interp import *
from l3lang.repl import *
from l3gui.main import *
from l3gui.cruft.pylab import *
from l3gui.widgets import *
from l3gui.l3canvas import *
from l3gui.misc import *
from l3gui.misc import _get_props, _set_props, _safed
from l3lang.globals import *

from pickle import dumps
import pdb, os
from pdb import pm
from pprint import pprint

import sys
import pdb
def enter_debug(type, value, traceback):
    pdb.post_mortem(traceback)

if 0:
    sys.excepthook = enter_debug

# reset via
if 0:
    sys.excepthook = sys.__excepthook__

def inline(st):
    exec(st, globals())

# NO: gtk.threads_init()
gobject.threads_init()
gtk.main()


import profile, pstats
prof = profile.Profile()
prof.run('gtk.main()')

ps = pstats.Stats(prof)
ps.strip_dirs()
ps.sort_stats('cumulative').print_stats(40)
ps.sort_stats('time', 'stdname').print_stats(40)

# Function ...  was called by
ps.print_callers()

# Function ... called
ps.print_callees()

#*    Misc. tests:
fail_here

in running gnome-terminal inside the phenix setup:

    (gnome-terminal:979): Pango-WARNING **: No builtin or dynamically
    loaded modules were found. Pango will not work correctly. This
    probably means there was an error in the creation of:
    '/net/ribbon/scratch1/phenix/phenix-1.22a/build/intel-linux-2.4/python/etc/pango/pango.modules'
    You may be able to recreate this file by running
    pango-querymodules.

Trying this gives

    # Pango Modules file
    # Automatically generated file, do not edit
    #
    # ModulesPath = /net/ribbon/scratch1/phenix/phenix-1.22a/build/intel-linux-2.4/python/lib/pango/1.4.0/modules
    #

which is a nonexistent file..

#
#*    state handling
#**        save
foo = save_state(w_, '/tmp/foo.state')

#
#**        view
# VERY useful for debugging: tree structure, plain text!
pprint(utils.as_list(get_current_state(w_)))

#
#**        load
bar = load_state(w_, '/tmp/foo.state')
gtk.main()

pprint(utils.file_unpickle('/tmp/bar.1'))


#
#*    Iterators
#**        find all instances of l3If
lc = w_.lib_canvas.view
print lc._nodes
print filter(lambda x: isinstance(x, l3If), lc._nodes)

t_if = filter(lambda x: isinstance(x, l3If), lc._nodes)[0]
print t_if._d_cond_marker.get_property("points")

t_set = filter(lambda x: isinstance(x, l3Set), lc._nodes)
for set in t_set:
    print set._d_lhs_marker.get_property("points")




#
#*    Persistence
utils.file_pickle(tree)
#**        iterators
itr = iter([1,2,3])
for ii in itr:
    print ii

itr = iter([1,2,3])
fn = utils.file_pickle(itr)

#
#*    tree focus
w_.node_tree.view.widget.expand_to_path( w_.node_tree.view.start ) # yes
w_.node_tree.view.widget.row_activated(w_.node_tree.view.start, label_col )
# the cursor must be released somehow...
w_.node_tree.view.widget.set_cursor( w_.node_tree.view.start ) # no
w_.node_tree.view.widget.scroll_to_cell( w_.node_tree.view.start ) # no

#

#*    canvas-embedded text, running program
raw = list(w_.canvas.view._nodes)[1]

print raw._textview.get_events()

# Inert widget.
print raw._textview.get_property("sensitive")
raw._textview.set_property("sensitive", False)


print raw._ltext.get_bounds()
buff = raw._textview.get_buffer()
print buff.get_text(*buff.get_bounds())
raw._loutline.hide()




#*    canvas-embedded text widget
#**        setup
view = w_.canvas.view
ra = w_.canvas.view.root().add

c_root = ra(canvas.CanvasGroup)
c_text = gtk.TextView()
c_text.get_buffer().set_text("short text")

if 0:
    c_label = ra(canvas.CanvasWidget,
                 anchor = gtk.ANCHOR_NORTH_WEST,
                 # size_pixels = False,
                 # height = 2,
                 # width = 6,

                 size_pixels = True,
                 height = 20,
                 width = 200,

                 widget = c_text,
                 x = 15,
                 y = 15,
                 )
else:
    c_label = c_root.add(canvas.CanvasWidget,
                         anchor = gtk.ANCHOR_NORTH_WEST,
                         # size_pixels = False,
                         # height = 2,
                         # width = 6,

                         size_pixels = True,
                         height = 20,
                         width = 200,

                         widget = c_text,
                         x = 15,
                         y = 15,
                         )

c_label.show()
c_text.show()
if 0:
    c_root.hide()
    c_label.hide()
    c_text.hide()
print c_label.get_bounds()
#
#**        tags
#***            setup
buff     = c_text.get_buffer()
print [ii.get_offset() for ii in buff.get_bounds()]

tag_call = buff.create_tag()
tag_size = buff.create_tag( family = "Monospace")

buff.apply_tag(tag_size, *buff.get_bounds() )
buff.apply_tag(tag_call, *buff.get_bounds() )

print tag_call.get_priority()
print tag_size.get_priority()

#
#***            events
def tag_event(texttag, widget, event, iter):
    print event.type
    if event.type == gtk.gdk.BUTTON_PRESS:
        if event.button == 1:
            print "at: ", iter.get_offset()
            texttag.set_property("weight", pango.WEIGHT_HEAVY)

    elif event.type == gtk.gdk.BUTTON_RELEASE:
        if event.button == 1:
            texttag.set_property("weight", pango.WEIGHT_NORMAL)

    return False

tag_call.connect("event", lambda *a: tag_event(*a))

#
#***            dynamic property adjust
print tag_size.get_property("size-points")
tag_size.set_property("size-points", 12)

#
#**        buffer events
def re_tag_inserted_text(buffer, iter, text, length):
    iter_to = iter.copy()
    if not iter.backward_chars(length):
        return False

    if iter.has_tag(tag_size):
        print "no tagging needed"
        return False

    buff.apply_tag(tag_size, iter, iter_to )
    return False

buff.connect_after("insert-text", lambda *a: re_tag_inserted_text(*a))


#
#**        pango font adjust -- whole widget
# font_desc = c_text.get_pango_context().get_font_description()
# print font_desc.get_size()       # 10240 -- 10 pt font
# font_desc.set_size(fs_default / 10)

#*    canvas work
#
#**        canvas content
nds = w_.canvas.view._nodes

pprint(nds)
gtk.main()


#
#**        canvas text
view = w_.canvas.view
ra = w_.canvas.view.root().add
c_label = ra(canvas.CanvasText,
             x = 12, y = 12,
             size_points = 12 * 1,
             scale = 1.0,
             scale_set = True,
             text = "the test",
             )
# c_label.set_property("scale", 0.5)
print c_label.get_property("size-set")
flush_events()
#***            scale test
# The item only moves; no font size change.
def affine_scale(sx, sy):
    return (sx, 0.0,  0.0, sy,  0.0, 0.0)

c_label.affine_relative(affine_scale(0.2, 0.2))
flush_events()


#**        canvas text
#***            init
view = w_.canvas.view
ra = w_.canvas.view.root().add
c_label = ra(canvas.CanvasText,
             x = 12, y = 15,
             size = 10 * pango.SCALE,
             scale = 1.0,
             scale_set = True,
             size_set = True,
             text = "the test",
             )
#
#***            Resize test.
c_label.set_property("size", 15 * pango.SCALE) # Resize.  Good.
flush_events()

# c_label.affine_relative( (0.5, 0.0, 0.0, 0.5, 0.0, 0.0) ) # Only moves...
# flush_events()

#
#***            Moving item test.
# Drag region is the bounding box.  Easy to select.
class foo:
    pass

self = foo()

def drag_event(item, event):
    if event.type == gtk.gdk.BUTTON_PRESS:
        if event.button == 1:
            # sm: move group start
            self.remember_x = event.x
            self.remember_y = event.y
            return True

    elif event.type == gtk.gdk.MOTION_NOTIFY:
        if event.state & gtk.gdk.BUTTON1_MASK:
            #
            # Get the new position and move by the difference.
            #
            # sm: move group
            new_x = event.x
            new_y = event.y
            dx = new_x - self.remember_x
            dy = new_y - self.remember_y
            item.move(dx, dy)
            self.remember_x = new_x
            self.remember_y = new_y
            return True

    return False
print c_label.connect("event", drag_event)

#
#***            Font change test.
c_label.set_property("font", "monospace")    # Font style name
c_label.set_property("size", 12 * pango.SCALE) #
flush_events()

#
#***            Color change test.
c_label.set_property("fill-color", "red")
flush_events()

#

#**        CanvasText speed test
#***            create
view = w_.canvas.view
rg = w_.canvas.view.root().add(canvas.CanvasGroup)
ra = rg.add
def add(ii):
    c_label = ra(canvas.CanvasText,
                 x = 2 + (ii % 10) * 7,
                 y = 5 + ii / 10,
                 size = 10 * pango.SCALE,
                 scale_set = True,
                 scale = 1.8,           # ignored...
                 font = "monospace",
                 size_set = True,
                 text = "txt%d" % ii,
                 )
    return c_label
text_l = [add(ii) for ii in xrange(0,1000)]
flush_events()

#
# Zoom does change position, but not size.
#
#***            change font and size
# Slow!:
for ii in text_l:
    ii.set_property("font", "monospace")
    ii.set_property("size", 12 * pango.SCALE)
    flush_events()

# Fast:
for ii in text_l:
    ii.set_property("font", "monospace")
    ii.set_property("size", 8 * pango.SCALE)
flush_events()

# test
#   "scale" is ignored (maybe try cairo backend?)
for ii in text_l:
    ii.set_property("scale", 1.8)
    ii.set_property("scale_set", True)
flush_events()

#
#***            remove
# Fast:
rg.destroy()
flush_events()

#**        canvas text tags
view = w_.canvas.view

raw = l3Rawtext(view.w_, view)
## raw.destroy()

buff     = raw._ltext.get_buffer()
tag_call = buff.create_tag()
print tag_call.get_priority()
tag_call.set_priority(0)

def tag_event(texttag, widget, event, iter):
    print event.type
    # useless:         iter.get_offset() is always 0 for mouse buttons
    # never signalled: gtk.gdk.ENTER_NOTIFY, LEAVE_NOTIFY
    if event.type == gtk.gdk.BUTTON_PRESS:
        if event.button == 1:
            print "at: ", iter.get_offset()
            texttag.set_property("weight", pango.WEIGHT_HEAVY)

    elif event.type == gtk.gdk.BUTTON_RELEASE:
        if event.button == 1:
            texttag.set_property("weight", pango.WEIGHT_NORMAL)

    return False

print tag_call.get_property("size-set")
tag_call.set_property("size", view._pixpu_base * 10)
# tag_call.set_property("size-set", False)

tag_call.connect("event", lambda *a: tag_event(*a))

# To avoid:
#     Text reverts to old size after apply_tag...
#     buff.apply_tag(raw._tag_size, *buff.get_bounds() )
# The sequence create, edit, esc, apply_tag works...

raw._focus_dummy.grab_focus()

# raw._text_orig = buff.get_text(*buff.get_bounds())
buff.set_text( raw._text_orig )

buff.apply_tag(tag_call, *buff.get_bounds() )
# beg, _ = buff.get_bounds()
# end = beg.copy()
# end.forward_chars(4)
# beg.forward_chars(1)
# buff.apply_tag(tag_call, beg, end )

buff.apply_tag(raw._tag_size, *buff.get_bounds() )

# buff.get_tag_table().foreach(lambda tag, _: pprint(tag))

#
#**        text handling, raw



#
#**        pango font adjustments
# Using l3Rawtext
view = w_.canvas.view
ra = w_.canvas.view.root().add

font_desc = view.get_pango_context().get_font_description()
font_desc.set_stretch(pango.STRETCH_NORMAL)
fs_default = font_desc.get_size()       # 10240 -- 10 pt font
font_desc.set_size(fs_default / 10)

print font_desc.get_size()

# Any effect?
c_label = ra(canvas.CanvasRichText,
             x = 10,
             y = 10,
             width = 10,
             height = 5,
             text = "NEW SAMPLE TEXT",
             )
c_label.show()
## c_label.hide()
c_label.get_buffer()


#
#**        edges and nodes
w_.canvas.view
pprint(w_.canvas.view._nodes)
pprint(w_.canvas.view._edges)

pprint(w_.canvas.view._pgraph._nodes)
pprint(w_.canvas.view._pgraph._edges)

(w_.canvas.view._edges[0]).lower_to_bottom()


w_.canvas.view._nodes[(2, 'root')].get_bounds()

#
#**        canvas anti-aliasing
view = w_.canvas.view
view.get_property('aa')
view.set_property('aa', True)           # only at init time.

# canvas color
view = canvas.Canvas(aa = True)
view.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("grey5") )

#
#**        canvas item zoom, affine
view = w_.canvas.view
rt = list(view._nodes)[0]._ltext


#
#**        canvas zoom
view = w_.canvas.view

# n.a.
# view.set_center_scroll_region(True)
view.set_pixels_per_unit(view._pixpu * 1.6)
gtk.main()

# pixel units:
print view.get_scroll_offsets()               # pixels
print view.get_scroll_region()                # world
view.scroll_to
view.set_scroll_region

# Get widget size
wc = view.get_allocation().width        # pixels
hc = view.get_allocation().height

#   Get the display size.
cxw, cyw = view.c2w(wc/2, hc/2)

view.world_to_window(0, 0)
view.window_to_world(400, 400)

# world units
view.get_size()
view.get_size_request()

view.parent.children()
view.parent.get_child_visible

# No good:
# >>> view.parent
# <gtk.HBox object (GtkHBox) at 0x40d6e8c4>
# >>> view.parent.get_parent_window().get_size()
# (774, 785)




# Don't:
# c_label.destroy()
#
#    GnomeCanvas-ERROR **: file gnome-canvas-rich-text.c: line 1596
#    (blink_cb): assertion failed: (text->_priv->layout)

# Huge font...
c_label = ra(canvas.CanvasRichText,
             x = 2, y = 2,
             width = 10 * cp_.font_size, height = 1 * cp_.font_size,
             text = "sample text",
             )

#
#**        line drawing
ra = w_.canvas.view.root().add
edge = ra(
    canvas.CanvasLine,
    fill_color = 'black',
    first_arrowhead = 0,
    last_arrowhead = 0,
    line_style = gtk.gdk.LINE_SOLID,
    width_pixels = cp_.outline_width_normal,
    points = [ 0,0, 20,20 ],            # scaled units.
    )

point_l = (edge).get_property('points') # by value..

#
#**        motion via properties:
(edge).set_property('points', [ 0, 0, 20, 10])

(edge).set_property('points', [ 0, 0, 20, 5])

#

#*    canvas rich text
canv = w_.canvas.view

#
#**        focus dummy
focus_dummy = w_.canvas.view.root().add(canvas.CanvasItem)
focus_dummy.hide()
focus_dummy.grab_focus()

#
#**        the group
label_root = w_.canvas.view.root().add(canvas.CanvasGroup)

#
#**        the text
label_text = label_root.add(canvas.CanvasRichText,
                            x = 20,
                            y = 10,
                            width = 50,
                            height = 20,
                            grow_height = False,
                            ### grow_height = True,
                            editable = True,
                            cursor_visible = False,
                            cursor_blink = False,
                            )

#
#**        the outline.
print label_text.get_bounds()
x1, y1, x2, y2 = label_text.get_bounds()
x1, y1 = canv.c2w(int(x1), int(y1))
x2, y2 = canv.c2w(int(x2), int(y2))

label_rect = label_root.add(canvas.CanvasRect,
                            x1 = x1, y1 = y1,
                            x2 = x2, y2 = y2,
                            fill_color = 'green',
                            outline_color = 'blue',
                            width_pixels = cp_.outline_width_normal,
                            )

label_rect.lower(1)
label_rect.show()

#
#**        event handlers
def tag_event_1(texttag, widget, event, iter):
    if event.type == gtk.gdk.BUTTON_PRESS:
        if event.button == 1:
            print "tag_event_1: ", texttag
    return False

def tag_event(texttag, widget, event, iter):
    # useless:         iter.get_offset() is always 0 for mouse buttons
    # never signalled: gtk.gdk.ENTER_NOTIFY, LEAVE_NOTIFY
    if event.type == gtk.gdk.BUTTON_PRESS:
        if event.button == 1:
            print "at: ", iter.get_offset()
            texttag.set_property("weight", pango.WEIGHT_HEAVY)

    elif event.type == gtk.gdk.BUTTON_RELEASE:
        if event.button == 1:
            texttag.set_property("weight", pango.WEIGHT_NORMAL)

    return False

def select_event(item, event, data = None):
    if event.type == gtk.gdk.BUTTON_PRESS:
        print 'button!'
        ### item.grab_focus()
        if event.button == 3:
            item.hide()
        return False
    return False

#
#**        tags
# Creation order has no effect on size override.

buff     = label_text.get_buffer()
buff_text = "sample text"
buff.set_text(buff_text)

# Highlight.
gi = buff.get_iter_at_offset
ct = lambda color: buff.create_tag( background = color,
                                    size = int(10 * canv._pixpu_base),
                                    family = "Monospace",
                                    )
tag_call = map(ct, ["blue", "grey90"])

ii = buff_text.find(" ", 0)

# Size
tag_size = buff.create_tag( size = int(10 * canv._pixpu_base),
                            family = "Monospace",
                            )
### No text, serious delays:
### tag_size = buff.create_tag( size = 10 * pango.SCALE,
###                             family = "Monospace")

# Apply them.
#     Application order has no effect on override.
### buff.apply_tag(tag_size, *buff.get_bounds() )
### buff.apply_tag(tag_call[0],  gi(2), gi(ii))

### # The overlapping region has NO tag's text size.
### buff.apply_tag(tag_size,     gi(0), gi(4) )
### buff.apply_tag(tag_call[0],  gi(2), buff.get_bounds()[1])

buff.apply_tag(tag_size,     gi(0), gi(4) )
buff.apply_tag(tag_call[0],  gi(4), gi(7))
buff.apply_tag(tag_call[1],  gi(7), buff.get_bounds()[1])


# jj = buff_text.find(" ", ii + 1)
# if jj == -1:
#     jj = len(buff_text)
#
# buff.apply_tag(tag_call[1],
#                gi(ii), gi(jj))

print tag_size.get_priority()
print tag_call[0].get_priority()

# no effect on size...
# tag_size.set_priority(buff.get_tag_table().get_size() - 1)

if 0:
    tag_size.set_property("size", canv._pixpu_base * 1.2)

#
#**        Event connection.
tag_size.connect("event", lambda *a: tag_event(*a))
tag_call[0].connect("event", lambda *a: tag_event_1(*a))
tag_call[1].connect("event", lambda *a: tag_event_1(*a))

label_root.connect("event", select_event)




# No effect:
### canv.set_pixels_per_unit(canv._pixpu)
# ugly hack:
canv._pixpu = (1.00001 * canv._pixpu)
canv.set_pixels_per_unit(canv._pixpu)

#
#**        affine adjustment
# The affine transform changes the box bounds, but not the font size...
# label_text.affine_relative( (0.1, 0.0,
#                           0.0, 0.1,
#                           0.0, 0.0) )

# print label_text.get_bounds()
# print label_text.i2c_affine(tuple(range(0,6)))
# print label_text.i2w_affine(tuple(range(0,6)))

# label_text = label_root.add(canvas.CanvasRichText,
#              x = 40, y = 40,
#              width = 10 * cp_.font_size, height = 1 * cp_.font_size,
#              text = "sample text",
#              )
#
#**        tried and discarded

### No effect (events not received?):
### label_root.connect("event", select_event)
### label_root.connect("event", focus_event)


### massive delays when zooming:
### tag_size = buff.create_tag(# size = 12 * w_.canvas.view._pixpu,
###                            size_points = 12,
###                            )

### core dump after zoom:
### tag_size = buff.create_tag(# size = 12 * w_.canvas.view._pixpu,
###                            scale = 0.1,
###                            )

gtk.main()


#*    minimal window sample
def zoom_spin_button():
    spinner_adj = gtk.Adjustment(2.500,
                                 0.1, 5.0,
                                 0.1,
                                 1.0, 1.0)

    spinner = gtk.SpinButton(spinner_adj, 0.001, 3)

    # Minimal gtk window
    l_window = gtk.Window()
    l_window.connect("delete-event", lambda *args: l_window.destroy())
    l_window.set_border_width(0)
    l_window.add(spinner)
    l_window.show_all()

zoom_spin_button()
gtk.main()


#

#*    AST details
aa = String("""
first
second
""")
aa.source_string()                      # no_source...

aa = reader.parse("""
'''
first
second
'''
""").body()
print aa.source_string()                      # no_source...
print aa.l3_string()

#

#*    program formation from gui;  evaluation test.
def info(prog):
    print
    view.print_info(storage, prog, show_macros = True)

pgraph = w_.canvas.view._pgraph
chains, free_nodes = pgraph.identify_programs()
print "Free nodes found: ", free_nodes  # modeline / popup?

print "Chains found: ",
pprint(chains)

print "============================================"
print "Chain links:"
[info(storage.load(foo)) for foo in chains[0] ]


programs = [pgraph.chain_to_program(ch,
                                    w_.state_.def_env,
                                    w_.state_.storage,
                                    ) for ch in chains]
print "============================================"
print "     Program:"
info(programs[0])


print "============================================"
print "    Evaluation:"
value = programs[0].interpret(def_env, storage)

#*    bpath testing
#**        lines
cnvs = w_.canvas.view
from canvas import MOVETO_OPEN, MOVETO, LINETO, CURVETO, END

ls_path = canvas.path_def_new([(MOVETO, 10, 10),
                               (LINETO, 20, 30),
                               (LINETO, 30, 40),
                               (LINETO, 40, 45),
                               (END,)
                               ])
ls_item = cnvs.root().add(canvas.CanvasBpath,
                          width_units = 0.3,
                          outline_color = "black",
                          fill_color = "green",
                          )
ls_item.set_bpath(ls_path)
#***            Display
flush_events()
gtk.main()
#***            Scale
def affine_scale(sx, sy):
    return (sx, 0.0,  0.0, sy,  0.0, 0.0)

ls_item.affine_relative(affine_scale(0.7, 0.3))
flush_events()

ls_item.affine_relative(affine_scale(1/0.7, 1/0.3))
flush_events()


# cleanup
ls_item.destroy()

#
#**        curves
cnvs = w_.canvas.view
from canvas import MOVETO_OPEN, MOVETO, LINETO, CURVETO, END

def test_path(path):
    ls_path = canvas.path_def_new(path)
    ls_item = cnvs.root().add(canvas.CanvasBpath,
                              width_units = 1.0/w_.cp_.font_size * 2,
                              outline_color = "black",
                              fill_color = "green",
                              cap_style = gtk.gdk.CAP_ROUND,
                              join_style = gtk.gdk.JOIN_ROUND,
                              )
    ls_item.set_bpath(ls_path)
    
    ll, tt, rr, bb = canvas_item_get_bounds_world(ls_item)
    ls_item.move( 10 + w_.cp_.off_x - ll,
                  10 + w_.cp_.off_y - tt,
                  )
    gtk.main()
    ls_item.destroy()


#
#***            corner
#
# Rounded corner, leaving the bezier control points on the tangent lines.
#
#         p1 X
#            |
#            |
#            |
#         p2 O
#            |
#            +---O---------X
#                p3       p4
path = [(MOVETO, 0, 0), # p1
        (CURVETO,
         0, 8,
         2, 10,
         10,10,
         ),
        # (END,)   # close
        ]
test_path(path)

#
#***            rounded corner rectangle
# rectangle w/ rounded corners, leaving the bezier control points on
# the tangent lines.
# Note: using one definition, say on [0,1]x[0,1] and scaling will also
# scale the rounded edges, but these must have absolute size. [ie. the
# rounding size must be independent of the rectangle size.]
#

def path_rectangle_rounded(el, er,
                           et, eb,      # edge positions
                           cv, ch,      # corner point indentation
                           sv, sh,      # spline control point indentation
                           ):
    assert (el < er)
    assert (et < eb)

    assert ((er - el) > 2*ch)
    assert ((eb - et) > 2*cv)

    path = [(MOVETO, el, et + cv),

            (LINETO, el, eb - cv),
            (CURVETO ,                      # bottom left
             el      , eb - sv,
             el + sh , eb,
             el + ch , eb,
             ),

            (LINETO, er - ch, eb),          # bottom right
            (CURVETO  ,
             er -  sh , eb,
             er       , eb - sv,
             er       , eb - cv,
             ),

            (LINETO, er, et + cv),
            (CURVETO ,                      # top right
             er      , et + sv ,
             er - sh , et,
             er - ch , et,
             )       ,

            (LINETO, el + ch, et),
            (CURVETO ,                      # top left
             el + sh , et,
             el      , et + sv,
             el      , et + cv,
             ),
            ]
    return path

el = 0.0 + 20
er = 10 + 20
eb = 10 + 20
et = 0 + 20

cv = 1
ch = 1

sh = 0.2
sv = 0.2

test_path(path_rectangle_rounded(el, er, et, eb,
                                 cv, ch, sv, sh))

test_path(path_rectangle_rounded(5, 10, 5, 20,
                                 1, 1, 0.3, 0.3))

# 
#**        path group
# 
cnvs = w_.canvas.view
from canvas import MOVETO_OPEN, MOVETO, LINETO, CURVETO, END

class AttrDict:
    def __init__(self, dct):
        self.__dict__.update(dct)
# 
#***            no good
# 
# Scaling is expected to widen or heighten the object, retaining the
# left upper corner.  For this to happen with a simple call to
# affine_relative(), the upper left of the original must be at 0,0,
# and the drawing must be in the positive x and y directions.

# The problem here is insufficient use of CanvasGroups.

def draw_path(path, width_w, **props):
    # This drawing order shows nothing.

    # Draw the path. 
    ls_path = canvas.path_def_new(path)
    ls_rg = cnvs.root().add(canvas.CanvasGroup)
    ls_item = ls_rg.add(canvas.CanvasBpath, **props)
    ls_item.set_bpath(ls_path)

    # Reposition.
    flush_events()
    ll, tt, rr, bb = canvas_item_get_bounds_world(ls_item)
    ls_item.move( -ll, -tt)

    # not this order.
    # Scale to size.
    flush_events()
    sx = sy = width_w / (rr - ll)
    print "scale: ", sx,sy
    ## ls_item.affine_relative( (sx, 0.0,  0.0, sy,  0.0, 0.0) )

    flush_events()
    ll, tt, rr, bb = canvas_item_get_bounds_world(ls_item)
    ls_item.move( 20 + w_.cp_.off_x - ll,
                  10 + w_.cp_.off_y - tt,
                  )
    return AttrDict(locals())


def draw_path(path, width_w, **props):
    # Draw the path. 
    ls_path = canvas.path_def_new(path)
    ls_rg = cnvs.root().add(canvas.CanvasGroup)
    ls_item = ls_rg.add(canvas.CanvasBpath, **props)
    ls_item.set_bpath(ls_path)

    # Reposition.
    flush_events()
    ll, tt, rr, bb = canvas_item_get_bounds_world(ls_item)
    ls_item.move( 20 + w_.cp_.off_x - ll,
                  10 + w_.cp_.off_y - tt,
                  )

    # Scale using the local coordinates (the group is unchanged).
    sx = sy = width_w / (rr - ll)
    ls_item.affine_relative( (sx, 0.0,  0.0, sy,  0.0, 0.0) )

    return AttrDict(locals())

# 
#***            good: proper affine nesting.

#     @@ name the groups.
#   distinct nested groups for group, center, scale and
#   translation. 

def draw_path(path, width_w, **props):
    # Get groups.
    grp_trans = cnvs.root().add(canvas.CanvasGroup)
    grp_scale = grp_trans.add(canvas.CanvasGroup)
    grp_obj   = grp_scale.add(canvas.CanvasGroup)

    # Draw the path. 
    ls_path = canvas.path_def_new(path)
    ls_item = grp_obj.add(canvas.CanvasBpath, **props)
    ls_item.set_bpath(ls_path)

    # Reposition for scaling around center.
    # flush_events()
    ll, tt, rr, bb = canvas_item_get_bounds_world(grp_obj)
    grp_obj.move( -(ll + rr)/2, -(tt + bb)/2 )

    # Scale.
    # flush_events()
    ll, tt, rr, bb = canvas_item_get_bounds_world(grp_scale)
    sx = sy = width_w / (rr - ll)
    grp_scale.affine_relative( (sx, 0.0,  0.0, sy,  0.0, 0.0) )

    # Translate for visibility.
    # flush_events()
    ll, tt, rr, bb = canvas_item_get_bounds_world(grp_trans)
    grp_trans.move( 20 + w_.cp_.off_x - ll,
                    10 + w_.cp_.off_y - tt,
                    )

    return AttrDict(locals())


# 
#***            triangle right
path = [
    (MOVETO ,  0, 0 ),
    (LINETO ,  20, 20 ),
    (LINETO ,  0, 40 ),
    # (LINETO ,  0, 0 ),
    (END,)
    ]
# 
#***            triangle down
path = [
    (MOVETO, 0,0),
    (LINETO, 41,0),
    (LINETO, 21,20),
#    (LINETO, 0,0),  # use end to close (avoid irregularities)
    (END,)
    ]
# 
#***            draw
dct = draw_path(path,
                1,
                width_units = 1.0/w_.cp_.font_size * 2,
                outline_color = "black",
                fill_color = "black",
                cap_style = gtk.gdk.CAP_ROUND,
                join_style = gtk.gdk.JOIN_ROUND,
                )

w_.dct = dct
flush_events()

# Transforms.
print w_.dct.ls_item.i2w_affine(tuple(range(0,6)))
print w_.dct.grp_trans.i2w_affine(tuple(range(0,6)))

# Group tests.
w_.dct.grp_scale.affine_relative( (3, 0.0,  0.0, 3,  0.0, 0.0) ) # scale
w_.dct.grp_trans.move(2,2)

w_.dct.grp_trans.destroy()


#*    l3List test
cnvs = w_.canvas.view
list_ = l3List(w_, cnvs)

cnvs.display_bounding_box( list_._head.get_bounds())
list_.hide()

# list_._root_group.connect("event", lambda *a: list_.drag_event(*a))
# list_._head._root_group.connect("event", lambda *a: list_.drag_event(*a))
# list_._alist._root_group.connect("event", lambda *a: list_.drag_event(*a))

#*    bounding box testing
def bounds_world(item):
    x, y, u, v = item.get_bounds()
    return item.i2w(x,y) +  item.i2w(u,v)

# Outline bounds != outline corners.  Why?
print "bounds"
print rawt._loutline.get_bounds()
print [rawt._loutline.get_property(prop) for prop in ['x1', 'y1', 'x2', 'y2']]

def bbox_test():
    self__lgroup = cnvs.root().add(canvas.CanvasGroup)

    self__textview = gtk.TextView()
    self__ltext = self__lgroup.add(canvas.CanvasWidget,
                                   size_pixels = False,
                                   height = 1.5, ### params
                                   width = 20,

                                   widget = self__textview,
                                   x = 10,
                                   y = 10,
                                   )
    x1, y1, x2, y2 = self__ltext.get_bounds() # world units
    x1 -= 5 / cnvs._pixpu
    y1 -= 5 / cnvs._pixpu
    x2 += 5 / cnvs._pixpu
    y2 += 5 / cnvs._pixpu

    self__loutline = self__lgroup.add(
        canvas.CanvasRect,
        x1 = x1, y1 = y1,
        x2 = x2, y2 = y2,
        fill_color = 'white',
        outline_color = 'black',
        width_pixels = cp_.outline_width_normal,
        )
    st = utils.Shared()
    st.__dict__.update( locals())
    return st
st = bbox_test()

print "st bounds"
print st.self__loutline.get_bounds()
print [st.self__loutline.get_property(prop)
       for prop in ['x1', 'y1', 'x2', 'y2']]
# Off already.

#################################
def bbox_test():
    self__lgroup = cnvs.root().add(canvas.CanvasGroup)

    self__loutline = self__lgroup.add(
        canvas.CanvasRect,
        x1 = 10, y1 = 11,
        x2 = 20, y2 = 21,
        fill_color = 'white',
        outline_color = 'black',
        width_pixels = cp_.outline_width_normal,
        )
    st = utils.Shared()
    st.__dict__.update( locals())
    return st
st = bbox_test()

print "st bounds"
print st.self__loutline.get_bounds()
print [st.self__loutline.get_property(prop)
       for prop in ['x1', 'y1', 'x2', 'y2']]
# Off already.

#################################
def bbox_test():
    self__lgroup = cnvs.root().add(canvas.CanvasGroup)

    self__loutline = self__lgroup.add(
        canvas.CanvasRect,
        x1 = 10, y1 = 11,
        x2 = 20, y2 = 21,
        fill_color = 'white',
        outline_color = 'black',
        width_pixels = 1,
        )
    return utils.Shared(**locals())
st = bbox_test()

print "st bounds"
print st.self__loutline.get_bounds()
print [st.self__loutline.get_property(prop)
       for prop in ['x1', 'y1', 'x2', 'y2']]
# Off already.

print st.self__ltext.get_bounds()

#################################

x, y, u, v = bounds_world(rawt._loutline)
print u-x, v-y

print "group bounds"
print rawt._root_group.get_bounds()
x, y, u, v = bounds_world(rawt._root_group)
print u-x, v-y
print bounds_world(rawt._root_group)

print "text bounds"
print rawt._ltext.get_bounds()
x, y, u, v = bounds_world(rawt._ltext)
print u-x, v-y
print bounds_world(rawt._ltext)

print "world"
print rawt._loutline.i2w_affine(tuple(range(0,6)))
print rawt._root_group.i2w_affine(tuple(range(0,6)))

print "canvas"
print rawt._loutline.i2c_affine(tuple(range(0,6)))
print rawt._root_group.i2c_affine(tuple(range(0,6)))


#*    line testing
cnvs = w_.canvas.view
l_line = cnvs.root().add(canvas.CanvasLine,
                         fill_color = "black",
                         points = [10,1,
                                   20,2,
                                   10,3,
                                   20,5,
                                   ],
                         width_units = 0.2,
                         line_style = gtk.gdk.SOLID,
                         cap_style = gtk.gdk.CAP_ROUND,
                         join_style = gtk.gdk.JOIN_ROUND,
                         smooth = False,
                         spline_steps = 12,
                         )

l_line = cnvs.root().add(canvas.CanvasLine,
                         fill_color = "blue",
                         points = [14,1,
                                   24,2,
                                   14,3,
                                   24,5,
                                   ],
                         width_units = 0.2,
                         line_style = gtk.gdk.SOLID,
                         cap_style = gtk.gdk.CAP_ROUND,
                         join_style = gtk.gdk.JOIN_ROUND,
                         smooth = True,
                         spline_steps = 2,
                         )
print l_line.get_property('points')
flush_events()

#*    polygon testing
cnvs = w_.canvas.view
l_line = cnvs.root().add(canvas.CanvasPolygon,
                         fill_color = "black",
                         points = [10,1,
                                   20,2,
                                   10,3,
                                   20,5,
                                   ],
                         width_units = 0.2,
                         cap_style = gtk.gdk.CAP_ROUND,
                         join_style = gtk.gdk.JOIN_ROUND,
                         )

l_line = cnvs.root().add(canvas.CanvasPolygon,
                         fill_color = "black",
                         points = [20, 20,
                                   40, 20,
                                   40, 40,
                                   20, 40,
                                   20, 20,
                                   ],
                         width_units = 0.2,
                         cap_style = gtk.gdk.CAP_ROUND,
                         join_style = gtk.gdk.JOIN_ROUND,
                         )

print l_line.get_bounds()

print l_line.get_property('points')
flush_events()


#*    canvas ast insertion / display
tree = reader.parse("[d,e]")
tree = reader.parse("function = { |x,y| x - y }")

tree = reader.parse("""
[ function = not_yet({ |x,y| x - y }),
  macro = not_yet([ |x,y| z = x - y ]),
  raw_expression = a - b,
  list = [],
  inline_python = '''
from operator import *
from l3lang.test.functions import axpy
''',
  program = [],
]
""")
tree.setup(empty_parent(), def_env, storage)
view.print_info(storage, tree)
w_.lib_canvas.view.start_add_l3tree(tree)
flush_events()

#*    pango font work
pcon = w_.lib_canvas.view.get_pango_context()
pcon.list_families()
pcon.list_families()[0].get_name()
pcon.get_font_description()
print pcon.get_font_description().get_size() / pango.SCALE

# Select font.
sz = 20
sz_orig = 10
font_desc = pcon.get_font_description().copy()
font_desc.set_family("monospace")
font_desc.set_size(sz_orig * pango.SCALE)

# Set new canvas default.
pcon.set_font_description(font_desc)

# Get metrics.
def dump_metrics():
    font = pcon.load_font(font_desc)
    metrics = font.get_metrics()
    #    Font sizes are in pango_units.
    #    1 point == PANGO_SCALE  pango_units
    #    1 pango_point = 1/72 inch
    fheight = (metrics.get_ascent() +  metrics.get_descent())
    fwidth = metrics.get_approximate_digit_width()
    #     print fwidth / pango.SCALE
    #     print fheight / pango.SCALE
    return fwidth, fheight
    ## logi, ink = font.get_glyph_extents("m")

def lines(st):
    l1 = st.count('\n')
    if st[-1] == '\n':
        # No content in last line.
        l1 -= 1
    if len(st) > 0:
        l1 += 1
    return max(0, l1)

def width(st):
    return reduce(max, [len(line) for line in st.split('\n')], 0)

#
#**        Sizing using test string.
view = w_.canvas.view
ra = w_.canvas.view.root().add
c_string = "01234\n" * 8
c_label = ra(canvas.CanvasText,
             x = 12, y = 25,
             text = c_string,
             font_desc = font_desc,
             )
## c_label.destroy()
flush_events()

#
#**        Set 1 cu = 1 ex
l1, t1, r1, b1 = c_label.get_bounds()
print "x: cu / char", (r1 - l1) / width(c_string)
print "y: cu / char", (b1 - t1) / lines(c_string)
print view._pixpu_base
print view._pixpu
cu_p_char = (r1 - l1) / width(c_string)
view._pixpu = view._pixpu * cu_p_char
view.set_pixels_per_unit(view._pixpu )


xoff = 30
yoff = 70
#
#***            Draw long string.
lng_str = "the test string,\nusing two lines"
lng_label = ra(canvas.CanvasText,
               x = xoff, y = yoff,
               anchor = gtk.ANCHOR_NORTH_WEST,
               text = lng_str,
               font_desc = font_desc,
               )
## lng_label.destroy()
#   Verify canvas size ratio...
l1, t1, r1, b1 = lng_label.get_bounds()
print "x: cu / char", (r1 - l1) / width(lng_str)
print "y: cu / char", (b1 - t1) / lines(lng_str)

#   Compare to font metric ratio...
wid, hei = dump_metrics()
print "metric h / w", 1.0*hei/wid

#
#***            Draw substring at proper position.
# sub_str = "test"
# sub_start = lng_str.find(sub_str)
sub_top = 1
sub_lef = 6
sub_str = lng_str.split('\n')[sub_top][sub_lef:]
sub_label = ra(canvas.CanvasText,
               anchor = gtk.ANCHOR_NORTH_WEST,
               fill_color = "blue",
               x = xoff, y = yoff,
               text = sub_str,
               font_desc = font_desc,
               )
sub_label.move(sub_lef, 1.0 * sub_top / wid * hei)
## sub_label.destroy()
#   Verify size ratio...
l1, t1, r1, b1 = sub_label.get_bounds()
print "x: cu / char", (r1 - l1) / width(sub_str)
print "y: cu / char", (b1 - t1) / lines(sub_str)

#   Compare to font metric ratio...
wid, hei = dump_metrics()
print "metric h / w", 1.0*hei/wid


#
#**        resizing tests
#***            Set new font sizes.
fsize = sz * pango.SCALE
font_desc.set_size(fsize)
c_label.set_property('size', fsize)
lng_label.set_property('size', fsize)
sub_label.set_property('size', fsize)
flush_events()
#***            Set 1 cu = 1 ex (horizontal)
l1, t1, r1, b1 = c_label.get_bounds()
print "x: cu / char", (r1 - l1) / width(c_string)
print "y: cu / char", (b1 - t1) / lines(c_string)
print view._pixpu
cu_p_char = (r1 - l1) / width(c_string)
view._pixpu = view._pixpu * cu_p_char
view.set_pixels_per_unit(view._pixpu )
flush_events()
#***            Check vertical [cu]
l1, t1, r1, b1 = c_label.get_bounds()
print "x: cu / char", (r1 - l1) / width(c_string)
print "y: cu / char", (b1 - t1) / lines(c_string)
#***            Compare font metrics (points) at new sizes
wid, hei = dump_metrics()
print "metric h / w", 1.0*hei/wid

#
#**        Find matching font metric.
wid, hei = dump_metrics()
h_o_w = 1.0 * hei / wid
print "metric h / w", h_o_w

sz_bnds = range(4, 41)
print "pt.    height/     "
print "size   widtht      "
print "-------------------"
for sz in sz_bnds:
    fsize = sz * pango.SCALE
    font_desc.set_size(fsize)
    wid, hei = dump_metrics()
    sz_hw = 1.0 * hei / wid
    print "%-6d %.5f" % (sz, sz_hw)



#*    tree & clone structure
tw = TreeWork(storage)
loads = storage.load
printi = lambda xx: print_info(storage, xx)
printai = lambda xx: print_all_info(storage, xx)

# id from first  Symbol('abserr') inside If
mid = 30221
printai(loads(mid))
printai(tw.find_root(loads(mid)))
#
gtk.main()
# add header, run program, compare again
printai(loads(mid))
printai(tw.find_root(loads(mid)))
#
gtk.main()
# change `if abserrr...` to `if prints(abserrr...`
# run again, compare
# no longer connected:
#     printai(loads(mid))
#     printai(tw.find_root(loads(mid)))
printai(loads(30150))                   # main program

# search for prints...
# The subtree was replaced and is re-run, including abserr()


# stats for printing all info:
'''

wrt     return 1.0 / 2 * (xk + x / xk)
    single clone prefix dump, 20 lines
    infix, 1 line
    ratio 20

full sqrt.l3:
    prefix dump, no clones, 135 lines
    prefix dump, all clones, 770 lines
    infix, 30 lines - 10 comment = 20 lines
    ratio (/ 135.0 20) 6.75

def try(x, xn) body
    infix 4
    prefix 24 lines
    ratio 6


show only clone starting points: Function, Program

'''
#
#**        print_dot
def print_dot(storage, tree):
    # Print every expression and its clones.
    # This is far too much detail; only l3 Function clones are needed.

    iter = tree.prefix_grpd(storage)
    # Outermost.
    nd, clone_lev, indent = iter.next()
    print nd.source_string()

    try:
        while 1:
            nd, clone_lev, indent = iter.next()
            if clone_lev == '(':
                print '(clone '
                nd, clone_lev, indent = iter.next()
                print nd._id, nd.source_string()
            if clone_lev == ')':
                print ')'

    except StopIteration:
        pass

#
#**        print_dot
def print_dot(storage, tree):
    # Print expressions once, including clones.
    # For every expression, include the id and link to clone
    # parents/sources.
    #
    # Problems:
    # The if expressions are missing their lead indent.
    # The clone sources may not appear explicitly (with id); e.g., in
    #
    # 1 30920
    # if abserr(x, xn) > 0.0001:
    #         return loop(x, xkp1(x, xn))
    #     else:
    #         return xn
    #
    # 30254 -> 30920 /* clone of */
    # 30867 -> 30920 /* cloned by */
    #
    # the Function 30254 is referenced, but only shows up inside other
    # text.
    #

    iter = tree.prefix_grpd(storage)
    gaa = lambda id: storage.get_attributes(
        id, ["clone_of", "cloned_by", "interp_env"])

    try:
        while 1:
            nd, clone_lev, indent = iter.next()
            if isinstance(nd, Program):
                nid = nd._id
                print clone_lev, nid
                print nd.source_string()
                #
                of, by, env = gaa(nid)
                if of:
                    print "%(of)d -> %(nid)d /* clone of */" % locals()
                if by:
                    print "%(by)d -> %(nid)d /* cloned by */" % locals()
                print
    except StopIteration:
        pass

#
#**        print_dot
def print_dot(storage, tree):
    # Print expressions once, including clones.
    # For every expression, include the id and link to clone
    # parents/sources.
    #
    # Problems:
    # - The if expressions are missing their lead indent.
    # - The clone sources may not appear explicitly (with id);
    # - Displaying ALL clones takes too much room;

    iter = tree.prefix_grpd(storage)
    gaa = lambda id: storage.get_attributes(
        id, ["clone_of", "cloned_by", "interp_env"])

    try:
        while 1:
            nd, clone_lev, indent = iter.next()
            if isinstance(nd, Program):
                nid = nd._id
                print nid, '[label="%s"]; /* clone level %d */' % \
                      (repr(nd.source_string())[1:-1].replace(r"\n",
                                                              "\l"),
                        clone_lev)
                #
                of, by, env = gaa(nid)
                if of:
                    print "%(of)d -> %(nid)d [color=green]; /* clone of */" % locals()
                if by:
                    print "%(by)d -> %(nid)d [color=blue]; /* cloned by */" % locals()
                print
    except StopIteration:
        pass
#
#**        use
print """
digraph foo {
        page = "8.5,11.0";            /* size of single physical page */
        size="7.5,10.0";              /* graph size */
        /* rotate=90; */
        /* ratio=fill; */
        /* rankdir=LR; */
        node [shape=plaintext];
"""
print_dot(storage, t_tree)
print "}"

# Program P is a clone of a Function F
#               cloned by a Call C



sload = storage.load

#*    l3 paper
#**        program
t_string = open('../l3lang/test/sqrt.l3').readlines()
t_tree = reader.parse("".join(t_string))
t_tree.setup(empty_parent(), def_env, storage)[0].set_outl_edges(self.w_, None)
# print_info(storage, t_tree)
print_all_info(storage, t_tree)
w_.canvas.view.clear_state( )
w_.canvas.view.start_add_l3tree(t_tree)
flush_events()
gtk.main()

#*    error tracking
#**        filtered
# put tree on canvas.
# use B-3 -> hide.
# zoom.
import linecache, sys
def traceit(frame, event, arg):
    if event == "line":
        file = frame.f_code.co_filename
        # SET NAME:
        if "usr/tmp/python-" in file:
            lineno = frame.f_lineno
            line = linecache.getline(file, lineno)
            print "line %d: %s" % (lineno, line.rstrip())
            sys.stdout.flush()

    return traceit
sys.settrace(None)
sys.settrace(traceit)
#**        all
import linecache, sys
def traceit(frame, event, arg):
    if event == "line":
        file = frame.f_code.co_filename
        # SET NAME:
        if 1:
            ## if "usr/tmp/python-" in file:
            lineno = frame.f_lineno
            line = linecache.getline(file, lineno)
            print "%s:%d: %s" % (file, lineno, line.rstrip())
            sys.stdout.flush()

    return traceit
sys.settrace(None)
sys.settrace(traceit)

#
pdb.run('w_.canvas.view.centered_zoom(0.1)')

# The warning is printed through
#     def warn...
# in
#     intel-linux-2.4/python/lib/python2.4/warnings.py
# Maybe trap these?


#*    local evaluation
self._canvas.display_bounding_box(canvas_item_get_bounds_world(self._blank))
self._canvas.display_bounding_box(self.get_bounds_world())
self._canvas.display_bounding_box(canvas_item_get_bounds_world(self._d_lhs_marker))
self._canvas.display_bounding_box(canvas_item_get_bounds_world(self._root_group))
self._canvas.display_bounding_box(canvas_item_get_bounds_world(self._blank))

# bbox checks
# self._canvas.display_bounding_box(self.get_bounds_world())
self._canvas.display_bounding_box(canvas_item_get_bounds_world(self._root_group))

self._canvas.display_bounding_box(self._expander.get_bounds_world())

#*    phenix refine display
#**        program
t_tree = reader.parse_file(
    os.path.expandvars('$SPXROOT/l3lang/test/refine.l3'))
t_tree.setup(empty_parent(), def_env, storage).set_outl_edges(self.w_, None)
## print_info(storage, t_tree)
## view.print_all_info(storage, t_tree)
w_.canvas.view.clear_state()
# SLOW!  But interesting to watch.
w_.canvas.view.start_add_l3tree(t_tree)
flush_events()
gtk.main()
#**        program, limited depth
t_tree = reader.parse_file(
    os.path.expandvars('$SPXROOT/l3lang/test/refine.l3'))
t_tree.setup(empty_parent(), def_env, storage).set_outl_edges(self.w_, None)

# hide nodes at level N
vista = w_.canvas.view._vis_tab = VisibilityTable()    # Reset.
N = 7
vista.hide_at(t_tree, N)

# Check
# ld = w_.state_.storage.load
# for id in vista.keys():
#     print ld(id)

# Render
w_.canvas.view.clear_state()
w_.canvas.view.start_add_l3tree(t_tree)
flush_events()
gtk.main()
#**        nested Map
t_tree = reader.parse("""
input = {
    pdb = {
        file_name = 'None',},
    xray_data = {
        file_name = 'None',
        labels = 'None',},
    r_free_flags = {
        file_name = 'None',
        label = 'None',
        test_flag_value = 'None',
        disable_suitability_test = 'False',},
    experimental_phases = {
        file_name = 'None',
        labels = 'None',},
    monomers = {
        file_name = 'None',},}
""")
t_tree.setup(empty_parent(), def_env, storage).set_outl_edges(self.w_, None)
## print_info(storage, t_tree)
## view.print_all_info(storage, t_tree)
w_.canvas.view.clear_state()
w_.canvas.view.start_add_l3tree(t_tree)
flush_events()
gtk.main()
#**        various file's layout
def layout_file(name, level = 7):

    t_tree = reader.parse_file(os.path.expandvars(name))
    t_tree.setup(empty_parent(), def_env, storage).set_outl_edges(self.w_, None)

    # hide nodes at LEVEL.
    vista = w_.canvas.view._vis_tab = VisibilityTable()    # Reset.
    vista.hide_at(t_tree, level)

    # Render
    w_.canvas.view.clear_state()
    w_.canvas.view.start_add_l3tree(t_tree)
    flush_events()
    gtk.main()

layout_file('$SPXROOT/l3lang/test/refine.l3')
layout_file('$SPXROOT/l3lang/test/ctf.l3')
layout_file('$SPXROOT/l3lang/test/golem_1.l3')

#*    legible data dumping via PyYAML (yaml)
import yaml
print yaml.load("""
- foo
- bar
- baz
""")
print yaml.dump(['foo', 'bar', 'baz'])

# full state
#   1. Slow
#   2. yaml.representer.RepresenterError: recursive objects are not
# allowed: Env(30288, Env-30269-skel.blck, [Program at 0x40d79a8c, id
# 30290]) 
yaml.dump(get_current_state(w_), stream = sys.stdout)
## yaml.dump(py_obj, stream = None)

yaml.load(ya_str)

#*    legible data dumping via syck (yaml)
#**        Trivial test.
import syck
print syck.load("""
- foo
- bar
- baz
""")
print syck.dump(['foo', 'bar', 'baz'])

# 
#**        Full state.

# [Mon Apr 30 10:28:42 2007]
# Fails on <weakproxy> instances
# with error
#   AttributeError: viewList instance has no attribute '__reduce__'
# and there is no customization hook.
syck.dump(get_current_state(w_), sys.stdout)


# [Mon Apr 30 10:35:19 2007]
# Try a detour via pickle.  This fails too:
#     (.:16125): GLib-GObject-WARNING **: cannot retrieve class for
#     invalid (unclassed) type `<invalid>' ** ERROR **: file pygenum.c:
#     line 60 (pyg_enum_repr): assertion failed:
#     (G_IS_ENUM_CLASS(enum_class)) aborting...
#     /net/hooknose/scratch1/hohn/t2/sparx-bin/bin/sparx.wrap: line 36:
#     16125 Aborted python -i
#     Process Python exited abnormally with code 134
# 
# Works after removing some more unnecessary gtk information.
import pickle, syck
n_tree = pickle.loads( pickle.dumps(get_current_state(w_), protocol=2))
syck.dump(n_tree, sys.stdout)


# l3 graphical node (local eval)
import syck
print syck.dump(self)

# l3 tree (local eval)
print syck.dump(self._l3tree)

# 
#**        misc
print syck.load("""
connections:
    horizontal layout
     -requires
     -display choice
---
connections:
    [horizontal layout, requires, >
        display choice display choice display choice
        display choice display choice display choice
    ]
---
connections:
    horizontal layout:
        requires: display choice (direction)
        afd: display
---
>
  2d layout -- requires -- comment parsing

""")
#
#**        Compare state files via their yaml representations.
import pickle, syck
from l3lang.utils import *

n_tree_1 = file_unpickle('st.1')
syck.dump(n_tree_1, open('st.1.syck', 'w'))

n_tree_2 = file_unpickle('st.2')
syck.dump(n_tree_2, open('st.2.syck', 'w'))




#*    local evaluation
#**        code & clones
==== local console; exit with end-of-file (ctrl-d) ====

import l3lang.view as view
view.print_all_info(self.w_.state_.storage, self._l3tree)

#**        code & values

#*    canvasgroup
lcan = w_.lib_canvas.view
pprint(lcan.root().item_list)
# local eval
pprint(self._root_group.item_list)

#*    visibility tables testing.
lcan = w_.lib_canvas.view
(lcan._vis_tab)
pprint(lcan._vis_tab)

# Assuming constant tree and node offset:
manually_chosen = {30098: 'vis_hide',
                   30136: 'vis_hide',
                   30143: 'vis_hide',
                   30148: 'vis_hide',
                   30158: 'vis_hide',
                   30166: 'vis_hide'}

lcan._vis_tab.replace_table( manually_chosen)
# use "apply mark" on topmost element.

#*    state files
/tmp/st.lib             - compacted library only.
/tmp/st.for-pre-run     - loop sample


#*    l3 loop templates
#**        range iteration,  function version.
# for i in range(n,m):
#   body
def _for(_n, _m, body):
    if (_n < _m):
        body(_n)
        return _for(_n + 1, _m, body)
    else:
        return 0

_for(1, 7, {|i| print_(i) })
_for(30, 31, {|i| foo })
# _for(1, 5, lambda i: print_(i) )
# Nesting is no problem:
_for(1, 4, {|i|
            _for(-5, -1,
                 {|j| print_("%d %d" % (i,j)) })})
#**        range, macro version
#       [Thu Feb  9 16:29:39 2006]
# Better not to use this one...
#
# for i in range(n,m):
#   body
def _form(_n, _m):
    if (_n < _m):
        i = _n
        body
        return _form(_n + 1, _m)
    else:
        return 0

_form(1, 5)

# Paste, insert body, change "i" to new name, run.
# Nesting requires unique _for names; the following has a problem::
def _for(_n, _m):
    if (_n < _m):
        ii = _n
        print_(ii)
        ########
        def _for(_n, _m):
            if (_n < _m):
                jj = _n
                ########
                print_("%d %d" % (ii, jj))
                ########
                return _for(_n + 1, _m)
            else:
                return 0
        _for(1, 5)
        ########
        return _for(_n + 1, _m)         # wrong _for
    else:
        return 0
_for(1, 5)


#*    context for values

# Given multiple values from a single expression and its clones, find
# the dynamic context -- the changed arguments to 
# enclosing Functions, or (sigh) the changed values of the stacked
# bindings.  Complicated.

import l3lang.view
st = w_.state_.storage
load = w_.state_.storage.load
tw = ast.TreeWork(w_.state_.storage)



#*    value extraction
#
# LOADING RESETS THE STORAGE TABLE AND OTHERS!
#
#   U s e   t h e   w _   a c c e s s   p a t h !
#
import l3lang.view
st = w_.state_.storage
load = w_.state_.storage.load
tw = ast.TreeWork(w_.state_.storage)

# Toplevel id (static).
l3nd = w_.selector.get_selection()
c_id = l3nd._l3tree._id
print load(c_id)
print load(c_id).source_string()

# Dynamic id(s).
clone_l = st.get_attribute(c_id, "interp_clone")
print len(clone_l)

# If no values:  is the correct storage instance used?
for id in clone_l:
    print id, st.get_attribute(id, 'interp_result')

# For deeper nesting, follow the clone trail.
st.get_attribute(id, "interp_clone")

# Get the context
cctxt = tw.get_call_ctxt(c_id)
#   Raw.
pprint( cctxt)
#   With text.
tw.print_call_ctxt(cctxt)

# If clones were cloned, these may be non-empty?
# for id in clone_l:
#      print tw.get_call_ctxt(id)

# symbol('i') evaluated, has time stamp, but no value, no env? --
# Use correct storage!
id = clone_l[1]
print id, st.get_attribute(id, 'interp_result')
print id, st.get_attribute(id, 'interp_env')
pprint( [id, st.get_attribute_table(id)])

# enclosing program
parent = tw.find_first_parent(load(id), (ast.Function, ast.Program))
view.print_info(st, parent)
print parent._eval_env.all_bindings() # empty
print parent._eval_env.lookup('i') # value, at last...
global L3_TRACE
L3_TRACE = 1
print parent._eval_env.ie_lookup('i')


#*    environment viewing
# local eval for program
(self._l3tree)._block_env.all_bindings()
(self._l3tree)._eval_env.all_bindings()
(self._l3tree)._eval_env.ls()
# has print_'s time stamp, but no print.
# [after loading /tmp/st.post-loop ]

#*    state examination
# Failed to complete:
print syck.dump(get_current_state(w_))

import pickle, pickletools
st = get_current_state(w_)
pickletools.dis(pickle.dumps(st, protocol=2))


# local eval
import pickle

print (self._l3tree)._eval_env.lookup('print_')
# <function print_ at 0x40402ed4>

n_tree = pickle.loads( pickle.dumps(self._l3tree, protocol=2))
print (n_tree)._eval_env.lookup('print_')

#*    voea etc. sample problem
#**        python support functions
from l3lang.external.golem import \
     create_write_gridprojections, \
     anglist2doc, \
     spidbprg, \
     Golem

#
#**        voea
#
# caption = 'Delta theta (degrees)',
if 1:
    deltheta = 10
    anglist = Golem.voea(deltheta)
else:
    0
#
#**        gridproj
#
if 1:
    # caption = 'Filename of volume to project',
    volfile = "volext.spi"

    # caption = 'Kaiser-Bessel K parameter',
    K = 6

    # caption = 'Kaiser-Bessel alpha parameter',
    alpha = 1.75

    # caption = 'Projection file pattern',
    filepattern = 'proj{****}.tst'

    # print vol.get_xsize()
    create_write_gridprojections(
        Golem.getImage(volfile),
        anglist, K, alpha, filepattern)
else:
    0
#
#**        anglist2doc
# caption = 'Spider doc filename',
if 1:
    docfname = 'foo-doc.tst'
    anglist2doc(anglist, docfname)
else:
    0
#
#**        spidbprg
#
if 1:
    # caption = 'Projection file pattern',
    filepattern = 'proj{****}.tst'          # above!

    # caption = 'Projection start index',
    minindx = 1

    # caption = 'Projection stop index',
    maxindx = 10

    # caption = 'Name of angle Spider doc file',
    angdocfile = docfname                   # above!

    # caption = 'Name of reconstructed volume',
    newvolname = 'newvol.spi'
    spidbprg(filepattern,
             minindx,
             maxindx,
             angdocfile,
             newvolname,
             )
else:
    0

# comparison in v4 shows newvolname to be poor...

#*    voea/bp as single program
#**        program
tree = reader.parse("""
inline '''
from l3lang.external.golem import \
     create_write_gridprojections, \
     anglist2doc, \
     spidbprg, \
     Golem
'''

def _for(_n, _m, body):
    if (_n < _m):
        body(_n)
        return _for(_n + 1, _m, body)
    else:
        return 0

def body(deltheta):
    if 1:
        anglist = Golem.voea(deltheta)
    else:
        0
    #
    # gridproj
    #
    if 1:
        # caption = 'Filename of volume to project',
        volfile = "volext.spi"

        # caption = 'Kaiser-Bessel K parameter',
        K = 6

        # caption = 'Kaiser-Bessel alpha parameter',
        alpha = 1.75

        # caption = 'Projection file pattern',
        filepattern = 'proj{****}.tst'

        # print vol.get_xsize()
        create_write_gridprojections(
            Golem.getImage(volfile),
            anglist, K, alpha, filepattern)
    else:
        0
    #
    # anglist2doc
    # caption = 'Spider doc filename',
    if 1:
        docfname = 'foo-doc.tst'
        anglist2doc(anglist, docfname)
    else:
        0
    #
    # spidbprg
    #
    if 1:
        # caption = 'Projection file pattern',
        filepattern = 'proj{****}.tst'          # above!

        # caption = 'Projection start index',
        minindx = 1

        # caption = 'Projection stop index',
        maxindx = 10

        # caption = 'Name of angle Spider doc file',
        angdocfile = docfname                   # above!

        # caption = 'Name of reconstructed volume',
        newvolname = 'newvol.spi'
        spidbprg(filepattern,
                 minindx,
                 maxindx,
                 angdocfile,
                 newvolname,
                 )
    else:
        0

_for(40, 42, body)
""")
#
#**        Directory setup.
import shutil, os
ev = os.path.expandvars
os.chdir(ev('$SPXROOT/l3lang/test'))
print os.getcwd()
shutil.rmtree('bp-test')
os.mkdir('bp-test')
os.chdir(ev('$SPXROOT/l3lang/test/bp-test'))
print os.getcwd()
shutil.copyfile(ev('$SPXROOT/l3lang/test/volext.spi'), 'volext.spi')

#
#**        setup and execute
if 1:                                   # graphical
    tree.setup(empty_parent(), def_env, w_.state_.storage).set_outl_edges(self.w_, None)
    # view.print_info(storage, tree)
    w_.canvas.view.start_add_l3tree(tree)
    flush_events()

if 2:                                   # textual
    from l3lang import utils, reader, ast, view, interp
    play_env = interp.get_child_env(def_env, storage)
    tree.setup(empty_parent(), def_env, storage).set_outl_edges(self.w_, None)
    tree.interpret(def_env, storage)
    pprint(os.listdir(os.getcwd()))



#*    voea/bp using docstrings
# /net/cci/hohn/w/tmp/eman2/libpyEM/Golem
#**        program
tree = reader.parse("""
inline '''
from l3lang.external.golemwrap import *
'''

def _for(_n, _m, body):
    if (_n < _m):
        body(_n)
        return _for(_n + 1, _m, body)
    else:
        return 0

def body(deltheta):
    anglist = voea(deltheta)

    #
    # Param setting.
    #
    filepattern = 'proj{****}.tst'

    #
    # gridproj
    #
    gridproj(getImage("volext.spi"), # nesting...
             anglist,
             6,
             1.75,
             {filepattern = filepattern})

    # Param setting.
    docfname = 'foo-doc.tst'
    #
    # anglist2doc
    #
    anglist2doc(anglist, docfname)

    #
    # spidbprg
    #
    spidbprg(filepattern,
             1,
             10,
             docfname,
             'newvol.spi',
             )

_for(40, 42, body)
""")
#"
#**        Directory setup.
import shutil, os
ev = os.path.expandvars
os.chdir(ev('$SPXROOT/l3lang/test'))
print os.getcwd()
shutil.rmtree('bp-test')
os.mkdir('bp-test')
os.chdir(ev('$SPXROOT/l3lang/test/bp-test'))
print os.getcwd()
shutil.copyfile(ev('$SPXROOT/l3lang/test/volext.spi'), 'volext.spi')

#
#**        setup and execute
if 1:                                   # graphical
    tree.setup(empty_parent(), def_env, w_.state_.storage).set_outl_edges(self.w_, None)
    # view.print_info(storage, tree)
    w_.canvas.view.start_add_l3tree(tree)
    flush_events()

if 2:                                   # textual
    from l3lang.external import golemwrap
    from l3lang import utils, reader, ast, view, interp
    play_env = interp.get_child_env(def_env, storage)
    tree.setup(empty_parent(), play_env, storage)
    tree.interpret(play_env, storage)
    pprint(os.listdir(os.getcwd()))

#*    voea/bp w/o spider
#**        program
tree = reader.parse("""
""")
#
#**        Directory setup.
import shutil, os
ev = os.path.expandvars
os.chdir(ev('$SPXROOT/l3lang/test'))
print os.getcwd()
shutil.rmtree('bp-test')
os.mkdir('bp-test')
os.chdir(ev('$SPXROOT/l3lang/test/bp-test'))
print os.getcwd()
shutil.copyfile(ev('$SPXROOT/l3lang/test/volext.spi'), 'volext.spi')

#
#**        setup and execute
if 1:                                   # graphical
    tree.setup(empty_parent(), def_env, w_.state_.storage)
    # view.print_info(storage, tree)
    w_.canvas.view.start_add_l3tree(tree)
    flush_events()

if 2:                                   # textual
    from l3lang import utils, reader, ast, view, interp
    play_env = interp.get_child_env(def_env, storage)
    tree.setup(empty_parent(), def_env, storage)
    tree.interpret(def_env, storage)
    pprint(os.listdir(os.getcwd()))


#*    beautification.
def pri(tree):
    view.print_info(storage, tree)

pri( reader.parse("aa = ff(xx)"))
pri( reader.parse("aa = ff(xx, yy)"))
pri( reader.parse("! aa = ! ff( !xx)"))

pri( reader.parse("aa = ff(xx)"))
pri( reader.parse("(aa, bb) = ff(xx)"))

reader.parse("!! aa symbol")


# ---------------------------------
from l3lang.ast import *
l3tree = reader.parse("a = f(x)")[0][0]
l3tree = reader.parse("(a,b) = f(x)")[0][0]
l3tree = reader.parse("(a,b) = f(x,y)")[0][0]
l3tree = reader.parse("a = f(x,y)")[0][0]

# ---------------------------------
# Test for the special case `a = f(x)` and its
# variations.

# def add_l3tree_set_tuple()
ma = ast.Matcher()
if ma.match(l3tree,
            Set(Marker('aa'),
                Call(Marker('ff'), Marker('xx')))):
    # Convert to a list of args and list of return values.
    _function = ma['ff']                # Symbol or expr.
    _in = ma['xx']                      # aList of args.
    out = ma['aa']
    if ma.match(out,                   # a,b,... =
                Tuple(Marker('aa'))):
        _out = ma['aa']                # aList
    elif ma.match(out,                 # a =
                  MarkerTyped(String('aa'), Symbol('symbol'))):
        _out = aList([ma['aa']])       # aList
    else:
        no_luck

    # Get documentation for _in, _out, and _function.
    print _out, _function, _in

    # Get paths for editables.
    #   Using the unique id.

    # Assemble table.


#*    ExprMarker
# The initial try at an expression marker.  This version is too buggy;
# the new one is in widgets.py, based on a merge of l3If and
# Placeholder.


class daExprMarker:
    pass

def __init__(self, w_, cnvs, l3_parent, l3_pidx, d_expr):
    """
    :Arguments:
        - `l3_parent`   The raw l3 ast parent of d_expr._l3tree.
            Without propagation of events from the l3 ast to its
            display (the current case), l3_parent should not be
            drawn.
        - `l3_pidx`     The index of d_expr._l3tree in l3_parent.
        - `d_expr`      The displayed expression to be handled here.
    """
    assert isinstance(d_expr, (wdgt.l3aList, wdgt.l3Base))
    assert isinstance(l3_parent, (ast.astType,))

    self.w_      = w_
    self._canvas = cnvs

    self._l3_parent = l3_parent
    self._l3_pidx = l3_pidx
    self._d_expr = d_expr

    # Display.
    self._root_group = cnvs.root().add(canvas.CanvasGroup)
    self._d_parent = None
    self._destroy_hook = []             # (func -> None) list
    self._vis_indic = None              # visibility status indicator

    # Marker(s).
    self._marker_points = {}
    self._d_marker = self.draw_marker()
    self._d_marker.lower_to_bottom()

    # Inform obj of marker.
    #   This directs physical detachment of d_expr from daExprMarker to
    #   daExprMarker.detach, which forwards the tree detachment.
    d_expr.setup_marker(self._d_marker, self)

    #
    # Align with respect to d_expr.
    #
    flush_events()
##     _, y, x, _ = d_if.get_bounds()
##     u, v, _, _ = d_cond.get_bounds()
##     d_cond.move(x - u, y - v)


    # Event handling.
    self._d_marker.connect("event", lambda *a: self.insert_event(*a))
    # Forwarding to parent is implicit.
    # # self._root_group.connect("event", lambda *a:
    # # self.drag_event(*a))
daExprMarker.__init__ = __init__

#
#**        destroy subtree and display
def destroy(self):
    self._d_expr
    self._destroy_hook
    self._root_group.destroy()
daExprMarker.destroy = destroy

l3Base.add_destroy_hook = add_destroy_hook
l3Base._run_destroy_hook = _run_destroy_hook

#
#**        dragging
l3Nested.drag_event = drag_event


def detach(self, d_expr):
    if d_expr != self._d_expr:
        raise DisplayError("Detaching invalid child.")

    # Reparent graphical parts.
    d_expr.reparent_to_root()

    # Update reference.
    self._d_expr = None

    # Shift body parts.
    pass

    # Refresh decoration.
    pass

    # Move following items in parent.
    if self._d_parent:
        self._d_parent.new_size_for(self)

    # Update tree data.
    if self.w_.fluid_ref(detach_l3_tree = True):
        #   Remove the old.
        self._l3_parent.detach_child(self.d_expr._l3tree._id,
                                     self.w_.state_.storage)
        #   Insert a placeholder, to keep indices valid.
        dummy = ast.placeholder(self._l3_parent, self.w_.state_.storage)
        self._l3_parent.insert_child(self._l3_pidx, dummy)
daExprMarker.detach = detach

def draw_marker(self):
    cp = self.w_.cp_
    points = [0                   , 0,
              cp.exp_marker_width , 0,
              cp.exp_marker_width , cp.exp_marker_height,
              0                   , cp.exp_marker_height,
              0                   , 0,
              ]
    _marker = self._root_group.add(
        canvas.CanvasPolygon,
        fill_color  = "black",
        points      = points,
        width_units = 0.2,
        cap_style   = gtk.gdk.CAP_ROUND,
        join_style  = gtk.gdk.JOIN_ROUND,
        )
    self._marker_points[_marker] = points
    return _marker
daExprMarker.draw_marker = draw_marker

def insert_event(self, item, event):
    if event.type == gtk.gdk.BUTTON_PRESS:
        if event.button == 2:
            print "insert_event"
            if not self.w_.selector.have_selection():
                ten_second_message("No node selected.")
                return False
            else:
                self.insert(item, self.w_.selector.get_selection())
                return False
daExprMarker.insert_event = insert_event

def insert(self, marker, newobj):
    assert isinstance(newobj, l3Base)

    if marker != self._d_marker:
        raise DisplayError("Insertion from wrong marker.")

    # Avoid special cases:
    if newobj.contains_recursive(self._d_expr):
        ten_second_message("Cannot insert object into itself.")
        return

    if newobj._canvas != self._canvas:
        ten_second_message("Cannot move objects across displays.  "
                           "Use copy instead.")
        return

    # Validate slot.
    old_entry = self._l3_parent[self._l3_pidx]
    slot_available(self.w_, old_entry)

    # Update reference.
    self._d_expr = d_expr = newobj

    # Reparent object display.
    newobj.reparent(self)

    # Position new child.
    flush_events()
    l1, t1, _, _ = self._d_marker.get_bounds()
    l2, t2, _, _ = self._d_expr.get_bounds()
    d_expr.move(l1 - l2, t1 - t2)

    # Reposition rest.

    # Refresh decoration.

    # Move following items in parent.
    if self._d_parent:
        self._d_parent.new_size_for(self)

    # Update tree data.
    if self.w_.fluid_ref(insert_l3_tree = True):
        self._l3_parent.replace_child(old_entry._id, newobj._l3tree)

    # Inform obj of marker.
    newobj.setup_marker(self._d_marker, self)
    pass
daExprMarker.insert = insert


def get_bounds(self):
    """ Marker interface used in Nested.drag_event.
    """
    return marker_get_bounds(self.w_, self._d_marker)
daExprMarker.get_bounds = get_bounds


#
#**        exprmarker test
l3_parent = reader.parse("a = f(x,y)")[0][0]
play_env = interp.get_child_env(def_env, storage)
l3_parent.setup(empty_parent(), play_env, storage)

l3_pidx = 0
d_expr = w_.canvas.view.start_add_l3tree(l3_parent[l3_pidx])
d_expr.move(100, 100)

the_marker = daExprMarker(w_, w_.canvas.view, l3_parent, l3_pidx, d_expr)

find_child_index

#*    stuff
other = ...
[ future(operation, i, other) for i in list]

# operation is a pure function, no envs.  Future need not provide
# environment.
#
def print_(st):
    print st

def compute(_n, _m):
    if (_n < _m):
        print_( (_n + 1) * _m)
        return compute(_n + 1, _m)
    else:
        return _n

compute(10,20)

tree = reader.parse("""
inline '''
from l3lang.l3_utils import print_, pprint
'''
def compute(_n, _m):
    if (_n < _m):
        print_( (_n + 1) * _m)
        return compute(_n + 1, _m)
    else:
        return _n
""")
tree.setup(empty_parent(), def_env, w_.state_.storage)
w_.canvas.view.start_add_l3tree(tree)

# use functions,
# use tail calls,
# do not:'
j = 0
for i in range(0,10):
    j = j + i
pass

#*    eman2 cruft
from pprint import pprint
from pdb import pm
import atexit, pdb, sys, os
from EMAN2 import EMData
import EMAN2

ev = os.path.expandvars
print os.getcwd()
os.chdir(ev('$SPXROOT/l3lang/test'))
def load( file_name ):
    img = EMData()
    img.read_image( file_name )
    return img
#**        List available functionality.
pprint(EMAN2.Processors.get_list())
pprint(EMAN2.Projectors.get_list())
pprint(EMAN2.Reconstructors.get_list())
pprint(EMAN2.Cmps.get_list())
#**        Dump ALL get_list containing members.
for nm in dir(EMAN2):
    try:
        memb = eval("EMAN2.%s.get_list" % nm)
    except AttributeError:
        continue
    print ("EMAN2.%s.get_list" % nm)
    print memb()
#**        simple mirror test
from EMAN2 import *
e = EMData()
e.set_size(3,3)
e.set_value_at(0,1, 1.0)
print "original at start"
e.print_image()

# Use the processors listed by .get_list() above.
    f1 = e.process("mirror",{"axis":'x'})
    print "new mirrored image"
    f1.print_image()

    print "original after mirror (unchanged)"
    e.print_image()

    e.process_inplace("mirror",{"axis":'x'})
    print "original after in-place mirror"
    e.print_image()

    # Another possibility:
    e = EMData()
    e.set_size(3,3)
    e.set_value_at(0,0, 1.0)
    print "original at start"
    e.print_image()

    e = e.process("mirror",{"axis":'x'})
    print "new mirrored image"
    e.print_image()


#*    sample from andres' paper
from EMAN2 import *
#**        build 3d model
inline """
from EMAN2 import EMNumPy
def img_sphere(size, radius, axis, center, fill):
    from EMAN2 import EMData
    e = EMData()
    e.set_size(size, size, size)
    e.process_inplace('testimage.circlesphere',
                        {'radius':radius, 'axis':axis,
                         'c':center, 'fill':fill}) 
    return e

"""
# "
sphere = img_sphere(32, 20, 'z', 15, 1)
# Not useful.
# sphere.print_image()                    

# No slicing on emdata, use numpy array.
t1 = EMNumPy.em2numpy(sphere)

# View all 2d slices
for ix in range(0,32):
    t1[ix, 0:None, 0:None]

t1[0, 0]
#**        Try circle.
inline """
from EMAN2 import EMNumPy
def img_circle(size, radius, center, fill):
    from EMAN2 import EMData
    e = EMData()
    e.set_size(size, size)
    e.process_inplace('testimage.circlesphere',
                      {'radius':radius, 'c':center, 'fill':fill}) 
    return e
"""
circle = img_circle(32, 20, 15, 1)
t1 = EMNumPy.em2numpy(circle)


#**        dither / diffuse
#**        form projections
#**        apply CTF
#**        add noise
#**        calculate SSNR
#**        and much more...
# See "http://www.sciencedirect.com/science?_ob=ArticleURL&_udi=B6WM5-4HVMFBS-1&_user=635986&_coverDate=03%2F31%2F2006&_fmt=full&_orig=search&_cdi=6925&view=c&_acct=C000059612&_version=1&_urlVersion=0&_userid=635986&md5=0e4b1d3ea658eb6330884fef78c4b035&ref=full#SECX2"
# 2D alignment and classification. Particles were normalized, low- and
#high-pass filtered, soft-masked, and then centered. The low- and
#high-pass filters were centered at 1/5 pixels1 


#*    large list display
xx = range(0,20000)
# Insert as list list returns but keeps using 100% cpu

xx = range(0,2000)
# insert as list:
# RuntimeError: maximum recursion depth exceeded in cmp

# The number of bounds calculations (marker_get_bounds() and others)
# is ~ N, and bounds calculations are VERY slow...  

#*    Starting volume
volume = load( "model.tcp" )

proj = volume.project("pawel",
                      {"angletype" : "SPIDER",
                       "anglelist" : [10, 20, 30],	#  phi, theta, psi
                       "radius" : 35 })



#*    visual program representation
#**        calc
# Very convoluted example, heavily using higher-order functions.
# Ugly in graphical view.
t_tree = reader.parse_file(
    os.path.expandvars('$SPXROOT/l3lang/test/calc.l3'))
t_tree.setup(empty_parent(), def_env, w_.state_.storage)
w_.canvas.view.clear_state()
w_.canvas.view.start_add_l3tree(t_tree)
flush_events()

#
#**        ctf
#***            full program
# Very clean top-to-bottom script.  Graphical view is still tedious.
# Good reference for prototyping 'nice' graphical view w/o loops.
tm_tree = reader.parse_file(
    os.path.expandvars('$SPXROOT/l3lang/test/ctf.l3'))
tm_tree.setup(empty_parent(), def_env, w_.state_.storage)
w_.canvas.view.clear_state()
tm_tree_l3pic = w_.canvas.view.start_add_l3tree(tm_tree)
flush_events()

#
#***            concise view, program level, static
# manually written
# no program editing (static)
t0_tree = reader.parse("""
"project from volume"
"convolve image and ctf"
"produce noisy image"
"apply ctf correction"
"filter"
""")
t0_tree.setup(empty_parent(), def_env, w_.state_.storage)
t0_tree_pic = w_.canvas.view.start_add_l3tree(t0_tree)
flush_events()
t0_tree_pic.move(+8, +5.5)                     # Relative movement.

#
#***            concise view, program + calls, static
# manually written
# no program editing (static)
t1_tree = reader.parse("""
project_from_volume(spi.pj3)

convolve_image_and_ctf(spi.ft, spi.tfc, spi.mu, spi.ft)

produce_noisy_image(spi.mo, spi.adf)

apply_ctf_correction(spi.tfcts)

filter(spi.fq)
""")
t1_tree.setup(empty_parent(), def_env, w_.state_.storage)
t1_tree_pic = w_.canvas.view.start_add_l3tree(t1_tree)
flush_events()
t1_tree_pic.move(+35, +7.5)                     # Relative movement.

#
#
#***            concise view, program + manually selected details
# manually written.
# For ctf.l3, selected full subtrees are the desirable level of detail, so
# no reordered node display is needed.
# To keep the connection to the (function name based) outline view,
# the function names can appear explicitly around the expansion.
# For the present sample, this is redundant.  Maybe not for others?
t3_tree = reader.parse("""
project_from_volume(
    spi.pj3(
    testimg = spi.pj3("/home/hohn/w/phenix/l3lang/test/volext.spi",
                      [64, 64],
                      [45.5, 27.2, -30.0])))

convolve_image_and_ctf(
    spi.ft,
    spi.tfc(
    ctf = spi.tfc((2),            # cs[mm]
                  (10000, 0.025), # defocus(a), lambda(a)
                  (64, 64),       # dimension of the 2d array
                  (0.17),         # maximum spatial frequency
                  (0.005, 0),     # source size[a-1], defocus spread[a]
                  (0, 0),         # astigmatism[a], azimuth[deg]
                  (0.1, 10000),   # amplitude contrast ratio, gaussian envelope halfwidth
                  (-1),           # sign (+1 or -1)
                  )),
    spi.mu,
    spi.ft)

produce_noisy_image(
    spi.mo(
    noise = call_kwd_func(spi.mo, [64, 64], "random",
                          { use_gaussian = 'y', mean = 1,
                            std_deviation = 200} )),
    spi.adf)

apply_ctf_correction(
    spi.tfcts(
    restored_img = spi.tfcts(spi.img_to_stack(noisyimg),
                             [1, 1],
                             spi.img_to_stack(ctf),
                             100))
    )

filter(spi.fq)
""")
t3_tree.setup(empty_parent(), def_env, w_.state_.storage)
t3_tree_pic = w_.canvas.view.start_add_l3tree(t3_tree)
flush_events()
t3_tree_pic.move(+70, +7.5)                     # Relative movement.

#
#***            concise view, program + some full details
# manually written
# full program editing in the details
# This version is disjoint from the others; it's missing the
# "outline headers"
t2_tree = reader.parse("""
project_from_volume(
    testimg = spi.pj3("/home/hohn/w/phenix/l3lang/test/volext.spi",[64, 64],
                      [45.5, 27.2, -30.0]))

convolve_image_and_ctf(spi.ft,
                       ctf = spi.tfc(
    (2),            # cs[mm]
    (10000, 0.025), # defocus(a), lambda(a)
    (64, 64),       # dimension of the 2d array
    (0.17),         # maximum spatial frequency
    (0.005, 0),     # source size[a-1], defocus spread[a]
    (0, 0),         # astigmatism[a], azimuth[deg]
    (0.1, 10000),   # amplitude contrast ratio, gaussian envelope halfwidth
    (-1),           # sign (+1 or -1)
    ), spi.mu, spi.ft)

produce_noisy_image(
    noise = call_kwd_func(spi.mo, [64, 64], "random",
                      { use_gaussian = 'y', mean = 1,
                        std_deviation = 200} ),
    spi.adf)

apply_ctf_correction(restored_img = spi.tfcts(spi.img_to_stack(noisyimg),
                                              [1, 1],
                                              spi.img_to_stack(ctf),
                                              100)
                     )

filter(spi.fq)
""")
t2_tree.setup(empty_parent(), def_env, w_.state_.storage)
t2_tree_pic = w_.canvas.view.start_add_l3tree(t2_tree)
flush_events()
t2_tree_pic.move(+135, +7.5)                     # Relative movement.


#*    LOOP language addition.
import pdb
import l3lang
from l3lang import utils, reader, ast, view, interp

t2_tree = reader.parse("""
inline "from pprint import pprint"
loop dtheta(iteration, deltheta) from (1, 40):
    if iteration <= 10:
        pprint([iteration, deltheta])
        return dtheta(iteration + 1, deltheta / 2.0)
    else:
        return deltheta / 2.0
""")

play_env = interp.get_child_env(def_env, w_.state_.storage)

if 2:                                   # textual
    t2_tree.setup(empty_parent(), play_env, w_.state_.storage)
    t2_tree.interpret(play_env, w_.state_.storage)

if 1:                                   # graphical
    t2_tree.setup(empty_parent(), play_env, w_.state_.storage)
    # view.print_info(storage, tree)
    # pdb.run('w_.canvas.view.start_add_l3tree(t2_tree)')
    # b l3Loop.__init__
    ## fix:
    ##l3gui.misc.DisplayError: internal _container / _parent inconsistency.
    ##w_.canvas.view.clear_state()
    w_.canvas.view.start_add_l3tree(t2_tree)
    flush_events()
    gtk.main()


#*    LIST language addition.
import pdb
import l3lang
from l3lang import utils, reader, ast, view, interp

# w/o label.
t2_tree = reader.parse("""
inline "from pprint import pprint"
list:
    a
    iteration = 5
    if iteration <= 10:
        pprint([iteration])
    else:
        pprint((iteration, "small"))
    b
""")

# with label.
t2_tree = reader.parse("""
inline "from pprint import pprint"
list "A short block":
    a
    iteration = 5
    if (iteration <= 10):
        pprint([iteration])
    else:
        pprint((iteration, "small"))
    b
""")

play_env = interp.get_child_env(def_env, w_.state_.storage)

if 1:                                   # graphical
    t2_tree.setup(empty_parent(), play_env, w_.state_.storage)
    w_.canvas.view.start_add_l3tree(t2_tree)
    flush_events()
    gtk.main()

if 2:                                   # textual
    t2_tree.setup(empty_parent(), play_env, w_.state_.storage)
    t2_tree.interpret(play_env, w_.state_.storage)





#*    jsb paper
"""
The reference files for these fragments are now

    l3lang/test/voea-bp.py, v1.7 and beyond

    voea-bp-var.l3, v1.5 and beyond
"""
#**        utils (evaluate this!)
def canvas_add_program(text, xx = 0, yy = 0):
    t0_tree = reader.parse(text)
    t0_play_env = interp.get_child_env(def_env, w_.state_.storage)
    t0_tree.setup(empty_parent(), t0_play_env, w_.state_.storage)
    t0_tree_l3pic = w_.canvas.view.start_add_l3tree(t0_tree)
    t0_tree_l3pic.move(xx, yy)          # Relative movement.

def canvas_add_1st_expr(text, xx = 0, yy = 0):
    t0_tree = reader.parse(text)[0][0]
    t0_tree.setup(empty_parent(), def_env, w_.state_.storage)
    t0_tree_l3pic = w_.canvas.view.start_add_l3tree(t0_tree)
    t0_tree_l3pic.move(xx, yy)          # Relative movement.

def canvas_add_ast(self, t0_tree, xx = 0, yy = 0):
    from l3lang import utils, reader, ast, view, interp
    st = self.w_.state_
    t0_play_env = interp.get_child_env(st.def_env, st.storage)
    t0_tree.setup(ast.empty_parent(), t0_play_env, st.storage)
    t0_tree_l3pic = self.start_add_l3tree(t0_tree)
    t0_tree_l3pic.move(xx, yy)          # Relative movement.
    return t0_tree_l3pic
l3Canvas.canvas_add_ast = canvas_add_ast


# print "\n".join(map(lambda (x,y): "%g %g" % (x,y),
#     (zip([1,2], [3,4]))))

#
#***            plot
def plot(x_l, y_l):
    data  = "\n".join(map(lambda (x,y): "%g %g" % (x,y),
                          zip(x_l, y_l)))
    os.system("""
gnuplot -persist <<EOF
#
set term post 24
set out "/tmp/foo.ps"
#
set grid
set nokey
## set title "comparison"
#
plot '-' w lines lw 6
%s
e
EOF
ps2epsi /tmp/foo.ps /tmp/foo.eps
gv /tmp/foo.eps &
""" % data)
    pass

## plot([1,2], [3,4])

#
#***            directory of program run
# See Program.directory

#
#**        voea-bp
# #***            concise view, program level, static
# # manually written
# # no program editing (static)
# t0_tree = reader.parse("""
# "get reference volume"
# "iterate delta theta"(
#     "calculate angles",
#     "projections",
#     "backproject",
#     "compare volumes",
#     "repeat if necessary")
# """)
# t0_play_env = interp.get_child_env(def_env, w_.state_.storage)
# t0_tree.setup(empty_parent(), t0_play_env, w_.state_.storage)
# ## w_.canvas.view.clear_state()
# t0_tree_l3pic = w_.canvas.view.start_add_l3tree(t0_tree)
# t0_tree_l3pic.move(+8.5, +7.5)           # Relative movement.
# flush_events()

#
# #***            concise view, program + calls, static
# # manually written
# # no program editing (static)
# t1_tree = reader.parse("""
# "get reference volume"
# "iterate delta theta"(
#     "calculate angles"(anglist = voea(deltheta)),
#     "projections"(proj_list = gridproj(ref_vol,
#                                anglist,
#                                6,
#                                1.75)),
#     "backproject"(back_projection = backproj(proj_list,
#                                     anglist,
#                                     0.4),
#                                     ),
#     "compare volumes"(ccc = EMAN2.Cmps.get("ccc").cmp(back_projection,
#                       ref_vol)),
#     "repeat if necessary"(if_branch))
# """)
# t1_play_env = interp.get_child_env(def_env, w_.state_.storage)
# t1_tree.setup(empty_parent(), t1_play_env, w_.state_.storage)
# ## w_.canvas.view.clear_state()
# t1_tree_l3pic = w_.canvas.view.start_add_l3tree(t1_tree)
# t1_tree_l3pic.move(+42, +7.5)           # Relative movement.
# flush_events()

#
#***            full program
# Very clean top-to-bottom script
tm_tree = reader.parse_file(
    os.path.expandvars('$SPXROOT/l3lang/test/voea-bp.l3'))
tm_play_env = interp.get_child_env(def_env, w_.state_.storage)
tm_tree.setup(empty_parent(), tm_play_env, w_.state_.storage)
# w_.canvas.view.clear_state()
tm_tree_l3pic = w_.canvas.view.start_add_l3tree(tm_tree)
# tm_tree_l3pic.move(+77, +7.5)           # Relative movement.
gtk.main()

#
#**        Add text tree to canvas

# num_angles
# [12L, 13L, 27L, 59L, 195L]
# Figure
canvas_add_1st_expr("""
# ccc
[
0.8645,
0.3051,
0.1503,
0.2821,
0.5498,
]
""")
canvas_add_1st_expr("""
# deltheta
[
42,
34,
26,
18,
10,
]
""")

canvas_add_program("""
from l3lang.test.voea_bp_support import *
plot(
# deltheta
[
42,
34,
26,
18,
10,
],
# ccc
[
0.8645,
0.3051,
0.1503,
0.2821,
0.5498,
]
)
""", xx=30, yy=50)

# Figure from gnuplot

#
#**        get the diretory structure
#***            in full
## use program -> eval locally
# Static structure
(self._l3tree)._block_env.print_tree()

# After execution.
(self._l3tree)._eval_env.print_tree()

# As lists of names.
#   Too much content.
from pprint import pprint
pprint((self._l3tree)._block_env.get_tree())
pprint((self._l3tree)._eval_env.get_tree())

#
#***            More filtered.
## use program -> eval locally
from pprint import pprint
listing = (self._l3tree)._eval_env.get_dynamic_subtrees()
pprint(listing)

dir_struct = self._l3tree.directory(listing)
dir_view = self._canvas.canvas_add_ast(dir_struct)
dir_view.move(10, 10)

#
#***            More^2 filtered.
## use program -> eval locally
dir_struct = self._l3tree.directory_l3()
dir_view = self._canvas.canvas_add_ast(dir_struct)
dir_view.move(20, 20)

# Figure
dir_struct = List(aList([List(aList([List(aList([Symbol('volume'), Symbol('projections'), Symbol('errs'), Symbol('anglist'), Symbol('proj_list'), Symbol('back_projection'), Symbol('cross_corr'), Symbol('volfile'), Symbol('freqs'), Symbol('num_angles'), Symbol('angle_file'), Symbol('fsc_vals')])), Symbol('iteration'), Symbol('deltheta')])), List(aList([List(aList([Symbol('volume'), Symbol('projections'), Symbol('errs'), Symbol('anglist'), Symbol('proj_list'), Symbol('back_projection'), Symbol('cross_corr'), Symbol('volfile'), Symbol('freqs'), Symbol('num_angles'), Symbol('angle_file'), Symbol('fsc_vals')])), Symbol('iteration'), Symbol('deltheta')])), List(aList([List(aList([Symbol('volume'), Symbol('projections'), Symbol('errs'), Symbol('anglist'), Symbol('proj_list'), Symbol('back_projection'), Symbol('cross_corr'), Symbol('volfile'), Symbol('freqs'), Symbol('num_angles'), Symbol('angle_file'), Symbol('fsc_vals')])), Symbol('iteration'), Symbol('deltheta')])), List(aList([List(aList([Symbol('volume'), Symbol('projections'), Symbol('errs'), Symbol('anglist'), Symbol('proj_list'), Symbol('back_projection'), Symbol('cross_corr'), Symbol('volfile'), Symbol('freqs'), Symbol('num_angles'), Symbol('angle_file'), Symbol('fsc_vals')])), Symbol('iteration'), Symbol('deltheta')])), List(aList([List(aList([Symbol('volume'), Symbol('projections'), Symbol('errs'), Symbol('anglist'), Symbol('proj_list'), Symbol('back_projection'), Symbol('cross_corr'), Symbol('volfile'), Symbol('freqs'), Symbol('num_angles'), Symbol('angle_file'), Symbol('fsc_vals')])), Symbol('iteration'), Symbol('deltheta')])), Symbol('dtheta'), Symbol('spi'), Symbol('ref_vol')]))

dir_view = w_.canvas.view.canvas_add_ast(dir_struct)
dir_view.move(20, 20)

#
import gtk
gtk.main()                              # use menu->exit

#*    presentation-may-2006
#**        info
# xterm reference size 120x50
# plot:
# convert /tmp/foo.ps -rotate 90 -resize 400x400 /tmp/foo.png
#**        library setup
tm_tree = reader.parse("""
list "library":
    list "em tasks":
        anglist2doc
        ccc
        fsc
        getImage
        gridproj
        imglist2stack
        spidbprg
        voea

    list "eman2 members":
        volume.save

    list "spider members":
        spi.bprg

    list "tools":
        init_spider
        setup_directory
        plot

    list "programming":
        PROGRAM
        { |x,y| x - y }
        a
        1
        1.1
        (a = 2)
        call_function(a, b)
        []
        {a = 1, b = 2}
        if condition:
            yes
        else:
            no

        inline "from l3lang.test.voea_bp_support import *"

        loop name(arg) from (start_val):
            body
""")

tm_play_env = interp.get_child_env(def_env, w_.state_.storage)
tm_tree.setup(empty_parent(), tm_play_env, w_.state_.storage)

tm_tree_l3pic = w_.canvas.view.start_add_l3tree(tm_tree)

gtk.main()

#
#**        value insertion preview
# Figure
canvas_add_1st_expr("""
# ccc
[
0.8645,
0.3051,
0.1503,
0.2821,
0.5498,
]
""")
canvas_add_1st_expr("""
# deltheta
[
42,
34,
26,
18,
10,
]
""")

canvas_add_program("""
inline "from l3lang.test.voea_bp_support import *"
plot(
# deltheta
[
42,
34,
26,
18,
10,
],
# ccc
[
0.8645,
0.3051,
0.1503,
0.2821,
0.5498,
]
)
""", xx=30, yy=50)


#*    sparx functions as of [Jun-13-2006]
#
#**        Changed sparx imports to fix this.
#**        Trying to get an overview of the sparx functions
# as of [Jun-13-2006]
import sparx
pprint(dir(sparx))

# For this, the module structure SHOULD be useful.  But it isn't
# available interactively.

# The following doesn't do much good.  There are no comments, and
# e.g. do_alignment (visible via dir(sparx)) is not shown at all.
help(sparx)

# This fails ...
from sparx import libpy
# with ImportError: cannot import name libpy.
# Checking,
sparx
# is <module 'sparx' from '/net/hooknose/scratch1/hohn/rh9/ia32/installer-0.5/sparx-bin/opt/sparx/libpy/sparx.pyc'>

# All because of the
#     from utilities import *
#     from filter import *
#     from projection import *
#     from fundamentals import *
#     from statistics import *
#     from alignment import *
#     from morphology import *
#     from reconstruction import *
# in libpy/sparx.py!
help(sparx.do_alignment)

# So, I manually get the structure.
# sparx
#     utilities
#     filter
#     projection
#     fundamentals
#     statistics
#     alignment
#     morphology
#     reconstruction

# For nested modules, this should give information
prfx, lst = ("sparx",
             ["utilities",
              "filter",
              "projection",
              "fundamentals",
              "statistics",
              "alignment",
              "morphology",
              "reconstruction",
              ]
             )
[help("%s.%s" % (prfx, itm)) for itm in lst]

# But here it fails with
#     no Python documentation found for 'sparx.utilities'
#     no Python documentation found for 'sparx.filter'
#     no Python documentation found for 'sparx.projection'
#     ...
# again because of the import *
#
#**        Overview of the sparx functions as of [Tue Jun 13 13:05:25 2006]
import sparx.libpy as spx
pprint(dir(spx))

# module/function listing
for mod in dir(spx):
    if not is_module(mod): continue
    for func in dir(mod):
        if not is_function(func): continue
        print "module: %s\t\tfunction: %s" % (mod, func)


help(sparx)
#**        sparx.gui  console use:
import os
tm_tree = reader.parse_file(
    os.path.expandvars('$SPXROOT/l3gui/test/iref01.py'))
tm_play_env = interp.get_child_env(def_env, w_.state_.storage)
tm_tree.setup(empty_parent(), tm_play_env, w_.state_.storage)
tm_tree_l3pic = w_.canvas.view.start_add_l3tree(tm_tree)
gtk.main()

#*    sparx persistence
#**        eman 'native' I/O
import sparx as sx
volume = sx.getImage( "model.tcp" )

raw = sx.project(volume, [10.0, 20.0, 30.0, 0.0, 0.0], 35)
raw.set(sx = 0)
sx.info(raw)
print raw.get_attr_dict()

# Looks like spider format doesn't keep the attributes:
sx.dropImage(raw, "raw.spi", itype = 's')
raw_s = sx.getImage("raw.spi")
sx.info(raw_s)
print raw_s.get_attr_dict()
print raw_s.sx

# hdf works:
sx.dropImage(raw, "raw.hdf")
raw_s = sx.getImage("raw.hdf")
sx.info(raw_s)
print raw_s.get_attr_dict()
print raw_s.sx
#**        pickling
# Make sure there is no EMData.__getattr__ to avoid '__getinitargs__':
# 'Not exist' caught crap.
import l3lang.utils as ut
ut.file_pickle(raw)
raw_p = ut.file_unpickle("/tmp/foo")
sx.info(raw)
sx.info(raw_p)
print raw.get_attr_dict()
print raw_p.get_attr_dict()

# 
#**        l3 state table pickling
w_.state_.storage
import l3lang.utils as ut
ut.file_pickle(w_.state_.storage, "st.post-run-table")
# takes forever; remove 

#*    comment attachment
# Similar to reader.raw_lexer(code_s)
# 
import sys, token, tokenize
code_s = open(os.path.expandvars('$SPXROOT/l3gui/test/iref01.py')).read()

#
#**        comment parsing
# Comment type identifier.
COMMENT, _ = filter(lambda (k,v): v == 'COMMENT', token.tok_name.items())[0]
NEWLINE, _ = filter(lambda (k,v): v == 'NEWLINE', token.tok_name.items())[0]
NL, _ = filter(lambda (k,v): v == 'NL', token.tok_name.items())[0]


# 
#***            Cluster connected successive comment lines.
# When connecting to the parse tree, this style of clustering has a
# problem.  Given
#
#   # comment 1
#
#   # comment 2
#
#   code 1
#   code 2
#
# `comment 1` will attach to `code 1`, leaving `comment 2` to attach to
# `code 2`, which is not good.
#  
# Choices:
#   - drop one of the comments, attach other to `code 1`
#   - combine comments and attach to `code 1`
#   
gen = tokenize.generate_tokens(reader.string_readline(code_s))
st_accum = 0 
va_cluster = []                         # (string, begin, end) list
va_accum = []                           # va_cluster list
for ii in gen:
    (type, string, begin, end, line) = ii

    # Dump every comment. 
    if type == COMMENT:
        print "(%r, %s, %s)" % (string, begin, end)  

    # Cluster comment lines.
    if type in [COMMENT]:
        st_accum = 1
        va_cluster.append( (string, begin, end) )

    else:
        if st_accum:
            va_accum.append(va_cluster)
            st_accum = 0
            va_cluster = []

# Dump data.
pprint(va_accum, sys.stdout, 4, 2)

# 
#***            Cluster successive (incl.separated) comment lines
# Combine blank-separated comment blocks.
gen = tokenize.generate_tokens(reader.string_readline(code_s))
st_accum = 0 
va_cluster = []                         # (string, begin, end)  list
va_accum = []                           # va_cluster list
for ii in gen:
    (type, string, begin, end, line) = ii

    #     # Dump every comment. 
    #     if type == COMMENT:
    #         print "(%r, %s, %s)" % (string, begin, end)  

    # Cluster successive comment lines, even those separated by blanks.
    if type in [COMMENT]:
        st_accum = 1
        va_cluster.append( (string, begin, end) )

    elif type in [NEWLINE, NL]:
        if st_accum:
            va_cluster.append( (string, begin, end) )
    else:
        if st_accum:
            # Remove trailing empty newlines.
            tl = ['\n']
            while tl[0] == '\n':
                tl = va_cluster.pop()
            va_cluster.append(tl)
            # Keep cluster.
            va_accum.append(va_cluster)
            st_accum = 0
            va_cluster = []

pprint(va_accum)

# Print cluster.
for clus in va_accum:
    print clus[0][1]                    # begin
    for line in clus:
        print line[0],                   # body
    print clus[-1][2]                    # end
    print

# 
#***            Simplify comment structure
# Adjust lexeme row indexing, compact clusters, and remove unused data.
va_comments = []                        # ((last_row, first_col), string) list
va_comments = [
    ( (clus[-1][2][0] - 1, clus[0][1][1]),  # (last_row, first_col)
      "".join([line[0] for line in clus]))
    for clus in va_accum ]
pprint(va_comments)

# 
#**        Tree work
tree = reader.parse(code_s)

#
#***            extract a list of leading subnodes from the parse tree
lrow, lcol = -1, -1
tr_lines = []                           # [(start_row, col), node] list
for nd in tree.subtrees():
    (row, col), _ = nd.get_char_range()
    # Prune all but first expression on a line.
    if row != lrow:         
        tr_lines.append( [(row,col), nd] )
    ## tr_lines.append( [(row,col), nd] )
    lrow = row
pprint(tr_lines)


# 
#**        Find expression to attach to.
#   Consider only the first expression in given line.
#   Find the first expression following a comment end.
# 
def align_and_show():
    com_it = iter(va_comments)
    node_it = iter(tr_lines)
    com_row = lambda com: com[0][0]
    while 1:
        try: com = com_it.next()
        except StopIteration: break
        try: nd = node_it.next()
        except StopIteration: break
        com_line = com[0][0]
        # Find first line after current comment.
        while nd[0][0] <= com_line:
            nd = node_it.next()
        if com[0][1] != nd[0][1]:
            print "==== column mismatch"
        print com[1]
        print nd[1].source_string()
align_and_show()

##  import pdb; pdb.set_trace() 
##  

#*    selection handling
#   See pygtk-2.4.1/examples/ide/edit.py as example.

#*    gtk assertions
import warnings
warnings.filterwarnings("error", "assertion") # , GtkWarning

# But the C code just continues on.

# ... the bigger problem is that a fair amount of C code doesn't
# handle the case where the assertion fails ...



# Cause a sigsev
view = w_.canvas.view
ra = w_.canvas.view.root().add
c_label = ra(canvas.CanvasText,
             x = 12, y = 12,
             size_points = 12 * 1,
             scale = 1.0,
             scale_set = True,
             text = "the test",
             )
flush_events()
print c_label.get_bounds()
print c_label.__grefcount__
c_label.destroy()

#*    gtk proxy test
view = w_.canvas.view
ra = GtkObjProxy(w_.canvas.view.root())
c_label = GtkObjProxy(ra.add(canvas.CanvasText,
                          x = 12, y = 12,
                          size_points = 12 * 1,
                          scale = 1.0,
                          scale_set = True,
                          text = "the test",
                          ))
flush_events()
print c_label.get_bounds()
c_label.destroy()
print c_label._live
print c_label.get_bounds()


#*    python compile tests
# Accepted as complete input...
print compile('''
if 1:
    1
    2

    3
else:
    3
''', '<string>', 'single')


#*    l3 tests

# test if 
if a > 0:
    yes
else:
    no

# generic if (cond)

[
    # if
    x < 0,
    -x,
    # elif
    x = 0,
    0,
    # elif
    x > 0,
    x,
    # else
    True,
    0,
]

#*    gtk object pickling
# local eval of l3Rawtext
import l3lang.utils as ut
# Runs (works? Protocol 1 fails.).
ut.file_pickle( self._ltext.get_property("anchor"))
val = ut.file_unpickle("/tmp/foo")
val
# oops:
#     (.:19911): GLib-GObject-WARNING **: cannot retrieve class for
# invalid (unclassed) type `<invalid>' 
# 
# ** ERROR **: file pygenum.c: line 60 (pyg_enum_repr): assertion
# failed: (G_IS_ENUM_CLASS(enum_class)) 
# aborting...


#*    l3IfLoop addition
#**        test single loop
test_s = """
if "loop":
    def lname(largs):              
        lbody
    lname(cargs)                   
"""                                     # "

# 
#**        test expression
test_s = """
if "loop":
    def _loop(bar):
        #    calc 3d reconstruction   
        foo = bar + 10
        if "loop":
            def _lname(foo):
                if done:
                    return bar
                #    calc 3d reconstruction   
                #    filter                   
                1 
                #    repeat from gen. proj
                if 1:
                    return _lname(2 - foo)
            _lname(foo)                   

        #    filter                   
        2
        #    repeat from gen. proj
        if good:
            return _loop(bar + 1)
    _loop(1)                   
"""                                     # "
l3tree = reader.parse(test_s)
l3tree

# 
#**        l3ifloopbreak pattern 1
code_s = """
if COND:
    return LNAME(LARGS)
"""                                     # "
template = reader.parse(code_s)

# 
#**        l3ifloopbreak pattern 2
code_s = """
if ! COND:
    return ARGS
"""                                     # "
template = reader.parse(code_s)


# 
#**        test single loop w/ break
test_s = """
if "loop":
    def lname(largs):              
        first
        if 1:
            return 2
        last
    lname(cargs)                   
"""                                     # "

# 
#**        test single loop w/ continue
test_s = """
if "loop":
    def lname(largs):              
        first
        if 1:
            return lname(new_args)
    lname(cargs)                   
"""                                     # "


# 
#**        l3ifloop pattern
code_s = """
if "loop":
    def LNAME(LARGS):              
        LBODY
    LNAME2(CARGS)                   
"""                                     # "
template = reader.parse(code_s)
template.body()

# 
#**        matcher
# From template.body().
ma = ast.Matcher()
body = l3tree.body()
if ma.match(body,
            If(String('loop'),
               aList([Set(MarkerTyped(Symbol('LNAME'), Symbol('symbol')),
                          Function(MarkerTyped(Symbol('LARGS'), aList([])),
                                   MarkerTyped(Symbol('LBODY'), aList([])))),
                      Call(MarkerTyped(Symbol('LNAME2'), Symbol('symbol')),
                           MarkerTyped(Symbol('CARGS'), aList([])))]),
               Marker('_'))):
    if ma['LNAME'] != ma['LNAME2']:
        warn_("loop name mismatch; no special display")
    elif len(ma['LARGS']) != len(ma['CARGS']):
        warn_("arguments mismatched between loop definition and call")
    else:
        print_(ma['LNAME'])
        print_(ma['LARGS'])
        print_(ma['CARGS'])
        print_(ma['LBODY'])


#*    l3 tests
#**        load l3
from l3lang.l3 import *
run()
#**        interpretation order
def foo():
    'foo'
foo()
# 
#**        args
def foo(a,b, args):
    return args
foo(1,2, { a = 1})
# 
#**        print
print 1, "hello", 2 
print "hello", "world"
# 
#**        Function
inline "from math import *"
def f(a,b, c = cos(22), d = 10):
    c
def g(a,b):
    b
# errors:
f(1)                                    # too few
f(1,2,3)                                # too many
f(1,2, e = 1)                           # unknown keyword

# ok
p = g(1,2)
p = f(1,2, c = cos(22), d = 1)
p = f(1,2, c = 22, d = 1)
p = f(1,2, c = 'c')
p = f(1,2)
#**        Call to python
inline '''
def pyf(a,b, c=1):
    return a,b,c        
'''
pyf(10,20)
pyf(10,20, c = 30)
#**        If
last_ring = 100
if(last_ring==-1):
    last_ring=nx-2

(last_ring == -1)

# Ok:
last_ring == -1
last_ring <= -1


# old errors (pre-r460)
print parse('a = b')
print parse('a == b')
# error (r439):
print parse('(a == b)')
print parse('(a <= b)')
# ok:
print parse('(a = b)')
print parse('(av1*(int(nima/2)+(nima%2)) + av2*int(nima/2))/nima')


#*    l3 tests, in gui (repeated interpretation checks)
program:
    filter(lambda x: x > 2, [1,2,3,4])

if 1:
    inline "from l3lang.l3 import *"
    def f(a,b, c = cos(22), d = 10):
        c
    f(1,2)
    f(1, 2, c = 22)

if ("prog", "filter test"):
    filter(lambda x: x > 2, [1,2,3,4])

program:
    # subtract
    a - b

program:
    6* (3 - 2)

# using > "eval locally"
# >>> self._l3tree
# Program(aList([Call(Symbol('*'), aList([Int(6L), Call(Symbol('-'), aList([Int(3L), Int(2L)]))]))]))
# >>> self._l3tree._first_char
# (0, 0)
# >>> self._l3tree._last_char
# (2, 0)



#*    statement additions
#**        is, is not, not in
code_s = """
a is not None
b not in [c,d]
print hello, "world"
"""
reader.raw_lexer(code_s)


#*    l3 internals tests (from python)
#**        Call
tree = reader.parse('f(1,2, c = sin(22), d = 10)')

play_env = interp.get_child_env(def_env, storage)
tree.setup(empty_parent(), play_env, storage)
view.print_info(storage, tree, show_macros = True)

# Internals.
print tree.body().positional_args()
print tree.body().nargs()
print tree.body().named_args()
# Interpret.
tree.interpret(def_env, storage)
#**        Function
tree = reader.parse('''
def f(a,b, c = cos(22), d = 10):
    c
''')                                    # '''

play_env = interp.get_child_env(def_env, storage)
tree.setup(empty_parent(), play_env, storage)
view.print_info(storage, tree, show_macros = True)

func_ = tree.body()[1]
print func_.positional_block_args()
print func_.named_block_args()
print func_.nargs()

# Interpret.
tree.interpret(def_env, storage)
# 
#**        macro
#***            parse 
from l3lang.view import print_ast
def prnt(str):
    print_ast(reader.parse(str))
prnt('''
def "foo"():
    bar
''')
#***            eval 1
tree = reader.parse('''
aa = 1
def "foo"():
    print aa
foo()
''')

# Setup.
play_env = interp.get_child_env(def_env, storage)
tree.setup(empty_parent(), play_env, storage)
view.print_info(storage, tree, show_macros = True)

# Interpret.
tree.interpret(def_env, storage)
view.print_info(storage, tree)
view.print_all_info(storage, tree)
# ok: repeated .interpret() reuses first clone.
# 
#***            parse 2
code_s = ('''
aa 
print aa ;
bb
''')
reader.py_lexer(code_s)
print reader.parse(code_s)
# 
#***            eval 3 - single call
code_s = ('''
aa = 1
def "foo"():
    print aa
    aa 
foo()
''')
# reader.raw_lexer(code_s)
# reader.py_lexer(code_s)
tree = reader.parse(code_s)

# Setup.
play_env = interp.get_child_env(def_env, storage)
tree.setup(empty_parent(), play_env, storage)
view.print_info(storage, tree, show_macros = True)

# Interpret.
tree.interpret(play_env, storage)
view.print_info(storage, tree)
view.print_all_info(storage, tree)
# repeated .interpret() reuses first clone ?

# 
#***            eval 4 - variable value change
code_s = ('''
aa = 1
def "foo"():
    print aa
    aa = aa + 1
    print aa
foo()
''')
# reader.raw_lexer(code_s)
# reader.py_lexer(code_s)
tree = reader.parse(code_s)

# Setup.
play_env = interp.get_child_env(def_env, storage)
tree.setup(empty_parent(), play_env, storage)
view.print_info(storage, tree, show_macros = True)

# Interpret.
tree.interpret(play_env, storage)
view.print_env(play_env)
view.print_info(storage, tree)
view.print_all_info(storage, tree)
# repeated .interpret() reuses first clone ???

# 
#***            eval 5 -- loop
code_s = ('''
aa = 1
def "foo"():
    print aa
    aa = aa + 1
    if aa < 5:
        foo()
foo()
''')
# reader.raw_lexer(code_s)
# reader.py_lexer(code_s)
tree = reader.parse(code_s)

# Setup.
play_env = interp.get_child_env(def_env, storage)
tree.setup(empty_parent(), play_env, storage)
view.print_info(storage, tree, show_macros = True)

# Interpret.
tree.interpret(play_env, storage)
view.print_all_info(storage, tree)
tree.interpret(play_env, storage)
view.print_all_info(storage, tree)

view.print_info(storage, tree)
view.print_env(play_env)
# repeated .interpret() reuses first clone ???

#

#*    while loop
# 
#**        eval 6 -- `while` expansion
# '''
#   while C:
#       B

# Python
a = 1
while a < 3:
    print a
    a = a + 1
    
# l3 expanded
code_s = ('''
a = 1
if "while":
    def "_while_123"():
        # Condition
        if not (a < 3): 
            return 
        else:
            # Body
            print a
            a = a + 1
            # end Body
        return _while_123()
    _while_123()
''')
tree = reader.parse(code_s)

# Setup.
play_env = interp.get_child_env(def_env, storage)
tree.setup(empty_parent(), play_env, storage)

# Interpret.
tree.interpret(play_env, storage)
view.print_all_info(storage, tree)

tree.interpret(play_env, storage)
view.print_all_info(storage, tree)

view.print_env(play_env)

# 
#**        eval 7 -- `while` template
# '''
#   while C:
#       B

# l3 expanded
code_s = ('''
if "while":
    def "_while_ID"():
        if not C: 
            return 
        else:
            B
        return _while_ID()
    _while_ID()
''')
tree = reader.parse(code_s)

# Setup.
play_env = interp.get_child_env(def_env, storage)
tree.setup(empty_parent(), play_env, storage)
view.print_all_info(storage, tree)


# 
#**        eval 8 -- `while` tests
# '''
#***            execution
code_s = ('''
a = 1
while a < 3:
    print a
    a = a + 1
''')
tree = reader.parse(code_s)

# Setup.
play_env = interp.get_child_env(def_env, storage)
tree.setup(empty_parent(), play_env, storage)
view.print_info(storage, tree)

# Interpret.
tree.interpret(play_env, storage)
view.print_all_info(storage, tree)

tree.interpret(play_env, storage)
view.print_all_info(storage, tree)

view.print_env(play_env)
# 
#***            reference comparison
# Remove id / timestamp columns, then compare per-character.
ref_s = ('''
a = 1
if "while":
    def "_while_ID"():
        # Condition
        if not (a < 3): 
            return 
        else:
            # Body
            print a
            a = a + 1
            # end Body
        return _while_ID()
    _while_ID()
''')
ref_t = reader.parse(ref_s)
# Setup.
ref_env = interp.get_child_env(def_env, storage)
ref_t.setup(empty_parent(), ref_env, storage)
view.print_info(storage, ref_t)

# 
#**        eval 9 -- `while` pattern 
# '''
# l3 expanded
code_s = ('''
if "while":
    def "_while_ID"():
        if not COND: 
            return 
        else:
            BODY
        return _while_ID()
    _while_ID()
''')
tree = reader.parse(code_s)

# 
#**        while tests
program:
    a = 1
    while a < 3:
        print a
        a = a + 1

# Nesting, check identifiers.
program:
    a = 1
    while a < 3:
        print a
        a = a + 1
        bb = 1
        while bb < 3:
            print a, bb
            print a * bb
            bb = bb + 1




#*    for loop language addition
# for loop using while
# '
#**        iterator wrapper
class l3_iter:
    def __init__(self, seq):
        self._iter = iter(seq)
        self._next = None
        self._next_ready = False

    def more(self):
        if self._next_ready:
            return True
        try:
            self._next = self._iter.next()
            self._next_ready = True
            return True
        except StopIteration:
            self._next_ready = False
            return False

    def next(self):
        if self.more():
            self._next_ready = False
            return self._next
        raise InterpreterError("Request for elements from finished iterator.")
            
# 
#**        original for
# 
#     for V in I: 
#         B
# 
for (x,y) in [(1,2), (3,4)]:
    print x, y

# 
#**        expand to while
program:
    I = l3iter([(1,2), (3,4)])
    while I.more():
        (x,y) = I.next()
        print x, y

# To avoid pickling [(1,2), (3,4)] or I, use
# 
#     inline 'I = l3iter([(1,2), (3,4)])'
# 
program:
    inline 'from l3lang.interp import l3iter; I = l3iter([(1,2), (3,4)])'
    while I.more():
        (x,y) = I.next()
        print x, y


#
#**        embed l3 'tail call' form for while:
# while C:
#     B
# 
# if "while":
#     def "_while_ID"():
#         if not C:           # force boolean evaluation via not
#             return 
#         else:
#             B
#         return _while_ID()
#     _while_ID()
# 
program:
    if "for":
        inline 'from l3lang.interp import l3iter; I = l3iter([(1,2), (3,4)])'
        # orig. for
        def "_for_ID"():
            if not I.more(): 
                return 
            else:
                (x,y) = I.next()
                print x, y
            return _for_ID()
        _for_ID()

#
#**        iterator problems
# Problems:
# 1. The sequence cannot be edited (it's a string)
# 2. The indirection
#     - (value list VL) -> external (l3iter) -> (value sequence VS)
#    loses the time stamps between VL and VS, so that updates to VL
#    re-evaluate everything.

#    Thus, iterators are USELESS in l3: no persistence and no
#    incremental evaluation.

# 3. Are I.more and I.next called again on repeated interpretation?

# 4. External iterator assignments will cause failure:

for V in I:
    B

# is ok, but

I1 = get_iter()
for V in I1:
    B

# will fail.

# MAJOR PROBLEM:
#       Repeat execution of UNORDERED iterators will always fail.
# l3 execution imparts unique ids, IN ORDER of execution.  
#
# Retrieving all iterator values and storing them is not an option
# because iterators need not be finite.
#
# A prime example:  a true rand() MUST NOT be used in l3.  Unless it's
# a pseudo-random, FIXED sequence.


# [Thu Jul 12 11:57:28 2007]
# A possible adjustment is to let Call.interpret return all iterators
# as externals, and evaluating every time as done for Inline().  If
# done that way, the for loop also simplifies.

program:
    if "for":
        I = l3iter([(1,2), (3,4)])
        # orig. for
        def "_for_ID"():
            if not I.more(): 
                return 
            else:
                (x,y) = I.next()
                print x, y
            return _for_ID()
        _for_ID()

#
#**        iterator problems, part 2
# ---------------------------------
# Re-examining iterators in l3 is slow and makes interpretation yet
# more complicated.  Instead, 

# 1. make sure that l3iter instances pickle,

# 2. assume (and test that) the iterator given to l3iter produces a
#    constant sequence, and

# 3. let l3iter re-run the iterators (after unpickling, or better at
#    the first call to I.more) to get to the correct position in the
#    stream.
#    If the iterator itself performs a lot of work, nothing is gained
#    here... 

# Retrieving the iterator is a problem.
# It cannot be saved with the l3iter instance, but re-running
# l3iter(iterator) will create a new instance (losing the
# timestamp/position info).
# This requires some internal magic... 

#
#**        simplify to sequences
# ----------------------
# Backtrack to Python < 2.2, without the __iterator__ mess.  There,
#   ...
#   The for statement is used to iterate over the elements of a
#   sequence (string, tuple or list):
#   ...
# The
# sequence types support the len() and [] operators (__len__ and
# __getitem__).  Restricting to known-size sequences is a minor
# restriction for tracked, DISPLAYED items.

# For general iterators and simple uses, use Python directly.  The
# sample 

for k in edsummary.iterkeys(): 
    print k + ": %.3f" % edsummary[k]

# uses an unordered iterator, so it cannot work in l3.  It also performs
# a trivial task not worth tracking, so it could be in python.

# For this example, using 
for k in edsummary.keys(): 
    print k + ": %.3f" % edsummary[k]
# in l3 would be better.

# For sequences providing len() and getitem(), there are two choices:
#   1. another wrapper ( l3traverse() ) can be added, and the
#      following should work:  
program:
    if "for":
        I = l3traverse([(1,2), (3,4)])
        # orig. for
        def "_for_ID"():
            if not I.more(): 
                return 
            else:
                (x,y) = I.next()
                print x, y
            return _for_ID()
        _for_ID()
#   However, this wrapper would require access to l3 internals, so an
#   l3 version is preferable:

#   2. Use a tail call form to handle indexing 
# 
# for V in S:
#     B
# 
# In the following, ITEMS, IDX, LEN and LOOP are leaf nodes, to be
# replaced UNIQUELY in if.setup().
# 
# See also ref[Wed Jul 18 11:38:10 2007]
# 
program:
    if "for":
        ITEMS = [(1,2), (3,4)]
        IDX = 0
        LEN = len(ITEMS)
        # orig. for
        def "LOOP"():
            if IDX < LEN: 
                IDX = IDX + 1
                # V in S.
                (x,y) = ITEMS[ IDX - 1 ]
                # Body B
                print x, y
                # Iterate.
                return LOOP()
        LOOP()

#
#**        for tests
# List as text (no insertions)
program:
    for i in [1,2,3]:
        print i

# List as structure.
program:
    for i in [1,
              2,3]:
        print i

program:
    for (i, x) in [(1, 1), (2, 4), (3,9)]:
        print i, x

# Nesting, check identifiers.
program:
    for i in [1,2,3]:
        print i
        for jj in [1,2,3]:
            print i, jj, i*jj



#*    scitbx flex array
from scitbx.array_family import flex
data = flex.random_double(size = 4)

data.reshape(2,2)
# Traceback (most recent call last):
#   File "<console>", line 1, in ?
# ArgumentError: Python argument types in
#     double.reshape(double, int, int)
# did not match C++ signature:
#     reshape(scitbx::af::versa<double, scitbx::af::flex_grid<scitbx::af::small<long, 10u> > > {lvalue}, scitbx::af::flex_grid<scitbx::af::small<long, 10u> >)


#*    slice and getitem
#**        python slice
import numpy as N
import operator
na = N.arange(28).reshape((7,4))

print na[1,2]
# DIFFERENT:
print na.__getitem__( [1,2] )
print na.__getitem__( (1,2) )           # correct

print na[1:4,2]
print na.__getitem__( (slice(1,4), 2) )

print na[1:4]
print na.__getitem__( (slice(1,4), ) )
print na.__getitem__( [slice(1,4)] )

print na[1:3, 3:6]
print na.__getitem__( (slice(1,3), slice(3,6)) )
print operator.getitem(na, [slice(1,3), slice(3,6)] )
print operator.getitem(na, (slice(1,3), slice(3,6)) )

# Python reference values:
print na[1,2]
print na[1:4,2]
print na[1:4]
print na[1:3, 3:6]

# 
#**        l3 slice
# Compare with # Python reference values:
inline '''
import numpy as N
'''
na = N.arange(28).reshape((7,4))
print na
print na[1,2]
print na[1:4,2]
print na[1:4]
print na[1:3, 3:6]




#*    gtk images, numeric, eman
#'''
import EMAN2 as em
from EMAN2 import EMNumPy
import Numeric as N
import MLab as M
# 
#**        numeric -> eman
ema_arr = EMAN2.EMData()
EMNumPy.numpy2em(num_arr, ema_arr)
# 
#**        eman -> numeric
foo = em.EMData()
num_arr = EMNumPy.em2numpy(foo)
# 
EMNumPy.em2numpy(None)
# 
#**        numeric -> gtk
#***            1. get numeric array
num_arr = M.rand(4,4)
# 
#***            2.b. numeric -> gtk
#   Convert float array to 8-bit array.
rows, cols = num_arr.shape
bottom = M.min(M.min(num_arr))
top = M.max(M.max(num_arr))
scale = (top - bottom)
norm_arr = ((num_arr - bottom) / scale * 255).astype('b')
# 
#***            Use 3 8-bit arrays as (rgb) values.
t3 = N.outerproduct(norm_arr, N.array([1,1,1], 'b'))
t4 = N.reshape(t3, (rows, cols, 3))
pix_arr = N.array(t4, 'b')
# 
#***            Get the pixbuf.
# array input indexing is row, pixel, [r,g,b {,a} ]
pixbuf = gtk.gdk.pixbuf_new_from_array(pix_arr,
                                       gtk.gdk.COLORSPACE_RGB, 8)
# 
#***            3. display pixbuf as item
d_canv = w_.canvas.view
d_rt = d_canv.root().add(canvas.CanvasGroup)
d_rt.add(canvas.CanvasPixbuf,
         x = 20,                        # world units
         y = 20,
         height_set = True,
         width_set = True,
         width = 15,                    # world units
         height = 15,
         pixbuf = pixbuf,
         )
d_rt.destroy()

# Image orientation is consistent with `norm_arr` printed form.
# Indexing is (row, col) from upper left.
# Storage is row-major?

# This is the first view of a VALUE -- the others are astTypes.
# Useful value information is the program source.  


# "anchor"               GtkAnchorType         : Read / Write
# "height"               gdouble               : Read / Write
# "height-in-pixels"     gboolean              : Read / Write
# "height-set"           gboolean              : Read / Write
# "pixbuf"               GdkPixbuf             : Read / Write
# "width"                gdouble               : Read / Write
# "width-in-pixels"      gboolean              : Read / Write
# "width-set"            gboolean              : Read / Write
# "x"                    gdouble               : Read / Write
# "x-in-pixels"          gboolean              : Read / Write
# "y"                    gdouble               : Read / Write
# "y-in-pixels"          gboolean              : Read / Write


# # Other pixmap sources:
# image = gtk.Image()
# # use the current directory for the file
# try:
# pixbuf = gtk.gdk.pixbuf_new_from_file(GTKLOGO_IMAGE)
# image.set_from_pixbuf(pixbuf)
# # 
# image = gtk.Image()
# image.set_from_file(BUDDY_IMAGE);
# # image -> pixbuf
# image.get_pixbuf()
# # pixbuf -> canvas

# gtk.gdk.pixbuf_new_from_array
# gtk.gdk.Pixbuf.get_pixels_array
# gtk.gdk.Pixbuf.save
# call gtk.Image.queue_draw() after you change the Numeric array.


# # from EMAN2.EMNumPy import numpy2em, em2numpy


#*    gtk images, numpy, eman
#'''
import EMAN2 as em
from EMAN2 import EMNumPy
import numpy as N
# 
#**        numpy -> eman
ema_arr = em.EMData()
EMNumPy.numpy2em(num_arr, ema_arr)

# 
#**        eman -> numpy
import EMAN2
ti = EMAN2.test_image(type = 1, size = (128,64))
num_arr = EMNumPy.em2numpy(ti)
print num_arr.shape
# [Fri Mar 16 16:25:40 2007]: transposed: (64, 128)
# [Mon Mar 19 11:23:08 2007]: eman2 fix by Grant Tang. 
# [Thu Mar 22 10:36:14 2007]: order confusion between eman2/hdf/numpy...

# 
#**        numpy -> gtk
#***            1. get numpy array
num_arr = N.random.ranf( (5,4) )
# 
#***            2.b. numpy -> gtk
#   Convert float array to 8-bit array.
rows, cols = num_arr.shape
bottom = N.min(num_arr)
top = N.max(num_arr)
scale = (top - bottom)
norm_arr = ((num_arr - bottom) / scale * 255).astype(N.uint8).reshape(rows*cols)

# 
#***            Use 3 8-bit arrays as (rgb) values.
t3 = N.outer(norm_arr, N.array([1,1,1], 'b'))
t4 = N.reshape(t3, (rows, cols, 3))
pix_arr = N.array(t4, dtype=N.uint8)
# 
#***            Get the pixbuf.
# array input indexing is row, pixel, [r,g,b {,a} ]
#   TypeError: pixbuf_new_from_array() argument 1 must be array, not
#   numpy.ndarray  
# pixbuf = gtk.gdk.pixbuf_new_from_array(pix_arr,
#                                        gtk.gdk.COLORSPACE_RGB, 8)
pixbuf = gtk.gdk.pixbuf_new_from_data(
    pix_arr.tostring(order='a'),
    gtk.gdk.COLORSPACE_RGB,
    False,
    8,
    cols,
    rows,
    3*cols)
# 
#***            compare to input
pixbuf.get_pixels_array()
print pixbuf.get_pixels_array().shape
print pixbuf.get_height()
print pixbuf.get_width()

# 
#***            3. display pixbuf as item
d_canv = w_.canvas.view
d_rt = d_canv.root().add(canvas.CanvasGroup)
d_rt.add(canvas.CanvasPixbuf,
         x = 20,                        # world units
         y = 20,
         height_set = True,
         width_set = True,
         width = 15,                    # world units
         height = 15,
         pixbuf = pixbuf,
         )
d_rt.destroy()

#*    minimal EMData display test
#**        gui
# paste:
program:
    inline("import EMAN2 ")
    ti = EMAN2.test_image(type = 1, size = (128,64))
# program > menu > run code
# ti > menu > insert values

# 
#**        console
tm_tree = reader.parse('''
inline("import EMAN2 ")
ti = EMAN2.test_image(type = 0, size = (64,64))
''')
tm_play_env = interp.get_child_env(def_env, w_.state_.storage)
tm_tree.setup(empty_parent(), tm_play_env, w_.state_.storage)
tm_l3pic = w_.canvas.view.start_add_l3tree(tm_tree)

# run
tm_l3pic._l3tree.interpret(w_.state_.def_env, w_.state_.storage)
# insert list
val, val_id = ast.val2ast(tm_l3pic.get_values_list())\
             .setup(empty_parent(),
                    Env('dummy_env', None, None, storage),
                    storage)
w_.canvas.view.add_l3tree(val, RenderTable())

gtk.main()

#*    l3IfOutline addition
#   High-level interface via outlining.
#**        original tree (simple)
# Body from demo/proj-align/s1a.py, truncated for simplicity.
ref_t = reader.parse_file("./demo/proj-align/s1a-in.py")
ref_env = interp.get_child_env(def_env, storage)
ref_t.setup(empty_parent(), ref_env, storage)
view.print_info(storage, ref_t)

# outline-regexp
# (set-variable (quote outline-regexp) "[ 	]*\\#+\\*+")
# (set-variable (quote outline-regexp) "\\W+\" +" nil)
# 
#**        OR: original tree (full)
ref_t = reader.parse_file("./demo/proj-align/s1a.py")
ref_env = interp.get_child_env(def_env, storage)
ref_t.setup(empty_parent(), ref_env, storage)
# view.print_info(storage, ref_t)
#**        add outline cross-refs
ref_t.set_outl_edges(w_, None)
#**        examine embedded outline (text)
view.print_info(storage, ref_t, custom = "nd._outl_parent._id")
#**        display outline
ref_pic = w_.canvas.view.start_add_l3tree(ref_t)

#
#**        deprecated
# 
#***            outline tree, flat
ref_o = ref_t.get_outline(w_)
print ref_o

ref_prun = ref_o.prune(w_)
print ref_prun
view.print_info(storage,  ref_prun, custom = "nd._real_tree._id")

# 
#***            OR: outline tree, nested
ref_o = ref_t.get_outline(w_, outl_type = 'nested')
print ref_o
view.print_info(storage,  ref_o)

ref_prun = ref_o.prune(w_)
print ref_prun
view.print_info(storage,  ref_prun)
# 
#***            OR: outline tree, subtree
#      (nested outline to level N, then content)
# Pick a nested outline manually.
# here, 'Compute proj...'
storage.load(40886).set_outline('subtree')

ref_o = ref_t.get_outline(w_, outl_type = 'nested')
# print ref_o
# view.print_info(storage,  ref_o)

ref_prun = ref_o.prune(w_)
# print ref_prun
# view.print_info(storage,  ref_prun)

#
#***            display main
ref_t_pic = w_.canvas.view.start_add_l3tree(ref_t)

#
#***            display outline
ref_pic = w_.canvas.view.start_add_l3tree(ref_prun)

# 
#**        pattern
tm_tree = reader.parse('''
if ("outline", !! TITLE "sample"):
    BODY
''')
print tm_tree
#**        single expression
ref_s = '''
''' # "
l3tree = reader.parse(ref_s)
l3tree

# 
#**        single nesting level
code_s = """
"""                                     # "
template = reader.parse(code_s)

# 
#**        double nesting
code_s = """
"""                                     # "
template = reader.parse(code_s)


# 
#**        viewlist tests
vl, _ = viewList(aList([])).setup(empty_parent(), def_env, storage)
sl, _ = List(aList([])).setup(empty_parent(), def_env, storage)

vls = viewList().setup_viewlist(w_, ref_t)
print vls


#*    list display tests
#**        no comments
[1,
 [1,
  2],
 [1,
  [1,
   2],
  2],
 [1,
  2],
 2]
#**        comments
# Outer comment.
[1,
 [1,
  2],
 [1,
  # Inner comment.
  [1,
   2],
  2],
 [1,
  2],
 2]

#*    viewlist display tests
program:
    # Very important
    # things are to be done...
    if ("outline", "Load volume."):
        vol = getImage(ev("$SPXROOT/sparx/test/model001.tcp"))
        # See http://www.macro-em.org/sparxwiki/prep_vol
        (volft, kb) = prep_vol(vol) 

    # Like these wonderful ones...
    if ("outline", "Compute projection angles"):
        angles = even_angles(10.0, 90.0, 90.1, 0.0, 179.9,'P')
        nangles = len(angles)
        stack = "proj.hdf"
        sx = 0
        sy = 0

#*    system identification
import sys
sys.platform


#*    l3-system interface
#**        ShellCmds
sc = shell_cmds
## sc = ShellCmds()
sc.system('echo', ['a'], [])
sc.system_wrap('echo', 'a', n='')
#**        subdir
from  l3lang.l3 import *
run()
# l3 input
program:
    sd = subdir:
        a = 1
        touch('foo')
        pwd()

#*    system interface via shell wrapper
#**        prepare sample data directory, python
import os
ev = os.path.expandvars
def ignore(cmd, *args, **kwd):
    try:
        cmd(*args, **kwd)
    except:
        pass

ignore(os.mkdir, 'foo-sh-wrap')
os.chdir('foo-sh-wrap')
ignore(os.symlink,
       ev('$HOME/w/sparx/xmipp/Basic_Demo/walkthrough/taguA.tot'),
       'taguA.tot')
ignore(os.symlink,
       ev('$HOME/w/sparx/xmipp/Basic_Demo/walkthrough/taguA.tot.inf'),
       'taguA.tot.inf')
ignore(os.symlink,
       ev('$HOME/w/sparx/xmipp/Basic_Demo/walkthrough/tagtA.tot'),
       'tagtA.tot')
ignore(os.symlink,
       ev('$HOME/w/sparx/xmipp/Basic_Demo/walkthrough/tagtA.tot.inf'),
       'tagtA.tot.inf')

# 
#**        call using assumed return value structure
# This is completely impractical; see ref[Mon Apr 16 14:35:34 2007]
# 
os.system('pwd; ls')
(angles, coords, images, sel_tilt, sel_untilt) = xmipp_mark("taguA.tot")

# 
#**        call in directory 
# This is a minimalistic approach, letting the user select directory
# entries after execution.
# 
#***            using xmipp_mark found on system path
os.system('pwd; ls -l')
xmipp_mark(w_, "taguA.tot", tilted = "tagtA.tot")
# In xmipp_mark:
#     menu -> save coords     > tag.pos
#     menu -> save angles     > tag.ang
#     menu -> generate images > images.xmp
#                             > tilted.sel
#                             > untilted.sel
# 
os.system('pwd; ls -l')

# 
#***            using l3lang.external module
from  l3lang.external import xmipp_wrap 
xmipp_wrap.xmipp_mark("/net/cci/hohn/w/sparx/xmipp/Basic_Demo/walkthrough/taguA.tot",
                      tilted =
                      "/net/cci/hohn/w/sparx/xmipp/Basic_Demo/walkthrough/tagtA.tot")
# 
#**        with l3lang.external module
#***            using subdirectory, l3
# For batch programs, the toplevel should suffice:
from  l3lang.l3 import *
run()

# graphically:
# l3 input, using manually specified full paths.
program:
    inline "from l3lang.external.xmipp_wrap import *"
    sd = subdir:
        touch("test_file")
        touch("foobar")
        (status, log) = xmipp_mark(
            "/net/cci/hohn/w/sparx/xmipp/Basic_Demo/walkthrough/taguA.tot",
            tilted = "/net/cci/hohn/w/sparx/xmipp/Basic_Demo/walkthrough/tagtA.tot")

    print sd.foobar

    # Next, try commands using the file(s).
    sd1 = subdir:
        cp(sd.foobar, "foobar_copy")


# value inspection, via local eval:
#     self.w_.state_.storage.load(self._l3tree._id)

# 
#***            directory only tests, l3
# [Fri Apr 20 16:23:21 2007]
program:
    inline "from l3lang.external.xmipp_wrap import *"
    sd = subdir:
        touch("test_file")
        touch("foobar")

    print sd.foobar

    # Next, try commands using the file(s).
    sd1 = subdir:
        cp(sd.foobar, "foobar_copy")

    print sd1.foobar_copy
    
# [Thu Apr 19 15:32:15 2007]
# state save sequence
# (paste script)
# (save state st.no-exec.1)
#       file size: 63992
# (run script)
# (save state st.1)
#       file size: 66351


#*    rpy test
program:
inline 'from rpy import *'

d = r.capabilities()
if d['png']:
    device = r.png                      # Robj, won't pickle
    ext='png'
else:
    device = r.postscript
    ext='ps'

faithful_data = r.faithful              # short form:
ed = faithful_data["eruptions"] 


#*    numeric tests
program:
inline "import Numeric as N; import MLab as M"

num_arr = M.rand(4,4)          # Get numeric array.

num_arr = M.rand(4,4)
(rows, cols) = num_arr.shape
bottom = M.min(M.min(num_arr))
top = M.max(M.max(num_arr))
scale = (top - bottom)
norm_arr = ((num_arr - bottom) / scale * 255).astype('b') #   Convert float array to 8-bit array.

t3 = N.outerproduct(norm_arr, N.array([1,1,1], 'b'))
t4 = N.reshape(t3, (rows, cols, 3))
pix_arr = N.array(t4, 'b')       # Use 3 8-bit arrays as (rgb) values.
    

#*    external test collection
from EMAN2 import *
e = EMData()
e.set_size(3,3)
e.set_value_at(0,1, 1.0)
print "original at start"
e.print_image()

f1 = e.process("mirror",{"axis":'x'})
print "new mirrored image"
f1.print_image()

# 
#**        image display test
# [Fri Apr 20 16:48:25 2007]
program:
    inline("import EMAN2 ")
    ti = EMAN2.test_image(type = 1, size = (128,64))
# program > menu > run code
# ti > menu > insert list of values l3

# 
#**        Micrograph and particle selection
# Using the xmipp demo and data.
program:
    inline '''from  l3lang.external.xmipp_wrap import \
              xmipp_mark, xmipp_do_selfile'''
    orig = subdir:
        cp('$HOME/w/sparx/xmipp/Basic_Demo/walkthrough/taguA.tot',
           'taguA.tot')
        cp('$HOME/w/sparx/xmipp/Basic_Demo/walkthrough/taguA.tot.Common.pos',
           'taguA.tot.Common.pos')
        cp('$HOME/w/sparx/xmipp/Basic_Demo/walkthrough/taguA.tot.inf',
           'taguA.tot.inf')
        cp('$HOME/w/sparx/xmipp/Basic_Demo/walkthrough/tagtA.tot',
           'tagtA.tot')
        cp('$HOME/w/sparx/xmipp/Basic_Demo/walkthrough/tagtA.tot.Common.pos',
           'tagtA.tot.Common.pos')
        cp('$HOME/w/sparx/xmipp/Basic_Demo/walkthrough/tagtA.tot.inf',
           'tagtA.tot.inf')
           
    mark = subdir:
        # Do the following in xmipp_mark:
        # 
        # file -> load coords
        #     The file paths must be retrieved from l3, via
        #     orig subdir > menu > pwd 
        # 
        # file -> save angles
        # 
        # file -> generate images
        #       110 110 img_t
        # and
        #       110 110 img_u
        # Ignore the
        #       1551:SelFile::read: File
        #       ..../Subdir-30214/taguA.tot.tot.Common.sel
        #       not found 
        # warning.
        (status, log) = xmipp_mark( orig.taguA_tot,
                                    tilted = orig.tagtA_tot )

        # Selection file listing images in current directory.
        xmipp_do_selfile("img_t*.xmp", "tilted.sel")
        xmipp_do_selfile("img_u*.xmp", "untilted.sel")
    
    # Selection file listing images relative to main directory.
    # A full xmipp wrapper would hide selection files completely.
    print (mark.l3_dirname() + "/" + "img_t*.xmp", "tilted.sel")
    xmipp_do_selfile(mark.l3_dirname() + "/" + "img_t*.xmp", "tilted.sel")
    xmipp_do_selfile(mark.l3_dirname() + "/" + "img_u*.xmp", "untilted.sel")

# Note:
# Even without any matching input files, 
#       xmipp_do_selfile("img_u*.xmp", "untilted.sel")
# returns status zero ...

normalize
    xmipp_normalize

mask 
    xmipp_mask 

>> Need xmipp_show to view (intermediate) images.

# 
#**        generate phantom
inline "import l3lang.external.xmipp_wrap as xw"
vol, fourpr = xw.pdbphantom("input-data/1P30.pdb",
                            sampling_rate = 1.6,
                            high_sampling_rate = 0.8,
                            )


# 
#**        data alignment and classification
align2d 
    xmipp_align2d 

produce .doc file -- view as plain text. 


# 
#**        reconstruction
reconstruct
    xmipp_art

#
#**        nested loop data selection
# [Wed May 16 12:01:45 2007]
program:
    if "loop":
        def lname(i):
            subdir:
                touch("value-file")
            if i > 3:
                return i
            if True:
                return lname(i + 1)
        lname(1)


#*    pretty-printer
from  l3lang.pretty import *
# 
#**        manual construct / convert
doc = DocText("hello") | DocBreak(" ") | DocText("world")
# 
doc.toString(40)
doc.toString(4)

# 
#**        construct / convert
B = lambda : DocBreak(" ")
T = lambda t : DocText(t)
G = lambda x : DocGroupAuto(x)
doc = G(T("begin") | B() |
        G(T("stmt1;") | B() |
          T("stmt2;") | B() |
          T("stmt3;")) | B() |
        T("end;"))
# 
print doc.toString(40)
print doc.toString(8)
print doc.toString(4)

# 
#**        manual construct / convert
B = lambda : DocBreak(" ")
T = lambda t : DocText(t)
G = lambda x : DocGroupAuto(x)
GB = lambda x : DocGroupBreak(x)
GF = lambda x : DocGroupFill(x)
doc = G(T("begin") | B() |
         G(T("stmt1;") | B() |
           T("stmt3;"))
         | B() |
         T("end;"))
# 
print doc.toString(40)                  # no breaks
print doc.toString(20)                  # break outer group
print doc.toString(4)                   # break all

# 
#**        manual construct w/ indentation
B = lambda : DocBreak(" ")
C = lambda x,y: DocCons(x, y)
T = lambda t : DocText(t)
G = lambda x : DocGroupAuto(x)
GB = lambda x : DocGroupBreak(x)
GF = lambda x : DocGroupFill(x)
I = lambda r, x : DocNest(r, x)
doc = G(T("begin") |
        I(4,
          B() |
          G( C(C(T("stmt1;"), B()), T("stmt3;"))))
        | B() |
        T("end;"))
# 
print doc.toString(40)                  # no breaks
print doc.toString(20)                  # break outer group
print doc.toString(4)                   # break all

#*    full-tree pretty printing
#**        manual comparison
code_s = ('''
a = 1
def loop(a):
    if a < 3:
        print a
        loop(a + 1)
loop(1)
''')
tree = reader.parse(code_s)

# Setup.
play_env = interp.get_child_env(def_env, storage)
tree.setup(empty_parent(), play_env, storage)
# # view.print_info(storage, tree)
print tree.get_infix_string(40)
print tree.get_infix_string(10)

# 
#**        print-parse-compare cycle function
def comp_trees(otree, otree_tbl, pptree, pptree_tbl):
    # Compare trees and associated comments
    
    # Compare trees.
    print
    print "Check for differences..."
    print "--------------------------------------------"
    o_it = otree.top_down()
    pp_it = pptree.top_down()
    while 1:
        try: o_nd = o_it.next()
        except StopIteration: break
        pp_nd = pp_it.next()

        # Trees equal?
        if o_nd.eql_1(pp_nd):
            if otree_tbl.has_key(o_nd._id):
                if otree_tbl[o_nd._id].eql_1(pptree_tbl[pp_nd._id]):
                    continue
                else:
                    print ("Comment difference from line %d, col %d\n"
                           "                     to line %d, col %d\n" 
                           % (o_nd._first_char[0], o_nd._first_char[1],
                              pp_nd._first_char[0], pp_nd._first_char[1]))
                    print otree_tbl[o_nd._id]
                    print pptree_tbl[pp_nd._id]
            else:
                continue
        else:
            print ("Tree difference: line %d, col %d\n    original:\n%s\n"
                   "    new:\n%s\n"
                   %
                   (pp_nd._first_char[0],
                    pp_nd._first_char[1],
                    str(o_nd),
                    str(pp_nd),
                    ))

def ppc(code_s, width):
    # Original tree.
    tree = reader.parse(code_s).single_program()
    tree_env = interp.get_child_env(def_env, storage)
    tree.setup(empty_parent(), tree_env, storage)
    associate_comments(w_, tree, code_s)

    # Tree from printed output.
    pp_string = tree.get_infix_string(width, w_.deco_table_)

    print "Pretty-printed string, width %d: " % width
    print "--------------------------------------------"
    print pp_string

    pp_tree = reader.parse(pp_string).single_program()
    pp_env = interp.get_child_env(def_env, storage)
    pp_tree.setup(empty_parent(), pp_env, storage)
    associate_comments(w_, pp_tree, pp_string)
    
    comp_trees(tree, w_.deco_table_, pp_tree, w_.deco_table_)

def ppcm(code_s, width_list):
    for width in width_list:
        ppc(code_s, width)

def chars(st):
    for ch in st:
        print '-%c-' % ch,
      

# 
#**        string special case
#***            repr / str behavior
chars('a\nb')
chars(r'a\nb')
aa = reader.parse('"""a\nb"""')
chars(aa.body()._primary[0])

aa = reader.parse('"""Outer radius : %i\n"""')
chars(aa.body()._primary[0])


str(aa.body()._primary[0])
# str(aa)    converts unprintables into ascii
# repr(aa)   converts unprintables into ascii and escapes when needed

# Other cases:
repr('"""a"""')
# '\'"""a"""\''

repr("'''a'''")
# '"\'\'\'a\'\'\'"'
# 
#***            tests
ppc("""
'''if 1:
    from rpy import *
    d = r.capabilities()
    if d['png']:
        device = r.png                      # Robj, won't pickle
        ext='png'
    else:
        device = r.postscript
        ext='ps'
'''
""", 40)



# 
#**        def special case
ppc('''
def func(arg):
    body
''', 40)

ppc('''
def func(arg):
    body
''', 10)

#
#**        loop (set, function, operators)
ppcm('''
# Now for something else...
# ... like a comment
# ... across lines
a = 1
def loop(a):
    if a < 3:
        print a
        loop(a + 1)
loop(1)
''', [40, 30, 10])


#
#**        program (program, subdir, tuple, comment)
ppcm('''
program:
    # with comment
    orig = subdir:
        add(a,b)
''', [50])


ppcm('''
program:
    orig = subdir:
        cp('$HOME/w/sparx/xmipp/Basic_Demo/walkthrough/taguA.tot',
           'taguA.tot')
           
    mark = subdir:
        inline "from  l3lang.external.xmipp_wrap  import xmipp_mark"
        # file -> load coords
        #     The file paths must be retrieved from l3, via an Env
        #     listing from the original subdir.  

        (status, log) = xmipp_mark( orig.taguA_tot,
                                    tilted = orig.tagtA_tot )
''', [80, 70, 20])


#
#**        comment in expression
ppcm('''
[a,b,
 # the width
 w,
 h]
''', [70])

#
#**        outline
ppcm('''
"hello world"
''', [40])

ppcm('''
if ("outline",  "outer 1"):
    if ("outline",  "tools"):
        init_spider
        setup_directory
        plot

    if ("outline",  "inner 2"):
        'nothing here'
''', [60])


#*    Local node examination.
# From a widget with _l3tree
st = self.w_.state_.storage
st.load(self._l3tree._id)
res = st.get_attribute(self._l3tree._id, "interp_result")

#*    ctf, emdata and numpy
from sparx import *
from EMAN2 import EMNumPy
import numpy
from numpy import flipud, fliplr
import numpy as N
# 
#**        conversion check (indep.)
e1 = EMData()
e1.set_size(3,4)
e1.process_inplace('testimage.noise.uniform.rand')
e1.get_data_pickle()           # emdata -> std::vector -> python list



# 
#**        conversion check (indep.)
from sparx import *
from EMAN2 import EMNumPy
import numpy as N

n1 = N.random.ranf( (5,4) )
e1 = EMData(); EMNumPy.numpy2em(n1, e1)
n2 = EMNumPy.em2numpy(e1)
assert N.max(N.max(n2 - n1)) == 0, \
"Array conversion failed (error = %g)." % N.max(N.max(n2 - n1))
# 
#**        Get spider format image.
image = getImage('foo.spi')
# 
#**        Flip it to get correct layout.
img_num = flipud(fliplr(EMNumPy.em2numpy(image)))
img_em = EMData()
EMNumPy.numpy2em(img_num, img_em)
# 
#***            Verify the conversion.
num_2 = EMNumPy.em2numpy(img_em)
assert N.max(N.max(num_2 - img_num)) == 0, "Array conversion failed."
# 
# Check a single element
#       Lots of matches:
N.where( (img_num - img_em.get_value_at(0,0)) < 1e-14)
#       No matches.  This  means that the (0,1) element must be wrong,
#       not merely misplaced.  
N.where( (img_num - img_em.get_value_at(0,1)) < 1e-14)

# 
#**        Get the ctf.
ctf_img = filt_ctf(image, 2000, 2.0, 200,
                   1.6, 0.1, -1, 100)
# 
#**        Save the image with ctf applied.
dropImage(ctf_img, "ctf.spi", itype = 's')

#*    numpy and pickle
import numpy as N, pickle, types
from  pdb import pm
print N.version.version
if 1:
    # fails:
    aa = N.array([ 1, 2, 3, 4 ]);
    aa1 = aa.reshape
    print type(aa1) == types.BuiltinFunctionType
    pickle.dumps(aa1)
if 2:
    # works:
    pickle.dumps(N.reshape)


#*    Numeric and pickle
import Numeric as N, pickle
from  pdb import pm
if 1:
    # fails:
    aa = N.array([ 1, 2, 3, 4 ]);
    aa1 = aa.resize
    print type(aa1) == types.BuiltinFunctionType
    pickle.dumps(aa1)
if 2:
    # works
    pickle.dumps(N.reshape)

#*    outline traversal
# print the outline
for (nd, level) in w_.lib_tree.body().outl_top_down():
    print "  " * level, level, nd.l_label

#*    timed run
def time_func(fun, *args, **kwd):
    from time import time
    start = time()
    fun(*args, **kwd)
    print time() - start
    
#*    eman2 pickle tests
# - Works when run in one session.
# - Works across sessions.
import sparx as sx, l3lang.utils as ut
volume = sx.getImage( "model.tcp" )
print volume.get_xsize() * volume.get_ysize() * volume.get_zsize()

image = sx.project(volume, [10.0, 20.0, 30.0, 0.0, 0.0], 35)
print image.get_xsize()

# Image.
time_func(cPickle.dumps, image, protocol = 2)

ut.file_pickle(image, 'st.1')
image_ld = ut.file_unpickle('st.1')


# Volume.
import cPickle
# st = cPickle.dumps(volume, protocol=2)
time_func(cPickle.dumps, volume, protocol=2)

time_func(sx.dropImage, volume, "st.spi", itype = 's')

ut.file_pickle(volume, 'st.2')
#   Load in separate session.
import sparx as sx, l3lang.utils as ut
volume_ld = ut.file_unpickle('st.2')

vol_st = volume_ld.__getstate__()

#*    Set() special form: aa[ii] = rhs 
#**        single-index pattern derivation
aa = reader.parse("aa[i] = foo").body()

# Set(Call(Member(Symbol('operator'),
#                 Symbol('getitem')),
#          aList([Symbol('a'),
#                 Symbol('i')])),
#     Symbol('foo'))  

aa = reader.parse("! AA[ ! II ] = ! RHS").body()

Set(Call(Member(Symbol('operator'),
                Symbol('getitem')),
         aList([Marker('AA'),
                Marker('II')])),
    Marker('RHS')) 
# 
#**        transform to `operator.setitem(aa, ii, rhs)`
print reader.parse('operator.setitem(!AA, !II, !RHS)').body()

Call(Member(Symbol('operator'),
            Symbol('setitem')),
     aList([Marker('AA'),
            Marker('II'),
            Marker('RHS')]))  

# 
#**        numeric: single-bracket, multi-index 
import Numeric as N
aa = N.reshape(N.array([ 1, 2, 3, 4 ]), (2,2))
print aa[0,1]

# Syntax error...
print reader.parse("aa[0,1]").body()

# Parses:
print reader.parse("aa[(0,1)]").body()
# long form works:
print operator.getitem(aa, (0,1))

# Call(Member(Symbol('operator'),
#             Symbol('getitem')),
#      aList([Symbol('aa'),
#             Tuple(aList([Int(0),
#                          Int(1)]))]))

# So the single-index pattern will work.
# 
#**        tests
print reader.parse("aa[ii] = foo").body()
program:
    # Circumvents l3 value tracking.
    aa = [0,1,2]
    aa[0] = 4
    aa


#*    Set() special form: aa[ii][jj] = rhs 
#**        Original in
aa = reader.parse("! AA[ ! II ] [ !JJ ] = ! RHS").body()

Set(Call(Member(Symbol('operator'),
                Symbol('getitem')),
         aList([Call(Member(Symbol('operator'),
                            Symbol('getitem')),
                     aList([Marker('AA'),
                            Marker('II')])),
                Marker('JJ')])),
    Marker('RHS')) 

# 
#**        Transformed out.
print reader.parse('''
operator.setitem(
    operator.getitem(!AA, !II),
    !JJ,
    !RHS)''').body()
Call(Member(Symbol('operator'),
            Symbol('setitem')),
     aList([Call(Member(Symbol('operator'),
                        Symbol('getitem')),
                 aList([Marker('AA'),
                        Marker('II')])),
            Marker('JJ'),
            Marker('RHS')])) 
#
#**        tests
print reader.parse("aa[ii][jj] = foo").body()
program:
    # Circumvents l3 value tracking.
    aa = [[0,1], [2,3]]
    aa[0] 
    aa[1][0] = 10
    aa


#*    Set special form:  a.foo = 2
# Still uses setitem.
aa = utils.Shared()
aa.foo = 1
operator.setitem(aa, 'foo', 2)
print aa.foo

#*    string parsing
#**        n  behavior, manual eval
# This lets python convert the \n to an escape:
aa = reader.parse('"""Outer radius : %i\n""" % 123')
chars(aa.body()._primary[1][0]._primary[0])

# Setup.
play_env = interp.get_child_env(def_env, storage)
aa.setup(empty_parent(), play_env, storage)
view.print_info(storage, aa)

# Interpret.
news = aa.interpret(play_env, storage)
chars(news[0]) # has actual newline
#**        n  behavior, pasting
"""Outer radius : %i\n""" % 123
# run code => ('Outer radius : 123\\n', 5496)
#**        compare
manual eval:
    45065    -22    45065                  String('Outer radius : %i\n')

l3.py:
    >>> print """Outer radius : %i\n""" % 123
    Outer radius : 123\n

pasting:
    45072   5494    45072                  String('Outer radius : %i\\n')
# 
#**        difference, commit c2c6cff9496972d40011002de24d20abca1a6bab and before
# 
# This lets python convert the \n to an escape:
aa = reader.parse('"""Outer radius : %i\n""" % 123')

# This won't, and that's the behavior observed from the gui / toplevel.
aa = reader.parse(r'"""Outer radius : %i\n""" % 123')

# This also contains \\n instead of the control code.
pprint.pprint([ii for ii in tokenize.generate_tokens(string_readline(r'"Outer radius : %i\n"'))])

# 
#**        difference removed 
# both contain \n control code.
print reader.parse('"""Outer radius : %i\n""" % 123')

# This won't, and that's the behavior observed from the gui / toplevel.
print reader.parse(r'"""Outer radius : %i\n""" % 123')


#*    pytables for reading hdf
#**        raw experiments
import tables
# File is available after ali2d.l3 run
fi = tables.openFile("_K-8-133679/average.hdf")
# method listNodes in module tables.file:
fi.listNodes                            # show all
[li for li in fi.listNodes('/MDF/images')]

# RootGroup in module tables.group object:
fi.getNode('/')

fi.root == fi.getNode('/')              # true

for node in fi.root:
    print node

list(fi.root)                           # list from iterator
list(fi.root)[0]                        # first child
list(list(fi.root)[0])                  # first child's children



fi.listNodes('/')                       # get list
(fi.listNodes('/')[0])._v_name

for sub in fi.listNodes('/'):
    print sub._v_name

# caution:
#     >>> fi.getNode('/')
#     / (RootGroup) ''
#       children := ['MDF' (Group)]
#     >>> fi.getNode('/').getNode('MDF')
#     Segmentation fault

fi.getNode('/MDF/images')
fi.getNode('/MDF/images')._v_attrs      # print
fi.getNode('/MDF/images')._v_attrs._g_listAttr() # get list

fi.getNode('/MDF/images')._f_getChild('1')

fi.getNode('/MDF/images/1')
# tables.attributeset.AttributeSet'>
fi.getNode('/MDF/images/1')._v_attrs    # print info
fi.getNode('/MDF/images/1')._v_attrs._g_listAttr() # get list
(fi.getNode('/MDF/images/1')._v_attrs  )._f_list()   # get list (better)
fi.getNode('/MDF/images/1')._v_attrs._g_getAttr('EMAN.Class_average')
fi.getNode('/MDF/images/1')._v_attrs._g_getAttr('EMAN.nobjects')
fi.getNode('/MDF/images/1')._v_attrs._g_getAttr('EMAN.members')


# Array in module tables.array
arr = fi.getNode('/MDF/images')._f_getChild('1')._f_getChild('image')
arr = fi.getNode('/MDF/images/1/image')
arr.shape
a00 = arr[0,0]                          # Returns a 0-D array
a00 = arr[0:1,0:1]                      # Returns a slice copy.

# 
#**        relevant for eman2 use
# 
# 1. The pytable classes:
# 
#  / <class 'tables.group.RootGroup'>
#    MDF <class 'tables.group.Group'>
#      images <class 'tables.group.Group'>
#        0 <class 'tables.group.Group'>
#          image <class 'tables.array.Array'>
#        1 <class 'tables.group.Group'>
#          image <class 'tables.array.Array'>

# 2. The attributes of a Group
# ... members in a Group start with some reserved prefix, like _f_
# (for public methods), _g_ (for private ones), _v_ (for instance
# variables) or _c_ (for class variables). 
#
# tables.attributeset.AttributeSet'>
# fi.getNode('/MDF/images/1')._v_attrs    # print info

# Get list of attributes.
(fi.getNode('/MDF/images/1')._v_attrs)._f_list() 

# Get attribute with arbitrary name
(fi.getNode('/MDF/images/1')._v_attrs).__getattr__('EMAN.Class_average')
(fi.getNode('/MDF/images/1')._v_attrs).__getattr__('EMAN.members')

# Set attribute:
(fi.getNode('/MDF/images/1')._v_attrs  ).new_stuff = 1

# got tables.exceptions.FileModeError: the file is not writable

#
#**        tools
#***            Traverse the object tree
def nshow(fi, indent = 0):
    if hasattr(fi, '_v_name'):
        print "  "*indent, fi._v_name, type(fi)
        if isinstance(fi, tables.array.Array):
            # For tables.array, iteration is over the rows; this shows
            # 75 1-d numpy arrays per image.
            return
        for sub in fi:
            nshow(sub, indent = indent + 1)
    else:
        print "  "*indent, type(fi)
nshow(fi.root)

#
#***            simplified access interface for l3 use
# Inject l3_* methods.
def l3_len(self):
    'Return the number of images.'
    return self.getNode('/MDF/images')._v_nchildren
tables.file.File.l3_len = l3_len


def l3_getimg(self, index):
    'Return the image at `index`.'
    return self.getNode('/MDF/images')._f_getChild(str(index))._f_getChild('image')
tables.file.File.l3_getimg = l3_getimg


def l3_getatts(self, index):
    ''' Return a (name, value) list holding all the attributes of
    image `index`.
    '''
    ind = str(index)
    attset = self.root.MDF.images._f_getChild(ind)._v_attrs
    return [ (att, attset.__getattr__(att)) for att in attset._f_list()]
tables.file.File.l3_getatts = l3_getatts


def l3_getatt(self, index, name):
    ''' Return the `name` attribute of the image at `index`.'''
    ind = str(index)
    attset = self.root.MDF.images._f_getChild(ind)._v_attrs 
    return attset.__getattr__(str(name))
tables.file.File.l3_getatt = l3_getatt
    

# 
#***            tables access tests
fi.l3_len()
fi.l3_getimg(0)
fi.l3_getatts(0)
fi.l3_getatt(0, 'EMAN.Class_average')
fi.l3_getatt(0, 'EMAN.nobjects')
fi.l3_getatt(0, 'EMAN.members')



#*    nested main loop
# Using the simple function
if 1:
    def flush_events():
        # Updates bounding boxes, among others.
        while gtk.events_pending():
            gtk.main_iteration()

# a loop could be 
if 1:
    while True:
        draw()
        flush_events()
    
# as long as draw() does not block.

import thread 
from threading import Lock
import time
lck = Lock()
value = []
def _do():
    try:
        value.append(raw_input(prompt))
    except EOFError:
        value.append(EOFError())
    lck.release()
lck.acquire()
thread.start_new_thread(_do, ())
# Manual main loop until input is ready.
while not lck.acquire(False):
    self._do_event()
    time.sleep(0.01)
if isinstance(value[-1], Exception):
    raise value[-1]
return value[-1]

#*    gui/console hybrid use
#**        highly irregular data, not used
inline('from numpy import *; from pylab import *')
for ii in range(12*3):
    a1 = (ii, array([-2100, -200, -100 / 2.0, -1200]))
    # Added to loop body.
    a1[0:1]
    a1[0]
    a2 = (ii, sum(a1[1]))
    (ii % 2 == 0) and -200 or 0
        
# This is just too painful through the gui; there is too much
# heterogeneous data.
#**        highly irregular data, python
# Just defining and editing the textual data structure is FAR
# superior.  E.g.
# (month, amount, what)
specials = [(1, [(-200, "this")]),
            (22, [(300, "that")]),
            ]
regular = [(mon, [(-100, "one"),
                 (-200, "two"),
                 ]) for mon in range(0, 12*3)]
spec_d = dict(specials)
# for (month, mon_dat) in regular:
#     print (month, mon_dat + spec_d.get(month, []))
# Shorter:    
[  (month, mon_dat + spec_d.get(month, []))
   for (month, mon_dat) in regular]

# 
#**        highly irregular data, l3
inline('''
def combine(specials, regular, r_from, r_to):
    reg_list = [(mon, regular) for mon in xrange(r_from, r_to)]
    spec_d = dict(specials)
    return [  (month, mon_dat + spec_d.get(month, []))
              for (month, mon_dat) in reg_list]
''')
# '
specials = [(1, [(-200, "this")]),
            (22, [(300, "that")]),
            ]
regular = [(-100, "one"),
           (-200, "two"),
           ]
combined = combine(specials, regular, 0, 3*12)

# 
#**        embed in l3?
# if Env's keys include any python hashables, an Env can accumulate
# (int, val) bindings in addition to (name, val) bindings
stuff = if ("map", "embedded"):
    for ii in range(0, 3*12):
        l3.env().set(ii, [(-100, "one"), (-200, "two")])
        
    l3set(1,  [(-200, "this")])
    l3set(22, [(300, "that")])



#*    list comprehension
[(mon, [(-100, "one"),
        (-200, "two"),
        ]) for mon in range(0, 12*3)]
# As tail call, roughly:
def _foo(arg, seq, accum):
    accum.append()
    if more(seq):
        _foo(arg, seq[1:None], accum)
_foo(mon, range(0, 12*3), [])
# this is already FAR to tedious, and a tracked (l3) expansion is not
# desirable anyway.

#*    l3 script pieces
# From matplotlib-0.90.1/examples/date_demo1.py
# 
# This example requires an active internet connection since it uses
# yahoo finance to get the data for plotting
#
# The scripts are independently runnable, and must be edited to form a
# single program.

# 
#**        Collect finance data using yahoo's service.
if ("outline", "Collect finance data"):
    inline """if 1:
    from matplotlib.finance import quotes_historical_yahoo
    import datetime
    """
    # <span background="#ffdddd">Retrieve data for stock symbol.</span>        
    quotes = quotes_historical_yahoo(
        'INTC',
        datetime.date( 1995, 1, 1 ),
        datetime.date( 2004, 4, 12 ),
        asobject = True)
    if not quotes:
        error("Unable to get data")
    else:
        # <span background="#ffdddd">The data attributes of quotes.</span>
        quotes.date
        quotes.open
        quotes.close
        quotes.high
        quotes.low
        quotes.volume



# 
#**        Date plot.
if ("outline", "Date plot"):
    # <span background="#ffdddd">Plot details.</span>

    inline """if 1:
    import datetime

    def plot_date(fname, dates, values):
        from pylab import figure, show
        import datetime
        from matplotlib.dates import YearLocator, MonthLocator, DateFormatter

        fig = figure(figsize=(7,4), dpi=100)
        ax = fig.add_subplot(111)
        ax.plot_date(dates, values, '-')

        # Plot details -- ticks.
        years    = YearLocator()   # every year
        months   = MonthLocator()  # every month
        yearsFmt = DateFormatter('%Y')

        ax.xaxis.set_major_locator(years)
        ax.xaxis.set_major_formatter(yearsFmt)
        ax.xaxis.set_minor_locator(months)
        ax.autoscale_view()

        for label in ax.get_xticklabels() + ax.get_yticklabels():
            label.set_fontsize('8')

        # Plot details -- coords. message box.
        def price(x):
            return '$%1.2f' % x
        ax.fmt_xdata = DateFormatter('%Y-%m-%d')
        ax.fmt_ydata = price
        ax.grid(True)

        fig.savefig(fname)
        return fname
    """
    # <span background="#ffdddd">Plot data.</span>
    filename = plot_date('_plot-%d.png' % new_id(),
                         [datetime.date(1995,1,1).toordinal(),
                          datetime.date(1997,1,1).toordinal()],
                         [1, 2])
# 
#**        Moving average
if ("outline", "Moving average."):
    inline """if 1:
    import numpy
    def movavg(vec, n):
        # A[t] = (V[t] + V[t-1] + ... + V[ t-n+1 ]) / n
        # t = N .. N-n
        import numpy
        assert len(vec.shape) == 1, 'Expecting vector argument.'
        assert n > 1, 'Averaging over less than 2 elements.'
        (N, ) = vec.shape
        assert N > n, 'Not enough samples for averaging.'
        # 
        ma = copy(vec) * numpy.nan
        for t in xrange(n - 1, N):
            ma[t] = 1.0 * sum(vec[t-n+1: t + 1]) / n
        return ma
    """
    mov_avg = movavg(numpy.array([1, 2, 3, 4.0]),
                     2)
#
#**        Moving average via convolution
# n = 2
data = numpy.array([1.0, 2,3,4])
weight = numpy.array([1.0/2, 1.0/2])
mov_avg = numpy.convolve(data, weight, mode='same')
mov_avg[0:weight.shape[0] - 1] = numpy.nan

#
#**        Moving average, simple plot
import matplotlib
matplotlib.use("gtk")

from numpy import *
from pylab import *

# n = 2
data = [1.0, 2,3,4]
weight = [1.0/2, 1.0/2]
mov_avg = convolve(data, weight, mode='same')
mov_avg[0:len(weight) - 1] = nan

t1 = plot(mov_avg)                      # t1 won't pickle.
show(mainloop=False)
show()

# 
#**        Date plot as widget
if ("outline", "Date plot"):
    # <span background="#ffdddd">Plot details.</span>

    inline """if 1:
    import datetime

    def plot_date(dates, values):
        from pylab import figure, show
        import datetime
        from matplotlib.dates import YearLocator, MonthLocator, DateFormatter
        from matplotlib.backends.backend_gtk import FigureCanvasGTK \
             as FigureCanvas 

        fig = figure(figsize=(7,4), dpi=100)
        ax = fig.add_subplot(111)
        ax.plot_date(dates, values, '-')

        # Plot details -- ticks.
        years    = YearLocator()   # every year
        months   = MonthLocator()  # every month
        yearsFmt = DateFormatter('%Y')

        ax.xaxis.set_major_locator(years)
        ax.xaxis.set_major_formatter(yearsFmt)
        ax.xaxis.set_minor_locator(months)
        ax.autoscale_view()

        for label in ax.get_xticklabels() + ax.get_yticklabels():
            label.set_fontsize('8')

        # Plot details -- coords. message box.
        def price(x):
            return '$%1.2f' % x
        ax.fmt_xdata = DateFormatter('%Y-%m-%d')
        ax.fmt_ydata = price
        ax.grid(True)
        return fig
    """
    # <span background="#ffdddd">Plot data.</span>
    plot_date([datetime.date(1995,1,1).toordinal(),
               datetime.date(1997,1,1).toordinal()],
              [1, 2])

# The display widget is FigureCanvas(fig).  But canvas operations will
# destroy this occasionally so the raw figure handle is returned and
# L3 can display Native( fig : <matplotlib.figure.Figure> )
# via Widget(FigureCanvas(fig)).


#*    single-run mode
def inline(str):
    exec(str, globals())

inline("import matplotlib as M")
inline( "import pylab as P")
M.use('Agg')
fig = P.figure(figsize=(5,4), dpi=75)

# 
#**        doc strings as help entries
from types import InstanceType, MethodType
import pydoc
if isinstance(fig, InstanceType):
    helpfor = fig.__class__
    # All member functions of this class.
    for (name, memb) in pydoc.allmethods(helpfor).iteritems():
        _docstr = pydoc.getdoc(memb) 
        head, body = pydoc.splitdoc(_docstr)
        print "=================================\n", name
        print head
        print "--------------------------------------------\n", body
else:
    helpfor = fig





# # Useless:
help(fig)

# # Help on instance of Figure in module matplotlib.figure object:
# # class instance(object)
# # ...
# # Good, but a LOT of content:
help( fig.__class__)



#* numpy arrays, python objects, l3 canvas grid
# Table layout is very useful and implicit when displaying a list of tuples.
# When editing a table, the underlying structure should BE a table to enforce
# dimensions (no shape change when items are removed) and allow row / column 
# manipulation.
import numpy as N
arr = N.array([['desc', 'val'],
               ['car', 1.0],
               ['house', 2.2]], 'O')
# 
#**     Add row
print arr
print arr[1, None]                            # 'car' row

arr_1 = N.concatenate(
    (arr[0:2, 0:None],                # Indexing stupidity to get rows 0 & 1 
     [['bike', 0.5]],
     arr[2:None, 0:None]
     ))
print arr_1

# 
#**     Add colum
print arr
print arr[0:None, 1]                    # 'val' column

arr_1 = N.concatenate(
    (
        arr[0:None, 0:1],             # Indexing stupidity to get rows 0 & 1 
        N.array([['ratio'],
                 [0.5],
                 [0.1]], 'O'),
        arr[0:None, 1:None]
        ), axis=1)
print arr_1


#*    matplotlib figures on l3 canvas (XY Plot script)

# 
#**        Another numpy stupidity
import matplotlib as M
import pylab as P
N = 5
nn = P.arange(0, N)
# Gives TypeError: only length-1 arrays can be converted to Python scalars
xx = sin(2* nn* pi/N)
# Works
xx = P.sin(2* nn* pi/N)

# 
#**        l3 polygon plots
if ("outline",  "XY Polygon Plot"):
    # <span background="#ffcccc">Plot details.</span>
    inline '''if 1:
    def plot_(fname, x, y):
        import matplotlib as M
        import pylab as P
        M.use('Agg')

        fig = P.figure(figsize=(5,4), dpi=75)
        ax = fig.add_subplot(111)
        ax.plot(x, y, '-', lw=2)

        ax.set_xlabel('')
        ax.set_ylabel('')
        ax.set_title('Polygon plot')
        ax.grid(True)

        fig.savefig(fname)
        return fname
    '''
    inline 'from pylab import *'
    for N in range(3, 12):
        nn = arange(0, N + 1)
        xx = sin(2* nn* pi/N)
        yy = cos(2* nn* pi/N)
        plot_(new_name('poly', '.png'), xx, yy)



# 
#**        python
import matplotlib as M
import pylab as P

def plot_xy(fname, x, y):
    M.use('Agg')
    plot = P.plot(x, y, '-', lw=2)

    # P.xlabel('time (s)')
    # P.ylabel('voltage (mV)')
    # P.title('About as simple as it gets, folks')
    # P.grid(True)

    P.savefig(fname)
# 
plot_xy('simple_plot.png', 
        P.arange(0.0, 1.0+0.01, 0.01),
        P.cos(2*2*pi*t)
        )

import cPickle
st = cPickle.dumps(plot, protocol=2)
# UnpickleableError: Cannot pickle <type 'Affine'> objects

# 
#**        l3 calling python
inline '''
def plot_(fname, x, y):
    import matplotlib as M
    import pylab as P
    M.use('Agg')

    plot = P.plot(x, y, '-', lw=2)

    # P.xlabel('time (s)')
    # P.ylabel('voltage (mV)')
    # P.title('About as simple as it gets, folks')
    # P.grid(True)

    P.savefig(fname)
'''
inline 'from math import pi; import matplotlib as M; import pylab as P'
t = P.arange(0.0, 1.0+0.01, 0.01)
s = P.cos(2 * pi * t)
plot_('simple_plot.png', s, t)


# Python image read (load) test
import matplotlib.image as mi
import numpy as N
img_arr = mi.imread('simple_plot.png')

# 
#**        l3 only
def plot_(fname, x, y):
    inline('import matplotlib as M; import pylab as P')
    M.use('Agg')
    plot = P.plot(x, y, '-', lw=2)

    # P.xlabel('time (s)')
    # P.ylabel('voltage (mV)')
    # P.title('About as simple as it gets, folks')
    # P.grid(True)

    P.savefig(fname)
inline 'from math import pi; import matplotlib as M; import pylab as P'
t = P.arange(0.0, 1.0+0.01, 0.01)
s = P.cos(2 * pi * t)
plot_('simple_plot.png', s, t)


#
#**        l3 only, with axis
# <span background="#ffcccc">Plot details.</span>
def plot_(fname, x, y):
    inline('import matplotlib as M; import pylab as P')
    M.use('Agg')

    fig = P.figure(figsize=(5,4), dpi=75)
    ax = fig.add_subplot(111)
    ax.plot(x, y, '-', lw=2)

    ax.set_xlabel('time (s)')
    ax.set_ylabel('voltage (mV)')
    ax.set_title('About as simple as it gets, folks')
    ax.grid(True)

    fig.savefig(fname)
    return fname
inline 'from math import pi; import pylab as P'
# <span background="#ffdddd">Plot data.</span>
x = P.arange(0.0, 1.0+0.01, 0.01)
y = P.cos(2 * pi * x)
# <span background="#ffdddd">Plot file.</span>
filename = plot_('_plot-%d.png' % new_id(), x, y)


# 
#**        l3 with explicit figure in subplot
# Plot details can be adjusted here.
inline '''
def plot_(fname, x, y):
    import matplotlib as M
    import pylab as P
    M.use('Agg')

    fig = P.figure(figsize=(5,4), dpi=75)
    ax = fig.add_subplot(111)
    ax.plot(x, y, '-', lw=2)

    ax.set_xlabel('time (s)')
    ax.set_ylabel('voltage (mV)')
    ax.set_title('About as simple as it gets, folks')
    ax.grid(True)

    fig.savefig(fname)
'''
inline 'from math import pi; import matplotlib as M; import pylab as P'
x = t = P.arange(0.0, 1.0+0.01, 0.01)
y = s = P.cos(2 * pi * t)
# Insert plot data here.
plot_('simple_plot.png', s, t)
#'
    
# 
#**        Plot without intermediate data file
# from .../matplotlib-0.90.1/examples/to_numeric.py

# 
#**        Interactive matplotlib window combined with l3gui
import matplotlib as M
import pylab as P

from matplotlib.axes import Subplot
from matplotlib.figure import Figure
from matplotlib.numerix import arange, sin, pi

antialias = False
if antialias:
    from matplotlib.backends.backend_gtkagg import \
         FigureCanvasGTKAgg as FigureCanvas  
    from matplotlib.backends.backend_gtkagg import \
         NavigationToolbar2GTKAgg as NavigationToolbar
else:
    from matplotlib.backends.backend_gtk import \
         FigureCanvasGTK as FigureCanvas

    from matplotlib.backends.backend_gtk import \
         NavigationToolbar2GTK as NavigationToolbar

def plot_figure():
    fig = Figure(figsize=(5,4), dpi=100)
    ax = fig.add_subplot(111)
    t = arange(0.0,3.0,0.01)
    s = sin(2*pi*t)
    ax.plot(t,s)
    ax.set_xlabel('time (s)')
    ax.set_ylabel('voltage (mV)')
    ax.set_title('About as simple as it gets, folks')
    ax.grid(True)
    return (ax, fig)


def plot_win(figure):
    win = gtk.Window()
    # win.connect("destroy", lambda x: gtk.main_quit())
    win.set_default_size(400,300)
    win.set_title("Interactive plot")

    vbox = gtk.VBox()
    win.add(vbox)

    canvas = FigureCanvas(figure)
    toolbar = NavigationToolbar(canvas, win)
    vbox.pack_start(canvas)
    vbox.pack_start(toolbar, False, False)

    win.show_all()
    # gtk.main()
    return win

# Everything is done through the figure and axis (good), but these
# don't pickle.  
(axis, figure) = plot_figure()
figure.savefig('testimage.png')          # Also available via plot window.
plot_win(figure)

gtk.main()                              # test only.


# 
xx = P.arange(0.0, 1.0+0.01, 0.01)
yy = P.cos(2*2*pi*xx)
fname = 'simple_plot.png'
if 1:
    M.use('GTKAgg')
    plot = P.plot(xx, yy, '-', lw=2)
    P.xlabel('time (s)')
    P.ylabel('voltage (mV)')
    P.title('About as simple as it gets, folks')
    P.grid(True)
    P.savefig(fname)



#*    empy macro tests
#**        simplest expansion
from l3lang.cruft import em
glo = {}
em.expand(
    '@[def emph(str)]<span background="#ffcccc"> @str </span>@[end def]', glo)
print em.expand("@emph('the comment')", glo)
#
#**        Single em interpreter, faster.
from l3lang.cruft import em
em_inter = em.Interpreter()
em_glo = {}
em_inter.expand(
    '@[def emph(str)]<span background="#ffcccc"> @str </span>@[end def]'
    '@[def strong(str)]<b> @str </b>@[end def]'
    , em_glo)
def emx(str):
    print em_inter.expand(str, em_glo)
# 
#***            Expand one time:
emx("@emph('the comment')")
emx("@strong('bold')")
emx("""@emph('the @strong("bold") comment')""")

# 
#***            Nested expansion:
emx("""@emph{the @strong("bold") comment}""")
# 
#***            Ordering in nested expansions.
#   In this, em_l inside def gives
#   NameError: global name 'em_l' is not defined
emx('''
@{em_l = 0}
@[def emph(str)]
@{global em_l;
em_l += 1}
@[if em_l % 2]
<span background="EVEN">
    @str
</span>
@[else]
<span background="ODD">
    @str
</span>
@[end if]
@{em_l -= 1}
@[end def]
''')
    
emx("@em_l")
emx("No emphasis @emph{some emphasis @emph{nested emphasis} some emphasis}")

# 
#***            Ordering in nested expansions, 2
# The following cannot work; the em_l state has to be decreased at some
# future time after the macro is expanded .  
class emph:
    em_l = 0
    def __call__(self, stri):
        self.__class__.em_l += 1
        
        if self.__class__.em_l % 2:
            '''
<span background="EVEN">
    @stri
</span>
            '''
        else:
            '''
<span background="ODD">
    @stri
</span>
            '''
        self.__class__.em_l -= 1

emph = _foo()
        

# 
#***            Ordering in nested expansions, 3
# The nested expansion order is inside-out (which is wrong).
emx('''
@{
def emph(stri):
    emph_level = empy.getGlobals().get('emph_level', -1) + 1
    empy.getGlobals()['emph_level'] = emph_level
    if (emph_level % 2) == 0:
        rv = empy.expand('<span background="EVEN"> @stri </span>', locals())
    else:
        rv = empy.expand('<span background="ODD"> @stri </span>', locals())
    empy.getGlobals()['emph_level'] = emph_level - 1
    return rv
}
''')
emx("@emph('foo')")
emx("@emph('hello @emph(this) world')")
emx("@emph{hello @emph{this} world}")

# 
#***            Ordering in nested expansions, 4
# 
emx('''
@{
def emph(stri):
    emph_level = empy.getGlobals().get('emph_level', -1) + 1
    empy.getGlobals()['emph_level'] = emph_level
    
    exp = lambda strng: empy.expand(strng, locals())
    if (emph_level % 2) == 0:
        rv = empy.expand(
            empy.expand(
                '<span background="EVEN"> @stri </span>',
                locals()),
            locals())
    else:
        rv = empy.expand(
            empy.expand(
                '<span background="ODD"> @stri </span>',
                locals()),
            locals())
    empy.getGlobals()['emph_level'] = emph_level - 1
    return rv
}
''')
# Works
emx("@emph('foo')")
# Gives  NameError: name 'emph' is not defined
emx("@emph('hello @emph(this) world')")
emx("@emph{hello @emph{this} world}")



#*    User-guiding emphasis
#**        highlight, raw pango markup
# <span background="#ffcccc">Plot details can be adjusted here.</span>
inline '''if 1:
def plot_(fname, x, y):
    import matplotlib as M
    import pylab as P
    M.use('Agg')

    fig = P.figure(figsize=(5,4), dpi=75)
    ax = fig.add_subplot(111)
    ax.plot(x, y, '-', lw=2)

    ax.set_xlabel('time (s)')
    ax.set_ylabel('voltage (mV)')
    ax.set_title('About as simple as it gets, folks')
    ax.grid(True)

    fig.savefig(fname)
'''
#'
inline 'from math import pi; import matplotlib as M; import pylab as P'
x = t = P.arange(0.0, 1.0+0.01, 0.01)
y = s = P.cos(2 * pi * t)
# <span background="#ffdddd">Insert plot data here.</span>
plot_(
    # <span background="#ffdddd">Output file name.</span>
    'simple_plot.png',
    # <span background="#ffdddd">Your X.</span>
    s,
    # <span background="#ffdddd">Your Y.</span>
    t)
# 
#**        highlight, m4 syntax.
# guide(Insert plot data here.)
plot_(
    # guide(Output file name.)
    'simple_plot.png',
    # guide(Your X.)
    s,
    # guide(Your Y.)
    t)
# 
#**        highlight, per-line prefix syntax.
#g* Insert plot data here.
plot_(
    #g* Output file name.
    'simple_plot.png',
    #g* Your X.
    s,
    #g* Your Y.
    t)
# 
#**        highlight, long syntax w/ line separation
# This leaves blank lines.
#<span background="#ffdddd">
# Insert plot data here.
#</span>
plot_(
    # <span background="#ffdddd">Output file name.</span>
    'simple_plot.png',
    # <span background="#ffdddd">Your X.</span>
    s,
    # <span background="#ffdddd">Your Y.</span>
    t)



# 
#**        Greyscale other text.
# From any root node: 
from l3gui.widgets import *
for nd in self.iter_visibles():
    if isinstance(nd, l3Rawtext):
        nd._ltext.set_property("fill-color", "#" + "d0" * 3)
# 
#**        Collect user guiding info from comments.
# Starting from 'eval locally',
#     >>> self
#     <l3gui.widgets.l3Program instance at 0xb780a70c>
#***            Find all comments.
dt = self.w_.deco_table_
for nd in self._l3tree.top_down():
    print nd, dt.get(nd._id, None)
#
#***            Traverse comments.
def iter_comments(self):
    dt = self.w_.deco_table_
    for nd in self._l3tree.top_down():
        comm = dt.get(nd._id, None)
        if comm != None:
            yield comm

#
#***            Find comments with highlighting.
for comm in iter_comments(self):
    if (comm.find("<span background") > 0):
        print comm

# 
#***            Accumulate comments.
print [ comm for comm in iter_comments(self) 
        if (comm.find("<span background") > 0)]
print [ comm.py_string()
        for comm in iter_comments(self) 
        if (comm.find("<span background") > 0)]
print '\n'.join([ comm.py_string()
        for comm in iter_comments(self) 
        if (comm.find("<span background") > 0)])

# 
#***            Add accumulation to this heading's comment.
# This changes the associated comment and adds state handling details.
# Viewing internal information can be a transient action (use popups)
# or a special view (independent display alongside).
# 
# Use deco_change_label
dt = self.w_.deco_table_
comm = dt.get(self._l3tree._id, None)

if comm != None:
    prepend = comm.py_string()
    self._old_label = prepend
else:
    prepend = ""
print (prepend + '\n' + 
       '\n'.join([ comm.py_string()
                   for comm in iter_comments(self) 
                   if (comm.find("<span background") > 0)]))

self.deco_change_label(label =
                       (prepend + '\n' + 
                        '\n'.join([ comm.py_string()
                                    for comm in iter_comments(self) 
                                    if (comm.find("<span background") > 0)])))

#
#***            Display accumulation as separate node
# Loosely connected to independent 
TODO: add as menu entry
done: view_as -> full member
self.view_as('flat')
    



#*    matplotlib and tools
from numerix import absolute, arange, array, asarray, ones, divide,\
     transpose, log, log10, Float, Float32, ravel, zeros,\
     Int16, Int32, Int, Float64, ceil, indices, \
     shape, which, where, sqrt, asum, compress, maximum, minimum, \
     typecode, concatenate, newaxis, reshape, resize, repeat, \
     cross_correlate, nonzero  

from matplotlib.numerix import absolute, arange, array, asarray, ones, divide,\
     transpose, log, log10, Float, Float32, ravel, zeros,\
     Int16, Int32, Int, Float64, ceil, indices, \
     shape, which, where, sqrt, asum, compress, maximum, minimum, \
     typecode, concatenate, newaxis, reshape, resize, repeat, \
     cross_correlate, nonzero  

inline 'import MLab as M; from matplotlib.numerix import transpose'
A = M.rand(4,4)
At = transpose(A)


# run from l3, gets stuck in
#   #0  dlamc3_ (a=0xb7a10030, b=0xb7a0ffe0) at Src/dlamch.c:685
# when run from l3 AND when run in l3's python:
#   l3 -> python -> run -> wait.
M.eig(A)

#*    screenshots via gtk
# For capturing images of selected items it's much better to use known
# coordinates and directly produce a file than using the capture ->
# edit -> convert -> save sequence via the gimp or others.

view = self.w_.canvas.view
selection = self.w_.selector.get_selection()

(sl, st, sr, sb) = selection.get_bounds()

# Get selection corners.
(wl, wt) = map(int, view.world_to_window(sl, st))
(wr, wb) = map(int, view.world_to_window(sr, sb))

# Compute selection dimensions.
width = wr - wl 
height = wb - wt

# Prepare pixbuf.
pixbuf = gtk.gdk.Pixbuf(gtk.gdk.COLORSPACE_RGB,
                        False, # has_alpha
                        8,     # bits_per_sample
                        width,
                        height) 

# get_from_drawable(src, cmap, src_x, src_y, dest_x, dest_y, width, height)
status = pixbuf.get_from_drawable(
    view.get_parent_window(),  # gtk.gdk.Window(gtk.gdk.Drawable)
    gtk.gdk.colormap_get_system(),
    wl, wt,
    0, 0,
    width, height
    )

if status == None:
    print "capture failed"

pixbuf.save("/tmp/foo.png", "png")

#*    system state work                        :l3:memory_use:
# good start:
for (mname, memb) in self.__dict__.iteritems():
    print "%-20s %40s" % (mname, memb)

# Check completeness.
set([1,2]) - set([1])
set(self.__dict__.keys()) - set(dir(self)) # Empty, good.

# 
#**        Print an ordered table of member DATA. 
def sort(list):
    list.sort()
    return list

print self
for mname in sort(self.__dict__.keys()):
    memb = self.__dict__[mname]
    print "    %-20s %-40s" % (mname, memb)

# 
#**        Print an ordered table of member DATA. 
def pot1(datum):
    print "%s.%s instance at %x" % (datum.__module__,
                                    datum.__class__.__name__,
                                    (id(self)))

    for mname in sort(datum.__dict__.keys()):
        memb = datum.__dict__[mname]
        print "    %-20s %-40.39s" % (mname, memb)
pot1(self._l3tree)
'''
l3lang.ast.Program instance at -488123d4
    _attributes          {}                                      
    _block_env           Env-30005-anonymous                     
    _first_char          (28, 8)                                 
    _id                  30021                                   
'''

# 
#**        ordered table of member data 2
#   Lines numbered,
#   first type occurence listed as subtree
from types import *
def pot2(datum, line = 1, handled = set()):
    # Filter built-in and already printed types
    if not (type(datum) == InstanceType):
        return line

    classname = "%s.%s" % (datum.__module__, datum.__class__.__name__)
    if classname in handled:
        return line
    handled.add(classname)

    # Print info
    print "%s.%s instance at %x" % (datum.__module__,
                                    datum.__class__.__name__,
                                    (id(self)))
    for mname in sort(datum.__dict__.keys()):
        memb = datum.__dict__[mname]
        print "%4d %-20s %-40.39s" % (line, mname, memb)
        line = pot2(memb, line = line + 1, handled = handled)
    return line
pot2(self._l3tree, line = 1, handled = set())
'''
l3lang.ast.Program instance at -488123d4
   1 _attributes          {}                                      
   2 _block_env           Env-30005-anonymous                     
l3lang.ast.Env instance at -488123d4
   3 _bindings            {('active', 'df'): None, ('pixel', 'df' 
   4 _bindings_ids        {('active', 'df'): 30509, ('pixel', 'df 
   5 _bindings_memory     {}                                      
   6 _children            [Env(30536, Env-30005-anonymous, [Progr 
   7 _def_env_id          30001                                   
'''
# '

# 
#**        print ordered table of member data 3
#   Lines numbered,
#   first type occurence listed as subtree,
#   indentation for levels, 
#   all later occurrences of a type refer to line number
from types import *
def sort(list):
    list.sort()
    return list
def pot3(datum, line = 1, handled = {}, indent = 1):
    # Filter built-in and already printed types
    if not (type(datum) == InstanceType):
        return line

    space = "    " * indent
    classname = "%s.%s" % (datum.__module__, datum.__class__.__name__)
    if handled.has_key(classname):
        print "%4d%s%s.%s instance -- see line %d" % (line,
                                                space,
                                                datum.__module__,
                                                datum.__class__.__name__,
                                                handled[classname])
        return line + 1
    else:
        handled[classname] = line

    # Print info
    print "%4d%s%s.%s instance at %x" % (line,
                                         space,
                                         datum.__module__,
                                         datum.__class__.__name__,
                                         (id(self)))
    line += 1
    for mname in sort(datum.__dict__.keys()):
        memb = datum.__dict__[mname]
        print "%4d%s%-20s %-40.39s" % (line, space, mname, memb)
        line = pot3(memb, line = line + 1, handled = handled,
                    indent = indent + 1)
    return line
# 210 lines
pot3(self, line = 1, handled = {}, indent = 1)
#  46 lines
pot3(self._l3tree, line = 1, handled = {}, indent = 1)


# 
#**        print ordered table of member data 4
#   Lines numbered,
#   first type occurence listed as subtree,
#   indentation for levels, 
#   repeated instances of a fixed-member type refer to first's line number,
#   distinct instances of container types (Shared) produce new listing.  
from types import *
import l3lang.ast
def sort(list):
    list.sort()
    return list

truncate_lines = "%-120.119s" 
truncate_lines = "%-80.79s" 
def pot4(datum, line = 1, handled = {}, indent = 1):
    # Filter built-in types.
    #   inheriting from ListType has subtle problems.
    if isinstance(datum, l3lang.ast.aList):
        pass
    else:
        if not (type(datum) == InstanceType):
            return line

    space = "    " * indent
    classname = "%s.%s" % (datum.__module__, datum.__class__.__name__)

    # Only cross-reference already visited data.
    if classname not in ['l3lang.utils.Shared']:
        if handled.has_key(classname):
            print "%4d%s%s.%s instance -- see line %d" % (line,
                                                    space,
                                                    datum.__module__,
                                                    datum.__class__.__name__,
                                                    handled[classname])
            return line + 1
        else:
            handled[classname] = line

    else:
        if handled.has_key(id(datum)):
            print "%4d%s%s.%s visited instance -- see line %d" % (
                line,
                space,
                datum.__module__,
                datum.__class__.__name__,
                handled[id(datum)])
            return line + 1
            
        else:
            handled[id(datum)] = line

    # Print info for this datum.
    if indent < 2:                      
        # Only show headers for objects not already described.
        print "%4d%s%s.%s instance at 0x%x" % (line,
                                             space,
                                             datum.__module__,
                                             datum.__class__.__name__,
                                             (id(datum) & 0xFFFFFFFFL))
        line += 1

    # Visit datum's children.
    for mname in sort(datum.__dict__.keys()):
        memb = datum.__dict__[mname]
        print truncate_lines % ("%4d%s%-20s %-60s" % (line, space, mname, memb))
        line = pot4(memb, line = line + 1, handled = handled,
                    indent = indent + 1)
    return line
# self is noise_add.l3
pot4(self, line = 1, handled = {}, indent = 1)
# 327 lines
#  325    remember_y           60.25 
#  326    w_                   <l3lang.utils.Shared instance at 0xb777512c> 
#  327        l3lang.utils.Shared visited instance -- see line 21 
# 
# 1352 lines without back-reference to already-visited Shareds, e.g.
# 
# 1146    w_                   <l3lang.utils.Shared instance at 0xb777512c> 
# 1147        __fluids             {} 
# 1148        _non_pickling_keys   [] 
# 

'''
Include traversal of lists, dicts to cover this:

This is missing _l3tree contents because 
    type(self._alist.__dict__['_l3tree'])
    <class 'l3lang.ast.aList'>

But this is not true; type returns
    >>> type(self._l3tree[0])
    <class 'l3lang.ast.aList'>
while the instance members are all present:
    >>> dir(self._l3tree[0])
    ... '_first_char', '_id', '_last_char', '_parent',...
And:
    >>> isinstance(self._l3tree[0], l3lang.ast.aList)
    True

Other NESTED types inheriting from python built-ins:
    class aList(ListType):
    class vaList(aList):

'''



# 
#**        print ordered table of member data 5
#   - Lines numbered,
#   - first type occurence listed as subtree,
#   - indentation for levels, 
#   - repeated instances of a fixed-member type refer to first's line number,
#   - distinct instances of container types (Shared) produce new listing.  
#   - make line numbers org-indexable, via [[line]] and <<line>> 
# 
# Tried
# - including indentation level for text reading, didn't help
from types import *
import l3lang.ast
def sort(list):
    list.sort()
    return list

truncate_lines = "%-120.119s" 
truncate_lines = "%-80.79s" 
def pot5(datum, line = 1, handled = {}, indent = 1):
    '''
    This produces a reasonable state dump of the l3 widget provided as
    `datum`.  The contained l3 program is included but not traversed
    recursively, so associated data and clones are completely ignored.
    '''    
    # Filter built-in types.
    #   inheriting from ListType has subtle problems.
    if isinstance(datum, l3lang.ast.aList):
        pass
    else:
        if not (type(datum) == InstanceType):
            return line

    space = "    " * indent
    classname = "%s.%s" % (datum.__module__, datum.__class__.__name__)

    # Only cross-reference already visited data.
    if classname not in ['l3lang.utils.Shared']:
        if handled.has_key(classname):
            print "<<%03d>>%s%s.%s instance -- see line [[%03d]]" % (
                line,
                space,
                datum.__module__,
                datum.__class__.__name__,
                handled[classname])
            return line + 1
        else:
            handled[classname] = line

    else:
        if handled.has_key(id(datum)):
            print "<<%03d>>%s%s.%s visited instance -- see line [[%03d]]" % (
                line,
                space,
                datum.__module__,
                datum.__class__.__name__,
                handled[id(datum)])
            return line + 1
            
        else:
            handled[id(datum)] = line

    # Print info for this datum.
    if indent < 999:
        # Only show headers for objects not already described.
        print "<<%03d>>%s%s.%s instance at 0x%x" % (
            line,
            space,
            datum.__module__,
            datum.__class__.__name__,
            (id(datum) & 0xFFFFFFFFL))
        line += 1

    # Visit datum's children.
    for mname in sort(datum.__dict__.keys()):
        memb = datum.__dict__[mname]
        print truncate_lines % ("<<%03d>>%s%-20s %-60s" % (line, space, mname, memb))
        line = pot5(memb, line = line + 1, handled = handled,
                    indent = indent + 1)
    return line
# self is noise_add.l3
pot5(self, line = 1, handled = {}, indent = 1)

# 
#**        print ordered table of member data, org version 1
#   - use hex id as address, via org
#   - first type occurence listed as subtree,
#   - indentation for levels, 
#   - repeated instances of a fixed-member type refer to first's id
#   - distinct instances of container types (Shared) produce new listing.  
#   - no line numbers
#   - include emacs outline decoration (org-mode)
from types import *
import l3lang.ast
import l3lang.ast as A

def sort(list):
    list.sort()
    return list

def hexid(datum):
    return (id(datum) & 0xFFFFFFFFL)

# 
#***            special printers
from EMAN2 import EMData
from numpy import ndarray

accum_types = (EMData, ndarray)

special_printers = {}
accumulate_interp_res = {}          # Accumulate stuff as side-effect.
limit_output_lines = 100

# 
#****                Accumulate by EXCLUDING types.
def _special(datum, line = 1, handled = {}, indent = 1):
    '''Print additional information for RamMem._attr_tables.'''
    first_line = line
    space = "    " * indent
    for (id_, dic1) in datum.iteritems():
        # 3002  {...}
        # mem_str = max_str_len % str(dic1)
        mem_str = "{...}"
        if (line - first_line) < limit_output_lines:
            print truncate_lines % (
                "%s%s%-20s %s" % (
                    "*" * indent,
                    space[0: -indent],
                    id_,
                    mem_str))
            line += 1
        # clone_of   {...}
        for key in sort(dic1.keys()):
            # Side effect: accumulation
            if key == 'interp_result':
                se_val = dic1[key]
                # These checks are superficial; real code must use
                # pickle w/o memo dict for verification.
                if not isinstance(se_val, (A.astType, A.aNone, A.aList)):
                    if (isinstance(se_val, ListType) and
                        len(se_val) > 0 and
                        isinstance(se_val[0], (A.astType, A.aNone, A.aList))):
                        pass
                    else:
                        accumulate_interp_res[id_] = dic1[key]
            # Print SOME output.
            if (line - first_line) < limit_output_lines:
                mem_str = max_str_len % str(dic1[key])
                print truncate_lines % (
                    "%s%s%-20s %s" % (
                        "*" * (indent + 1),
                        space[0: -(indent + 1)] + "    ",
                        key,
                        mem_str))
                line += 1
    return line
special_printers[('RamMem', '_attr_tables')] = _special

# 
#****                Accumulate by INCLUDING types.
def _special(datum, line = 1, handled = {}, indent = 1):
    '''Print additional information for RamMem._attr_tables.'''
    first_line = line
    space = "    " * indent
    for (id_, dic1) in datum.iteritems():
        # 3002  {...}
        # mem_str = max_str_len % str(dic1)
        mem_str = "{...}"
        if (line - first_line) < limit_output_lines:
            print truncate_lines % (
                "%s%s%-20s %s" % (
                    "*" * indent,
                    space[0: -indent],
                    id_,
                    mem_str))
            line += 1
        # clone_of   {...}
        for key in sort(dic1.keys()):
            # Side effect: accumulation
            if key == 'interp_result':
                se_val = dic1[key]
                if isinstance(se_val, accum_types):
                    accumulate_interp_res[id_] = se_val

            # Print SOME output.
            if (line - first_line) < limit_output_lines:
                mem_str = max_str_len % str(dic1[key])
                print truncate_lines % (
                    "%s%s%-20s %s" % (
                        "*" * (indent + 1),
                        space[0: -(indent + 1)] + "    ",
                        key,
                        mem_str))
                line += 1
    return line
special_printers[('RamMem', '_attr_tables')] = _special

# 
#****                Accumulate by INCLUDING types and lists/tuples of types
def _special(datum, line = 1, handled = {}, indent = 1):
    '''Print additional information for RamMem._attr_tables.'''
    first_line = line
    space = "    " * indent
    for (id_, dic1) in datum.iteritems():
        # 3002  {...}
        # mem_str = max_str_len % str(dic1)
        mem_str = "{...}"
        if (line - first_line) < limit_output_lines:
            print truncate_lines % (
                "%s%s%-20s %s" % (
                    "*" * indent,
                    space[0: -indent],
                    id_,
                    mem_str))
            line += 1
        # clone_of   {...}
        for key in sort(dic1.keys()):
            # Side effect: accumulation
            if key == 'interp_result':
                se_val = dic1[key]
                if isinstance(se_val, accum_types):
                    accumulate_interp_res[id_] = se_val
                if isinstance(se_val, (ListType, TupleType)) and \
                   (len(se_val) > 0) and \
                   (len(                   
                    [ ob for ob in se_val
                      if isinstance(ob, accum_types)]) > 0):
                    accumulate_interp_res[id_] = se_val                    
                

            # Print SOME output.
            if ((line - first_line) < limit_output_lines):
                mem_str = max_str_len % str(dic1[key])
                print truncate_lines % (
                    "%s%s%-20s %s" % (
                        "*" * (indent + 1),
                        space[0: -(indent + 1)] + "    ",
                        key,
                        mem_str))
                line += 1
    return line
special_printers[('RamMem', '_attr_tables')] = _special

# 
#***            main printer
truncate_lines = "%-80.79s" 
max_str_len = "%-60.59s" 

truncate_lines = "%-160.159s" 
max_str_len = "%-.90s" 
from time import time
def poto1(datum, line = 1, handled = {}, indent = 1):
    # Filter built-in types.
    #   inheriting from ListType has subtle problems.
    if isinstance(datum, l3lang.ast.aList):
        pass
    else:
        if not (type(datum) == InstanceType):
            return line

    space = "    " * indent
    classname = "%s.%s" % (datum.__module__, datum.__class__.__name__)

    # Only cross-reference already visited data.
    if classname not in ['l3lang.utils.Shared']:
        if handled.has_key(classname):
            print "%s%s.%s instance -- see id [[0x%x]]" % (
                space,
                datum.__module__,
                datum.__class__.__name__,
                handled[classname])
            return line + 1
        else:
            handled[classname] = hexid(datum)

    else:
        if handled.has_key(id(datum)):
            print "%s%s.%s visited instance -- see id [[0x%x]]" % (
                space,
                datum.__module__,
                datum.__class__.__name__,
                handled[id(datum)])
            return line + 1
            
        else:
            handled[id(datum)] = hexid(datum)

    # Print info for this datum.
    if indent < 2:
        print "%s%s%s.%s instance at <<0x%x>>" % (
            "*" * indent,
            space[0: -indent],
            datum.__module__,
            datum.__class__.__name__,
            hexid(datum))
        line += 1

    # Visit datum's children.
    for mname in sort(datum.__dict__.keys()):
        memb = datum.__dict__[mname]
        # Include timing information with lines.
        start = time()

        # For instances, produce org-mode <<label>>
        # mem_str = max_str_len % str(memb)
        mem_str = max_str_len % str(memb)
        if "instance at 0x" in mem_str:
            mem_str = "%s at <<0x%x>>" % (mem_str[0:-15], hexid(memb))
        elif isinstance(memb, (InstanceType, ClassType, ListType, DictType)):
            mem_str = "%s at <<0x%x>>" % (mem_str, hexid(memb))
        print truncate_lines % (
            "%s%s%-20s %s [%.4fs]" % (
                "*" * indent,
                space[0: -indent],
                mname,
                mem_str,
                time() - start))
        # Default printout for child.
        line = poto1(memb, line = line + 1, handled = handled,
                    indent = indent + 1)
        # Additional custom printout.
        try:
            line = special_printers[(datum.__class__.__name__,
                                     mname)](memb, line = line + 1,
                                             handled = handled,  
                                             indent = indent + 1)
        except KeyError:
            pass
    return line
# 
#**        Get internals dump
# Run gui
# insert program
# run program
# use 'evaluate locally` on datum
# 
# Then:
self
poto1(self, line = 1, handled = {}, indent = 1)

poto1(w_.state_.storage, line = 1, handled = {}, indent = 1)

# 
#**        Compute disk / ram ratio
#       Given
#           Sr = Ram state
#           Sd = disk state  (from accumulate_interp_res)
#           St = total state (from save_state)
#           S0 = initial state (overhead, from save_state at time 0)
#           R = ratio
# 
#       we want 
#           Sr(t) = R * Sd(t)
#       and
#           St(t) = Sr(t) + Sd(t) + S0
# 
#       Using known values, 
#           Sr = St - Sd - S0
#       so
#             R = (St - Sd - S0) / Sd 
#       should (approximately) hold.

# Run gui
# insert program
# save state to st.t0
# run program
# exit to toplevel
# 
# Then:
poto1(w_.state_.storage, line = 1, handled = {}, indent = 1)
print "items collected: ", len(accumulate_interp_res)

from l3lang import utils
utils.file_cPickle(get_current_state(w_), "st.total")
utils.file_cPickle(accumulate_interp_res, "st.disk")

S0 = os.path.getsize("st.t0")
St = os.path.getsize("st.total")
Sd = os.path.getsize("st.disk")

print "ram / disk ratio", 1.0 * (St - Sd - S0) / Sd 

# 
#**        view interp_result 
for (id_, val) in accumulate_interp_res.iteritems():
    print "%10d %-.50s" % (id_, val)

# What is accumulate_interp_res[33122]?
type(accumulate_interp_res[33122])
print w_.state_.storage.get_attribute_names(33122)
print w_.state_.storage.get_attribute(33122, 'interp_env')


# 
#**        object sizes
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

pckl_file = StringIO()
pcklr = pickle.Pickler(pckl_file, protocol = 2)
pcklr.memo
pcklr.dump(accumulate_interp_res)
print "pickle size", pckl_file.tell()

# memo[id(obj)] = memo_len, obj
# too many values with no back-link information to identify source.
for (id_, (index, obj)) in pcklr.memo.iteritems():
    print "%-11x %-6d %-.50s" % (id_, index, obj)
    


#*    Custom pickle with printout.
# Collecting through storage is unreliable; modify pickle itself.
#**        params
import pickle
from EMAN2 import EMData
from numpy import ndarray

try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

accum_types = (EMData, ndarray)
pckl_file = StringIO()
g_display_size = 10000
truncate_lines = "%-60.59s" 

# 
#**        pickle modifications.
from types import *
from copy_reg import dispatch_table
from copy_reg import _extension_registry, _inverted_registry, _extension_cache
import marshal
import sys
import struct

def _real_save(self, obj):
    # Info dump
    me_start = pckl_file.tell()
    def dump_info():
        obj_size = pckl_file.tell() - me_start
        if isinstance(obj, accum_types) or (obj_size > g_display_size):
            print "| %8d | \"%s\" | " % (obj_size,
                                              truncate_lines % repr(obj))
    # -- Info dump

    # Check for persistent id (defined by a subclass)
    pid = self.persistent_id(obj)
    if pid:
        self.save_pers(pid)
        dump_info()
        return

    # Check the memo
    x = self.memo.get(id(obj))
    if x:
        self.write(self.get(x[0]))
        dump_info()
        return

    # Check the type dispatch table
    t = type(obj)
    f = self.dispatch.get(t)
    if f:
        f(self, obj) # Call unbound method with explicit self
        dump_info()
        return

    # Check for a class with a custom metaclass; treat as regular class
    try:
        issc = issubclass(t, TypeType)
    except TypeError: # t is not a class (old Boost; see SF #502085)
        issc = 0
    if issc:
        self.save_global(obj)
        dump_info()
        return

    # Check copy_reg.dispatch_table
    reduce = dispatch_table.get(t)
    if reduce:
        rv = reduce(obj)
    else:
        # Check for a __reduce_ex__ method, fall back to __reduce__
        reduce = getattr(obj, "__reduce_ex__", None)
        if reduce:
            rv = reduce(self.proto)
        else:
            reduce = getattr(obj, "__reduce__", None)
            if reduce:
                rv = reduce()
            else:
                raise PicklingError("Can't pickle %r object: %r" %
                                    (t.__name__, obj))

    # Check for string returned by reduce(), meaning "save as global"
    if type(rv) is StringType:
        self.save_global(obj, rv)
        dump_info()
        return

    # Assert that reduce() returned a tuple
    if type(rv) is not TupleType:
        raise PicklingError("%s must return string or tuple" % reduce)

    # Assert that it returned an appropriately sized tuple
    l = len(rv)
    if not (2 <= l <= 5):
        raise PicklingError("Tuple returned by %s must have "
                            "two to five elements" % reduce)

    # Save the reduce() output and finally memoize the object
    self.save_reduce(obj=obj, *rv)
    dump_info()

# Overwrite method.
def save(self, obj):
    # Produces too many empty () pairs.
    print "(",
    try:
        _real_save(self, obj)
    finally:
        print ")"

def save(self, obj):
    _real_save(self, obj)

pickle.Pickler.save = save

# 
#**        Compute.
accum_types = (EMData, ndarray)
pckl_file = StringIO()
g_display_size = 100000
truncate_lines = "%-60.59s" 
pcklr = pickle.Pickler(pckl_file, protocol = 2)
pcklr.memo

# First get state_.storage,
pcklr.dump(w_.state_.storage)

# ... then get the REST of state_
pcklr.dump(w_.state_)

#*    Custom pickle, collect info, get prefix dump
# Collecting through storage is unreliable; modify pickle itself.
#**        params
import pickle
from EMAN2 import EMData
from numpy import ndarray

try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

g_accum_types = (EMData, ndarray)
g_pckl_file = StringIO()
g_display_size = 10000
g_truncate_lines = "%-60.59s" 
g_info_stack = []


# 
#**        pickle modifications.
from types import *
from copy_reg import dispatch_table
from copy_reg import _extension_registry, _inverted_registry, _extension_cache
import marshal
import sys
import struct

def _real_save(self, obj):
    # Info dump
    me_start = g_pckl_file.tell()
    def dump_info():
        obj_size = g_pckl_file.tell() - me_start
        if isinstance(obj, g_accum_types) or (obj_size > g_display_size):
            # Forming the full repr() takes a LOT of time; maybe
            # settle for the type() info...
            g_info_stack.append( (obj_size, g_truncate_lines % repr(obj), obj) )
    # -- Info dump

    # Check for persistent id (defined by a subclass)
    pid = self.persistent_id(obj)
    if pid:
        self.save_pers(pid)
        dump_info()
        return

    # Check the memo
    x = self.memo.get(id(obj))
    if x:
        self.write(self.get(x[0]))
        dump_info()
        return

    # Check the type dispatch table
    t = type(obj)
    f = self.dispatch.get(t)
    if f:
        f(self, obj) # Call unbound method with explicit self
        dump_info()
        return

    # Check for a class with a custom metaclass; treat as regular class
    try:
        issc = issubclass(t, TypeType)
    except TypeError: # t is not a class (old Boost; see SF #502085)
        issc = 0
    if issc:
        self.save_global(obj)
        dump_info()
        return

    # Check copy_reg.dispatch_table
    reduce = dispatch_table.get(t)
    if reduce:
        rv = reduce(obj)
    else:
        # Check for a __reduce_ex__ method, fall back to __reduce__
        reduce = getattr(obj, "__reduce_ex__", None)
        if reduce:
            rv = reduce(self.proto)
        else:
            reduce = getattr(obj, "__reduce__", None)
            if reduce:
                rv = reduce()
            else:
                raise PicklingError("Can't pickle %r object: %r" %
                                    (t.__name__, obj))

    # Check for string returned by reduce(), meaning "save as global"
    if type(rv) is StringType:
        self.save_global(obj, rv)
        dump_info()
        return

    # Assert that reduce() returned a tuple
    if type(rv) is not TupleType:
        raise PicklingError("%s must return string or tuple" % reduce)

    # Assert that it returned an appropriately sized tuple
    l = len(rv)
    if not (2 <= l <= 5):
        raise PicklingError("Tuple returned by %s must have "
                            "two to five elements" % reduce)

    # Save the reduce() output and finally memoize the object
    self.save_reduce(obj=obj, *rv)
    dump_info()

# Overwrite method.
def save(self, obj):
    global g_info_stack
    _tmp = g_info_stack
    g_info_stack = []
    try:
        _real_save(self, obj)
    finally:
        if len(g_info_stack) > 0:
            _tmp.append(g_info_stack)
        g_info_stack = _tmp

pickle.Pickler.save = save

# 
#**        view info
def dump_stack(stack, indent):
    # Unwrap outer lists
    while isinstance(stack[-1], ListType):
        stack = stack[-1]
    # Get the main object head and print info.
    (size, rep, obj) = stack[-1]
    print "%s %-8d %s" % ("*" * indent, size, rep)
    # Get head's content, if any
    [dump_stack(ii, indent + 1) for ii in stack[0:-1]]

# 
#**        Use.
g_accum_types = (EMData, ndarray)
g_pckl_file = StringIO()
g_display_size = 1
g_display_size = 1000
g_truncate_lines = "%-60.59s" 
g_info_stack = []
pcklr = pickle.Pickler(g_pckl_file, protocol = 2)
pcklr.memo

# Collect data.
pcklr.dump(w_.state_)
#   OR
pcklr.dump(w_.cp_)
# Inspect
dump_stack(g_info_stack, 0)

# 
g_display_size = 1
pcklr.dump(self._l3tree)
dump_stack(g_info_stack, 0)

#*    dbm interface
import anydbm as any, whichdb
#**        Prepare.
db = any.open('l3db.st', 'c')
db.close()
print whichdb.whichdb('l3db.st')
#**        Save
db = any.open('l3db.st', 'c')
# db[key] = val
storage.sweep(db)
db.close()

#*    Proxy for transparent persistence
class Proxy:
    pass

def __init__(self, obj):
    self._obj = obj
Proxy.__init__ = __init__

def to_cache(self, id):
    # TODO
    self._obj = None
    self._id = id
Proxy.to_cache = to_cache

def __getattr__(self, m_name):
    m_unbnd = getattr(self._obj.__class__, m_name)

    def _unwrap(*args):
        nargs = []
        for arg in args:
            if isinstance(arg, Proxy):
                nargs.append(arg._obj)
            else:
                nargs.append(arg)
        return (m_unbnd(self._obj, *nargs))
    
    return _unwrap
Proxy.__getattr__ = __getattr__

#
#**        proxy numeric tests
import numpy as N
from cPickle import dumps
n1 = N.random.ranf( (5,4) )
n1_prox = Proxy(n1)
print n1_prox + n1_prox
# Size comparison.
# TODO: getstate / setstate
print len(dumps(n1))
print len(dumps(n1_prox))
n1_prox.to_cache(1234)
print len(dumps(n1_prox))

# Useless in-memory size guesses (spread too much)
plist = [Proxy(n1) for ii in xrange(10)]
id(plist[-1]) - id(plist[-2])
ppos = [id(foo) for foo in plist]
ppos.sort()
print map(lambda (x,y): x-y, zip(ppos[1:None], ppos[0:-2]))

#
#**        proxy emdata tests
from EMAN2 import EMData
e1 = EMData()
arr_size = (3,4)
arr_size = (64, 64)
print "array size", arr_size
e1.set_size(*arr_size)
e1.process_inplace('testimage.noise.uniform.rand')
print sys.getrefcount(e1)
e1_prox = Proxy(e1)
print sys.getrefcount(e1)
# Operators.
print e1_prox + e1_prox

# Sizes.
print len(dumps(e1))
print len(dumps(e1_prox))
e1_prox.to_cache(1234)
print len(dumps(e1_prox))



#
#**        create test cases
import numpy as N
import weakref
from cPickle import dumps

n1 = N.random.ranf( (5,4) )
n1_prox = Proxy(n1)
n1_weak = weakref.proxy(n1)

# Initial refcount.
print sys.getrefcount(n1)
# 2

# Refcount with member ref.
n1t = n1.tofile
print sys.getrefcount(n1)
# 3

# weakref's proxy adds no new fields:
set(dir(n1_weak)) - set(dir(n1_prox._obj))

from EMAN2 import EMData
e1 = EMData()
e1.set_size(3,4)
e1.process_inplace('testimage.noise.uniform.rand')
print sys.getrefcount(e1)
e1prox = Proxy(e1)
e1weak = weakref.proxy(e1)

#
#**        Try operators
#***            numeric array works
print n1_prox + n1_prox

n1.__add__(n1)


# 
#***            emdata fails
# TypeError: unsupported operand type(s) for +: 'instance' and 'instance'
print e1prox + e1prox
import operator
print operator.add(e1prox, e1prox)

# This runs, 
print e1weak + e1weak
print operator.add(e1weak, e1weak)
# but produces the equal? result:
# <libpyEMData2.EMData object at 0x156239cc>
# <libpyEMData2.EMData object at 0x156239cc>

# Something is missing causing the discrepancy:
print e1weak.__add__(e1weak)
print e1.__add__(e1)
#   NotImplemented
#   <libpyEMData2.EMData object at 0x15623cdc>

print n1.__add__
print e1.__add__
# <method-wrapper object at 0x1955d3cc>
# <bound method EMData.__add__ of <libpyEMData2.EMData object at 0x1613da54>>

print EMData.__add__(e1,e1)
print EMData.__add__(e1prox,e1prox)
print EMData.__add__(e1weak,e1weak)
# <libpyEMData2.EMData object at 0x15623ca4>
# NotImplemented

e1prox._make_member_proxy('__add__')

# Now this runs (work -- who knows):
print e1prox + e1prox
print e1prox.__add__
print e1weak.__add__
# <bound method EMData.__add__ of <libpyEMData2.EMData object at 0x1613da54>>
# <bound method EMData.__add__ of <libpyEMData2.EMData object at 0x1613da54>>

# TypeError: unsupported operand type(s) for +: 'EMData' and 'instance'
print 1.0 * e1prox + e1prox

# works
print e1weak + e1weak


#*    Replace original class with proxy wrapper
# Looks like there is no need for metaclasses, as this will do
# everything needed.  However, this will not intercept C code creating
# instances, so it is too limited in practice.

# Retain original
import EMAN2
eman2_emdata = EMAN2.EMData

# Make replacement.
class EMData(Proxy):
    def __init__(self, *args):
        self._obj = eman2_emdata(*args)

# Bind replacement.
# This binding remains valid for all FUTURE references, so it must be
# done before other scripts import the module.
EMAN2.EMData = EMData


# Test
e1 = EMAN2.EMData()
e1.set_size(3,4)
e1.process_inplace('testimage.noise.uniform.rand')
e1.print_image()

# The (C) member functions directly create the new object, so the
# indirection above fails.
tst_add = (e1 + e1)
tst_add.print_image()

#*    Pickling uniquely 
# To get accurate memory stats, avoid repeated pickling given nested
# references, e.g.
#   A, B, [A,B]
# 
import pickle
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

pckl_file = StringIO()
pcklr = pickle.Pickler(pckl_file, protocol = 2)
print pcklr.memo
def pickle_size():
    return pckl_file.tell()


#*    string deepcopy test
from copy import deepcopy, copy
orig = 'the original string'
cp = deepcopy(orig)
print id(orig) - id(cp)
# so strings 'copy' as reference.
