#
# Author:  Michael H. Hohn (mhhohn@lbl.gov)
#
# Copyright (c) 2006, The Regents of the University of California
# 
# See legal.txt and license.txt
#

"""
The l3 display canvas widget, using the gnome-canvas via pygtk.

The main canvas class is defined here, along with the rendering
auxiliaries RenderTable and  VisibilityTable.

"""
#*    Header.
import string, tokenize, re

from types import *
from math import *
from copy import deepcopy, copy
from pprint import pprint

import pygtk
import pango
import gtk
import gobject
try:
    import gnomecanvas as canvas
    from gnomecanvas import MOVETO_OPEN, MOVETO, LINETO, CURVETO
except:
    # Try backported version used in sparx.
    import canvas
    from canvas import MOVETO_OPEN, MOVETO, LINETO, CURVETO

from l3lang import utils, reader, ast
from l3lang.ast import Call, Marker, Member, Symbol, aList
from l3gui import widgets as wdgt
from l3gui import misc
from l3gui.misc import \
     associate_comments,\
     triple_quote, \
     flush_events, \
     DisplayError, \
     _set_props, \
     warn_

kn = gtk.gdk.keyval_from_name
key_c = kn("c")
key_Windows_L = kn("Super_L")
key_Alt_L = kn("Alt_L")
# >>> hex(kn("Super_L"))
# '0xffeb'
# >>> hex(kn("Alt_L"))
# '0xffe9'
# >>> hex(kn("Control_L"))
# '0xffe3'


#*    l3Canvas
#
#**        class & __init__
class l3Canvas(canvas.Canvas):
    pass

# This canvas coordinates macros and their display.
def __init__(self, w_):
    canvas.Canvas.__init__(self)
    #
    self.cp_ = w_.cp_                   # canvas parameters
    self.w_ = w_                        # Application structures.
    cp_ = w_.cp_
    self._zoom_set = {}                 # text widget -> None
    self._font_desc = None              # pango font descriptor 

    # Display state.
    self._vis_tab = VisibilityTable()

    # (node id) -> l3Rawtext
    # l3Rawtext -> node id
    self._nodes = {}

    # (id | CanvasLine) -> (CanvasLine | id)
    # id is (src node id, dest node id)
    self._edges = {}

    #
    # Macro Groupings
    #
    ra = self.root().add
    self._macro_call = ra(canvas.CanvasGroup)
    self._macro_block = ra(canvas.CanvasGroup)
    self._macro_special = ra(canvas.CanvasGroup)

    #
    # Canvas geometry.
    # ---------------------------------
    # Background.
    self.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("grey80") )

    # Minimum viewport size.
    self.set_size_request(cp_.width, cp_.height) 

    #
    # Scaling. Scale.  Use the ex as the world unit.
    #
    # #     self._pixpu_base = ( 1.0 * cp_.font_ex) ### / cp_.print_dpi )
    # #     self.set_pixels_per_unit(self._pixpu_base)
    # #     self._pixpu = self._pixpu_base  # Current value
    self.init_fontdesc()
    self.init_pixpu()
    

    # Canvas size
    expi = 1.0 * cp_.screen_dpi / cp_.font_ex
    self._abs_zoom = zm = 1.0
    w_world = (cp_.paper.width + 2 * cp_.margin) / zm * expi
    h_world = (cp_.paper.height + 2 * cp_.margin) / zm * expi
    self.set_scroll_region(cp_.off_x, cp_.off_y,
                           w_world + cp_.off_x,
                           h_world + cp_.off_y)

    # Display "Paper"
    mar = cp_.margin
    paper = self.root().add(canvas.CanvasRect,
                            x1 = mar * expi / zm + cp_.off_x,
                            y1 = mar * expi / zm + cp_.off_y,
                            x2 = ((mar + cp_.paper.width) * expi / zm
                                  + cp_.off_x),
                            y2 = ((mar + cp_.paper.height) * expi / zm
                                  + cp_.off_y),
                            fill_color = 'white',
                            outline_color = 'black',
                            width_pixels = self.cp_.outline_width_normal,
                            )
    paper.lower_to_bottom()
    paper.connect("event", lambda *a: self.paper_event(*a))
    self._paper = paper

    #     Common item parts
    # Tag table and assorted tags.
    ctt = self._common_tag_table = gtk.TextTagTable()
    fst = self._font_size_tag = gtk.TextTag()
    fst.set_property("size-points", cp_.font_size)
    fst.set_property("font", cp_.rawtext_font)
    ctt.add(fst)
    #
    # Events
    #
    self.connect("size-allocate", lambda *a: self.resize_event(*a) )
    self.connect("event", lambda *a: self.canvas_event(*a) )
    self.show()
l3Canvas.__init__ = __init__


def init_fontdesc(self):
    # Pango font work
    pcon = self.get_pango_context()

    # Select font.
    sz_orig = 10
    font_desc = pcon.get_font_description().copy()
    font_desc.set_family(self.w_.cp_.canvas_font_family)
    font_desc.set_size(sz_orig * pango.SCALE)

    # Set new canvas default.
    pcon.set_font_description(font_desc)
    self._font_desc = font_desc
l3Canvas.init_fontdesc = init_fontdesc

def lines(st):
    l1 = st.count('\n')
    if st[-1] == '\n':
        # No content in last line.
        l1 -= 1
    if len(st) > 0:
        l1 += 1
    return max(0, l1)

def width(st):
    return reduce(max, [len(line) for line in st.split('\n')], 0)


def init_pixpu(self):
    # Start with guess.
    self._pixpu = ( 1.0 * self.cp_.font_ex)
    self.set_pixels_per_unit(self._pixpu)

    # Form text for measuring.
    ra = self.root().add
    c_string = "01234\n" * 8            # 5 chars, 8 lines
    c_label = ra(canvas.CanvasText,
                 x = 12, y = 25,
                 text = c_string,
                 font_desc = self._font_desc,
                 )
    flush_events()
    #
    # Set 1 cu = 1 ex
    l1, t1, r1, b1 = c_label.get_bounds()
    cu_p_char = (r1 - l1) / width(c_string)
    self._pixpu = self._pixpu * cu_p_char
    self._pixpu_base = self._pixpu
    self.set_pixels_per_unit(self._pixpu )
    
    c_label.destroy()
l3Canvas.init_pixpu = init_pixpu

def resize_event(self, widget, allocation, *rest):
    ww = allocation.width
    hh = allocation.height
    ### self.set_scroll_region(0, 0, ww * self._pixpu, hh * self._pixpu)
l3Canvas.resize_event = resize_event

def display_bounding_box(self, (x1, y1, x2, y2), color = "orange"):
    outline = (self.root().add(
        canvas.CanvasLine,
        fill_color = color,
        points = [x1, y1,
                  x2, y1,
                  x2, y2,
                  x1, y2,
                  x1, y1,
                  ],
        width_pixels = 2,
        line_style = gtk.gdk.SOLID,
        cap_style = gtk.gdk.CAP_ROUND,
        join_style = gtk.gdk.JOIN_ROUND,
        smooth = True,
        spline_steps = 12,
        ))

    def destroy_event(item, event):
        if  event.type == gtk.gdk.ENTER_NOTIFY:
            outline.destroy()
        return True
    outline.connect("event", destroy_event)
    pass
l3Canvas.display_bounding_box = display_bounding_box


def obj_to_world(obj, x1, y1, x2, y2):
    i2w = obj.get_property("parent").i2w
    u1, v1 = i2w(x1, y1)
    u2, v2 = i2w(x2, y2)
    return u1, v1, u2, v2


def obj_get_bounds_world(obj):
    if hasattr(obj, "get_bounds_world"):
        return obj.get_bounds_world()

    i2w = obj.get_property("parent").i2w
    x1, y1, x2, y2 = obj.get_bounds()
    u1, v1 = i2w(x1, y1)
    u2, v2 = i2w(x2, y2)
    return u1, v1, u2, v2


def bbox_wait(self, *objs):
    for ob in objs:
        self.display_bounding_box(obj_get_bounds_world(ob))
    gtk.main()
l3Canvas.bbox_wait = bbox_wait

    

#
#**        insertion   of new l3 node
def add_l3tree_clipboard(self, left, top):
    clipboard = gtk.Clipboard(selection='PRIMARY') # CLIPBOARD for windows?
    paste_single_prog = self.w_.fluid_ref(paste_single_prog = True)
    def receive_text(clipboard, text, data):
        if text in ["", None]:
            return False
        else:
            # Got text.
            
            # Parse.
            indent_workaround = False
            try:
                mat = re.match("[ \t]+", text)
                if mat:
                    # Try to work around code fragment with lead indentation.
                    progr = reader.parse("program:\n" + text)
                    indent_workaround = True
                else:
                    # Properly indented code.
                    progr = reader.parse(text)

            except SyntaxError, e:
                #   Error -> parse as string, display node for editing
                text = ('"""# ERROR: unable to parse:\n%s\n' 
                        'Original:\n%s"""' % (e, text))
                progr = reader.parse(text)

            except tokenize.TokenError:
                #   Error -> parse as string, display node for editing
                text = '"""# ERROR: unable to parse\n%s"""' % text
                progr = reader.parse(text)


            # Tree setup.
            if indent_workaround:
                progr = progr.single_program()
            progr.setup(ast.empty_parent(), self.w_.state_.def_env,
                        self.w_.state_.storage)[0].\
                        set_outl_edges(self.w_, None)
            
            # Comment association.
            associate_comments(self.w_, progr, text)
            
            # Display program / nodes.
            if paste_single_prog:
                # Display single program.
                l3pic = self.start_add_l3tree(progr)

                # Position.
                flush_events()
                pl, pt, pr, pb = l3pic.get_bounds_world()
                l3pic.move(left - pl, top - pt)

            else:
                # Display independent nodes.
                shift_y = 0
                for tree in [ee for ee in progr.entries()]: # copy for iteration
                    # Independent nodes
                    tree.detach_from_parent_rec(self.w_.state_.storage)

                    # Node.
                    l3pic = self.start_add_l3tree(tree)

                    # Position.
                    flush_events()
                    pl, pt, pr, pb = l3pic.get_bounds_world()
                    l3pic.move(left - pl, top + shift_y - pt)
                    shift_y += pb - pt + self.w_.cp_.marker_padding
    pass
    clipboard.request_text(receive_text, )
l3Canvas.add_l3tree_clipboard = add_l3tree_clipboard


#
#**        main insertion entry point
def start_add_l3tree(self, l3tree, rentab = None):
    #
    # add_l3tree and l3* constructors are mutually recursive;
    # start_add_l3tree is the starting point for insertion of an
    # l3tree w/o current display.
    #

    ## rendering speed up
    # This causes a short (or long...) flicker, but improves rendering
    # speed by a factor of 7. 
    if 0:
        sx1, sy1 = self.get_scroll_offsets()
        self.scroll_to(+1000, +1000)

    if rentab == None:
        rentab = self._vis_tab.get_render_table(self.w_.state_.storage, l3tree)
        rentab.set_rendering_defaults(l3tree)
    pic = self.add_l3tree(l3tree, rentab)

    ## rendering speed up
    if 0:
        self.scroll_to(sx1, sy1)

    # Position new tree in display.
    flush_events()
    if self.w_.fluid_ref(position_tree = True):
        ## lp, tp, _, _ = pic.get_bounds_world()
        lp, tp = 0, 0
        ls, ts, _, _ = self.get_scroll_region()
        loff, toff = self.get_scroll_offsets()
        loff /= self._pixpu
        toff /= self._pixpu
        pic.move(ls + loff + 10 - lp,
                 ts + toff + 10 - tp)
    # 
    return pic
l3Canvas.start_add_l3tree = start_add_l3tree


#
#**        main insertion dispatcher: add_l3tree
# Overview: Current display selection method
# From (occur "\\(if \\|else\\|#\\)" nil)
if 0:
    if rentab.is_visible(nid):
        if force_text:
            Text
        else:
            if rentab.is_text_preferred(nid):
                Text
            else:
                Structure # if possible
    else:
        Placeholder


def add_l3tree(self, l3tree, rentab, force_text = False):
    nid = l3tree._id

    ## rendering speed up
    #     if not l3tree._parent:
    #         sx1, sy1 = self.get_scroll_offsets()
    #         self.scroll_to(+1000, +1000)

    #     if not l3tree._parent:
    #         self._paper.raise_to_top()

    if rentab.is_visible(nid):
        ma = ast.Matcher()
        # Special TEXTUAL display cases. 
        #   Indexing with slices (a[1:2] etc).  First, the outer
        #   __getitem__ call.  From 
        #   print reader.parse('! NAME[! START: !STOP]')
        #   with modifications.
        if   ma.match(l3tree,
                      Call(Member(Symbol('operator'), Symbol('getitem')),
                           aList([Marker('NAME'), Marker('BODY')]))):
            l3pic = wdgt.l3Rawtext(
                self.w_, self, nid, rentab, 
                render_only = [ma['NAME'], ma['BODY']])

        # Special display for slice() operator, part of above indexing.
        #   print reader.parse('BODY[! FIRST: !LAST]')
        elif ma.match(l3tree,
                      Call(Symbol('slice'),
                           aList([Marker('FIRST'), Marker('LAST'),
                                  Symbol('None')]))):
            l3pic = wdgt.l3Rawtext(
                self.w_, self, nid, rentab, 
                render_only = [ma['FIRST'], ma['LAST']])

        # Special display for slice() operator, part of above indexing.
        #   print reader.parse('BODY[! FIRST: !LAST : !STEP]')
        elif ma.match(l3tree,
                      Call(Symbol('slice'),
                           aList([Marker('FIRST'), Marker('LAST'),
                                  Marker('STEP')]))):
            l3pic = wdgt.l3Rawtext(
                self.w_, self, nid, rentab, 
                render_only = [ma['FIRST'], ma['LAST'], ma['STEP']])

        elif force_text:
            # Text
            l3pic = wdgt.l3Rawtext(self.w_, self, nid, rentab)
        else:
            if rentab.is_text_preferred(nid):
                # Text
                l3pic = wdgt.l3Rawtext(self.w_, self, nid, rentab)
            else:
                # Structure if possible
                constructor = { ast.Function: wdgt.l3Function,
                                ast.If      : wdgt.l3if_chooser,
                                ast.Inline  : wdgt.l3Inline,
                                ast.Call    : wdgt.l3Call,
                                ast.Loop    : wdgt.l3Loop,

                                ast.Native  : wdgt.l3Native,
                                ast.Tuple   : wdgt.l3List,
                                ast.List    : wdgt.l3List,
                                ast.cls_viewList: wdgt.l3ViewList,
                                ast.Map     : wdgt.l3Map,
                                ast.Subdir  : wdgt.l3Map,
                                ast.Program : wdgt.l3Program,
                                ast.Set     : wdgt.l3Set,
                                }.get(l3tree.__class__)
                # Display as text if no structure view is available.
                if constructor == None:
                    l3pic = wdgt.l3Rawtext(self.w_, self, nid, rentab)
                else:
                    l3pic = constructor(self.w_, self, nid, rentab)

    else:
        # Placeholder
        l3pic = wdgt.Placeholder(self.w_, self, nid, rentab)

    return l3pic
l3Canvas.add_l3tree = add_l3tree

def register_l3tree_pic(self, nid, l3pic):
    # Node tracking.
    misc.unique_set(self._nodes, nid, l3pic)
    misc.unique_set(self._nodes, l3pic, nid)
    def _remove():
        ### print "removing ", l3pic
        del self._nodes[ nid ]
        del self._nodes[ l3pic ]
    ## print "adding hook for: ", l3pic
    l3pic.add_destroy_hook(_remove)

    ## rendering 
    #     if not l3tree._parent:
    #         self.scroll_to(sx1, sy1)

    ## self.scroll_to(sx1, sy1)
    #
    #     if not l3tree._parent:
    #         self._paper.lower_to_bottom()

    return l3pic
l3Canvas.register_l3tree_pic = register_l3tree_pic

#
#**        decoration creation dispatcher
def add_header_of(self, l3tree, rentab, ):
    # See also add_l3tree.

    # Retrieve header element for l3tree.
    comm = self.w_.deco_table_[l3tree._id]
    cid = comm._id

    if rentab.is_visible(cid):
        # Text
        l3pic = wdgt.l3Comment(self.w_, self, cid, rentab)
    else:
        # Reduced text.
        l3pic = wdgt.l3Comment(self.w_, self, cid, rentab,
                               summary_text = True)
    # Node tracking.
    # duplicated with l3Rawtext.__init__
    ## self.register_l3tree_pic(cid, l3pic)
    return l3pic
l3Canvas.add_header_of = add_header_of

#
#**        removal   of node from canvas
def remove_node(self, nid):
    #
    # Update display.
    #
    canv_item = self._nodes[ nid ]

    del self._nodes[ nid ]
    del self._nodes[ canv_item ]

    canv_item.destroy()
l3Canvas.remove_node = remove_node

#
#**        removal   of edge from canvas
def remove_edge(self, eid):
    #
    # Update display.
    #
    edge = self._edges[ eid ]

    del self._edges[ eid ]
    del self._edges[ edge ]

    edge.destroy()
l3Canvas.remove_edge = remove_edge

#
#**        removal of edge/node from graph
def remove_node_edge_all(self, canv_item):
    nid = self._nodes[ canv_item ]
    self.w_.selection.unselect()
    return True
l3Canvas.remove_node_edge_all = remove_node_edge_all

#
#**        event handler  for paper
def paper_copy_tree(self, event):
    if self.w_.selector.have_selection():
        node, nid = misc.node_copy(
            self.w_, 
            self.w_.selector.get_selection().l3tree())

        # Display
        it = self.start_add_l3tree(node)
        flush_events()
        ox1, oy1, ox2, oy2 = it.get_bounds_world()
        it.move(event.x - ox1, event.y - oy1)
        return False

    else:
        self.w_.ten_second_message(
            "No node selected; trying clipboard.")
        self.add_l3tree_clipboard(event.x, event.y)
        return False
l3Canvas.paper_copy_tree = paper_copy_tree
    


def paper_event(self, canv_item, event):
    # This does not receive keyboard events.
    if event.type == gtk.gdk.BUTTON_PRESS:
        but = event.button

        if (but == 2 or 
            (but == 1 and (event.state & gtk.gdk.SHIFT_MASK))):
            # Place a copy of the current selection at point.
            def body():
                return self.paper_copy_tree(event)

            if (but == 2) and (event.state & gtk.gdk.SHIFT_MASK):
                print "XX: nodes"
                self.w_.with_fluids(body,
                                    position_tree = False,
                                    paste_single_prog = False)
            else:
                # Default
                self.w_.with_fluids(body,
                                    position_tree = False,
                                    paste_single_prog = True)

        if event.button == 1:
            # Initial world position.
            self._off_x, self._off_y = self.get_scroll_offsets() # pixels
            self.remember_x = event.x_root
            self.remember_y = event.y_root
            self._moved = False
            return False

    elif event.type == gtk.gdk.MOTION_NOTIFY:
        if event.state & gtk.gdk.BUTTON1_MASK:
            #
            #   sm: move paper
            #
            dx = self.remember_x - event.x_root 
            dy = self.remember_y - event.y_root 

            # Move paper in the direction of mouse movement (hand).
            # pixels
            self.scroll_to( int(dx) + self._off_x,
                            int(dy) + self._off_y )

            # Update for next motion.
            self._moved = True
            return True

    elif event.type == gtk.gdk.BUTTON_RELEASE:
        if (event.button == 1 and
            not (event.state & gtk.gdk.SHIFT_MASK)):
            if not self._moved:
                # Clear current selection.
                self.w_.selector.unselect()
l3Canvas.paper_event = paper_event

#
#**        event handler  for canvas

def canvas_event(self, canv_item, event):
    # event coordinates here are in the window (pixel) system, not
    # world.
    if event.type == gtk.gdk.BUTTON_RELEASE:
        # sm: edge finish
        if event.state & gtk.gdk.BUTTON1_MASK: # needed?
            ### obsolete?
            # Edge drawing may be good for attributes.
            pass

    elif event.type == gtk.gdk.BUTTON_PRESS:
        if event.button == 3:
            #
            # pop-up menu of canvas actions
            #
            # return True
            pass

    elif event.type == gtk.gdk.KEY_PRESS:
            # Ctrl-C (copy)
            if event.state & gtk.gdk.CONTROL_MASK:
                if event.keyval == key_c:
                    self.w_.selector.copy_selection()
    return False
l3Canvas.canvas_event = canvas_event

#
#**        zoom adjustment
def add_zoom_text(self, text_wgt):
    self._zoom_set[text_wgt] = 1
l3Canvas.add_zoom_text = add_zoom_text

def remove_zoom_text(self, text_wgt):
    del self._zoom_set[text_wgt]
l3Canvas.remove_zoom_text = remove_zoom_text


def zoom_text(self, wdgt, factor):
    # wdgt interface:
    #     _ltext
    #     _ltext_size_1
    if wdgt._ltext:                     # zooming while editing?
        wdgt._ltext.set_property("size",
                                 self.w_.cp_.font_size * factor * pango.SCALE)
        u1, v1, u2, v2 = wdgt._ltext.get_bounds()
        ox, oy = wdgt._ltext_size_1
        # Avoid ZeroDivisionError for empty strings.
        if ox == 0: ox = 1
        if oy == 0: oy = 1
        ppu = self._pixpu
        ratio_h = 1.0 * (u2 - u1) * ppu / ox
        ratio_v = 1.0 * (v2 - v1) * ppu / oy
        return (ratio_h, ratio_v)
    else:
        return factor, factor
l3Canvas.zoom_text = zoom_text

def centered_zoom(self, factor):
    self.set_center_scroll_region(True)

    # Font scaling is not uniform in x and y; here, adjust font sizes and find 
    # most significant scale factors.
    # 
    msf_h, msf_v = factor, factor
    for ii in self._nodes.iterkeys():
        if isinstance(ii, wdgt.l3Base):
            fh, fv = ii.zoom(factor)
            msf_h = max(msf_h, fh)
            msf_v = max(msf_v, fv)

    for ii in self._zoom_set.iterkeys():
        fh, fv = self.zoom_text(ii, factor)
        msf_h = max(msf_h, fh)
        msf_v = max(msf_v, fv)

    # Set the new scale.
    #
    ff = max(msf_h, msf_v)
    self._font_size_tag.set_property("size-points", self.cp_.font_size * ff)

    # Ensure integer pixel / unit ratio for exact character alignment. 
    zoom = int(ff * self._pixpu_base)
    ff = zoom / self._pixpu_base

    # #     print "zoom: ", zoom
    # #     print "ff: ", ff
    # #     print "_pixpu_base: ", self._pixpu_base

    self.set_pixels_per_unit(zoom)
    self._pixpu = zoom
    self._abs_zoom = ff
    # propagate value back to display?
l3Canvas.centered_zoom = centered_zoom


# Needed before set_center_scroll_region was wrapped; also useful for
# rectangle-based zooming:
def anchored_centered_zoom(self, anchor_x, anchor_y, factor):
    # Zooming is done around the anchor, i.e.,  the anchor point in
    # the middle.

    zoom = factor * self._pixpu_base
    #
    # Details for shifting viewport from
    # gnome_canvas_set_pixels_per_unit()
    #
    #     Find the coordinates of the anchor point in units.
    #
    sx1, sy1, sx2, sy2 = self.get_scroll_region()
    #
    #     In world_to_window(),
    #         WINY = (pixels_per_unit)*(WORLDY - scroll_y1) + zoom_yofs;
    #     so zoom_yofs is obtained via:
    zoom_xofs, zoom_yofs = self.world_to_window(sx1, sy1)
    ax = ( self.get_hadjustment().value + anchor_x ) / self._pixpu + \
         sx1 + zoom_xofs
    ay = ( self.get_vadjustment().value + anchor_y) / self._pixpu + \
         sy1 + zoom_yofs

    # Calculate the new offset of the upper left corner.
    x1 = ((ax - sx1) * zoom) - anchor_x;
    y1 = ((ay - sy1) * zoom) - anchor_y;

    # Set the new scale
    self.set_pixels_per_unit(zoom)
    self._pixpu = zoom

    # Adjust the viewport
    self.scroll_to(int(x1), int(y1))
l3Canvas.anchored_centered_zoom = anchored_centered_zoom

# 
#**        parent traversal
def displayed_parents(self, l3tree):
    # yield all displayed parents (of type l3Base (widget)) of l3tree
    # (of type astType)  
    tw = ast.TreeWork(self.w_.state_.storage)
    for par in tw.all_parents(l3tree):
        l3pic = self._nodes.get(par._id)
        if (l3pic is not None):
            yield l3pic
l3Canvas.displayed_parents = displayed_parents    

# 
#**        font info
def font_ascent_units(self):
    # Ignore zoom factor.
    pcon = self.get_pango_context()
    font = pcon.load_font(pcon.get_font_description())
    metrics = font.get_metrics()
    return (metrics.get_ascent() / pango.SCALE) / self._pixpu_base
l3Canvas.font_ascent_units = font_ascent_units

def font_wh(self):
    # Return the character (width, height, h / w) for the current font.
    pcon = self.get_pango_context()
    font = pcon.load_font(self._font_desc)
    metrics = font.get_metrics()
    #    Font sizes are in pango_units.
    #    1 point == PANGO_SCALE  pango_units
    #    1 pango_point = 1/72 inch
    fheight = (metrics.get_ascent() +  metrics.get_descent())
    fwidth = metrics.get_approximate_digit_width()
    return fwidth, fheight, 1.0 * fheight / fwidth
l3Canvas.font_wh = font_wh    

def bbox_offset_units(self):
    return 1.0 / self._pixpu_base
l3Canvas.bbox_offset_units = bbox_offset_units


#*    VisibilityTable
#
#  visibility flags:
#  
#     vis_neutral   Default. is weakly visible (if parent is not hidden).
#     vis_show      is always visible.
#     vis_hide      is hidden
# 
#     Basic operations:
#         Flag individual nodes (vis_show/hide/nothing).
#         Use get_render_table() to get per-node visibility.
#         Call add_l3tree to render.
#
#
#     Visibility is applied to the children by the parent node, in
#     add_l3tree.
#
#     The node's visibility has precedence over its parent's.
#
#     For simple pickling and independence, no reference to storage is
#     kept.
#
class VisibilityTable(dict):
    def __init__(self):
        #
        # self is a
        #     id -> visibility flag
        # table.
        # Absent id's are treated as neutral, so an empty
        # table results in full visibility.
        #
        pass

def replace_table(self, dict):
    self.clear()
    self.update(dict)
VisibilityTable.replace_table = replace_table

def get_render_table(self, storage, l3tree):
    # The actual per-node visibility resulting from the vis_* flags.
    _node_vis = RenderTable()             # id -> bool map

    #     Top-down sweep to get initial visibility.
    # Invisibility (vis_hide) only propagates down a tree.
    #
    def _node(tree, parent_stat = True):
        nid = tree._id
        status = self.get(nid)

        if status == "vis_hide":
            _node_vis[nid] = False
            tree.visit_children(_node, parent_stat = False)

        elif status == "vis_show":
            _node_vis[nid] = True
            tree.visit_children(_node, parent_stat = True)

        elif status in ["vis_neutral", None]:
            _node_vis[nid] = parent_stat
            tree.visit_children(_node, parent_stat = parent_stat)

        else:
            raise DisplayError("Internal error: "
                               "Invalid visibility type: " + status)
    _node(l3tree)

    return _node_vis
VisibilityTable.get_render_table = get_render_table

def clear_tree(vista, tree):
    # Clear existing markings for this tree.
    for nd, dent in tree.top_down_indented():
        if vista.has_key(nd._id):
            del vista[nd._id]
VisibilityTable.clear_tree = clear_tree

#*    RenderTable
class RenderTable(dict):
    '''
    1. Table of the actual per-node visibility resulting from
       the vis_* flags.
    2. Table of text vs. structure view for a given node.

    For 1., the table is a simple (id -> bool) map.
        2., ( 'id', 'textual') -> bool 

    '''
    pass

def is_visible(self, id):
    return self.get(id, True)
    # #     try:
    # #         return self[ id ]
    # #     except KeyError:
    # #         warn_("no visibility information for %d" % id)
    # #         return True
RenderTable.is_visible = is_visible

def is_text_preferred(self, id):
    return self.get( (id, 'textual'), False)
RenderTable.is_text_preferred = is_text_preferred

def mark_textual(self, tree):
    self[ (tree._id, 'textual') ] = True
RenderTable.mark_textual = mark_textual

def mark_visible(self, tree):
    # For macro rendering.
    for nd in tree.top_down():
        self[nd._id] = True
    return self
RenderTable.mark_visible = mark_visible

def set_rendering_defaults(self, tree):
    """
    Mark logical expressions and single-line expressions for textual
    display. 
    """
    ### compact expression display
    ma = ast.Matcher()
    for nd in tree.top_down():
        # Mark infix math expressions like 'a-b'
        if ma.match(nd, Call(Marker('name'), Marker('args'))):
            if ma['name'] in ['neg', 'and', 'or', 'not', '&', '|', ':', '==',
                              '!', '!!', '!=', '>', '>=', '<', '<=', '-',
                              '%', '+', '/', '//', '**', '*', ]:
                self.mark_textual(nd)
                ### stop traversal of subtrees here?
        # Always use long form for:
        if isinstance(nd, ast.Program):
            continue
        # Mark single-line expressions
        if nd.has_source_string():
            if nd.num_lines() == 1:
                self.mark_textual(nd)
    ### long display
    return 1
    ### text display
    self.mark_textual(tree)
RenderTable.set_rendering_defaults = set_rendering_defaults

def get_state(self, tree):
    ''' Return the state info for the `tree` (tree : astType) entry.'''
    # __getstate__ would be for the whole table.
    return [self.get( (tree._id, 'textual'), False), # text?
            self.get(  tree._id, True)]                # visible?
RenderTable.get_state = get_state

def set_state(self, tree, state):
    ''' Set the state info for `tree`.'''
    self[ (tree._id, 'textual')] = state[0]
    self[ tree._id ]             = state[1]
RenderTable.set_state = set_state

#*    subtree swapping
def replace_visible(self, child):
    # Collect info.
    ox1, oy1, ox2, oy2 = child.get_bounds_world()
    l3tree = child._l3tree

    # Remove existing display.
    child.destroy()

    # Draw (visible) subtree.
    tree_disp = self.start_add_l3tree(l3tree)

    # Attach it.
    flush_events()
    nx1, ny1, nx2, ny2 = tree_disp.get_bounds_world()
    tree_disp.move(ox1 - nx1, oy1 - ny1)

    # Resize parent.
    pass
l3Canvas.replace_visible = replace_visible

#*    saving
def root_nodes(self):
    roots = {}                          # id -> node
    for key, l3pic in self._nodes.iteritems():
        # Brute-force search, inefficient for large forests...
        if isinstance(key, (IntType, LongType)):
            rt, rid = l3pic.find_root()
            roots[ rid ] = rt
    return roots
l3Canvas.root_nodes = root_nodes

def get_state(self):
    #
    # Simple values.
    #
    simple                 = utils.Shared(
        _paper             = wdgt.st_CanvasRect(self.w_, self._paper, []),
        _vis_tab           = self._vis_tab,
        )

    #
    # self._nodes
    #
    # Starting pickling in a subtree and (randomly) getting the
    # parents is bad for pickling and reconstruction.
    #
    # To get a clean tree save/load cycle, only topmost nodes of
    # visible trees are used as starting points.
    # 
    # Table of canvas root items.
    roots = self.root_nodes()           # id -> node

    root_tbl = {}                       # id -> state
    for rtid, rt in roots.iteritems():
        # Tables for tree state coupled via ids.
        tables = wdgt.start_set_tables(rt)
        # Graphical node state.
        root_tbl[ rtid ] = (rt.get_state(), tables)

    # Nothing to save:
    #     _common_tag_table

    return utils.Shared(roots = root_tbl, simple = simple)
l3Canvas.get_state = get_state


#*    loading
def clear_state(self):
    #
    # Clear the canvas.
    #
    roots = self.root_nodes()           # id -> node

    for _, node in roots.iteritems():
        # Hide the tree.
        node._root_group.hide()

        # Delete in steps.
        def body():
            node.destroy()
            node._l3tree.delete(self.w_.state_.storage)

        self.w_.with_fluids(body,
                       detach_l3_tree = False,
                       insert_l3_tree = False,
                       )

    self._nodes = {}
    self._edges = {}
l3Canvas.clear_state = clear_state


def set_state( self, shared ):
    root_tbl = shared.roots
    simple = shared.simple

    #
    # Assemble simple values
    #
    _set_props(self._paper, simple._paper)
    self._vis_tab           = simple._vis_tab

    #
    # Assemble nodes.
    #
    for nid, (state, tables) in root_tbl.iteritems():
        #
        #     Set up self._nodes and the canvas items.
        #
        l3tree = self.w_.state_.storage.load(nid)
        l3tree.set_outl_edges(self.w_, None)
        tree = self.start_add_l3tree(l3tree, rentab = tables.rentab)
        #
        #     Update item properties.
        #
        tree.set_state( state )
    pass
l3Canvas.set_state = set_state

