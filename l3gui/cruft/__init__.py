#
# Author:  Michael H. Hohn (mhhohn@lbl.gov)
#
# Copyright (c) 2006, The Regents of the University of California
# 
# See legal.txt and license.txt
#

#
__version__ = "$Revision: 320 $"[11:-2]
__author__  = "$Author: hohn $"[9:-2]
    
