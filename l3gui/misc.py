#
# Author:  Michael H. Hohn (mhhohn@lbl.gov)
#
# Copyright (c) 2006, The Regents of the University of California
# 
# See legal.txt and license.txt
#

"""
Miscellaneous functions and classes.

"""
import exceptions, thread, sys, os
from copy import copy, deepcopy
import gobject, gtk
try:
    import gnomecanvas as canvas
    from gnomecanvas import MOVETO_OPEN, MOVETO, LINETO, CURVETO
except:
    # Try backported version used in sparx.
    import canvas
    from canvas import MOVETO_OPEN, MOVETO, LINETO, CURVETO
from math import *

from l3lang import utils, ast, interp, view
import l3lang.globals as G

#*    event separation via thread locks
# 
# wrapping whole event handlers, as in
#   self._root_group.connect("event",
#       lambda *a: misc.single_event(self.drag_event, *a))
# really throws off the handling.
# 
# This single-event restriction must be used locally to block specific
# other events when a significant (event-affecting) state modification
# is about to happen.
# 
# See l3Nested.drag_event for an example.
# 
_the_lck = thread.allocate_lock()
def single_event(func, *args):
    if _the_lck.acquire(0):
        try:
            func(*args)
        finally:
            _the_lck.release()
    else:
        pass


#*    data / file content -> l3 conversion
#**        file type dispatcher
def file2ast(fname, shape = 'list'):
    ''' Produce a raw astType for the content of file `fname`.

    The `shape` argument is 'list' or 'square'; the 'list' form will
    not change the "natural" shape of the data whereas 'square'
    adjusts the shape for better space utilization.

    The returned astType is not .setup().

    For unrecognized file types, `fname` is returned.
    '''
    assert isinstance(fname, ast.String), "Expecting file name."
    
    # File exists?
    if not os.path.exists(fname):
        return fname

    # File type check by extension.
    (root, ext) = os.path.splitext(fname)

    _identity = lambda aa: aa
    return {
        '.hdf' : _file2ast_hdf,
        '.png' : png2pixbuf,            # todo: handle files
        '' : _identity,
        }[ext](fname, shape = shape)
# 
#**        hdf file handler
def _file2ast_hdf(fname, shape = 'list'):
    '''Convert an hdf5 file containing a list of images.

    The contents are converted to numpy arrays and a new l3 list
    returned; editing the new list will not affect the original.  
    '''
    import l3lang.external.hdf_mixins as hm
    if not hm.l3_have_pytables:
        G.logger.message("pytables is not available.  Unable to load hdf file.")
        return fname

    import tables
    fi = tables.openFile(fname)
    num_img = fi.l3_len()
    # 
    # For the file to be closed here, the l3_getimg() arrays must be
    # copied as numpy arrays.
    # 
    vals = [ (ii, fi.l3_getimg(ii)[:,:]) for ii in range(0, num_img) ]

    # Produce list of lists for viewing, if requested.
    M = len(vals)
    if (M > 3) and shape == 'square':
        cols = int(floor(sqrt(M)))
        vals = [tuple(vals[ii : ii + cols]) for ii in range(0, M, cols)]

    # Form the ast
    rv = ast.val2ast(vals)

    fi.close()
    return rv

    
# 
#**        array to image conversions
#***            numpy array to pixbuf
def arr2pix(num_arr):
    ''' Convert a 2D float array to a greyscale pixmap. '''
    import numpy as N
    assert len(num_arr.shape) == 2, "Expected 2D array."

    # Scale float array to 8-bits.
    rows, cols = num_arr.shape
    bottom = N.min(num_arr)
    top = N.max(num_arr)
    scale = (top - bottom)
    norm_arr = ((num_arr - bottom) / scale * 255).astype(N.uint8)\
               .reshape(rows*cols)

    # Use 3 8-bit arrays as (rgb) values.
    t3 = N.outer(norm_arr, N.array([1,1,1], 'b'))
    t4 = N.reshape(t3, (rows, cols, 3))
    pix_arr = N.array(t4, dtype=N.uint8) # 

    # Get the pixbuf.
    #   array input indexing is row, pixel, [r,g,b {,a} ]
    pixbuf = gtk.gdk.pixbuf_new_from_data(
        pix_arr.tostring(order='a'),
        gtk.gdk.COLORSPACE_RGB,
        False,
        8,
        cols,
        rows,
        3*cols)

    return pixbuf

# 
#***            emdata conversions
def emdata2pixbuf(emd):
    from EMAN2 import EMNumPy, EMData
    import numpy as N
    import gtk

    assert isinstance(emd, EMData), "Expected EMdata instance"
    # eman -> numeric -> gtk
    num_arr = EMNumPy.em2numpy(emd)
    return arr2pix(num_arr)
# 
#***            png conversion
def png2pixbuf(png_file, shape = 'list'):
    import matplotlib.image as mi
    import numpy as N
    if isinstance(png_file, ast.String):
        png_file = png_file.py_string()
    img_arr = mi.imread(png_file)
    rows, cols, bytes = img_arr.shape

    # Scale from [0,1] to [0,255]
    img_arr = N.array(255 * img_arr, dtype=N.uint8)

    # Get the pixbuf.
    #   array input indexing is row, pixel, [r,g,b {,a} ]
    pixbuf = gtk.gdk.pixbuf_new_from_data(
        img_arr.tostring(order='a'),
        gtk.gdk.COLORSPACE_RGB,
        True,
        8,
        cols,
        rows,
        bytes * cols)

    return pixbuf
## misc.png2pixbuf = png2pixbuf
    # # # Convert.
    # # pixbuf = gtk.gdk.Pixbuf(gtk.gdk.COLORSPACE_RGB,
    # #                         has_alpha = True,
    # #                         bits_per_sample = 8,
    # #                         width = cols,
    # #                         height = rows)

    # # array = pixbuf_get_pixels_array(pixbuf)
    # # array[:,:,:] = image_array
    

#*    utility
def escape_markup(text):
    '''Change html-like markup in `text` so that GnomeCanvasText ignores it.
    See http://developer.gnome.org/doc/API/2.0/pango/PangoMarkupFormat.html 
    '''
    return text.replace("<", "&lt;")
    

def triple_quote(text):
    return '"""%s"""' % text

class DisplayError(exceptions.Exception):
    def __init__(self, e_args=None):
        self.e_args = e_args
    def __str__(self):
        return repr(self.e_args)


def flush_events():
    # Update bounding boxes, among others.
    while gtk.events_pending():
        gtk.main_iteration()

def python_main_loop():
    try:
        while 1:
            gtk.main_iteration_do()
    except:
        pdb.pm()

def destroy_if_last(obj):
    if 0:
        rc = sys.getrefcount(obj)
        print "refcount: ", rc
        print "wiping ", obj
        # Ignore local obj, getrefcount arg
        if rc > 5:
             pdb.set_trace()
    obj.destroy()

def print_(strng):
    print strng
    sys.stdout.flush()

def warn_(strng):
    print >> sys.stderr, "Warning: ", strng
    sys.stderr.flush()

def slot_available(w_, old_entry):
    if w_.fluid_ref(insert_l3_tree = True):
        if not isinstance(old_entry, ast.aNone):
            raise DisplayError("Insertion in occupied slot.")


def canvas_item_get_bounds_world(item):
    i2w = item.get_property("parent").i2w
    x1, y1, x2, y2 = item.get_bounds()
    u1, v1 = i2w(x1, y1)
    u2, v2 = i2w(x2, y2)
    return u1, v1, u2, v2


#*    comment -> node connection
def associate_comments(w_, tree, src_string):
    from l3lang import reader, interp, ast
    tm_play_env = interp.get_child_env(w_.state_.def_env,
                                       w_.state_.storage)
    for nd, comment in reader.parse_comments(tree, src_string):
        ctree = w_.deco_table_[nd._id] = ast.Comment(comment)
        ctree.setup(ast.empty_parent(),
                    tm_play_env,
                    w_.state_.storage)



#*    affine rotation
def affine_rotate(theta):
    # Return rotation tuple for theta degrees. 
    s = sin (theta * pi / 180.0)
    c = cos (theta * pi / 180.0)
    return (c, s, -s, c, 0, 0)

#*    rounded corner rectangle
# Rectangle w/ rounded corners, leaving the bezier control points on
# the tangent lines.
# Note: using one definition, say on [0,1]x[0,1] and scaling will also
# scale the rounded edges, but these must have absolute size. [ie. the
# rounding size must be independent of the rectangle size.]
def path_rectangle_rounded(el, er,
                           et, eb,      # edge positions
                           cv, ch,      # corner point indentation
                           sv, sh,      # spline control point indentation
                           ):
    assert (el < er)
    assert (et < eb)

    # Suppress rounded corner for extreme cases.
    if not ((er - el) > 2*ch):
        ch = (er - el) / 3.0

    if not ((eb - et) > 2*cv):
        cv = (eb - et) / 3.0

    path = [(MOVETO, el, et + cv),

            (LINETO, el, eb - cv),
            (CURVETO ,                      # bottom left
             el      , eb - sv,
             el + sh , eb,
             el + ch , eb,
             ),

            (LINETO, er - ch, eb),          # bottom right
            (CURVETO  ,
             er -  sh , eb,
             er       , eb - sv,
             er       , eb - cv,
             ),

            (LINETO, er, et + cv),
            (CURVETO ,                      # top right
             er      , et + sv ,
             er - sh , et,
             er - ch , et,
             )       ,

            (LINETO, el + ch, et),
            (CURVETO ,                      # top left
             el + sh , et,
             el      , et + sv,
             el      , et + cv,
             ),
            ]
    return path

#*    file selection popup
def file_selection(call_w_selection):
    #
    # Let the user select a filename NAME, and call
    # call_w_selection(NAME)
    #
    fs = gtk.FileSelection("Select file")

    def wrap(widget):
        fname = fs.get_filename()
        print ("Selected filename: %s\n" % fname)
        call_w_selection(fname)

    fs.ok_button.connect( "clicked", wrap)
    fs.ok_button.connect_after( "clicked",
                                lambda *a: fs.destroy())
    fs.cancel_button.connect_after( "clicked",
                                    lambda *a: fs.destroy())
    fs.show_all()


#

#*    input validation
def validate_input(text):
    # Check the input text; use interactive widget if fixing is
    # required.
    ###
    pass

#*    selection handling
#**        Selectable interface
class Selectable:
    def __init__(self):
        raise Exception("Interface only.")

    def plain_view(self):
        raise DisplayError("interface only")

    def highlight(self):
        raise DisplayError("interface only")

    def l3tree(self):
        raise DisplayError("interface only")


# 
#**        selector
class Selector:
    """ Item selection handler.

    This class handles the selection data, holding zero or more
    selected L3 display types.

    Typically, Button-1 selects one item, clearing the existing selection.
    Ctrl-Button-1 selects one item, adding it to the existing
    selection.
    """

    def __init__(self, w_):
        self._current_node_l = []       # Selectable list
        self.w_ = w_

def have_selection(self):
    return len(self._current_node_l) > 0
Selector.have_selection = have_selection

def unselect(self):
    if self.have_selection():
        [foo.plain_view() for foo in self._current_node_l]
        self._current_node_l = []
Selector.unselect = unselect

# 
#***            Single-selection interface
def set_selection(self, item):
    ''' Clear existing selection and add item. '''
    assert isinstance(item, Selectable)
    # Adjust existing node.
    if self.have_selection():
        [foo.plain_view() for foo in self._current_node_l]

    # Set up new selection.
    self._current_node_l = [item]
    item.highlight()

    # Highlight associated tree also.
    source_id_l = item._l3tree.getthe('value_source_tree_id')
    if source_id_l:
        for source_id in source_id_l:
            linked_pic = utils.key_of(source_id, item._canvas._nodes)
            if linked_pic:
                self.add_to_selection(linked_pic)
Selector.set_selection = set_selection

def get_selection(self):
    if self.have_selection():
        return self._current_node_l[-1]
    else:
        return None
Selector.get_selection = get_selection

# 
#***            Multi-selection interface
def add_to_selection(self, item):
    """ Add item to selection if it is not there. """
    assert isinstance(item, Selectable)
    if self.have_selection():
        if item not in self._current_node_l:
            self._current_node_l.append(item)
            item.highlight()
    else:
        self._current_node_l = [item]
        item.highlight()
Selector.add_to_selection = add_to_selection

def remove(self, item):
    """ Remove item from selection. """
    assert isinstance(item, Selectable)
    if self.have_selection():
        if item in self._current_node_l:
            self._current_node_l.remove(item)
            item.plain_view()
Selector.remove = remove

def toggle_selection(self, item):
    """
    Add item to selection if not there; remove it if present already.
    """
    assert isinstance(item, Selectable)
    if self.have_selection():
        if item not in self._current_node_l:
            self._current_node_l.append(item)
            item.highlight()
        else:
            self._current_node_l.remove(item)
            item.plain_view()
    else:
        self._current_node_l = [item]
        item.highlight()
Selector.toggle_selection = toggle_selection
    
def get_selection_list(self):
    '''
    '''
    return copy(self._current_node_l)
Selector.get_selection_list = get_selection_list

#
#**        copy selection to clipboard

# Avoid
#   GtkClipboard prematurely finalized
# problems
gtk_primary_clipboard = gtk.Clipboard(selection='PRIMARY')
gtk_clipboard_clipboard = gtk.Clipboard(selection='CLIPBOARD')

def copy_selection(self):
    from l3lang import reader, interp, ast
    if not self.have_selection():
        return 
    s_tree = self.get_selection().l3tree()

    # Get the pretty-printed string and verify the tree.
    comm_dct = self.w_.deco_table_

    pp_string = s_tree.get_infix_string(self.w_.pprint_width,
                                        comm_dct) 
    pp_tree = reader.parse(pp_string)   ## .single_program()
    pp_env = interp.get_child_env(self.w_.state_.def_env,
                                  self.w_.state_.storage)
    pp_tree.setup(ast.empty_parent(), pp_env, self.w_.state_.storage)
    associate_comments(self.w_, pp_tree, pp_string)
    diff = s_tree.tree_difference(comm_dct, pp_tree.body(), comm_dct)
    if diff == '':
        gtk_primary_clipboard.set_text(pp_string)
        gtk_clipboard_clipboard.set_text(pp_string)
    else:
        G.logger.warn("Unable to copy tree:\n%s\n" % diff)
Selector.copy_selection = copy_selection


#*    Property getting / setting
def _pickle_these(*args):               ### test only
    for data in args:
        utils.file_pickle( data )

def _get_props(item, prop_list, *properties):
    # Return a (property, value) list
    # When property is of the form "*-set", it is queried and only if
    # true is the following property used.
    # See also _set_props()

    skip = 0
    for pp in properties:
        if skip:
            skip -= 1
            continue

        if len(pp) > 4 and pp[-4:None] == "-set":
            if not item.get_property(pp):
                skip += 1
                continue
        try:
            prop = item.get_property(pp)
        except:
            print "_get_props Failed for item ", item,
            print ", property: ", pp
            continue

        # Retrieve non-pickling properties as text.
        # [ first needed for .set_property("anchor", 'GTK_ANCHOR_NORTH_WEST')]
        if pp in ["anchor"]:
            prop = prop.value_name
            
        prop_list.append( (pp, prop) )
    return prop_list

def _set_props(item, prop_list):
    # Update the ITEM's properties.
    #
    # prop_list:  (property, value) list
    #
    # See also _get_props().
    #
    for (prop, value) in prop_list:
        try:
            item.set_property(prop, value)
        except Exception, foo:
            print foo,  prop, value

#*    unique dictionary insertion
def unique_set(dic, key, val):
    if dic.has_key(key):
        raise KeyError("Key `%s` exists in dictionary." % key)
    dic[key] = val
    return dic
## misc.unique_set = unique_set


#*    safe deletion
def _safed(obj):
    if obj is None: return
    obj.destroy()

#*    gtk object proxy 
# Only dereference the gtk object while it is alive.
class GtkObjProxy:
    def __init__(self, obj):
        self._obj = obj
        self._live = True
        obj.connect("destroy", self._destroyed)

    def __getattr__(self, nm):
        if self._live:
            return self._obj.__getattribute__(nm)
        else:
            raise Exception("Accessing dead gtk object.")

    def _destroyed(self, widget):
        self._live = False


#*    main I/O entry points.
#
#**        load script
def load_script(w_, filename):
    from l3lang import reader, interp, ast
    # Tree setup.
    text = "".join(open(filename, "r").readlines())
    tm_tree = reader.parse(text)
    tm_play_env = interp.get_child_env(w_.state_.def_env,
                                       w_.state_.storage)
    tm_tree.setup(ast.empty_parent(), tm_play_env, w_.state_.storage)
    tm_tree.set_outl_edges(w_, None)
    
    # Comment association.
    associate_comments(w_, tm_tree, text)

    # Node.
    tm_tree_l3pic = w_.canvas.view.start_add_l3tree(tm_tree)
    return None
# 
#**        load library
def load_library(w_, filename, maxdepth = 1):
    ''' Load library entries from file, display without Program.'''
    # See also add_l3tree_clipboard
    from l3lang import reader, interp, ast

    # Tree setup.
    text = "".join(open(filename, "r").readlines())
    tm_tree = reader.parse(text)
    tm_play_env = interp.get_child_env(w_.state_.def_env,
                                       w_.state_.storage)
    tm_tree.setup(ast.empty_parent(), tm_play_env, w_.state_.storage)
    tm_tree.set_outl_edges(w_, None)
    
    # Comment association.
    associate_comments(w_, tm_tree, text)

    # Display independent nodes.
    shift_y = 0
    for tree in [ee for ee in tm_tree.entries()]: # copy for iteration
        # Independent nodes
        tree.detach_from_parent_rec(w_.state_.storage)

        # Limit tree display depth.
        if isinstance(tree, (ast.cls_viewList, ast.Program)):
            tree.outl_flat_display(maxdepth)

        # Node.
        l3pic = w_.lib_canvas.view.start_add_l3tree(tree)

        # Position.
        flush_events()
        pl, pt, pr, pb = l3pic.get_bounds_world()
        l3pic.move(0.0, shift_y)
        shift_y += pb - pt + w_.cp_.marker_padding

    return None

#
#**        save
def save_state(w_, filename):
    #  Faster.
    return utils.file_cPickle( get_current_state(w_), filename = filename)
    # Better debug info.
    # # return utils.file_pickle( get_current_state(w_), filename = filename)

def get_current_state(w_):
    return utils.Shared(
        ### selection saving must be via REFERENCE -- maybe use _id
        ### tags later.
        ### selection       = w_.selector.get_selection().get_state(),

        # Data.
        cp_             = w_.cp_,
        state_          = w_.state_,
        deco_table_     = w_.deco_table_,

        # Instances.
        canvas_view     = w_.canvas.view.get_state(),
        lib_canvas_view = w_.lib_canvas.view.get_state(),
        )

#
#**        load
def load_state(w_, filename):
    data = utils.file_unpickle(filename)
    state_ = data.state_

    #
    # Clear existing canvas content first.
    #
    w_.canvas.view.clear_state( )
    w_.lib_canvas.view.clear_state( )

    # Restore previous data structures.
    w_.cp_.pull(data.cp_)
    w_.state_.pull(state_)
    w_.deco_table_ = data.deco_table_

    # Restore non-persistent environment.
    interp.setup_envs(state_.storage, state_.root_env, state_.def_env)

    # And rebuild the canvas' display and data.
    w_.canvas.view.set_state      ( data.canvas_view )
    w_.lib_canvas.view.set_state  ( data.lib_canvas_view )

    ### selection saving...
    # if data.selection != None:
    #     selection       = w_.selector.get_selection().get_state(),
    #     w_.selector.set_selection (data.selection)

    # Ugly hack to force update
    for canv in [w_.canvas.view, w_.lib_canvas.view]:
        canv._pixpu = (1.00001 * canv._pixpu)
        canv.set_pixels_per_unit(canv._pixpu)
    # Restore missing graph edges.
    #     w_
    #
    # add_l3tree handles
    #     _canvas
    #     _parent
    #     _marker
    #     _container
    #
    # Ignored for now:
    #     _tag_state
    #


#
#**        print PS
def print_display(w_):
    # Produce a postscript dump of the text of both canvases on stdout.
    #
    # A full "screenshot" including all graphics requires more
    # adjustments.  In particular:
    #   --  for 'show', the PS position is (left, baseline);
    #       the gc (gnome canvas) position is (center, center)
    #   --  stroke width has to be included
    #   --  coloring

    print """
% Right Canvas State
% Change orientation, (0,0) in upper left
0 11 72 mul translate

1 4 div dup scale                       % adjust to fit page

/fscale 10 def
/Courier findfont fscale 1.7 mul  scalefont setfont 


% Change orientation, y increases down.
% Scale so 1 unit == font height + depth
/fntunit                                % x y : x y 
{
    fscale mul exch
    fscale mul exch
} def

/gcmoveto                               % x y : -- 
{ fntunit -1 mul moveto } def

/gclineto                               % x y : -- 
{ fntunit -1 mul lineto } def

/gcshow                                 % string : --
{   % move point from (left, baseline) to (left, top)
    0 fscale 1 mul neg rmoveto
    show
} def
"""
    # left canvas: w_.lib_canvas.view.get_state(),

    w_.with_fluids(lambda : w_.canvas.view.get_state(),
                   dump_ps = True)


#*    stdin/out/error handling
class StdInOutErr:
    pass

def __init__(self, w_):
    self.w_ = w_
    self._stack = []
    self._sys = (sys.stdin, sys.stdout, sys.stderr)
StdInOutErr.__init__ = __init__


def std_connection(self):
    # Reset to defaults.
    (sys.stdin, sys.stdout, sys.stderr) = self._sys
StdInOutErr.std_connection = std_connection


def push(self):
    # Store stdin/out/err.  Used by other instances before they grab
    # stdin/out/err.  
    self._stack.append( (sys.stdin, sys.stdout, sys.stderr) )
StdInOutErr.push = push


def pop(self):
    # Restore stdin/out/err.  Used by other instances to release
    # stdin/out/err. 
    (sys.stdin, sys.stdout, sys.stderr) = self._stack.pop()
StdInOutErr.pop = pop

#*    application-level glue functions / classes
#**        Console visibility
def show_hide_console(w_):
    bot = w_.vpane.get_property("max-position")
    prop = w_.consoles.proportion_y
    if bot == w_.vpane.get_property("position"):
        # make it visible
        top = w_.vpane.get_property("min-position")
        w_.vpane.set_property("position", prop * top + (1 - prop) * bot)
    else:
        # hide it.
        w_.vpane.set_property("position", bot)

def show_program_only(w_):
    # Hide vertical pane.
    w_.vpane.set_property("position",
                          w_.vpane.get_property("max-position"))
    # Hide library canvas.
    w_.hpane.set_property("position",
                          w_.hpane.get_property("min-position"))


def show_consoles(w_):
    top = w_.vpane.get_property("min-position")
    bot = w_.vpane.get_property("max-position")
    pos = w_.vpane.get_property("position")
    prop = w_.consoles.proportion_y
    min_pos = prop * top + (1 - prop) * bot
    if pos > min_pos:
        # enlarge visible area
        w_.vpane.set_property("position", min_pos)
# 
#**        Custom notebook for consoles
class ConsoleNtbk(gtk.Notebook):
    pass

def __init__(self, w_):
    gtk.Notebook.__init__(self)
    self.w_ = w_
    self.set_tab_pos(gtk.POS_LEFT)
    self._entries = {}                  # (name -> widget) dict
ConsoleNtbk.__init__ = __init__  

def add_page(self, widget, name):
    # Add and track `widget` under label `name`.
    assert not self._entries.has_key(name), "Page already exists."
    label = gtk.Label(name)
    label.set_padding(2, 2)
    self.append_page(widget, label)
    self._entries[name] = widget
ConsoleNtbk.add_page = add_page

def page_index(self, name):
    assert self._entries.has_key(name)
    idx = self.page_num(self._entries[name])
    assert idx != -1                    # Invalid _entries.
    return idx
ConsoleNtbk.page_index = page_index
    
def destroy_page(self, name):
    idx = self.page_index(name)
    self.remove_page(idx)
    self._entries[name].destroy()
    del self._entries[name]
ConsoleNtbk.destroy_page = destroy_page

# 
#**        l3 console with notebook / canvas / l3List connections
def add_l3appconsole(w_):
    from l3lang import reader
    from l3gui.cruft.pylab import l3Console
    from l3gui import widgets, misc
    # Introduce a new Program.
    storage = w_.state_.storage 
    def_env = w_.state_.def_env
    program = reader.empty_program().setup(ast.empty_parent(),
                                           def_env,
                                           storage)[0]
    program.set_outl_edges(w_, None)
    # no comments: associate_comments(w_, program, text)
    program_l3pic = w_.canvas.view.start_add_l3tree(program)

    # Set up the console.
    name_s = "l3 Console"
    console = l3Console( w_, locals(),
                         quit_handler =
                         lambda *args: (w_.notebk_consoles.destroy_page(name_s),
                                        w_.stdinouterr.pop()
                                        ))
    w_.notebk_consoles.add_page(console, name_s)
    w_.stdinouterr.push()
    console.std_to_widget()
    console.write("==== l3 Console; exit with end-of-file (ctrl-d) ====\n",
                  style='error')
    console.banner()

    # Connect Program and console.
    console.associate_alist(program_l3pic._alist, def_env, storage)

    # Display the console.
    misc.show_consoles(w_)
    cs = w_.notebk_consoles
    cs.set_current_page(cs.page_index(name_s))
    console.grab_focus()

# 
#**        terminal console with notebook / canvas / l3List connections
def new_termconsole(w_):
    '''
    Start a new Program, and connect it to the main terminal.
    Terminal history accumulates as entries into the Program.
    '''
    from l3lang import reader
    # Introduce a new Program.
    storage = w_.state_.storage 
    def_env = w_.state_.def_env
    program = reader.empty_program().setup(ast.empty_parent(),
                                           def_env,
                                           storage)[0]
    program.set_outl_edges(w_, None)
    # no comments: associate_comments(w_, program, text)
    program_l3pic = w_.canvas.view.start_add_l3tree(program)

    # Set up the console.
    from l3lang import repl
    console, interpreter = repl.get_console_interpreter_pair(def_env, storage)
    def_env.import_external('console', console)
    def_env.import_external('interp', interpreter)

    # Connect Program and console.
    disp_alist = program_l3pic._alist

    # Display the console.
    w_.stdinouterr.push()
    w_.stdinouterr.std_connection()

    def root_display(cons_prog):
        body = cons_prog.body()

        # Ignore empty programs.
        if isinstance(body, ast.aNone):
            storage.remove(cons_prog._id)
            return

        # Extract body from Program. 
        cons_prog._primary[0].detach_child(body._id, storage)

        # Delete Program().  This avoids leaving unpickleable objects
        # in the storage.
        storage.remove(cons_prog._id)

        # Display node.
        body.set_outl_edges(w_, None)
        body_l3pic = w_.canvas.view.start_add_l3tree(body)
        disp_alist.append(body_l3pic)

    
    console.interact(display_via = root_display,
                     do_event = flush_events,
                     associate_comments =
                     (lambda tree, src: associate_comments(w_, tree, src)),
                     )


#*    Visual interpretation tracing
class DisplayInterp:
    ''' Indicate progress by selecting the node about to be interpreted.
    '''
    pass

def __init__(self, w_, l3pic):
    self.w_ = w_
    self._active = True
    self._l3pic = l3pic                  # The (l3Base) display.
DisplayInterp.__init__ = __init__

def __call__(self, l3tree, env, storage):
    # todo: the context of the call could be shown to indicate loop
    # progress.  Or display an iteration count as part of the
    # highlight. 

    # Tree still visible?
    if self._active:
        if hasattr(self._l3pic, '_root_group'):
            self.w_.selector.set_selection(self._l3pic)
            gtk.main_iteration(block = False)
DisplayInterp.__call__ = __call__

def __getstate__(self):
    # A DisplayInterp must be an in-memory only attachment.
    return {'_active' : False}
DisplayInterp.__getstate__ = __getstate__

def __setstate__(self, stuff):
    self.__dict__.update(stuff)
DisplayInterp.__setstate__ = __setstate__

def __deepcopy__(self, memo):
    return DisplayInterp(self.w_, self._l3pic)
DisplayInterp.__deepcopy__ = __deepcopy__

#*    Copy node and deco_table entries
def node_copy(w_, orig, ):
    node, nid = deepcopy(orig).\
                setup(ast.empty_parent(),
                      w_.state_.def_env,
                      w_.state_.storage)

    # Copy relevant table entries.
    oit = orig.top_down()
    nit = node.top_down()
    dt = w_.deco_table_
    try:
        while 1:
            id_o = oit.next()._id
            if dt.has_key(id_o):
                # Copy node's comment.
                comm = deepcopy(dt[id_o])
                comm.setup(ast.empty_parent(),
                           w_.state_.def_env,
                           w_.state_.storage)
                dt[nit.next()._id] = comm
            else:
                nit.next()
    except StopIteration:
        pass

    # Set outline edges.
    node.set_outl_edges(w_, None)
    return node, nid
