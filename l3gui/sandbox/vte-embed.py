# 
# Use vte as terminal for the same process, via InteractiveConsole.
# 
#         Character handling between user and terminal (rev.389)

#         Interim: input via event, output via stdout. (rev.390)

#         Too messy / slow:
#             connect stdin/out/err.
#             connect events to settrace.
#             Run InteractiveConsole

#         The terminal must manually feed input (by character!) to the
#         pseudo-process.  As result:
#             - pasting to the terminal still requires attention to
#               minute detail. 

#             - Use vte for separate processes, the pylab approach for
#               this process. 



#* imports
import sys, string
import gtk
import vte

#*    exit handler
def child_exited_cb(terminal):
	gtk.main_quit()

#*    selection handler 
def selected_cb(terminal, column, row, cb_data):
	if (row == 15):
		if (column < 40):
			return 1
	return 0

#* 
def restore_cb(terminal):
	(text, attrs) = terminal.get_text(selected_cb, 1)
	print "A portion of the text at restore-time is:"
	print text


#*    Receive (user -> terminal -> child process) input.

class IONav:
    pass

def __init__(self, interpreter):
    self._more = 0
    self._line = []
    self._interpreter = interpreter
IONav.__init__ = __init__

def userinput_cb(self, terminal, str, strlen):
    ''' User input -> terminal. '''
    # # print "received: '%s'" % str

    terminal.feed(str)

    # Accumulate a full line.
    if str != '\r':
        self._line.append(str)
        return

    terminal.feed('\r\n')

    # Execute line or accumulate more.
    self._more = ic.push("".join(self._line))
    self._line = []

    if self._more:
        prompt = sys.ps2
    else:
        prompt = sys.ps1
    terminal.feed('\r')
    terminal.feed(prompt)
IONav.userinput_cb = userinput_cb


class SimulateIn:
    """
    A fake input file object.  It receives input from a vte widget.
    """
    def __init__(self, fn, ionav):
        self._fn = fn
        self._ionav = ionav
    def close(self): pass
    flush = close
    def fileno(self):    return self._fn
    def isatty(self):    return False
    def read(self, a):   return self.readline()
    def readline(self):
        self._ionav.has_input()

        self.console.input_mode = True
        while self.console.input_mode:
            while gtk.events_pending():
                gtk.main_iteration()
        s = self.console.input
        self.console.input = ''
        return s+'\n'
    def readlines(self): return []
    def write(self, s):  return None
    def writelines(self, l): return None
    def seek(self, a):   raise IOError, (29, 'Illegal seek')
    def tell(self):      raise IOError, (29, 'Illegal seek')
    truncate = tell



class SimulateOut:
    """
    A fake output file object.  It sends output to a vte widget.
    """
    def __init__(self, fn, terminal):
        self.fn = fn
        self._terminal = terminal
    def close(self): pass
    flush = close
    def fileno(self):    return self.fn
    def isatty(self):    return False
    def read(self, a):   return ''
    def readline(self):  return ''
    def readlines(self): return []
    def write(self, s):
        self._terminal.feed(s)
    def writelines(self, l):
        for s in l:
            self._terminal.write(s)
    def seek(self, a):   raise IOError, (29, 'Illegal seek')
    def tell(self):      raise IOError, (29, 'Illegal seek')
    truncate = tell




#* "child process"
from code import InteractiveConsole
ic_dict = locals()
ic = InteractiveConsole(ic_dict)

ionav = IONav(ic)

#* main w/o process
if __name__ == '__main__':
    #** Defaults.
    blink = 1
    emulation = "xterm"
    font = "Monospace 10"
    scrollback = 100
    transparent = 0
    visible = 0
    audible = 0
    #** terminal proper 
    window = gtk.Window()
    terminal = vte.Terminal()
    if (transparent):
        terminal.set_background_transparent(True)
    terminal.set_cursor_blinks(blink)
    terminal.set_emulation(emulation)
    terminal.set_font_from_string(font)
    terminal.set_scrollback_lines(scrollback)
    terminal.set_audible_bell(audible)
    terminal.set_visible_bell(visible)
    #** connections
    terminal.connect("commit", lambda *args: ionav.userinput_cb(*args))
    terminal.connect("child-exited", child_exited_cb)
    terminal.connect("restore-window", restore_cb)
    # No command fork.
    terminal.show()

    #** Banner.
    try:
        sys.ps1
    except AttributeError:
        sys.ps1 = ">>> "
    try:
        sys.ps2
    except AttributeError:
        sys.ps2 = "... "
    cprt = 'Type "help", "copyright", "credits" or "license" for more information.'
    terminal.feed("Python %s on %s\r\n%s\r\n" %
                  (sys.version, sys.platform, cprt))
    
    #** Redirect file descriptors.
    #     sys.stdin = SimulateIn(0, ionav)
    sys.stdout = SimulateOut(1, terminal)
    #     sys.stderr = SimulateOut(2, ionav)

    #** main window
    scrollbar = gtk.VScrollbar()
    scrollbar.set_adjustment(terminal.get_adjustment())

    box = gtk.HBox()
    box.pack_start(terminal)
    box.pack_start(scrollbar)

    window.add(box)
    window.show_all()


    gtk.main()
