"""
Some graph editing code, initially tried as simple interface to l3lang
(taken from l3canvas.py).  This graph model is incompatible with l3;
it allows general links without restriction.

May be useful at some future time...
"""
#
#***                add_mstart
def add_mstart(self, macro, node_id = None):
    # Insert a new graphical Inline representation on the canvas.
    #
    # If NODE_ID is given, only update the view, not the model;
    # the default is to update the model with a clone of MACRO.
    #

    #
    # Graphics
    #
    wd = self.cp_.macro_call.width
    ht = self.cp_.macro_call.height
    s_root = self._macro_call.add(canvas.CanvasGroup)

    s_rect = s_root.add(canvas.CanvasRect,
                        x1 = 0, y1 = 0,
                        x2 = wd, y2 = ht,
                        fill_color = 'lightgreen',
                        outline_color = 'black',
                        # width_units = 1.0, # scaled
                        # unscaled
                        width_pixels = self.cp_.outline_width_normal,
                        )

    s_port_out = s_root.add(canvas.CanvasRect,
                            x1 = wd/2 - 1, y1 = ht - 1,
                            x2 = wd/2 + 1, y2 = ht,
                            fill_color = 'blue',
                            outline_color = 'black',
                            width_pixels = self.cp_.outline_width_normal,
                            )

    s_label = s_root.add(canvas.CanvasText,
                         x = wd / 2, y = ht / 2,
                         size_points = self.cp_.font_size,
                         scale = 1.0,
                         scale_set = True,
                         anchor = gtk.ANCHOR_CENTER,
                         text = macro._name.l3_string(),
                         )

    #
    # Graphical events
    #
    s_root.connect("event", lambda *a: self.s_root_event(*a) )
    s_port_out.connect("event",
                      lambda *a: self.s_port_event(*a), "s_port_out")

    #
    # Data
    #
    if node_id is None:     nid = self._pgraph.newnode( deepcopy(macro) )
    else:                   nid = node_id

    #
    self._nodes[ (nid, 'root') ] = s_root
    self._nodes[ (nid, 'rect')] = s_rect
    self._nodes[ (nid, 'port_out') ] = s_port_out
    self._nodes[ (nid, 'label') ] = s_label
    self._nodes[ s_root ] = nid
l3Canvas.add_mstart = add_mstart

#
#***                add_mcall
def add_mcall(self, macro, node_id = None):
    # Insert a new graphical Macro representation on the canvas.
    #
    # If NODE_ID is given, only update the view, not the model;
    # the default is to update the model with a clone of MACRO.
    #

    #
    # Graphics
    #
    wd = self.cp_.macro_call.width
    ht = self.cp_.macro_call.height
    c_root = self._macro_call.add(canvas.CanvasGroup)

    c_rect = c_root.add(canvas.CanvasRect,
                        x1 = 0, y1 = 0,
                        x2 = wd, y2 = ht,
                        fill_color = 'white',
                        outline_color = 'black',
                        # width_units = 1.0, # scaled
                        # unscaled
                        width_pixels = self.cp_.outline_width_normal,
                        )

    c_port_out = c_root.add(canvas.CanvasRect,
                            x1 = wd/2 - 1, y1 = ht - 1,
                            x2 = wd/2 + 1, y2 = ht,
                            fill_color = 'blue',
                            outline_color = 'black',
                            width_pixels = self.cp_.outline_width_normal,
                            )

    c_label = c_root.add(canvas.CanvasText,
                         x = wd / 2, y = ht / 2,
                         size_points = self.cp_.font_size,
                         scale = 1.0,
                         scale_set = True,
                         anchor = gtk.ANCHOR_CENTER,
                         text = macro._name.l3_string(),
                         )

    #
    # Graphical events
    #
    c_root.connect("event", lambda *a: self.c_root_event(*a) )
    c_port_out.connect("event",
                      lambda *a: self.c_port_event(*a), "c_port_out")
    #
    # Data
    #
    if node_id is None:     nid = self._pgraph.newnode( deepcopy(macro) )
    else:                   nid = node_id
    #
    self._nodes[ (nid, 'root') ] = c_root
    self._nodes[ (nid, 'rect')] = c_rect
    self._nodes[ (nid, 'port_out') ] = c_port_out
    self._nodes[ (nid, 'label') ] = c_label
    self._nodes[ c_root ] = nid
l3Canvas.add_mcall = add_mcall



def mcall_updated_col(cellrend, tree_path, new_text, (l_, cb_col)):
    #
    # Update model and view.
    #
    print
    print "mcall_updated_col entered."

    assert isinstance(l_.mcall, Macro)
    validate_input(new_text)
    model = l_.node_tree.model

    #
    # Replace child in all parents.
    #
    #     Get parents
    parent_path = map(string.atoi, tree_path.split(":"))
    parent, child = mcall_parent_child(l_.mcall, parent_path, cb_col)
    assert child.has_parent()
    real_parent = l_.w_.state_.storage.load( child.parent_id() )

    # Tricky event:
    #     - click twice on mcall edit field
    #     - make no change
    #     - click on another edit field
    # This signals "edited" and enters mcall_updated_col.
    # So, check for changed text first.
    #
    if child.l3_string() == new_text:
        return True

    #     Get  new child
    value = reader.parse(new_text).body()
    value.setup(real_parent, l_.w_.state_.def_env, l_.w_.state_.storage)

    #     Replace in expansion tree.
    child.deep_replace(value, l_.w_.state_.storage)

    #     Replace in Macro tree if needed.
    try:    parent.replace_child(child._id, value)
    except ast.ReplacementError: pass

    # Update viewing model.
    iter = model.get_iter_from_string(tree_path)
    model.set(iter, cb_col, new_text)

    return True

#
#**            editing     mcall popup
def mcall_parent_child(mcall, path, column):
    #
    # Given (gtk) PATH and COLUMN, get appropriate AST parent and child.
    #
    assert isinstance(column, (IntType, LongType))
    assert isinstance(path, ListType)
    assert isinstance(mcall, Macro)

    # The column in a treeview callback is only used when referring to
    # nested Macro arguments:
    #
    #   pointing to a List() L, return L and its content at COLUMN
    #   pointing to an Immediate() I, return Macro and I
    #
    if len(path) == 1:
        return mcall, mcall.element_at(path)

    elif len(path) == 2:
        par = mcall.element_at(path)
        return par, par.element_at([column])

    else:
        raise Exception("Internal error: mcall structure.")



def mcall_edit(macro_canvas, canv_item, w_):
    # Edit the entries of a Macro() macro representation
    nid = macro_canvas._nodes[ canv_item ]
    macro_id = macro_canvas._pgraph.get_node( nid )._macro_id
    mcall = w_.state_.storage.load(macro_id)
    assert isinstance(mcall, Macro)

    l_ = utils.Shared()
    l_.mcall = mcall
    l_.w_ = w_
    #
    # Tree model
    #
    ( l_.descrip_col,
      l_.value_col,
      l_.edit_desc,
      l_.edit_val,
      ) = range(0,4)
    model = gtk.TreeStore(gobject.TYPE_STRING,
                          gobject.TYPE_STRING,
                          gobject.TYPE_BOOLEAN,
                          gobject.TYPE_BOOLEAN,
                          )

    # First three rows
    for des, val_idx in [("name", 0), ("synopsis", 1), ("Python function", 2)]:
        # One row.
        iter = model.append(None)
        model.set(iter,
                  l_.descrip_col, des,
                  l_.value_col, mcall.element_at([val_idx]).l3_string(),
                  l_.edit_desc, False,
                  l_.edit_val,  True,
                  )

    # Subtree rows.
    for header, hd_idx in [("Input arguments", 3), ("Output arguments", 4)]:
        iter = model.append(None)
        model.set(iter,
                  l_.descrip_col, header,
                  l_.value_col, "",
                  l_.edit_desc, False,
                  l_.edit_val,  False,
                  )
        for dv_list in mcall.element_at([hd_idx]).entries():
            desc, value = [field for field in dv_list.entries()]
            sub_iter = model.append(iter)
            model.set(sub_iter,
                      l_.descrip_col, (desc.l3_string()),
                      l_.value_col, (value.l3_string()),
                      l_.edit_desc, True,
                      l_.edit_val,  True,
                      )

    l_.node_tree = utils.Shared()
    l_.node_tree.model = model

    #
    # Tree View
    #
    l_.node_tree.widget  = gtk.TreeView(l_.node_tree.model)
    l_.node_tree.widget.set_property("headers-visible", 0)

    #
    # Column renderers.
    #
    l_.node_tree.column = [0,1]
    #
    render_0 = gtk.CellRendererText()
    render_0.connect("edited", mcall_updated_col, (l_, l_.descrip_col))
    l_.node_tree.column[0] = gtk.TreeViewColumn(
        "Description",
        render_0,
        text = l_.descrip_col,
        editable = l_.edit_desc,
        )
    l_.node_tree.widget.append_column(l_.node_tree.column[0])
    #
    render_1 = gtk.CellRendererText()
    render_1.connect("edited", mcall_updated_col, (l_, l_.value_col))
    l_.node_tree.column[1] = gtk.TreeViewColumn(
        "Value",
        render_1,
        text = l_.value_col,
        editable = l_.edit_val,
        )
    l_.node_tree.widget.append_column(l_.node_tree.column[1])

    #
    # Build popup widget
    #
    l_.button = utils.Shared()
    l_.button.ok = gtk.Button("OK")
    l_.button.cancel = gtk.Button("Cancel")

    l_.hbox = gtk.HBox()
    l_.hbox.pack_start(l_.button.ok)
    l_.hbox.pack_start(l_.button.cancel)

    l_.vbox = gtk.VBox()
    l_.vbox.pack_start(l_.node_tree.widget, expand = True)
    l_.vbox.pack_start(l_.hbox, expand = False)

    l_.window = gtk.Window()
    l_.window.connect("delete-event", lambda *args: l_.window.destroy())
    l_.window.set_border_width(0)
    l_.window.add(l_.vbox)

    #
    # Events and handlers
    #
    l_.selector.get_selection().set_mode(gtk.SELECTION_BROWSE)

    def _fin(*args):
        macro_canvas.update_mcall(nid, mcall)
        l_.window.destroy()

    l_.button.ok.connect("clicked", _fin)
    l_.button.cancel.connect("clicked",
                             (lambda *args:
                              pprint("Cancel is not available yet.")))

    l_.window.show_all()


#
#**            update of mcall display
def update_mcall(self, nid, mcall):
    assert isinstance(mcall, Macro)
    self._nodes[ (nid, 'label') ].set_property("text", mcall._name.l3_string())
l3Canvas.update_mcall = update_mcall

#
#**            update of mstart display
def update_mstart(self, nid, mstart):
    assert isinstance(mstart, Inline)
    self._nodes[ (nid, 'label') ].set_property("text",
                                               mstart._name.l3_string())
l3Canvas.update_mstart = update_mstart


#
#**            editing     mstart popup
def mstart_updated(l_, name_txt, py_code_txt):
    #
    # Convert the raw strings NAME_TXT and PY_CODE_TXT to l3 ASTs and
    # replace the originals.
    #
    validate_input(triple_quote(name_txt))

    # Replace AST nodes.
    name_value = reader.parse(triple_quote(name_txt)).body()
    name_value.setup(l_.mstart,
                     l_.w_.state_.def_env, l_.w_.state_.storage)
    l_.mstart.element_at([0]).deep_replace(name_value, l_.w_.state_.storage)

    code_value = reader.parse(triple_quote(py_code_txt)).body()
    code_value.setup(l_.mstart,
                     l_.w_.state_.def_env, l_.w_.state_.storage)
    l_.mstart.element_at([1]).deep_replace(code_value,
                                           l_.w_.state_.storage)

    return True


def mstart_edit(macro_canvas, canv_item, w_):
    #
    # Edit the entries of a mstart() macro representation.
    #
    nid = macro_canvas._nodes[ canv_item ]
    mstart = macro_canvas._pgraph.get_node( nid )

    l_ = utils.Shared()
    l_.mstart = mstart
    l_.w_ = w_

    # Form labels.
    l_.label_name = gtk.Label(str = 'name')
    l_.label_code = gtk.Label(str = 'Python\nCode')

    # Form text widgets.
    l_.text_ = [gtk.TextView(), gtk.TextView()]
    for ind in [0,1]:
        l_.text_[ind].get_buffer().set_text(
            mstart.element_at([ind]).l3_string() )

    # Set minimum text sizes.
    l_.text_[0].set_size_request(12 * 20, 12 * 2)
    l_.text_[1].set_size_request(12 * 20, 12 * 20)

    # Add Decoration.
    l_.text_frame = [ gtk.Frame(), gtk.Frame() ]
    for ind in [0,1]:
        l_.text_frame[ind].add(l_.text_[ind])
        l_.text_frame[ind].set_shadow_type(gtk.SHADOW_OUT)

    #
    # Set up table layout.
    #
    l_.table = gtk.Table(rows = 2, columns = 2)
    l_.table.attach(l_.label_name,
                    0, 1, 0, 1,
                    xoptions = gtk.SHRINK,
                    yoptions = gtk.SHRINK,
                    )
    l_.table.attach(l_.label_code,
                    0, 1, 1, 2,
                    xoptions = gtk.SHRINK,
                    yoptions = gtk.SHRINK,
                    )
    ind = 0
    l_.table.attach(l_.text_frame[ind],
                    1, 2, ind, ind + 1,
                    xoptions = gtk.EXPAND | gtk.FILL,
                    yoptions = gtk.FILL,
                    )
    ind = 1
    l_.table.attach(l_.text_frame[ind],
                    1, 2, ind, ind + 1,
                    xoptions = gtk.EXPAND | gtk.FILL,
                    yoptions = gtk.EXPAND | gtk.FILL,
                    )

    #
    # Build popup widget.
    #
    l_.button = utils.Shared()
    l_.button.ok = gtk.Button("OK")
    l_.button.cancel = gtk.Button("Cancel")

    l_.hbox = gtk.HBox()
    l_.hbox.pack_start(l_.button.ok)
    l_.hbox.pack_start(l_.button.cancel)

    l_.vbox = gtk.VBox()
    l_.vbox.pack_start(l_.table, expand = True)
    l_.vbox.pack_start(l_.hbox, expand = False)

    l_.window = gtk.Window()
    l_.window.connect("delete-event", lambda *args: l_.window.destroy())
    l_.window.set_border_width(0)
    l_.window.add(l_.vbox)

    #
    # Events and handlers.
    #
    def _fin(*args):
        # Inline update.
        b0 = l_.text_[0].get_buffer()
        frm0, to0 = b0.get_bounds()
        b1 = l_.text_[1].get_buffer()
        frm1, to1 = b1.get_bounds()
        mstart_updated(l_, b0.get_text(frm0, to0), b1.get_text(frm1, to1) )

        # New text.
        macro_canvas.update_mstart(nid, mstart)

        l_.window.destroy()

    l_.button.ok.connect("clicked", _fin)

    l_.button.cancel.connect("clicked",
                             (lambda *args:
                              pprint("Cancel is not available yet.")))
    l_.window.show_all()


#
#**        insertion   of edge on canvas
def add_edge(self, src, target, edge_id = None, canv_edge = None):
    #
    # Get edge id.
    #
    if edge_id is None: e_id = self._pgraph.newedge(src, target)
    else:               e_id = edge_id
    #
    # Get canvas edge.
    #
    if canv_edge is None:
        # Draw new edge on canvas
        edge = self.root().add(
            canvas.CanvasLine,
            fill_color = 'black',
            first_arrowhead = 0,
            last_arrowhead = 0,
            line_style = gtk.gdk.LINE_SOLID,
            width_pixels = self.cp_.edge_width,
            points = [ 0, 0, 1, 1 ],
            )
        edge.show()
    else:
        edge = canv_edge
    #
    # Display
    #
    edge.lower_to_bottom()
    edge.raise_(1)                      # Above paper.

    #
    # Keep data.
    #
    self._edges[ e_id ] = edge
    self._edges[ edge ] = e_id
    #
    return edge
l3Canvas.add_edge = add_edge
