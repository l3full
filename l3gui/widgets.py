#
# Author:  Michael H. Hohn (mhhohn@lbl.gov)
#
# Copyright (c) 2006, The Regents of the University of California
#
# See legal.txt and license.txt
#

"""
Canvas "widgets".  These come in groups:

    graphical analogs to l3's core types
        as found in l3lang.ast.
        They provide editing, dragging, and composition functionality,
        and allow for graphical program construction (useful for
        higher-level functions), and graphical program and data
        examination.

    simple display support
        These are additional items needed for display, like
        Placeholder and Header.

"""
#*    Header
#
# policy for flush_events
# To ensure proper sizing, flush_events is called after display
# additions are made.
# Using flush_events in every get_bounds() call slows display down
# significantly.

import sys, weakref, traceback, string, types, os
from math import floor, sqrt
from pprint import pprint

import pango
import gtk
import gobject
try:
    import gnomecanvas as canvas
    from gnomecanvas import MOVETO_OPEN, MOVETO, LINETO, CURVETO
except:
    # Try backported version used in sparx.
    import canvas
    from canvas import MOVETO_OPEN, MOVETO, LINETO, CURVETO

import l3lang.globals as G
from l3lang import reader, ast, view, utils
from l3lang.ast import empty_parent, Set, Marker, Int, Call, Symbol, \
     Macro, aList, List, Return, Member
import l3gui.misc as misc
from l3gui.misc import \
     print_, \
     warn_, \
     DisplayError, \
     flush_events, \
     _get_props, \
     _set_props, \
     path_rectangle_rounded, \
     canvas_item_get_bounds_world, \
     destroy_if_last, \
     _safed, \
     Selectable,\
     slot_available


#*    key mappings
kn = gtk.gdk.keyval_from_name
key_Left = kn("Left")
key_Right = kn("Right")
key_Up = kn("Up")
key_Down = kn("Down")
key_Return = kn("Return")
key_Escape = kn("Escape")
key_Windows_L = kn("Super_L")
key_Alt_L = kn("Alt_L")


#*    control flow lines (element display)
class ContinueLine:
    pass

class BreakLine:
    pass

def __init__(self, w_, cnvs, child, parent):
    x1, y1 = child.continue_start_pt()
    xn, yn = parent.continue_stop_pt()

    # World to parent coordinates.
    _, _, _, _, sx, sy = parent.i2w_affine(tuple(range(0,6)))
    x1 -= sx
    y1 -= sy
    xn -= sx
    yn -= sy

    # Line moves left and up.
    points = [x1, y1,
              xn, y1,
              xn, yn,]
    self._line = parent._root_group.add(
        canvas.CanvasLine,
        fill_color  = "green",
        points      = points,
        width_units = 0.3,
        cap_style   = gtk.gdk.CAP_ROUND,
        join_style  = gtk.gdk.JOIN_ROUND,
        )
ContinueLine.__init__ = __init__


def __init__(self, w_, cnvs, child, parent):
    x1, y1 = child.break_start_pt()
    xn, yn = parent.break_stop_pt()

    # World to parent coordinates.
    _, _, _, _, sx, sy = parent.i2w_affine(tuple(range(0,6)))
    x1 -= sx
    y1 -= sy
    xn -= sx
    yn -= sy

    # Line moves right and down.
    points = [x1, y1,
              xn, y1,
              xn, yn,]
    self._line = parent._root_group.add(
        canvas.CanvasLine,
        fill_color  = "red",
        points      = points,
        width_units = 0.3,
        cap_style   = gtk.gdk.CAP_ROUND,
        join_style  = gtk.gdk.JOIN_ROUND,
        )
BreakLine.__init__ = __init__

def destroy(self):
    _safed(self._line)
    self._line = None
ContinueLine.destroy = destroy
BreakLine.destroy = destroy

def refresh(self):
    pass
ContinueLine.refresh = refresh
BreakLine.refresh = refresh


#*    Label ( element header display )
class Label:
    # Display of a textual element, inteded to be part of another
    # construct.  Thus, Label has no event handling of its own, no
    # menu, no independent hiding, etc.
    #
    # Appearance similar to l3Rawtext.
    pass

def __init__(self, w_, cnvs, root_group, header_text,
             x = 0, y = 0, font = None, fill_color = "white"):
    if font is None:
        font = w_.cp_.label_font

    # Outline follows l3Rawtext.__init__, but the contents are too
    # distinct to merge.
    self._destroy_hook = []             # (func -> None) list
    self._reparent_to_root_hook = []             # (func -> None) list

    self.w_ = w_
    self._canvas = cnvs

    # l3 tree.

    # Item cross-referencing.

    # Text edit state.

    # Focus redirection.

    # Display Group.
    self._root_group = root_group.add(canvas.CanvasGroup)

    # Display self.
    if 1:
        #
        #       Text content / format / scale
        #
        self._ltext = misc.GtkObjProxy(self._root_group.add(
            canvas.CanvasText,
            anchor = gtk.ANCHOR_NORTH_WEST,
            x = x,
            y = y,
            size = self.w_.cp_.font_size_labels * 1.0 * pango.SCALE,
            scale = 1.0,
            scale_set = True,
            font = font,
            size_set = True,
            markup = header_text,
            ))
        self._ltext.show()
        flush_events()
        x1, y1, x2, y2 = self._ltext.get_bounds() # world units
        # Reference size.
        self._ltext_size_1 = ((x2 - x1) * self._canvas._pixpu,
                              (y2 - y1) * self._canvas._pixpu)

        # Current scale size.
        self._ltext.set_property(
            "size",
            self.w_.cp_.font_size_labels * self._canvas._abs_zoom * pango.SCALE)

        self._canvas.add_zoom_text(self)

        # Text events.

        # Text outline.
        x1, y1, x2, y2 = self._ltext.get_bounds() # world units
        pad = w_.cp_.textview.outline_padding / cnvs._pixpu
        x1 -= pad
        y1 -= pad
        x2 += pad
        y2 += pad

        self._loutline = self._root_group.add(
            canvas.CanvasRect,
            x1 = x1, y1 = y1,
            x2 = x2, y2 = y2,
            fill_color = fill_color,
            outline_color = fill_color,
            width_pixels = w_.cp_.header_outline_width,
            )
        self._loutline.lower(1)
        self._loutline.show()

        # Outline events.

    # Display subtrees.

    # Highlight outline.

    # Borders etc.
Label.__init__ = __init__

def connect(self, *args):
    self._root_group.connect(*args)
Label.connect = connect

def get_property(self, prop):
    return self._root_group.get_property(prop)
Label.get_property = get_property

def set_property(self, prop, val):
    return self._root_group.set_property(prop, val)
Label.set_property = set_property

#*    Image ( element header display )
class Image:
    # Display of an image, intended to be part of another
    # construct.  Thus, Image has no event handling of its own, no
    # menu, no independent hiding, etc.
    #       See also class Label.
    pass

def __init__(self, w_, cnvs, root_group, pixbuf,
             x = 0, y = 0, parent = None):

    # Outline follows l3Rawtext.__init__, but the contents are too
    # distinct to merge.
    self._destroy_hook = []                      # (func -> None) list
    self._reparent_to_root_hook = []             # (func -> None) list

    self._parent = parent
    self.w_ = w_
    self._canvas = cnvs
    self._img_scale = w_.cp_.image_scale

    # l3 tree.

    # Item cross-referencing.

    # Text edit state.

    # Focus redirection.

    # Display Group.
    self._root_group = root_group.add(canvas.CanvasGroup)

    # Display self.
    if w_.cp_.image_natural_size:
        #
        #       Image content at original pixel size
        #
        # Display without aspect distortion.
        pixwid = pixbuf.get_width()
        pixhei = pixbuf.get_height()
        self._ltext = misc.GtkObjProxy(self._root_group.add(
            canvas.CanvasPixbuf,
            x = x,
            y = y,
            anchor = gtk.ANCHOR_NORTH_WEST,
            height_set = True,
            width_set = True,
            height_in_pixels = True,
            width_in_pixels = True,
            width = pixwid * self._img_scale,
            height = pixhei * self._img_scale,
            pixbuf = pixbuf,
            ))
        self._ltext.show()
        flush_events()
        x1, y1, x2, y2 = self._ltext.get_bounds() # world units

    else:
        #
        #       Image content / format / scale
        #
        # Display without aspect distortion.
        pixwid = pixbuf.get_width()
        pixhei = pixbuf.get_height()
        woh = min(1.0 * pixwid / pixhei, 1)
        how = min(1.0 * pixhei / pixwid, 1)
        self._ltext = misc.GtkObjProxy(self._root_group.add(
            canvas.CanvasPixbuf,
            x = x,
            y = y,
            anchor = gtk.ANCHOR_NORTH_WEST,
            height_set = True,
            width_set = True,
            width = w_.cp_.image_width * woh * self._img_scale,
            height = w_.cp_.image_height * how * self._img_scale,
            pixbuf = pixbuf,
            ))
        self._ltext.show()
        flush_events()
        x1, y1, x2, y2 = self._ltext.get_bounds() # world units

    # Display subtrees.

    # Highlight outline.

    # Borders etc.
Image.__init__ = __init__

# # def resize(self, which, value):
# #     # UNTESTED
# #     ''' Resize the pixmap to `value` world units.
# #     `which` is "width" or "height"
# #     '''
# #     assert which in ['width', 'height'], "`which` must be width or height"
# #     self._ltext.set_property(which, value)
# #     # Move following items in parent.
# #     if self._parent:
# #         self._parent.new_size_for(self)
# # Image.resize = resize

def scale(self, ratio):
    get = self._ltext.get_property
    set = self._ltext.set_property

    set('height', get('height') * ratio)
    set('width',  get('width') * ratio)

    # Move following items in parent.
    if self._parent:
        self._parent.new_size_for(self)

    self._img_scale *= ratio
Image.scale = scale

def set_default_size(self):
    self.w_.cp_.image_scale = self._img_scale
Image.set_default_size = set_default_size


def connect(self, *args):
    self._root_group.connect(*args)
Image.connect = connect

def get_property(self, prop):
    return self._root_group.get_property(prop)
Image.get_property = get_property

def set_property(self, prop, val):
    return self._root_group.set_property(prop, val)
Image.set_property = set_property

#*    Widget ( element header display )
class Widget:
    # Display of an embedded widget, intended to be part of another
    # canvas construct.  Thus, Widget has no event handling of its own, no
    # menu, no independent hiding, etc.
    #       See also class Label.
    pass

def __init__(self, w_, cnvs, root_group, widget,
             x = 0, y = 0, parent = None):

    # Outline follows l3Rawtext.__init__, but the contents are too
    # distinct to merge.
    self._destroy_hook = []                      # (func -> None) list
    self._reparent_to_root_hook = []             # (func -> None) list

    self._parent = parent
    self.w_ = w_
    self._canvas = cnvs

    # l3 tree.

    # Item cross-referencing.

    # Text edit state.

    # Focus redirection.

    # Display Group.
    self._root_group = root_group.add(canvas.CanvasGroup)

    # Display self.
    #
    #       Widget content at original pixel size
    #
    # Display without aspect distortion.
    self._ltext = misc.GtkObjProxy(self._root_group.add(
        canvas.CanvasWidget,
        x = x,
        y = y,
        anchor = gtk.ANCHOR_NORTH_WEST,
        size_pixels = False,
        width = 40,
        height = 30,
        widget = widget,
        ))
    self._ltext.show()
    widget.show()
    flush_events()
    x1, y1, x2, y2 = self._ltext.get_bounds() # world units

    # Display subtrees.

    # Highlight outline.

    # Borders etc.
    pass
Widget.__init__ = __init__

def scale(self, ratio):
    get = self._ltext.get_property
    set = self._ltext.set_property

    set('height', get('height') * ratio)
    set('width',  get('width') * ratio)

    # Move following items in parent.
    if self._parent:
        self._parent.new_size_for(self)

Widget.scale = scale


def connect(self, *args):
    self._root_group.connect(*args)
Widget.connect = connect

def get_property(self, prop):
    return self._root_group.get_property(prop)
Widget.get_property = get_property

def set_property(self, prop, val):
    return self._root_group.set_property(prop, val)
Widget.set_property = set_property

#*    uWidget ( element header display )
class uWidget:
    # Base class for trivial "widgets" like buttons.
    #
    # Full widgets
    #   a) make the display sluggish
    #   b) cannot mix (overlap) with the canvas elements
    #   c) do not nest with canvas items
    #
    # uWidgets are always added to another construct (like l3List) for
    #   a) convenience
    #   b) display aestetics
    #
    # uWidgets handle events only to call appropriate functions of the
    # main construct.
    # uWidgets provide no independent hiding, etc.
    #
    # See also class Label.
    pass

def __init__(self, w_, cnvs, root_group,
             action = (lambda _: None)):
    #
    # Comment outline follows l3Rawtext.__init__.
    #
    self._destroy_hook = []                      # (func -> None) list
    self._reparent_to_root_hook = []             # (func -> None) list

    self.w_ = w_
    self._canvas = cnvs
    self._action = action

    # l3 tree.

    # Item cross-referencing.

    # Text edit state.

    # Focus redirection.

    # Display Group.
    self._root_group = root_group.add(canvas.CanvasGroup)
    grp_trans = self._root_group.add(canvas.CanvasGroup)
    grp_scale = grp_trans.add(canvas.CanvasGroup)
    grp_obj   = grp_scale.add(canvas.CanvasGroup)

    self._grp_trans = grp_trans         # Use to move
    self._grp_scale = grp_scale         # Use to scale.
    self._grp_obj = grp_obj    # Draw objects into this.
                               # Use .move to reposition for scaling center.

    # Display self.
    self.draw()

    # Display subtrees.

    # Highlight outline.

    # Borders etc.
uWidget.__init__ = __init__

def connect(self, *args):
    self._root_group.connect(*args)
uWidget.connect = connect

def get_property(self, prop):
    return self._root_group.get_property(prop)
uWidget.get_property = get_property

def set_property(self, prop, val):
    return self._root_group.set_property(prop, val)
uWidget.set_property = set_property

def draw(self):
    raise Exception("uWidget.draw must be overridden.")
    # self.draw is to be overriden in subclass.
    # The following code serves as template.
    #
    # Default uWidget content.
    #
    self._ltext = misc.GtkObjProxy(self._grp_obj.add(
        canvas.CanvasText,
        anchor = gtk.ANCHOR_NORTH_WEST,
        x = 0,
        y = 0,
        size = self.w_.cp_.font_size * 1.0 * pango.SCALE,
        scale = 1.0,
        scale_set = True,
        font = None,
        size_set = True,
        markup = "uW",
        ))
    self._ltext.show()

    # Reference size.
    flush_events()
    x1, y1, x2, y2 = self._ltext.get_bounds() # world units
    self._ltext_size_1 = ((x2 - x1) * self._canvas._pixpu,
                          (y2 - y1) * self._canvas._pixpu)

    # Current scale size.
    self._ltext.set_property(
        "size",
        self.w_.cp_.font_size * self._canvas._abs_zoom * pango.SCALE)

    # Register for zooming.
    # # self._canvas.add_zoom_text(self)

    # Content events.
    self.connect("event", lambda *a: self.button_event(*a))
uWidget.draw = draw


def button_event(self, item, event):
    if event.type == gtk.gdk.BUTTON_RELEASE:
        if event.button == 1:
            # # print 'uWidget button press.  Override method.'
            self._action(event)
            return True
uWidget.button_event = button_event


#
#**        uBlank
class uBlank(uWidget):
    '''Display a 1x1 pixel.  Use to experiment with layout, without
    removing code.
    '''
    pass
#
#**        uVisibleSymbol ( element header display )
class uVisibleSymbol(uWidget):
    pass

#
#**        uPartiallyVisible ( element header display )
class uPartiallyVisible(uWidget):
    pass

#
#**        uInvisibleSymbol ( element header display )
class uInvisibleSymbol(uWidget):
    pass

#
#**        common drawing
def _draw_common_triangle(self):
    #
    # Triangle right.
    #

    # This kind of glyph design is silly; using a predefined character
    # is far preferable.
    cp = self.w_.cp_
    # width 1, height 2
    points = [0 , 0,
              1 , 1,
              0 , 2,
              0 , 0,
              ]

    # Draw triangle.
    self._ltext = misc.GtkObjProxy(self._grp_obj.add(
        canvas.CanvasPolygon,
        fill_color  = "black",
        points      = points,
        width_units = 0.2,
        cap_style   = gtk.gdk.CAP_ROUND,
        join_style  = gtk.gdk.JOIN_ROUND,
        ))

    # Draw background / padding.  This will also rotate / scale.
    pad = self.w_.cp_.symbol_padding

    #     pad = 0.0
    #     self._loutline = misc.GtkObjProxy(self._grp_obj.add(
    #         canvas.CanvasRect,
    #         x1 = - pad,
    #         y1 = - pad,
    #         x2 = 1 + pad,
    #         y2 = 2 + pad,
    #         fill_color = "white",
    #         outline_color = "white",
    #         width_pixels = self.w_.cp_.outline_width_normal,
    #         ))
    #     self._loutline.lower(1)

    # Reposition for scaling around center.
    ll, tt, rr, bb = canvas_item_get_bounds_world(self._grp_obj)
    self._grp_obj.move( -(ll + rr)/2, -(tt + bb)/2 )

    # Scale.
    sx = sy = cp.symbol_width / (1.0 - 0.0) # width from `points`
    self._grp_scale.affine_relative( (sx, 0.0,  0.0, sy,  0.0, 0.0) )

    # Now add background.
    ll, tt, rr, bb = canvas_item_get_bounds_world(self._grp_scale)
    self._loutline = misc.GtkObjProxy(self._grp_trans.add(
        canvas.CanvasRect,
        x1 = ll - pad,
        y1 = tt - pad,
        x2 = rr + pad,
        y2 = bb + pad,
        fill_color = "white",
        outline_color = "white",
        width_pixels = self.w_.cp_.outline_width_normal,
        ))
    self._loutline.lower(1)

    self._ltext.show()

    # Content events.
    self.connect("event", lambda *a: self.button_event(*a))
uWidget._draw_common_triangle = _draw_common_triangle

def draw(self):
    self._draw_common_triangle()
uInvisibleSymbol.draw = draw

def draw(self):
    #
    # Triangle down & right.
    #
    self._draw_common_triangle()

    # Rotate 45 deg.
    self._grp_scale.affine_relative( misc.affine_rotate(45) )
uPartiallyVisible.draw = draw

def draw(self):
    #
    # Triangle down.
    #
    self._draw_common_triangle()

    # Rotate 90 deg.
    self._grp_scale.affine_relative( misc.affine_rotate(90) )
uVisibleSymbol.draw = draw


def draw(self):
    # Draw triangle.
    self._ltext = misc.GtkObjProxy(self._grp_obj.add(
        canvas.CanvasRect,
        x1 = 0, 
        y1 = 0,
        x2 = 0.1,
        y2 = 0.1,
        fill_color = "white",
        outline_color = "white",
        width_pixels = self.w_.cp_.outline_width_normal,
        ))

    self._ltext.show()
    # Content events.
    ## self.connect("event", lambda *a: self.button_event(*a))
uBlank.draw = draw



#*    l3Base
class l3Base(Selectable):
    pass

def __init__(self):
    self._parent = None
    self._destroy_hook = []             # (func -> None) list
    self._reparent_to_root_hook = []             # (func -> None) list
    self._vis_indic = None              # visibility status indicator

    # Selection menu.
    self._selection_popup_cb = {}         # menu item -> connection args
    self._selection_popup = gtk.Menu()    # dummy
    self._selection_popup_list = [
        [ gtk.MenuItem("print values in context"),
          ("activate", lambda *args: self.selection_values_in_context(args) )],

        [ gtk.MenuItem("evaluate locally"),
          ("activate", lambda *args: self.selection_eval_local(args))],

        [ gtk.MenuItem("evaluate locally, use console"),
          ("activate", lambda *args: self.eval_local_console(args))],

        [ gtk.MenuItem("item screenshot to /tmp/foo.png"),
          ("activate", lambda *args: self.item_screenshot("/tmp/foo.png"))],

        ]
l3Base.__init__ = __init__

#
#**        Screenshot of selected item to file
def item_screenshot(self, filename):
    print "capturing image from RIGHT canvas."
    # todo: get item's canvas
    # todo: remove popup menu before capture.
    view = self.w_.canvas.view          
    selection = self.w_.selector.get_selection()
    (sl, st, sr, sb) = selection.get_bounds()

    # Get selection corners.
    #   These are the canvas window coordinates.
    (wl, wt) = map(int, view.world_to_window(sl, st))
    (wr, wb) = map(int, view.world_to_window(sr, sb))

    # Compute selection dimensions.
    width = wr - wl 
    height = wb - wt

    # Prepare pixbuf.
    pixbuf = gtk.gdk.Pixbuf(gtk.gdk.COLORSPACE_RGB,
                            False, # has_alpha
                            8,     # bits_per_sample
                            width,
                            height) 

    # get_from_drawable(src, cmap, src_x, src_y, dest_x, dest_y, width, height)
    status = pixbuf.get_from_drawable(
        view.window,
        gtk.gdk.colormap_get_system(),
        wl, wt,
        0, 0,
        width, height
        )

    if status == None:
        print "capture failed"

    pixbuf.save("/tmp/foo.png", "png")
l3Base.item_screenshot = item_screenshot


#
#**        value display from selection
def selection_values_in_context(self, args):
    # args: (menuitem)
    st = self.w_.state_.storage
    tw = ast.TreeWork(st)
    #
    l3nd = self.w_.selector.get_selection_list()

    # Self is data source.
    cctxt = tw.get_call_ctxt(self.getid())

    # Form the call graph pruning list.
    l3nd.remove(self)

    # Show pruning list.
    print "------------------------------------------------------------------"
    print "Showing only data created through:"
    for nd in l3nd:
        print nd.l3tree().source_string()

    # Get reduced calling context.
    pruned = tw.prune_cctxt(
        cctxt,
        map(l3Base.getid, l3nd)) # Remaining selection for pruning.

    # Display values
    def id_v_ts(leaves):
        print "clone id, clone timestamp, value"
        print "--------------------------------------------"
        for (lid, val) in leaves:
            print "%s, %s, %s" % (lid, st.ie_.get_timestamp(lid), val)
    id_v_ts(tw.cctxt_leaves(pruned))

    pass
l3Base.selection_values_in_context = selection_values_in_context


#
#**        ast related
def contains_recursive(self, other):
    oid = other._l3tree._id
    for node in self._l3tree.top_down():
        # Note: aList([]) == aList([]) is always True
        if node._id == oid:
            return True
    return False
l3Base.contains_recursive = contains_recursive

def l3tree(self):
    return self._l3tree
l3Base.l3tree = l3tree

def getid(self):
    return self._l3tree._id
l3Base.getid = getid


#
#**        canvas display functions
def dump_bbox(self):
    print "Bounds:", self.get_bounds_world()
l3Base.dump_bbox = dump_bbox

def zoom(self, factor):
    return (factor, factor)
l3Base.zoom = zoom

def i2w_affine(self, tuple_):
    return self._root_group.i2w_affine(tuple_)
l3Base.i2w_affine = i2w_affine

def get_anchor(self):
    # Use upper left.
    x, y, u, v = self.get_bounds()
    return x, y
l3Base.get_anchor = get_anchor

#
#**        structural
def reparent(self, newp):
    assert isinstance(newp, (l3aList, l3Base))

    # Remove from current parent.
    if self._parent:
        self._parent.detach(self)
        self._parent = None

    # Attach to new parent.
    self._parent = newp
    self._root_group.reparent(newp._root_group)
    if self._outline:
        # Avoid repositioning mess.
        self.plain_view()
        self.highlight()
l3Base.reparent = reparent

def reparent_to_root(self):
    # Graphics.
    self._run_reparent_to_root_hook()

    _, _, _, _, x1, y1 = self.i2w_affine(tuple(range(0,6)))
    self._root_group.reparent(self._canvas.root())
    _, _, _, _, nx1, ny1 = self.i2w_affine(tuple(range(0,6)))
    self._root_group.move(x1 - nx1, y1 - ny1)

    # State.
    self._parent = None
l3Base.reparent_to_root = reparent_to_root

#
#**        Selection handling
def plain_view(self):
    if not self._outline:
        return
    destroy_if_last(self._outline)
    self._outline = None
l3Base.plain_view = plain_view

def highlight(self):
    if self._outline:
        return
    flush_events()

    # Draw outline.
    x1, y1, x2, y2 = self.get_bounds_world()
    pad = self.w_.cp_.highlight_padding
    x1 -= pad
    y1 -= pad
    x2 += pad
    y2 += pad

    ### self._outline = self._root_group.get_property("parent").add(
    self._outline = (self._canvas.root().add(
        canvas.CanvasLine,
        fill_color = "orange",
        points = [x1, y1,
                  x2, y1,
                  x2, y2,
                  x1, y2,
                  x1, y1,
                  ],
        width_units = self.w_.cp_.highlight_thickness,
        line_style = gtk.gdk.SOLID,
        cap_style = gtk.gdk.CAP_ROUND,
        join_style = gtk.gdk.JOIN_ROUND,
        smooth = True,
        spline_steps = 12,
        ))
    ### self._loutline.set_property("fill-color", 'beige')
    ### self._ltext.set_property("fill-color", 'beige')

    # Event handling.
    self._outline.connect("event", lambda *a: self.selection_popup_event(*a))
    self._outline.connect("event", lambda *a: self.selection_magnify_event(*a))
    pass
l3Base.highlight = highlight


def selection_popup_event(self, widget, event):
    if event.type == gtk.gdk.BUTTON_PRESS:
        if event.button == 3:
            self.selection_init_popup()
            self._selection_popup.popup(None, None, None,
                                         event.button, event.time)
            return True
    return False
l3Base.selection_popup_event = selection_popup_event


def selection_init_popup(self):
    # Prepare a popup menu including the original menu items (in
    # _selection_popup_list) plus any inserted via selection_prepend().

    new_menu = gtk.Menu()
    for item, conargs in self._selection_popup_list:

        # To insert menu items in new_menu, re-use existing widgets
        # after removing them from their old menu.
        if item.get_parent() == self._selection_popup:
            if self._selection_popup_cb.has_key(item):
                # Remove old event handler.
                item.disconnect(self._selection_popup_cb[item])
            self._selection_popup.remove(item)
        new_menu.append( item )

        # Connect new event handler.
        if conargs != None:
            # Callback arguments are (event name, handler)
            args = tuple(conargs)
            self._selection_popup_cb[item] = item.connect(*args)
        item.show()

    if self._selection_popup:
        self._selection_popup.destroy()

    self._selection_popup = new_menu
l3Base.selection_init_popup = selection_init_popup

def selection_magnify_event(self, item, event):
    if event.type == gtk.gdk.ENTER_NOTIFY:
        if self.w_.fluid_ref(trace_gui_events = False):
            print "selection_magnify_event"
        item.set_property("width_units",
                          self.w_.cp_.highlight_thickness *
                          self.w_.cp_.hover_magnification)
        return False

    elif event.type == gtk.gdk.LEAVE_NOTIFY:
        if self.w_.fluid_ref(trace_gui_events = False):
            print "selection_magnify_event"
        item.set_property("width_units",
                          self.w_.cp_.highlight_thickness)
        return False
l3Base.selection_magnify_event = selection_magnify_event

#*    l3Nested editing
class l3Nested(l3Base):
    pass

def __init__(self, w_, cnvs, l3tree, rentab):
    l3Base.__init__(self)

    # Display.
    self._root_group = cnvs.root().add(canvas.CanvasGroup)
    self._pup = CommonPopup(self)
    self.explicit_title = None

    # Rendering options.
    self._render_mode = rentab.get_state(l3tree)

    # Headerless state.
    self._header = None

    # Undecorated state.
    self._deco = None

    # Events
    self._root_group.connect("event", lambda *a: self.drag_event(*a))

    # movement state
    self.remember_x = None
    self.remember_y = None

    # Canvas registration.  Must preceed children's construction.
    cnvs.register_l3tree_pic(l3tree._id, self)
l3Nested.__init__ = __init__

#
#**        special display
def zoom(self, factor):
    return l3Base.zoom(self, factor)
l3Nested.zoom = zoom



#*    Placeholder
class Placeholder(l3Nested):
    pass

def __init__(self, w_, cnvs, tree_id, rentab):
    # l3 tree.
    l3tree = w_.state_.storage.load(tree_id)
    l3Nested.__init__(self, w_, cnvs, l3tree, rentab)

    # Bindings to keep. ### move to nested.__init__
    self.w_      = w_
    self._canvas = cnvs
    self._marker_points = {}            # marker -> point list
    self._l3tree = l3tree

    # Display elements.
    # # self._blank = self.draw_blank()
    self._blank = uInvisibleSymbol(w_, cnvs, self._root_group,
                                   action = lambda _: self.show_subtree())

    # Highlighting.
    self._outline = None

    # Item cross-referencing.
    self._marker = None
    self._container = None

    # Borders etc.
    # #     l3Nested.init_deco(self)
    self.init_header(rentab)
    self.init_deco(rentab)

    # Event handling.
    ## self._blank.connect("event", lambda *a: self.expand_event(*a))
Placeholder.__init__ = __init__

#*    TextEdit
class TextEdit:
    pass

def __init__(self, w_, l3parent, text, tag_table, cnvs, refpos = None):
    """
    Interface of `l3parent` must include:
        ed_new_text
        ed_restore_text
        finish_edit
        rebuild_tree

        get_bounds_world (if `refpos` is not given)
        l3parent._canvas


    Typical calling sequence from `other`:

    other.start_edit -> TextEdit() (te)

    te handles its own events; the two possible exits are:

    te.text_aborted -> other.ed_restore_text
                    -> other.finish_edit

    te.text_finished -> other.rebuild_tree(new_text)
                     -> other.ed_new_text(new_text)
                     -> other.finish_edit()
    """
    self.w_ = w_
    self._l3parent = l3parent
    self._text_orig = text
    self._canvas = cnvs
    if refpos == None:
        l1, t1, _, _ = l3parent.get_bounds_world()
    else:
        l1, t1 = refpos

    # Edit Buffer.
    self._edit_buff = buff = gtk.TextBuffer(table = tag_table)
    buff.set_text(text)
    buff.apply_tag(l3parent._canvas._font_size_tag, *buff.get_bounds() )

    buff.connect_after("insert-text",
                       lambda *a: self.re_tag_inserted_text(*a))

    # Edit View.
    self._view = gtk.TextView(buffer = buff)
    self._view.connect("key-press-event", lambda *a: self.on_keypress(*a))

    # Scrollbars.
    self._view_s = gtk.ScrolledWindow()
    self._view_s.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
    self._view_s.add(self._view)

    # Ok button.
    self._ok = gtk.Button(label = "Ok", stock = gtk.STOCK_OK)
    self._ok.connect("clicked", lambda *a: self.text_finished())

    # Cancel button.
    self._cancel = gtk.Button(label = "Cancel", stock = gtk.STOCK_CANCEL)
    self._cancel.connect("clicked", lambda *a: self.text_aborted())

    # Assembly.
    #   Top row.
    self._ass_r1 = gtk.HBox()
    self._ass_r1.pack_start(self._view_s, expand = True)
    #   Bottom row.
    self._ass_r2 = gtk.HBox()
    self._ass_r2.pack_start(self._ok, expand = True)
    self._ass_r2.pack_start(self._cancel, expand = True)
    #   Stack them
    self._ass_col = gtk.VBox()
    self._ass_col.pack_start(self._ass_r1, expand = True)
    self._ass_col.pack_start(self._ass_r2, expand = False)

    # Pop up edit window.
    #   Positioning is controlled by the window manager.
    self._window = gtk.Window()
    self._window.set_property("title", "edit expression")
    self._window.set_property("window-position", gtk.WIN_POS_MOUSE)
    self._window.connect("delete-event", lambda *a: self.text_aborted())
    self._window.set_default_size(350, 200)
    self._window.set_border_width(0)
    self._window.add(self._ass_col)
    main_win = cnvs.get_parent()
    while not isinstance(main_win, gtk.Window):
        main_win = main_win.get_parent()
    self._window.set_transient_for(main_win)
    self._window.show_all()
TextEdit.__init__ = __init__

def on_keypress(self, widget, event):
    # Escape.
    if event.keyval == key_Escape:
        self.text_aborted()
        return True

    # Ctrl-Return
    if event.state & gtk.gdk.CONTROL_MASK:
        if event.keyval == key_Return:
            self.text_finished()
            return True

    return False
TextEdit.on_keypress = on_keypress

def text_aborted(self):
    # Paired with text_start (focus event?)
    if self.w_.fluid_ref(trace_gui_events = False):
        print "\ntext_aborted"

    # Remove editor before adding new text.
    del self._edit_buff
    self._window.destroy()

    # Update text.
    self._l3parent.ed_restore_text()

    # Finish.
    self._l3parent.finish_edit()
TextEdit.text_aborted = text_aborted

def text_finished(self):
    # Paired with text_start (focus event?)
    if self.w_.fluid_ref(trace_gui_events = False):
        print "\ntext_finished"

    # Get text.
    buf = self._edit_buff
    new_text = buf.get_text(*buf.get_bounds())

    # Remove editor before adding new text.
    del self._edit_buff
    self._window.destroy()

    # Update text.
    self._l3parent.rebuild_tree(new_text)
    self._l3parent.ed_new_text(new_text)

    # Finish.
    self._l3parent.finish_edit()
TextEdit.text_finished = text_finished

def re_tag_inserted_text(self, buffer, iter, text, length):
    # Keep new text at proper size etc.
    iter_to = iter.copy()
    if not iter.backward_chars(length):
        return False

    if iter.has_tag(self._l3parent._canvas._font_size_tag):
        return False

    buffer.apply_tag(self._l3parent._canvas._font_size_tag, iter, iter_to )
    return False
TextEdit.re_tag_inserted_text = re_tag_inserted_text

#*    ExtProcConfirm
class ExtProcConfirm:
    """
    After running an external process (which may not return a useful
    status), provide
          Success
          Failed
    options, and a
          text area
    to attach any desired log information.
    """
    pass

def __init__(self, w_,  cnvs, answer_store, text = ""):
    # See also TextEdit.
    #
    # answer_store::
    #       an empty list, to be populated with the user's response,
    #       in the form [(status, log)]
    #
    self.w_ = w_
    self._answer_store = answer_store
    self._text_orig = text
    self._canvas = cnvs

    # Edit Buffer.
    self._edit_buff = buff = gtk.TextBuffer(table = cnvs._common_tag_table)
    buff.set_text(text)
    buff.apply_tag(self._canvas._font_size_tag, *buff.get_bounds() )

    buff.connect_after("insert-text",
                       lambda *a: self.re_tag_inserted_text(*a))

    # Edit View.
    self._view = gtk.TextView(buffer = buff)
    self._view.connect("key-press-event", lambda *a: self.on_keypress(*a))
    self._view.set_size_request(w_.cp_.width,
                                w_.cp_.height) # Minimum viewport size.

    # Scrollbars.
    self._view_s = gtk.ScrolledWindow()
    self._view_s.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
    self._view_s.add(self._view)

    # Ok button.
    self._ok = gtk.Button(stock = gtk.STOCK_YES)
    self._ok.connect("clicked", lambda *a: self.cmd_success())

    # Failed button.
    self._failed = gtk.Button(stock = gtk.STOCK_NO)
    self._failed.connect("clicked", lambda *a: self.cmd_failed())

    # Assembly.
    #   Top row.
    self._ass_r1 = gtk.HBox()
    self._ass_r1.pack_start(self._view_s, expand = True)
    #   Bottom row.
    self._ass_r2 = gtk.HBox()
    self._ass_r2.pack_start(self._ok, expand = True)
    self._ass_r2.pack_start(self._failed, expand = True)
    #   Stack them
    self._ass_col = gtk.VBox()
    self._ass_col.pack_start(self._ass_r1, expand = True)
    self._ass_col.pack_start(self._ass_r2, expand = False)

    # Pop up edit window.
    #   Positioning is controlled by the window manager.
    self._window = gtk.Window()
    self._window.set_property("title", "User query")
    self._window.set_property("window-position", gtk.WIN_POS_MOUSE)
    self._window.connect("delete-event", lambda *a: self.cmd_failed())
    self._window.set_border_width(0)
    self._window.add(self._ass_col)
    main_win = cnvs.get_parent()
    while not isinstance(main_win, gtk.Window):
        main_win = main_win.get_parent()
    self._window.set_transient_for(main_win)
    self._window.show_all()
ExtProcConfirm.__init__ = __init__

def on_keypress(self, widget, event):
    # Escape.
    if event.keyval == key_Escape:
        self.cmd_failed()
        return True

    # Ctrl-Return
    if event.state & gtk.gdk.CONTROL_MASK:
        if event.keyval == key_Return:
            self.cmd_success()
            return True

    return False
ExtProcConfirm.on_keypress = on_keypress

def cmd_failed(self):
    # Remove editor before adding new text.
    del self._edit_buff
    self._window.destroy()

    # Finish.
    self._answer_store.append( (1, "") )     # (status, log)
ExtProcConfirm.cmd_failed = cmd_failed

def cmd_success(self):
    # Get text.
    buf = self._edit_buff
    new_text = buf.get_text(*buf.get_bounds())

    # Remove editor before adding new text.
    del self._edit_buff
    self._window.destroy()

    # Finish.
    if new_text == self._text_orig:
        self._answer_store.append( (0, "success") ) # (status, log)
    else:
        self._answer_store.append( (0, new_text) ) # (status, log)
ExtProcConfirm.cmd_success = cmd_success

def re_tag_inserted_text(self, buffer, iter, text, length):
    # Keep new text at proper size etc.
    iter_to = iter.copy()
    if not iter.backward_chars(length):
        return False

    if iter.has_tag(self._canvas._font_size_tag):
        return False

    buffer.apply_tag(self._canvas._font_size_tag, iter, iter_to )
    return False
ExtProcConfirm.re_tag_inserted_text = re_tag_inserted_text

#*    CommonPopup
class CommonPopup:
    # Add-on for classes supporting the standard popups.

    def verify_interface(self, prim_node):
        # An interface base class check will not verify actual presence of
        # needed members.  This function does.
        #
        # However, constructors may not construct these in time for
        # this check...
        assert prim_node._root_group
        assert prim_node.destroy
        assert prim_node._l3tree
        assert prim_node.w_
        assert prim_node.mark_as


def destroy_selected(self, node):
    sel = node.w_.selector
    items = sel.get_selection_list()
    sel.unselect()
    [self.start_destroy(itm) for itm in items]
CommonPopup.destroy_selected = destroy_selected


def start_destroy(self, node):
    w_ = node.w_

    # Hide the tree.
    node._root_group.hide()

    # Delete in steps.
    def body():
        node.destroy()
        node._l3tree.delete(w_.state_.storage)

    w_.with_fluids(body,
                   detach_l3_tree = False,
                   insert_l3_tree = False,
                   )
    pass
CommonPopup.start_destroy = start_destroy

def _insert_list(self, shape = 'list'):
    # Setup.
    from l3gui.l3canvas import RenderTable
    storage = self._prim_node.w_.state_.storage
    cnvs = self._prim_node._canvas

    # Get the values in the current calling context (path must pass
    # multiple selections), ignore None values.  
    if 1:
        # From selection_values_in_context
        tw = ast.TreeWork(storage)
        l3nd = self._prim_node.w_.selector.get_selection_list()

        # Self._prim_node is data source.
        cctxt = tw.get_call_ctxt(self._prim_node.getid())

        # Form the call graph pruning list.
        try: l3nd.remove(self._prim_node)
        except ValueError: pass

        # Get reduced calling context.
        pruned = tw.prune_cctxt(
            cctxt,
            map(l3Base.getid, l3nd))

        # Warn if context is not unique.
        # TODO.

        # Available info:   clone id, clone timestamp, value
        # (cid, storage.ie_.get_timestamp(cid), val)
        vals = [ val
                 for (cid, val) in tw.cctxt_leaves(pruned)
                 if val != None]

    # Use values of top-level node if nothing remains.
    if len(vals) == 0:
        vals = [val
                for (id_, val) in self._prim_node.get_values_list()
                if val != None]

    # For a list of length M, try a sqrt(M) * sqrt(M) layout.
    M = len(vals)
    if (M > 3) and shape == 'square':
        cols = int(floor(sqrt(M)))
        vals = [tuple(vals[ii : ii + cols]) for ii in range(0, M, cols)]

    # Convert to l3 ast
    val, val_id = \
         ast.val2ast( vals,
                      file_contents =
                      self._prim_node.w_.cp_.display_file_content)\
                      .setup(empty_parent(),
                             ast.Env('dummy_env', None, None, storage),
                             storage)
    val.set_outl_edges(self._prim_node.w_, None)

    # Special emphasis for this non-permanent value list.
    val.set_emphasis("valuelist")

    # Add label with source string, or id if not available.
    ptree = self._prim_node._l3tree
    desc = ptree.source_substring()
    if desc == 'no_source':
        val.set_label('Value(s) for %d' % self._prim_node._l3tree._id )
    else:
        val.set_label('Value(s) for %s' % desc )

    # Retain source node id for later.
    val.setthe(value_source_tree_id =
               [v.l3tree()._id for v in l3nd + [self._prim_node]])

    # Display the structure.
    pic = cnvs.add_l3tree(val, RenderTable())

    # Position to the right of expression.
    lp, tp, rp, bp = pic.get_bounds_world()
    lo, to, ro, bo = self._prim_node.get_bounds_world()
    pic.move(ro - lp, to - tp)
CommonPopup._insert_list = _insert_list


def __init__(self, prim_node):
    self._prim_node = prim_node
    self._widget_cb = {}
    self._widget = gtk.Menu()           # dummy
    #
    self._popup_list = [

        [ gtk.MenuItem("add comment"),
          ("activate", lambda *args: prim_node.deco_add_comment() )],

        [ gtk.MenuItem("copy to clipboard (as raw text)"),
          ("activate", lambda *args: prim_node.w_.selector.copy_selection())],

        [ gtk.MenuItem("delete"),
          ("activate", lambda *args: self.start_destroy(prim_node))],

        [ gtk.MenuItem("delete selected"),
          ("activate", lambda *args: self.destroy_selected(prim_node))],

        [ gtk.MenuItem("dump code"),
          ("activate",
           lambda *args: view.print_info(prim_node.w_.state_.storage,
                                         prim_node._l3tree))],
        [ gtk.MenuItem("dump values"),
          ("activate",
           lambda *args: prim_node.dump_values() ) ],

        [ gtk.MenuItem("export values as string"),
          ("activate",
           lambda *args: prim_node.string_export() ) ],

        # #     [ gtk.MenuItem("dump [values list]"),
        # #       ("activate",
        # #        lambda *args: pprint(prim_node.get_values_list()))],

        # #     [ gtk.MenuItem("dump (values list) as l3 AST"),
        # #       ("activate",
        # #        lambda *args: pprint(ast.val2ast(prim_node.get_values_list())) )],

        [ gtk.MenuItem("insert values as l3 list"),
          ("activate", lambda *args: self._insert_list() )],

        [ gtk.MenuItem("insert values as l3 list list "),
          ("activate", lambda *args: self._insert_list(shape = 'square') )],

        [ gtk.MenuItem("evaluate locally"),
          ("activate", lambda *args: prim_node.eval_local(args))],

        [ gtk.MenuItem("evaluate locally, use console"),
          ("activate", lambda *args: prim_node.eval_local_console(args))],

        [ gtk.MenuItem("exit"),
          ("activate", lambda *args : gtk.main_quit() )],

        [ gtk.MenuItem("hide"),
          ("activate", lambda *args: prim_node.hide_subtree() )],

        [ gtk.MenuItem("select value"),
          ("activate",
           lambda *args: DataSelPopup(prim_node.w_, prim_node._l3tree))],

        ]
    # Pop up when l3 node is pressed.
    prim_node._root_group.connect("event", lambda *a: self.popup_event(*a))
CommonPopup.__init__ = __init__

#
#**        pop-up base menu
def init_popup(self):
    # Start new Menu.
    new_menu = gtk.Menu()

    # Tear-off item.
    mi = gtk.TearoffMenuItem()
    new_menu.append(mi)
    mi.show()

    # Run pre_popup_hook, if any.
    getattr(self._prim_node, "pre_popup_hook", lambda : None)()

    # Populate the Menu.
    for mi, conargs in self._popup_list:
        # Insert in new menu, re-using existing widgets.
        if mi.get_parent() == self._widget:
            if self._widget_cb.has_key(mi):
                mi.disconnect(self._widget_cb[mi]) # remove old event handler
            self._widget.remove(mi)
        new_menu.append( mi )
        # Connect new event handler.
        if conargs != None:
            self._widget_cb[mi] = mi.connect(*conargs)
        mi.show()
    #
    if self._widget:
        self._widget.destroy()
    #
    self._widget = new_menu
CommonPopup.init_popup = init_popup

def popup_event(self, widget, event):
    # Pop up on button-3 press.
    if event.type == gtk.gdk.BUTTON_PRESS:
        if event.button == 3:
            self.init_popup()
            self._widget.popup(None, None, None,
                               event.button, event.time)
            return True
    return False
CommonPopup.popup_event = popup_event

#
#**        menu additions
def prepend(self, item_init):
    # Menu additions can be made at any time.
    #
    # item_init  has form
    #        [ gtk.MenuItem("dump code"),
    #           ("activate",
    #            lambda *args: view.print_info(prim_node.w_.state_.storage,
    #                                          prim_node._l3tree))]
    #   or
    #        [gtk.SeparatorMenuItem(), None]
    #
    self._popup_list.insert(0, item_init)
CommonPopup.prepend = prepend

#*    DataSelPopup
class DataSelPopup:
    pass


def __init__(self, w_, l3tree):
    self.w_ = w_
    self._l3tree = l3tree
    self._widget_cb = {}
    self._widget = gtk.Menu()           # dummy

    self.init_popup()

    self._widget.popup(None, None, None, 1, 1)
                       ## event.button, event.time)
DataSelPopup.__init__ = __init__


def traverse_values_list(self):
    # Produce a (menuitem, function) list for datum selection.
    st = self.w_.state_.storage
    c_id = self._l3tree._id

    # Dynamic id(s).
    clone_l = st.get_attribute(c_id, "interp_clone")
    if clone_l:
        G.logger.info("%d has %d clones.\nValues:" % (c_id, len(clone_l)))
        # todo: this should recurse?
        for id in clone_l:
            # todo: use loop (var, value) pairs, not just id
            yield [ gtk.MenuItem("clone %d" % id),
                    ("activate",
                     lambda *args: DataSelPopup(self.w_, st.load(id)))]
    else:
        # Toplevel or final id.
        yield [ gtk.MenuItem("value operations"),
                ("activate",
                 # todo: ValueOperPopup
                 lambda *args: DataSelPopup(self.w_, self._l3tree))]
DataSelPopup.traverse_values_list = traverse_values_list


def init_popup(self):
    # Start new Menu.
    new_menu = gtk.Menu()

    # Tear-off item.
    mi = gtk.TearoffMenuItem()
    new_menu.append(mi)
    mi.show()

    # Populate the Menu.
    for mi, conargs in self.traverse_values_list():
        # Insert in new menu, re-using existing widgets.
        if mi.get_parent() == self._widget:
            if self._widget_cb.has_key(mi):
                mi.disconnect(self._widget_cb[mi]) # remove old event handler
            self._widget.remove(mi)
        new_menu.append( mi )
        # Connect new event handler.
        if conargs != None:
            self._widget_cb[mi] = mi.connect(*conargs)
        mi.show()
    # Remove duplicate popups.
    if self._widget:
        self._widget.destroy()
    #
    self._widget = new_menu
DataSelPopup.init_popup = init_popup



#*    l3Loop
# This construct is a MACRO using other l3 constructs.
# Current restrictions:
#   - no editing of the loop name
#     a. requires synchronized replacements
#     b. but then, no customizable loop template is possible
#     c. so custom editing support is required
#
class l3Loop(l3Nested):
    pass

def __init__(self, w_, cnvs, tree_id, rentab):
    # layout:
    # loop CALL from FROMARGS
    #      BODY
    #
    # l3 tree.
    l3tree = w_.state_.storage.load(tree_id)
    assert isinstance(l3tree, ast.Loop)
    l3Nested.__init__(self, w_, cnvs, l3tree, rentab)

    # Bindings to keep.
    self.w_      = w_
    self._canvas = cnvs
    self._l3tree = l3tree

    # Highlighting.
    self._outline = None

    # Item cross-referencing.
    self._marker = None
    self._container = None

    # # self._marker_points = {}            # marker -> point list

    #
    # Create display elements.
    #
    #       loop
    d_loop = Label(w_, cnvs, self._root_group, "loop")

    #       CALL
    #
    ### Editing this tree requires updates to two other trees and is
    ### deferred for now.
    d_call = cnvs.add_l3tree(l3tree.l_view_call, rentab)
    d_call.reparent(self)

    #       from
    d_from = Label(w_, cnvs, self._root_group, "from")

    #       FROMARGS
    d_from_args = l3aList(w_, cnvs,
                          l3tree.l_from_args[0],
                          rentab,
                          root_group = self._root_group,
                          draggable = False,
                          parent = self,
                          )

    #       BODY
    d_body = l3aList(w_, cnvs,
                     l3tree.l_body[0],
                     rentab,
                     root_group = self._root_group,
                     draggable = False,
                     parent = self,
                     )

    # Marker(s).
    #   Only editing of the macro leaves makes sense.  This implies
    #   that the macro itself cannot be edited, greatly simplifying
    #   the code.

    # Inform obj of marker.
    d_call.setup_marker(None, self)     ### need marker substitute.

    # Bindings to keep.
    # Indexed access.  Match l3tree indexing where applicable.
    self._d_elements = [
        # l3 ast.
        d_call,
        d_from_args,
        d_body,

        # Rest
        d_loop,
        d_from,
        ]
    self._update_refs()

    #
    # Align display elements, starting from d_loop.
    #
    self._align_display()

    # Event handling.

    # Borders etc.
    self.init_header(rentab)
    self.init_deco(rentab)

    pass
l3Loop.__init__ = __init__

def _update_refs(self):
    (self._d_call,
     self._d_from_args ,
     self._d_body  ,

     self._d_loop  ,
     self._d_from,
     ) = self._d_elements
l3Loop._update_refs = _update_refs

def _align_display(self):
    #
    # Align display elements, starting from d_loop.
    #
    flush_events()

    [d_call,
     d_from_args,
     d_body,

     d_loop,
     d_from,
     ] = self._d_elements
    #   d_call
    l1, t1, r1, b1 = d_loop.get_bounds()
    l2, t2, r2, b2 = d_call.get_bounds()
    d_call.move(r1 - l2, t1 - t2)

    #   d_from
    l1, t1, r1, b1 = d_call.get_bounds()
    l2, t2, r2, b2 = d_from.get_bounds()
    d_from.move(r1 - l2, t1 - t2)

    #   d_from_args
    l1, t1, r1, b1 = d_from.get_bounds()
    l2, t2, r2, b2 = d_from_args.get_bounds()
    d_from_args.move(l1 - l2, b1 - t2)

    #   d_body
    l1, t1, r1, b1 = d_call.get_bounds()
    l2, t2, r2, b2 = d_from_args.get_bounds()
    l3, t3, r3, b3 = d_body.get_bounds()
    # Should really use the lowest bound of (loop CALL from FROMARGS)...
    d_body.move(l1 - l3, max(b1, b2) - t3)
    pass
l3Loop._align_display = _align_display


def new_size_for(self, child):
    self._align_display()

    # Refresh decoration.
    self.refresh_deco()

    # Propagate.
    if self._parent:
        self._parent.new_size_for(self)
    return
l3Loop.new_size_for = new_size_for

def destroy(self):
    l3Nested.destroy(self)

    _safed(self._d_loop)
    _safed(self._d_from)
    _safed(self._d_call)
    _safed(self._d_from_args)
    _safed(self._d_body)
    ### self._l3tree.delete(self.w_.state_.storage)
    l3Nested.destroy_deco(self)
l3Loop.destroy = destroy


#*    marker / label pair
# Labels are the second type loosely connected to markers (display
# items being the other).  As for the marker/display connection,
# separate lists are used to keep each, instead of one list holding
# tuples.

# Markers and Labels may move independently -- so they are not in a
# canvasgroup.

# class MarkerLabel:
#     __slots__ = ['marker', 'label']

#*    l3aList
class l3aList(Selectable):
    ''' An l3aList is the actual display of aList content.  All
    entries are stacked vertically.

    Every entry may be removed by dragging it out of the list.

    New entries are added by pasting (mouse-2) on an item separator.
    '''
    pass


def l3tree(self):
    return self._l3tree
l3aList.l3tree = l3tree

#
#**        init
def __init__(self, w_, cnvs, l3tree, rentab,
             root_group = None,
             draggable = True,
             parent = None,
             insertion_on_markers = True,
             hide_markers = False,
             ):
    assert isinstance(l3tree, ast.aList)
    self._insertion_on_markers = insertion_on_markers
    self._hide_markers = hide_markers
    self._parent = parent
    self._canvas = cnvs
    self.w_ = w_
    self._l3tree = l3tree
    self._destroy_hook = []             # (func -> None) list
    self._reparent_to_root_hook = []    # (func -> None) list
    self._dlist = []                    # child element list
    self._marker_lst = []               # (marker list)
    self._mark_ = 0                     # (int) current mark position

    # Set up graphical base.
    if root_group:
        self._root_group = (root_group.add(canvas.CanvasGroup))
    else:
        self._root_group = (cnvs.root().add(canvas.CanvasGroup))

    if draggable:
        self._root_group.connect("event", lambda *a: self.drag_event(*a))

    # Canvas registration.  Must preceed children's construction.
    cnvs.register_l3tree_pic(self._l3tree._id, self)

    # Set up nested elements.
    #     The marker entry preceeds the list entry.
    self._dlist = [cnvs.add_l3tree(child, rentab) for child in l3tree.entries()]
    for itm in self._dlist:
        itm.reparent(self)

    if hide_markers:
        self._marker_lst = [ self.draw_marker(0, 0, hidden_color = 'white')]+\
                           [ self.draw_marker(0, 0, hidden_color = 'white') 
                             for _ in l3tree.entries()]
    else:
        self._marker_lst = [ self.draw_marker(0, 0)]+\
                           [ self.draw_marker(0, 0) for _ in l3tree.entries()]

    # Insert leader on left to force proper bounding box of self in parent.
    self._leader = self._root_group.add(
        canvas.CanvasLine,
        fill_color = 'white',
        first_arrowhead = 0,
        last_arrowhead = 0,
        width_pixels = 1,
        points = [ 0,0, self.w_.cp_.label_indent,0 ],
        )

    # Position elements for bounding box correctness.  See also .insert().
    flush_events()
    vpad = w_.cp_.marker_padding
    if self._l3tree.getthe('layout') == 'horizontal':
        self._adj_h(self.w_.cp_.label_indent, 0, self._marker_lst, self._dlist)
    else:
        self._adj_v(self.w_.cp_.label_indent, 0, self._marker_lst, self._dlist)

    # Marker menu.
    self._marker_widget_cb = {}         # menu item -> connection args
    self._marker_widget = gtk.Menu()    # dummy
    self._marker_popup_list = ([
        # args are (menuitem, marker index) (for now)
        [ gtk.MenuItem("evaluate locally"),
          ("activate", lambda *args: self.marker_eval_local(args))],

        [ gtk.MenuItem("set mark"),
          ("activate", lambda *args: self.marker_set_mark(*args))],

        [ gtk.MenuItem("select region (from here to mark)"),
          ("activate", lambda *args: self.marker_select_region(*args))],

        ])

    # Insert spacer under last marker to force proper bounding box of
    # self in parent. 
    self._spacer_bottom = self._root_group.add(
        canvas.CanvasLine,
        fill_color = 'white',
        first_arrowhead = 0,
        last_arrowhead = 0,
        width_pixels = 1,
        points = [ 0, 0, self.w_.cp_.marker_width, 0],
        )
    self._mv_spacer()

    # Event handling.
    if insertion_on_markers:
        for marker in self._marker_lst:
            self.bind_marker_events(marker)
l3aList.__init__ = __init__

def _mv_spacer(self):
    flush_events()    
    spcr = self._spacer_bottom
    spcr.lower_to_bottom()
    lm, tm, rm, bm = marker_get_bounds(self.w_, self._marker_lst[-1]).pad_tb()
    ls, ts, rs, bs = spcr.get_bounds()
    spcr.move(lm - ls, bm - ts)
l3aList._mv_spacer = _mv_spacer

def _adj_v(self, ax, ay, newmarker, newobj):
    # Position child elements for vertical list.

    # Move marker.
    newmarker[0].move(ax, ay)

    if newobj:
        # Inform obj of marker
        newobj[0].setup_marker(newmarker[0], self)

        # Move object.
        ml, _, _, mb = marker_get_bounds(self.w_, newmarker[0]).pad_tb()
        ol, ot, _, ob = newobj[0].get_bounds()
        newobj[0].move(ml - ol, mb - ot)

        # Move following markers and objects
        self._adj_v(ml,
                    mb + (ob - ot), # + vpad,
                    newmarker[1:], newobj[1:])
l3aList._adj_v = _adj_v


def _adj_h(self, ax, ay, newmarker, newobj):
    # Position child elements for horizontal list.

    # Move marker.
    newmarker[0].move(ax, ay)

    if newobj:
        # Inform obj of marker
        newobj[0].setup_marker(newmarker[0], self)

        # Move object.
        ml, mt, mr, mb = marker_get_bounds(self.w_, newmarker[0]).pad_lr()
        ol, ot, or_, ob = newobj[0].get_bounds()
        newobj[0].move(mr - ol, mt - ot)

        # Move following markers and objects
        self._adj_h(mr + (or_ - ol), # + hpad,
                    mt,
                    newmarker[1:], newobj[1:])
l3aList._adj_h = _adj_h

#
#**        TextEdit interface
def rebuild_tree(self, text):
    pass
l3aList.rebuild_tree = rebuild_tree

#
#**        marker event handlers

def marker_set_mark(self, menuitem, index):
    self._mark_ = index
l3aList.marker_set_mark = marker_set_mark

def marker_select_region(self, menuitem, index):
    self.verify_mark()
    # Order range.
    if index <= self._mark_:
        first, last = index, self._mark_
    else:
        first, last = self._mark_, index
    # Add items to selection.
    add = self.w_.selector.toggle_selection
    for ii in range(first, last):
        add(self._dlist[ii])
l3aList.marker_select_region = marker_select_region

def verify_mark(self):
    'Ensure the mark is a valid index.'
    mx = len(self._dlist)
    if self._mark_ > mx:    self._mark_ = mx
    if self._mark_ < 0:     self._mark_ = 0
l3aList.verify_mark = verify_mark


def bind_marker_events(self, marker):
    marker.connect("event", lambda *a: self.marker_handle_event(*a))
l3aList.bind_marker_events = bind_marker_events

def marker_handle_event(self, item, event):
    # 
    # Destroy item key sequence.
    # 
    if event.type == gtk.gdk._2BUTTON_PRESS:
        if event.button == 3:
            ### The whole list, all entries, or a single entry.
            pass
        return True
    
    if event.type == gtk.gdk.BUTTON_PRESS:
        #
        # Range selection.
        #
        if event.button == 1:
            if event.state & gtk.gdk.SHIFT_MASK:
                # shift-button-1: select range
                self.marker_select_region(None, self._marker_lst.index(item))
                return True

            else:
                # button-1: Start selection
                self.marker_set_mark(None, self._marker_lst.index(item))
                return True
        # 
        # Item insertion.
        # 
        if event.button == 2:
            if self.w_.fluid_ref(trace_gui_events = False):
                print "insert_event"
            if not self.w_.selector.have_selection():
                self.w_.ten_second_message("No node selected.")
                return True
            else:
                # Insert all selected nodes, retaining selection order.
                offset = self._marker_lst.index(item)
                for (index, node) in enumerate(self.w_.selector.
                                               get_selection_list()):
                    self.insert(offset + index, node)
                return True
        #
        # Marker popup menu.
        #
        if event.button == 3:
            self.marker_init_popup(self._marker_lst.index(item))
            self._marker_widget.popup(None, None, None,
                                      event.button, event.time)
            return True
    #
    # Marker magnification.
    #
    if event.type == gtk.gdk.ENTER_NOTIFY:
        item.set_property("width_units",
                          self.w_.cp_.marker_thickness *
                          self.w_.cp_.hover_magnification)
        return True

    if event.type == gtk.gdk.LEAVE_NOTIFY:
        # canvas quirk: A button click on the enlarged marker
        # qualifies as LEAVE_NOTIFY only... 
        gobject.timeout_add(
            100, lambda : item.set_property("width_units",
                                            self.w_.cp_.marker_thickness) )
        return True
    return False
l3aList.marker_handle_event = marker_handle_event


def marker_prepend(self, item_init):
    # Menu additions can be made at any time.
    #
    # item_init  has form
    #        [ gtk.MenuItem("dump code"),
    #           ("activate",
    #            lambda *args: view.print_info(prim_node.w_.state_.storage,
    #                                          prim_node._l3tree))]
    #   or
    #        [gtk.SeparatorMenuItem(), None]
    #
    self._marker_popup_list.insert(0, item_init)
l3aList.marker_prepend = marker_prepend

def marker_init_popup(self, marker_idx):
    # Prepare a popup menu including the original menu items (in
    # _marker_popup_list) plus any inserted via marker_prepend().
    #
    # The marker index is passed to the popup's handler.
    new_menu = gtk.Menu()
    for item, conargs in self._marker_popup_list:
        # Insert menu items in new menu, re-using existing widgets.
        if item.get_parent() == self._marker_widget:
            if self._marker_widget_cb.has_key(item):
                # remove old event handler
                item.disconnect(self._marker_widget_cb[item])
            self._marker_widget.remove(item)
        new_menu.append( item )
        # Connect new event handler.
        if conargs != None:
            # (event name, handler, marker_idx)
            args = tuple(list(conargs) + [marker_idx])
            self._marker_widget_cb[item] = item.connect(*args)
        item.show()

    if self._marker_widget:
        self._marker_widget.destroy()

    self._marker_widget = new_menu
l3aList.marker_init_popup = marker_init_popup

#
#**        marker functions
# # l3alist local marker tests
# from widgets import marker_get_bounds, marker_get_anchor
# ma = self.draw_marker(2,3)
# print marker_get_bounds(self.w_, ma)
# print marker_get_anchor(self.w_, ma)

def draw_marker(self, ulx, uly, hidden_color = None):
    def _hline():
        # Horizontal line (Canvas object).
        cp = self.w_.cp_
        vpad = self.w_.cp_.marker_padding
        _marker = self._root_group.add(
            canvas.CanvasLine,
            fill_color = hidden_color or cp.marker_color,
            points = [ulx,
                      uly + vpad,
                      ulx + cp.marker_width,
                      uly + cp.marker_height + vpad,
                      ],
            width_units = cp.marker_thickness,
            line_style = gtk.gdk.SOLID,
            cap_style = gtk.gdk.CAP_ROUND,
            join_style = gtk.gdk.JOIN_ROUND,
            smooth = False,
            spline_steps = 12,
            )
        return _marker
    def _vline():
        # Vertical line (Canvas object).
        cp = self.w_.cp_
        hpad = self.w_.cp_.marker_padding
        _marker = self._root_group.add(
            canvas.CanvasLine,
            fill_color = hidden_color or cp.marker_color,
            points = [ulx + hpad,
                      uly,
                      ulx + cp.tuple_marker_width + hpad,
                      uly + cp.tuple_marker_height,
                      ],
            width_units = cp.marker_thickness,
            line_style = gtk.gdk.SOLID,
            cap_style = gtk.gdk.CAP_ROUND,
            join_style = gtk.gdk.JOIN_ROUND,
            smooth = False,
            spline_steps = 12,
            )
        return _marker
    return { None : _hline,
             'vertical': _hline,
             'horizontal': _vline,
             }[self._l3tree.getthe('layout')]()
l3aList.draw_marker = draw_marker


def i2w_affine(self, tuple_):
    return self._root_group.i2w_affine(tuple_)
l3aList.i2w_affine = i2w_affine

def reparent_to_root(self):
    # Graphics.
    self._run_reparent_to_root_hook()

    _, _, _, _, x1, y1 = self.i2w_affine(tuple(range(0,6)))
    self._root_group.reparent(self._canvas.root())
    _, _, _, _, nx1, ny1 = self.i2w_affine(tuple(range(0,6)))
    self._root_group.move(x1 - nx1, y1 - ny1)

    # State.
    self._parent = None
l3aList.reparent_to_root = reparent_to_root


#*    generic marker functions
def marker_get_properties(marker):
    # Collect opaque info for marker_to_head/plain
    return marker.get_property("points")

def marker_get_anchor(w_, marker):
    x, y, u, v = marker.get_bounds()
    ### magic offset values...
    try:
        wid = marker.get_property("width_units") # get CURRENT setting
    except TypeError:
        wid = 0
    return (x + wid, y + wid)

class mBounds(tuple):
    def set(self, **kw):
        self.__dict__.update(kw)
        return self

def pad_tb(self):
    # Pad (marker) bounds on top/bottom.
    x, y, u, v = self
    vpad = self.w_.cp_.marker_padding
    return mBounds((x , y - vpad,
                    u , v + vpad)).set(w_ = self.w_)
mBounds.pad_tb = pad_tb

def pad_lr(self):
    # Pad (marker) bounds on left / right.
    x, y, u, v = self
    vpad = self.w_.cp_.marker_padding
    return mBounds((x - vpad, y, u + vpad, v)).set(w_ = self.w_)
mBounds.pad_lr = pad_lr

## mBounds((1,2, 3,4)).set(w_ = w_)
## mBounds((1,2, 3,4)).set(w_ = w_).pad_lr()

def marker_get_bounds(w_, marker):
    x, y, u, v = marker.get_bounds()
    ### magic offset values...
    try:
        wid = marker.get_property("width_units") # get CURRENT setting
    except TypeError:
        wid = 0
    return mBounds((x + wid, y + wid, u - wid, v - wid)).set(w_ = w_)

def marker_pad_tb(w_, bounds):
    # Pad (marker) bounds on top/bottom.
    x, y, u, v = bounds
    vpad = w_.cp_.marker_padding
    return (x , y - vpad, u , v  + vpad)

def marker_pad_lr(w_, bounds):
    # Pad (marker) bounds on left / right.
    x, y, u, v = bounds
    vpad = w_.cp_.marker_padding
    return (x - vpad, y, u + vpad, v)


#*    l3Rawtext
#**        init & misc.
class l3Rawtext(l3Base):
    pass

def text_and_outline(self):
    #
    #       Text content / format / scale
    #
    ## rendering speed up
    if 0:
        self._root_group.move(+1000,0)
    if isinstance(self, l3Comment):
        # Allow pango text markup for comments.
        self._ltext = misc.GtkObjProxy(self._root_group.add(
            canvas.CanvasText,
            anchor = gtk.ANCHOR_NORTH_WEST,
            x = 0,
            y = 0,
            size = self._font_size,
            scale = 1.0,
            scale_set = True,
            ## font = self._font_desc,
            size_set = True,
            markup = "\n".join(view.leading_lines(self._text_orig,
                                                  num_lines=10)), 
            ))
    else:
        self._ltext = misc.GtkObjProxy(self._root_group.add(
            canvas.CanvasText,
            anchor = gtk.ANCHOR_NORTH_WEST,
            x = 0,
            y = 0,
            size = self._font_size,
            scale = 1.0,
            scale_set = True,
            font = self._font_desc,
            size_set = True,
            text = "\n".join(view.leading_lines(self._text_orig, num_lines=10)),
            ))
    # .destroy order checks.
    self._under_destruction = False
    def _check_destroy_order(widget):
        if not self._under_destruction:
            # An exception here will not give a useful
            # traceback. (event handling interferes?)
            print (".destroy from external source.")
            import pdb; pdb.set_trace()
    self._ltext.connect("destroy", _check_destroy_order)

    if self._new_text_props != None:
        _set_props(self._ltext, self._new_text_props)
        self._new_text_props = None

    self._ltext.show()
    flush_events()
    x1, y1, x2, y2 = self._ltext.get_bounds() # world units
    # Reference size.
    self._ltext_size_1 = ((x2 - x1) * self._canvas._pixpu,
                          (y2 - y1) * self._canvas._pixpu)

    # Current scale size.
    self._ltext.set_property(
        "size",
        self.w_.cp_.font_size * self._canvas._abs_zoom * pango.SCALE)

    # Text events.
    self._ltext.connect("event", lambda *a: self.destroy_event(*a))
    self._ltext.connect("event", lambda *a: self.start_edit(*a))
    self._ltext.connect("event", lambda *a: self.drag_event(*a))

    # Text outline.
    #     The CanvasText has TRANSPARENT background; here, it
    #     needs to be opaque.
    x1, y1, x2, y2 = self._ltext.get_bounds() # world units
    pad = self.w_.cp_.textview.outline_padding / self._canvas._pixpu
    x1 -= pad
    y1 -= pad
    x2 += pad
    y2 += pad

    self._loutline = self._root_group.add(
        canvas.CanvasRect,
        x1 = x1, y1 = y1,
        x2 = x2, y2 = y2,
        fill_color = self._fill_color,
        outline_color = self._outline_color,        ### black
        width_pixels = self.w_.cp_.outline_width_normal,
        )

    self._loutline.lower(1)
    self._loutline.show()

    # Outline events.
    self._loutline.connect("event", lambda *a: self.drag_event(*a))

    # Ensure correct sizing.
    # #     flush_events()
    # #     self._root_group.move(-1000,0)
    ## rendering speed up
    if 0:
        self._root_group.move(-1000,0)
    flush_events()

def __init__(self, w_, cnvs, tree_id, rentab, render_only = []):
    l3Base.__init__(self)
    self._canvas = cnvs
    self.w_ = w_

    # Parameters
    self.init_params()

    # l3 tree.
    l3tree = w_.state_.storage.load(tree_id)
    assert isinstance(l3tree, (ast.astType, ast.aNone, ast.aList)), \
           ("Cannot display %s" % l3tree)
    if isinstance(l3tree, (ast.aNone, ast.aList)):
        warn_("displaying internal types as text; "
              "expect clobbered display for:")
        warn_(l3tree.get_infix_string(40))

    self._text_orig = l3tree.l3_repr_dedented()
    self._l3tree = l3tree
    self._subtrees = []                 # l3Base nodes.

    # Rendering options.
    self._render_mode = rentab.get_state(l3tree)

    # Item cross-referencing.
    self._marker = None
    self._container = None

    # Headerless state.
    self._header = None

    # Undecorated state.
    self._deco = None

    # Text edit state.
    self._new_text_props = None

    # Focus redirection.
    self._focus_dummy = cnvs.root().add(canvas.CanvasItem)
    self._focus_dummy.hide()
    self._focus_dummy.grab_focus()

    # Display Group
    self._root_group = cnvs.root().add(canvas.CanvasGroup)
    self._pup = CommonPopup(self)
    self._editor = None

    # Canvas registration.  Must preceed children's construction.
    cnvs.register_l3tree_pic(l3tree._id, self)

    # Display self.
    text_and_outline(self)


    # Display subtrees.
    (row_par, col_par), _ = l3tree.get_char_range()
    l_par, t_par, r_par, b_par = self._ltext.get_bounds()
    _, _, unit_y = self._canvas.font_wh()
    if render_only:
        do_subtrees = render_only
    else:
        do_subtrees = l3tree.subtrees1()

    for sub in do_subtrees:
        # # print "Now:", sub
        # Display elements.
        disp_ch = cnvs.add_l3tree(sub, rentab, force_text = True)
        disp_ch.reparent(self)
        l_ch, t_ch, r_ch, b_ch = disp_ch._ltext.get_bounds()

        # Align.
        #   Get row, column offset.
        (row_ch, col_ch), _ = sub.get_char_range()

        #   Move into character position.
        disp_ch.move((l_par - l_ch) + (col_ch - col_par),
                     ((t_par - t_ch) + (row_ch - row_par)) * unit_y)

        # Track for later.
        self._subtrees.append(disp_ch)

    # Highlight outline.
    self._outline = None

    # Register for zooming.
    cnvs.add_zoom_text(self)

    # Borders etc.
    self.init_header(rentab)
    self.init_deco(rentab)
    self._root_group.show()

    # movement state
    self.remember_x = None
    self.remember_y = None

    pass
l3Rawtext.__init__ = __init__


def pre_popup_hook(self):
    ''' Conditional popup menu additions.
    Additions for
        1. Possible file names
        2. Known Filepaths
    '''
    # 2. Known Filepaths
    import l3lang.external.xmipp_wrap as xm
    from l3gui.l3canvas import RenderTable
    def _display_img(fname):
        storage = self.w_.state_.storage
        tree, tid = ast.Native(fname).setup(
            empty_parent(),
            ast.Env('dummy_env', None, None, storage),
            storage)
        tree.set_outl_edges(self.w_, None)

        pic = self._canvas.add_l3tree(tree, RenderTable())
        lp, tp, rp, bp = pic.get_bounds_world()
        lo, to, ro, bo = self.get_bounds_world()
        pic.move(ro - lp, to - tp)

    if (isinstance(self._l3tree, (ast.String)) and
        self._l3tree.isfile() and 
        not getattr(self, '_popup_known_filepaths_added', False)):
        map(self._pup.prepend, [
            [gtk.SeparatorMenuItem(), None],

            [ gtk.MenuItem(".spi -> xmipp_show -sel <path>"),
              ( "activate",
                lambda *args: xm.show(sel = self._l3tree.py_string()))],

            [ gtk.MenuItem(".spi -> xmipp_show -vol <path>"),
              ( "activate",
                lambda *args: xm.show(vol = self._l3tree.py_string()))],

            [ gtk.MenuItem(".txt -> $EDITOR <path>"),
              ( "activate",
                lambda *args:
                os.system(os.path.expandvars("$EDITOR %s &" % # clean up...
                                             self._l3tree.py_string())))],

            [ gtk.MenuItem(".png -> view image"),
              ( "activate",
                lambda *args:
                _display_img(self._l3tree))],

            # The table display restructures a simple list; for file
            # content, this requires additions in file2ast, for every
            # file content type. 
            [ gtk.MenuItem(".hdf file -> insert as l3 list list"),
              ("activate", lambda *args: self._insert_contents(shape='square'))],

            [ gtk.MenuItem(".hdf file -> insert as l3 list"),
              ("activate", lambda *args: self._insert_contents() )],

            ])
        self._popup_known_filepaths_added = True

    # 1. Possible file names
    elif (isinstance(self._l3tree, (ast.String, ast.Symbol, ast.Member)) and
        not getattr(self, '_popup_file_paths_added', False)):
            # Get values.
            va_l = self.get_values_list()

            # Remove invalid elements.
            va_l = filter(lambda (id, va):
                          isinstance(va, (ast.String, ast.Symbol,
                                          types.StringType)) and (id,va),
                          va_l)
            # Is there any content?
            if len(va_l) > 0:
                map(self._pup.prepend, [
                    [gtk.SeparatorMenuItem(), None],
                    [ gtk.MenuItem(".any -> insert (file path) list"),
                      ( "activate", lambda *args: self.get_filepaths_list())],
                    [ gtk.MenuItem(".png -> insert (file path, image) list"),
                      ( "activate", lambda *args: self.filep_img_list())],
                    ])
                self._popup_file_paths_added = True
l3Rawtext.pre_popup_hook = pre_popup_hook

def _insert_contents(self, shape = 'list'):
    # See also CommonPopup._insert_list
    from l3gui.l3canvas import RenderTable
    storage = self.w_.state_.storage
    cnvs = self._canvas
    fname = self._l3tree

    # Convert to l3 ast
    val, val_id = misc.file2ast(fname, shape = shape)\
                  .setup(empty_parent(),
                         ast.Env('dummy_env', None, None, storage),
                         storage)
    val.set_outl_edges(self.w_, None)

    # Special emphasis for this non-permanent value list.
    val.set_emphasis("valuelist")
    ## val.set_label('File contents of %d' % self._l3tree._id )
    val.set_label('File contents of %s' % self._l3tree.py_string())

    # Form display.
    pic = cnvs.add_l3tree(val, RenderTable())

    # Position to the right of expression.
    lp, tp, rp, bp = pic.get_bounds_world()
    lo, to, ro, bo = self.get_bounds_world()
    pic.move(ro - lp, to - tp)
l3Rawtext._insert_contents = _insert_contents


def get_filepaths_list(self):
    ''' Examine all values associated with self, and display those
    denoting valid file paths as list.
    '''
    from l3gui.l3canvas import RenderTable
    storage = self.w_.state_.storage
    cnvs = self._canvas
    # Get values.
    va_l = self.get_values_list()

    # Remove invalid elements.
    va_l = filter(lambda (id, va):
                  isinstance(va, (ast.String, ast.Symbol,
                                  types.StringType)) and (id,va),
                  va_l)

    # Form "path/name" list.
    path_l = [("%s/%s" % (storage.load(id).dirpath(self.w_), va)).strip('/')
              for (id, va) in va_l]

    # Keep only valid path names.
    path_l = filter(lambda nm: os.path.exists(nm) and nm, path_l)

    # Get l3 list.
    #   This is a (very) simplified ast.val2ast(), using ast.Filepath
    #   instead of ast.String.
    #
    val, val_id = ast.List(ast.aList(map(ast.FilepathString, path_l))).setup(
        empty_parent(),
        ast.Env('dummy_env', None, None, storage),
        storage)
    val.set_outl_edges(self.w_, None)

    # Special emphasis for this non-permanent value list.
    val.set_emphasis("filelist")
    val.set_label('File path list for %s' % self._l3tree.py_string() )

    # Form and position display.
    pic = cnvs.add_l3tree(val, RenderTable())
    lp, tp, rp, bp = pic.get_bounds_world()
    lo, to, ro, bo = self.get_bounds_world()
    pic.move(ro - lp, to - tp)
l3Rawtext.get_filepaths_list = get_filepaths_list


def filep_img_list(self):
    ''' Examine all values associated with self, and display those
    denoting valid file paths as (name = <image>) list.
    '''
    from l3gui.l3canvas import RenderTable
    storage = self.w_.state_.storage
    cnvs = self._canvas
    # Get values.
    va_l = self.get_values_list()

    # Remove invalid elements.
    va_l = filter(lambda (id, va):
                  isinstance(va, (ast.String, ast.Symbol,
                                  types.StringType)) and (id,va),
                  va_l)

    # Form "path/name" list.
    path_l = [("%s/%s" % (storage.load(id).dirpath(self.w_), va)).strip('/')
              for (id, va) in va_l]

    # Keep only valid path names.
    path_l = filter(lambda nm: os.path.exists(nm) and nm, path_l)

    # Get l3 list.
    #   This is a (very) simplified ast.val2ast().
    #
    from l3lang.ast import Set, Tuple, aList, FilepathString, Native
    val, val_id = ast.List(ast.aList(
        map(lambda path : Set(Symbol(path), Native(FilepathString(path))),
            path_l)
        )).setup(
        empty_parent(),
        ast.Env('dummy_env', None, None, storage),
        storage)
    val.set_outl_edges(self.w_, None)

    # Special emphasis for this non-permanent value list.
    val.set_emphasis("filelist")
    val.set_label('File path list for `%s`' % self._l3tree.py_string() )

    # Form and position display.
    pic = cnvs.add_l3tree(val, RenderTable())
    lp, tp, rp, bp = pic.get_bounds_world()
    lo, to, ro, bo = self.get_bounds_world()
    pic.move(ro - lp, to - tp)
l3Rawtext.filep_img_list = filep_img_list


def init_params(self):
    # Parameter settings, localized to allow subclass overrides
    self._font_desc = self.w_.cp_.rawtext_font
    self._font_size = self.w_.cp_.font_size * 1.0 * pango.SCALE
    self._fill_color = "white"
    self._outline_color = 'white'
l3Rawtext.init_params = init_params

def ed_new_size(self, height):
# #     self._edit_frame.set_property( "height", height )
    pass
l3Rawtext.ed_new_size = ed_new_size


def zoom(self, factor):
    l3Base.zoom(self, factor)
    if self._ltext:                     # zooming while editing?
        self._ltext.set_property("size",
                                 self.w_.cp_.font_size * factor * pango.SCALE)
        ### flush_events()
        u1, v1, u2, v2 = self._ltext.get_bounds()
        ox, oy = self._ltext_size_1
        if ox == 0: ox = 1
        if oy == 0: oy = 1
        ppu = self._canvas._pixpu
        ratio_h = 1.0 * (u2 - u1) * ppu / ox
        ratio_v = 1.0 * (v2 - v1) * ppu / oy
        return (ratio_h, ratio_v)
    else:
        return factor, factor
l3Rawtext.zoom = zoom

def setup_marker(self, marker, container):
    self._marker = marker
    self._container = container
l3Nested.setup_marker = setup_marker
l3Rawtext.setup_marker = setup_marker

#
#**        events

#
#***            start_edit
def start_edit(self, _textview, event):
    if event.type == gtk.gdk._2BUTTON_PRESS:
        if event.button == 1:
            # To suppress editing of nested text, find first parent
            # expression that has a non-rawtext parent.
            def find_last_text_parent(nd):
                if nd._parent == None:
                    return nd
                else:
                    if isinstance(nd._parent, l3Rawtext):
                        return find_last_text_parent(nd._parent)
                    else:
                        return nd
            if isinstance(self, l3Comment):
                node = self
            else:
                node = find_last_text_parent(self)

            # Remove text display.
            node._new_text_props = _get_props(node._ltext, [], "x", "y")
            node._under_destruction = True
            node._ltext.destroy()
            node._ltext = None
            # ... and the outline.
            node._loutline.destroy()
            node._loutline = None
            #
            # Use new widget for editing.
            if isinstance(node, l3Rawtext):
                params = node.w_.cp_.textview
            else:
                params = node.w_.cp_.inline

            node._editor = TextEdit(node.w_,
                                    node,
                                    node._text_orig,
                                    node._canvas._common_tag_table,
                                    node._canvas,
                                    )
            node._editor._view.show()

            node._editor._view.grab_focus()
            node._under_destruction = "partial"
            return True
    return False
l3Rawtext.start_edit = start_edit


#*    l3List / l3Map  editing
class l3List(l3Nested):
    pass

class l3ViewList(l3List):
    pass

class l3Map(l3Nested):
    pass

#
#**        l3List.__init__
def __init__(self, w_, cnvs, tree_id, rentab):
    # l3 tree.
    l3tree = w_.state_.storage.load(tree_id)
    assert isinstance(l3tree, (ast.Map, ast.List, ast.Tuple))
    assert not isinstance(l3tree, (ast.cls_viewList))
    l3Nested.__init__(self, w_, cnvs, l3tree, rentab)

    #
    #***            Display elements.
    alist = l3aList(w_, cnvs,
                    l3tree[0],
                    rentab,
                    root_group = self._root_group,
                    draggable = False,
                    parent = self,
                    )
    #
    #***            Label.
    head = Label(w_, cnvs, self._root_group, l3tree.deco_title_text())

    #
    # Expander
    #
    expander = uVisibleSymbol(w_, cnvs, self._root_group,
                              action = lambda _: self.hide_subtree())

    # Align.
    #    Expander Head
    #             Alist
    flush_events()                      # Update bounding boxes
    LE, TE, RE, BE = expander.get_bounds()
    LH, TH, RH, BH = head.get_bounds()
    head.move(RE - LH, TE - TH)

    LA, TA, _, _ = alist.get_bounds()
    alist.move(LH - LA, BH - TA)

    # Events.

    # Bindings to keep.
    self.w_ = w_
    self._canvas = cnvs
    self._alist = alist
    self._head = head
    self._expander = expander
    self._l3tree = l3tree

    # Highlighting.
    self._outline = None

    # Item cross-referencing.
    self._marker = None
    self._container = None

    # Popup menu additions
    if isinstance(self, l3Map):
        map(self._pup.prepend, [
            # Specials.
            [ gtk.SeparatorMenuItem(), None],

            [ gtk.MenuItem("examine dir"),
              ( "activate", lambda *args: self.insert_view())],

            [ gtk.MenuItem("print working directory (pwd)"),
              ( "activate", lambda *args: self.print_pwd())],

            ])

    # Borders etc.
    self.init_header(rentab)
    self.init_deco(rentab)
    pass
l3List.__init__ = __init__
l3Map.__init__ = __init__
#
#**        Env inspection
def insert_view(self):
    from l3gui.l3canvas import RenderTable
    storage = self.w_.state_.storage
    # Get the information.
    content = self.get_values_list()[0]
    if content is None:
        content = []
    val, val_id = ast.val2ast(content)\
                 .setup(empty_parent(),
                        ast.Env('dummy_env', None, None, storage),
                        storage)
    val.set_outl_edges(self.w_, None)
    val.set_label("Env listing")
    # Form and position display.
    pic = self._canvas.add_l3tree(val, RenderTable())
    lp, tp, rp, bp = pic.get_bounds_world()
    lo, to, ro, bo = self.get_bounds_world()
    pic.move(ro - lp, to - tp)
l3Map.insert_view = insert_view

#
#**        pwd
def print_pwd(self):
    st = self.w_.state_.storage
    val = st.get_attribute(self._l3tree._id, 'interp_result')
    if isinstance(val, ast.Env):
        G.logger.message("Directory name is %s" % val.directory_name())
l3Map.print_pwd = print_pwd


#*    l3Set / l3If / l3Call
class l3Set(l3Nested):
    pass

class l3If(l3Nested):
    # Layout:
    #     if COND:
    #         YES
    #     else:
    #         NO
    pass


# ---------------------------------
class l3IfOutline(l3Nested):
    pass

# ---------------------------------
class l3IfLoop(l3Nested):
    # Layout:
    #   loop
    #       BODY
    #
    pass


def continue_stop_pt(self):
    lc, tc, rc, bc = self._d_loop.get_bounds_world()
    return lc + (rc - lc)/10, bc        ## parameters
l3IfLoop.continue_stop_pt = continue_stop_pt

def break_stop_pt(self):
    lc, tc, rc, bc = self.get_bounds_world()
    return rc + 4, bc                   ## parameters
l3IfLoop.break_stop_pt = break_stop_pt


# ---------------------------------
# todo: combine l3If, l3IfLoop and l3IfWhile
#
class l3IfWhile(l3Nested):
    # Layout:
    #   while COND
    #       BODY
    #
    pass

def continue_stop_pt(self):
    lc, tc, rc, bc = self._d_loop.get_bounds_world()
    return lc + (rc - lc)/10, bc        ## parameters
l3IfWhile.continue_stop_pt = continue_stop_pt

def break_stop_pt(self):
    lc, tc, rc, bc = self.get_bounds_world()
    return rc + 4, bc                   ## parameters
l3IfWhile.break_stop_pt = break_stop_pt


# ---------------------------------
class l3IfLoopContinue(l3Nested):
    pass
def continue_start_pt(self):
    lc, tc, rc, bc = self.get_bounds_world()
    return lc, (tc + bc) / 2
l3IfLoopContinue.continue_start_pt = continue_start_pt


# ---------------------------------
class l3IfLoopBreak(l3Nested):
    pass
def break_start_pt(self):
    lc, tc, rc, bc = self.get_bounds_world()
    return rc, (tc + bc) / 2
l3IfLoopBreak.break_start_pt = break_start_pt


# ---------------------------------
class l3Call(l3Nested):
    pass

#
#**        if_chooser
def l3if_chooser(w_, cnvs, tree_id, rentab):
    """
    Choose appropriate display class based on subtree structure.
    """
    from l3lang.ast import Call, Set, MarkerTyped, Symbol, Function, \
         aList, If, String, Return, Macro, aNone, Tuple
    l3tree = w_.state_.storage.load(tree_id)
    assert isinstance(l3tree, ast.If)

    ma = ast.Matcher()

    # todo: use rentab to distinguish compact/expanded form?

    # Traverse parents' displays to see if self is inside a loop.
    in_l3ifloop = False
    for l3pic in cnvs.displayed_parents(l3tree):
        if isinstance(l3pic, l3IfLoop):
            in_l3ifloop = True
            l3ifloop = l3pic
            break
#
#***            if in_l3ifloop:
    if in_l3ifloop:
        # loop branches?
        #     if COND:
        #         return LNAME(LARGS)
        #
        if ma.match(l3tree,
                    If(Marker('_'),
                       aList([Return(Call(MarkerTyped(Symbol('LNAME'),
                                                      Symbol('_')),
                                          MarkerTyped(Symbol('LARGS'),
                                                      aList([]))))]),
                       Marker('_'))):
            if ma['LNAME'] == l3ifloop._matcher['LNAME']:
                # loop `continue`.
                return l3IfLoopContinue(w_, cnvs, tree_id, rentab)
            else:
                # loop `break`.
                return l3IfLoopBreak(w_, cnvs, tree_id, rentab)

        # loop break?
        #     if COND:
        #         return ARGS
        #
        if ma.match(l3tree,
                    If(Marker('_'),
                       aList([Return(Marker('_'))]),
                       Marker('_'))):
            return l3IfLoopBreak(w_, cnvs, tree_id, rentab)


#
#***            loop form?
    # loop form?
    #     if "loop":
    #         def LNAME(LARGS):
    #             LBODY
    #         LNAME2(CARGS)
    if ma.match(l3tree,
                If(String('loop'),
                   aList([Set(MarkerTyped(Symbol('LNAME'), Symbol('symbol')),
                              Function(MarkerTyped(Symbol('LARGS'), aList([])),
                                       MarkerTyped(Symbol('LBODY'), aList([])))),
                          Call(MarkerTyped(Symbol('LNAME2'), Symbol('symbol')),
                               MarkerTyped(Symbol('CARGS'), aList([])))]),
                   Marker('_'))):
        if ma['LNAME'] != ma['LNAME2']:
            warn_("loop name mismatch; no special display")
            return l3If(w_, cnvs, tree_id, rentab)
        elif len(ma['LARGS']) != len(ma['CARGS']):
            warn_("arguments mismatched between loop definition and call")
            return l3If(w_, cnvs, tree_id, rentab)
        else:
            return l3IfLoop(w_, cnvs, tree_id, rentab, ma)

#
#***            while loop?
    # while loop?
    # if "while":
    #     def "_while_ID"():
    #         if not COND:
    #             return
    #         else:
    #             BODY
    #         return _while_ID()
    #     _while_ID()
    #
    elif ma.match(l3tree,
                  If(String('while'),
                     aList([Set(Marker('_'),
                                Macro(aList([]),
                                      aList([If(Call(Symbol('not'),
                                                     Marker('COND-PARENT')),
                                                aList([Return(Marker('_'))]),
                                                Marker('BODY')),
                                             Return(Call(Marker('_'),
                                                         aList([])))]))),
                            Call(Marker('_'),
                                 aList([]))]),
                     aList([]))):
        if ma.match(ma['COND-PARENT'], aList([Marker('COND')])):
            ma['COND-IDX'] = 0
            def get_cond():
                return ma['COND-PARENT'][  ma['COND-IDX'] ]
            ma.get_cond = get_cond
            return l3IfWhile(w_, cnvs, tree_id, rentab, ma)

#
#***            for loop?
    #
    # For display of
    #
    #     for V in SEQ:
    #         B
    #
    # identify V, SEQ, and B.  Only self and those are displayed.
    #
    # See also l3lang/reader.py and If.setup()
    #
    # print reader.parse('''
    # if "for":
    #     ! ITEMS = ! SEQ
    #     ! IDX = 0
    #     ! LEN = len(! ITEMS)
    #     # orig. for
    #     def "LOOP"():
    #         if ! IDX < ! LEN:
    #             ! IDX = ! IDX + 1
    #             # V in S.
    #             ! V = ! ITEMS[ ! IDX - 1 ]
    #             # Body B
    #             [ ! B ]
    #             # Iterate.
    #             return ! LOOP()
    #     ! LOOP()
    # ''')
    # The following pattern is from above, with manual fixes for
    #       def "LOOP"() [ Set(Symbol('LOOP') ...) -> Set(Marker('LOOP'),...)]
    # and
    #       Marker('B')  [ aList([Marker('B')])  -> Marker('B') ]
    #
    elif ma.match(l3tree,
                  If(String('for'), aList([Set(Marker('ITEMS'), Marker('SEQ')), Set(Marker('IDX'), Int(0)), Set(Marker('LEN'), Call(Symbol('len'), aList([Marker('ITEMS')]))), Set(Marker('LOOP'), Macro(aList([]), aList([If(Call(Symbol('<'), aList([Marker('IDX'), Marker('LEN')])), aList([Set(Marker('IDX'), Call(Symbol('+'), aList([Marker('IDX'), Int(1)]))), Set(Marker('V'), Call(Member(Symbol('operator'), Symbol('getitem')), aList([Marker('ITEMS'), Call(Symbol('-'), aList([Marker('IDX'), Int(1)]))]))), List(Marker('B')), Return(Call(Marker('LOOP'), aList([])))]), aList([]))]))), Call(Marker('LOOP'), aList([]))]), aList([]))):
        #
        v_parent = w_.state_.storage.load(ma['V']._parent)
        ma.get_current_V = (lambda :
                            v_parent[0])
        ma.set_V = ( lambda new:
                     v_parent.replace_child(ma.get_current_V()._id, new))
        seq_parent = w_.state_.storage.load(ma['SEQ']._parent)
        ma.get_current_SEQ = (lambda :
                              seq_parent[1])
        ma.set_SEQ = ( lambda new:
                       seq_parent.replace_child(ma.get_current_SEQ()._id, new))
        ma.get_B = lambda : ma['B']

        return l3IfFor(w_, cnvs, tree_id, rentab, ma)
        # #     return l3Rawtext(w_, cnvs, tree_id, rentab,
        # #                      render_only = [ma['V'], ma['SEQ'], ma['B']])

    else:
        # plain if
        return l3If(w_, cnvs, tree_id, rentab)

## paste
## wdgt.l3if_chooser = l3if_chooser

#
#**        init
def __init__(self, w_, cnvs, tree_id, rentab):
    # layout:
    #   Lhs = Rhs
    #
    # l3 tree.
    l3tree = w_.state_.storage.load(tree_id)
    assert isinstance(l3tree, ast.Set)
    l3Nested.__init__(self, w_, cnvs, l3tree, rentab)

    # Bindings to keep.
    self._marker_points = {}            # marker -> point list
    self.w_ = w_
    self._canvas = cnvs

    #
    # Display elements.
    #       Lhs
    d_lhs = cnvs.add_l3tree(l3tree[0], rentab)
    d_lhs.reparent(self)

    #       Equal
    d_equal = Label(w_, cnvs, self._root_group, " = ")

    #       Rhs
    d_rhs = cnvs.add_l3tree(l3tree[1], rentab)
    d_rhs.reparent(self)

    # Marker(s).
    d_lhs_marker = self.draw_marker()
    d_rhs_marker = self.draw_marker()
    d_lhs_marker.lower_to_bottom()
    d_rhs_marker.lower_to_bottom()

    # Inform obj of marker.
    d_lhs.setup_marker(d_lhs_marker, self)
    d_rhs.setup_marker(d_rhs_marker, self)

    # Bindings to keep.
    self._d_equal = d_equal
    self._d_lhs = d_lhs
    self._d_rhs = d_rhs
    self._d_lhs_marker = d_lhs_marker
    self._d_rhs_marker = d_rhs_marker
    self._l3tree = l3tree

    # Align.
    self._align_to_lhs()

    # Highlighting.
    self._outline = None

    # Item cross-referencing.
    self._marker = None
    self._container = None

    # Event handling.
    d_lhs_marker.connect("event", lambda *a: self.insert_event(*a))
    d_rhs_marker.connect("event", lambda *a: self.insert_event(*a))

    # Borders etc.
    self.init_header(rentab)
    self.init_deco(rentab)

    # Hide markers.
    d_lhs_marker.hide()
    d_rhs_marker.hide()
    pass
l3Set.__init__ = __init__

def __init__(self, w_, cnvs, tree_id, rentab, matcher):
    # l3 tree.
    l3tree = w_.state_.storage.load(tree_id)
    assert isinstance(l3tree, ast.If)
    l3Nested.__init__(self, w_, cnvs, l3tree, rentab)

    # Bindings to keep.
    self.w_      = w_
    self._canvas = cnvs
    self._marker_points = {}            # marker -> point list
    self._matcher = ma = matcher
    self._l3tree = l3tree
    self._deco_cb_buffer = []           # callback list for add_item()

    #
    # Display elements.
    d_loop = Label(w_, cnvs, self._root_group, "loop")
    d_body = l3aList(w_, cnvs,
                     ma['LBODY'],
                     rentab,
                     root_group = self._root_group,
                     draggable = False,
                     parent = self,
                     )

    # Marker(s).
    # #     d_cond_marker = self.draw_marker()
    # #     d_cond_marker.lower_to_bottom()

    # Inform obj of marker.
    # #     d_cond.setup_marker(d_cond_marker, self)

    #
    # Align with respect to d_loop.
    #
    flush_events()
    #
    ll,  tl, rl, bl = d_loop.get_bounds()
    lb, tb, rb, bb = d_body.get_bounds()
    d_body.move(ll - lb, bl - tb)

    #   COND marker for child replacement.
    # #     _, y, x, _ = d_if.get_bounds()
    # #     u, v, _, _ = d_cond_marker.get_bounds()
    # #     d_cond_marker.move(x - u + w_.cp_.exp_marker_hoff,
    # #                        y - v + w_.cp_.exp_marker_voff)

    # Bindings to keep.
    # Indexed access.  Match l3tree indexing where applicable.
    self._d_elements = [
        # l3 ast.
        d_loop,
        d_body,

        # Rest
    # #         d_else,
    # #         d_if,
    # #         d_cond_marker,
        ]
    self._update_refs()

    # Highlighting.
    self._outline = None

    # Item cross-referencing.
    self._marker = None
    self._container = None

    # Event handling.
    # #     d_cond_marker.connect("event", lambda *a: self.insert_event(*a))

    # Borders etc.
    self.init_header(rentab)
    self.init_deco(rentab)

    pass
l3IfOutline.__init__ = __init__

def __init__(self, w_, cnvs, tree_id, rentab, matcher):
    # l3 tree.
    l3tree = w_.state_.storage.load(tree_id)
    assert isinstance(l3tree, ast.If)
    l3Nested.__init__(self, w_, cnvs, l3tree, rentab)

    # Bindings to keep.
    self.w_      = w_
    self._canvas = cnvs
    self._marker_points = {}            # marker -> point list
    self._matcher = ma = matcher
    self._l3tree = l3tree
    self._deco_cb_buffer = []           # callback list for add_item()

    #
    # Display elements.
    d_loop = Label(w_, cnvs, self._root_group, "loop")
    d_body = l3aList(w_, cnvs,
                     ma['LBODY'],
                     rentab,
                     root_group = self._root_group,
                     draggable = False,
                     parent = self,
                     )

    # Marker(s).
    # #     d_cond_marker = self.draw_marker()
    # #     d_cond_marker.lower_to_bottom()

    # Inform obj of marker.
    # #     d_cond.setup_marker(d_cond_marker, self)

    #
    # Align with respect to d_loop.
    #
    flush_events()
    #
    ll,  tl, rl, bl = d_loop.get_bounds()
    lb, tb, rb, bb = d_body.get_bounds()
    d_body.move(ll - lb, bl - tb)

    #   COND marker for child replacement.
    # #     _, y, x, _ = d_if.get_bounds()
    # #     u, v, _, _ = d_cond_marker.get_bounds()
    # #     d_cond_marker.move(x - u + w_.cp_.exp_marker_hoff,
    # #                        y - v + w_.cp_.exp_marker_voff)

    # Bindings to keep.
    # Indexed access.  Match l3tree indexing where applicable.
    self._d_elements = [
        # l3 ast.
        d_loop,
        d_body,

        # Rest
    # #         d_else,
    # #         d_if,
    # #         d_cond_marker,
        ]
    self._update_refs()

    # Highlighting.
    self._outline = None

    # Item cross-referencing.
    self._marker = None
    self._container = None

    # Event handling.
    # #     d_cond_marker.connect("event", lambda *a: self.insert_event(*a))

    # Borders etc.
    self.init_header(rentab)
    self.init_deco(rentab)

    pass
l3IfLoop.__init__ = __init__

def _update_refs(self):
    (self._d_loop,
     self._d_body ,
     ) = self._d_elements
l3IfLoop._update_refs = _update_refs


def __init__(self, w_, cnvs, tree_id, rentab, matcher):
    # l3 tree.
    l3tree = w_.state_.storage.load(tree_id)
    assert isinstance(l3tree, ast.If)
    l3Nested.__init__(self, w_, cnvs, l3tree, rentab)

    # Bindings to keep.
    self.w_      = w_
    self._canvas = cnvs
    self._marker_points = {}            # marker -> point list
    self._matcher = ma = matcher
    self._l3tree = l3tree
    self._deco_cb_buffer = []           # callback list for add_item()

    #
    # Display elements.
    d_loop = Label(w_, cnvs, self._root_group, "while")

    d_cond = cnvs.add_l3tree(ma.get_cond(), rentab)
    d_cond.reparent(self)

    d_body = l3aList(w_, cnvs,
                     ma['BODY'],
                     rentab,
                     root_group = self._root_group,
                     draggable = False,
                     parent = self,
                     )

    # Marker(s).
    d_cond_marker = self.draw_marker()
    d_cond_marker.lower_to_bottom()

    # Inform obj of marker.
    d_cond.setup_marker(d_cond_marker, self)

    #
    # Align with respect to d_loop.
    #
    flush_events()
    #
    llo, tlo, rlo, blo = d_loop.get_bounds()
    lco, tco, rco, bco = d_cond.get_bounds()
    d_cond.move(rlo + w_.cp_.loop_cond_sep - lco, tlo - tco)

    lco, tco, rco, bco = d_cond.get_bounds()
    lbo, tbo, rbo, bbo = d_body.get_bounds()
    d_body.move(lco - lbo, bco - tbo)

    #   COND marker for child replacement.
    lco, tco, rco, bco = d_cond.get_bounds()
    lma, tma,   _,   _ = d_cond_marker.get_bounds()
    d_cond_marker.move(lco - lma + w_.cp_.exp_marker_hoff,
                       tco - tma + w_.cp_.exp_marker_voff)

    # Bindings to keep.
    # Indexed access.  Match l3tree indexing where applicable.
    self._d_elements = [
        # l3 ast.
        d_cond,
        d_body,

        # Rest
        d_loop,
        d_cond_marker,
        ]
    self._update_refs()

    # Highlighting.
    self._outline = None

    # Item cross-referencing.
    self._marker = None
    self._container = None

    # Event handling.
    d_cond_marker.connect("event", lambda *a: self.insert_event(*a))

    # Borders etc.
    self.init_header(rentab)
    self.init_deco(rentab)

    pass
l3IfWhile.__init__ = __init__

def _update_refs(self):
    (self._d_cond,
     self._d_body,

     self._d_loop,
     self._d_cond_marker,
     ) = self._d_elements
l3IfWhile._update_refs = _update_refs



def __init__(self, w_, cnvs, tree_id, rentab):
    # l3 tree.
    l3tree = w_.state_.storage.load(tree_id)
    assert isinstance(l3tree, ast.If)
    l3Nested.__init__(self, w_, cnvs, l3tree, rentab)

    # Bindings to keep.
    self.w_      = w_
    self._canvas = cnvs
    self._marker_points = {}            # marker -> point list
    self._l3tree = l3tree

    #
    # Display elements.
    self._d_label = Label(w_, cnvs, self._root_group, "break")

    # Flow edge drawing.
    #   The actual line is drawn after the enclosing l3IfLoop is
    #   complete.
    def search_l3ifloop():
        for l3pic in cnvs.displayed_parents(l3tree):
            if isinstance(l3pic, l3IfLoop):
                return l3pic
        return None
    ifloop = search_l3ifloop()

    if ifloop is not None:
        def body():
            return BreakLine(w_, cnvs, self, ifloop)
        ifloop._deco_cb_buffer.append(body)
        # The line must not be drawn after self (or ifloop) is
        #   deleted, or self (or an intermediate tree containing self)
        #   is detached from ifloop.
        wref_ifloop = weakref.ref(ifloop)
        def rm_linedraw():
            if wref_ifloop() is None: return
            try:
                wref_ifloop()._deco_cb_buffer.remove(body)
            except ValueError:          # Body removed already.
                pass
        self.add_reparent_to_root_hook(rm_linedraw)
        # Parents up to ifloop (excluded).
        tw = ast.TreeWork(self.w_.state_.storage)
        for par in tw.all_parents(l3tree):
            if par is ifloop._l3tree: break
            l3pic = cnvs._nodes.get(par._id)
            if l3pic is not None:
                l3pic.add_reparent_to_root_hook(rm_linedraw)

    # Highlighting.
    self._outline = None

    # Item cross-referencing.
    self._marker = None
    self._container = None

    # Event handling.
    # #     d_cond_marker.connect("event", lambda *a: self.insert_event(*a))

    # Borders etc.
    self.init_header(rentab)
    self.init_deco(rentab)

    pass
l3IfLoopBreak.__init__ = __init__

def __init__(self, w_, cnvs, tree_id, rentab):
    # l3 tree.
    l3tree = w_.state_.storage.load(tree_id)
    assert isinstance(l3tree, ast.If)
    l3Nested.__init__(self, w_, cnvs, l3tree, rentab)

    # Bindings to keep.
    self.w_      = w_
    self._canvas = cnvs
    self._marker_points = {}            # marker -> point list
    self._l3tree = l3tree

    #
    # Display elements.
    self._d_label = Label(w_, cnvs, self._root_group, "continue")

    # Flow edge drawing.
    #   The actual line is drawn after the enclosing l3IfLoop is
    #   complete.
    def search_l3ifloop():
        for l3pic in cnvs.displayed_parents(l3tree):
            if isinstance(l3pic, l3IfLoop):
                return l3pic
        return None
    ifloop = search_l3ifloop()

    if ifloop is not None:
        def body():
            return ContinueLine(w_, cnvs, self, ifloop)
        ifloop._deco_cb_buffer.append(body)
        # The line must not be drawn after self (or ifloop) is
        #   deleted, or self (or an intermediate tree containing self)
        #   is detached from ifloop.
        wref_ifloop = weakref.ref(ifloop)
        def rm_linedraw():
            print "******** rm_linedraw: removed ContinueLine"
            if wref_ifloop() is None: return
            try:
                wref_ifloop()._deco_cb_buffer.remove(body)
            except ValueError:          # Body removed already.
                pass
        self.add_reparent_to_root_hook(rm_linedraw)
        # Parents up to ifloop (excluded).
        tw = ast.TreeWork(self.w_.state_.storage)
        for par in tw.all_parents(l3tree):
            if par is ifloop._l3tree: break
            l3pic = cnvs._nodes.get(par._id)
            if l3pic is not None:
                l3pic.add_reparent_to_root_hook(rm_linedraw)

    # Highlighting.
    self._outline = None

    # Item cross-referencing.
    self._marker = None
    self._container = None

    # Event handling.
    # #     d_cond_marker.connect("event", lambda *a: self.insert_event(*a))

    # Borders etc.
    self.init_header(rentab)
    self.init_deco(rentab)

    pass
l3IfLoopContinue.__init__ = __init__


def __init__(self, w_, cnvs, tree_id, rentab):
    # layout:
    #  +---------+
    #  |if||COND |
    #  +---------+
    #      +----------+
    #      | YES      |
    #      +----------+
    #  +----+
    #  |else|
    #  +----+
    #      +----------+
    #      | NO       |
    #      +----------+
    #
    #
    # l3 tree.
    l3tree = w_.state_.storage.load(tree_id)
    assert isinstance(l3tree, ast.If)
    l3Nested.__init__(self, w_, cnvs, l3tree, rentab)

    # Bindings to keep.
    self.w_      = w_
    self._canvas = cnvs
    self._marker_points = {}            # marker -> point list

    #
    # Display elements.
    #
    #       if
    d_if = Label(w_, cnvs, self._root_group, "If")

    #       COND
    d_cond = cnvs.add_l3tree(l3tree[0], rentab)
    d_cond.reparent(self)

    #       YES
    d_yes = l3aList(w_, cnvs,
                    l3tree[1],
                    rentab,
                    root_group = self._root_group,
                    draggable = False,
                    parent = self,
                    )

    #       else
    d_else = Label(w_, cnvs, self._root_group, "else")

    #       NO
    d_no = l3aList(w_, cnvs,
                   l3tree[2],
                   rentab,
                   root_group = self._root_group,
                   draggable = False,
                   parent = self,
                   )

    # Marker(s).
    d_cond_marker = self.draw_marker()
    d_cond_marker.lower_to_bottom()

    # Inform obj of marker.
    d_cond.setup_marker(d_cond_marker, self)

    #
    # Align with respect to IF.
    #
    flush_events()
    #   COND
    _, y, x, _ = d_if.get_bounds()
    u, v, _, _ = d_cond.get_bounds()
    d_cond.move(x - u, y - v)

    #   COND marker for child replacement.
    _, y, x, _ = d_if.get_bounds()
    u, v, _, _ = d_cond_marker.get_bounds()
    d_cond_marker.move(x - u + w_.cp_.exp_marker_hoff,
                       y - v + w_.cp_.exp_marker_voff)

    #   YES
    x, _, _, y = d_cond.get_bounds()
    u, v, _, _ = d_yes.get_bounds()
    d_yes.move(x - u, y - v)

    #   else
    x, _, _, _ = d_if.get_bounds()
    _, _, _, y = d_yes.get_bounds()
    u, v, _, _ = d_else.get_bounds()
    d_else.move(x - u, y - v)

    #   NO
    _, _, x, _ = d_if.get_bounds()
    _, _, _, y = d_else.get_bounds()
    u, v, _, _ = d_no.get_bounds()
    d_no.move(x - u, y - v)

    # Bindings to keep.
    # Indexed access.  Match l3tree indexing where applicable.
    self._d_elements = [
        # l3 ast.
        d_cond,
        d_yes,
        d_no,

        # Rest
        d_else,
        d_if,
        d_cond_marker,
        ]
    self._update_refs()

    self._l3tree = l3tree

    # Highlighting.
    self._outline = None

    # Item cross-referencing.
    self._marker = None
    self._container = None

    # Event handling.
    d_cond_marker.connect("event", lambda *a: self.insert_event(*a))

    # Borders etc.
    self.init_header(rentab)
    self.init_deco(rentab)

    pass
l3If.__init__ = __init__

def _update_refs(self):
    (self._d_cond,
     self._d_yes ,
     self._d_no  ,

     self._d_else,
     self._d_if  ,
     self._d_cond_marker,
     ) = self._d_elements
l3If._update_refs = _update_refs


def __init__(self, w_, cnvs, tree_id, rentab):
    # layout:
    #  +-----+
    #  |func | selector
    #  +-----+
    #  +----------+
    #  | args     |
    #  +----------+
    #
    # l3 tree.
    l3tree = w_.state_.storage.load(tree_id)
    assert isinstance(l3tree, ast.Call)
    l3Nested.__init__(self, w_, cnvs, l3tree, rentab)

    # Bindings to keep.
    self.w_      = w_
    self._canvas = cnvs
    self._marker_points = {}            # marker -> point list

    #
    # Display elements.
    #
    #       func
    d_func = cnvs.add_l3tree(l3tree[0], rentab)
    d_func.reparent(self)

    #       Selector
    d_selector = Label(w_, cnvs, self._root_group, " () ")

    #       args
    d_args = l3aList(w_, cnvs,
                     l3tree[1],
                     rentab,
                     root_group = self._root_group,
                     draggable = False,
                     parent = self,
                     )

    # Marker(s).
    d_func_marker = self.draw_marker()
    d_func_marker.lower_to_bottom()

    # Inform obj of marker.
    d_func.setup_marker(d_func_marker, self)

    #
    # Align with respect to FUNC.
    #
    flush_events()
    #   ARGS
    lf, tf, rf, bf = d_func.get_bounds()
    u, v, _, _ = d_args.get_bounds()
    d_args.move(lf - u, bf - v)

    #   selector
    ls, ts, rs, bs = d_selector.get_bounds()
    d_selector.move(rf - ls, tf - ts)

    #   FUNC marker for child replacement.
    x, y, _, _ = d_func.get_bounds()
    u, v, _, _ = d_func_marker.get_bounds()
    d_func_marker.move(x - u + w_.cp_.exp_marker_hoff,
                       y - v + w_.cp_.exp_marker_voff)

    # Bindings to keep.
    # Indexed access.  Match l3tree indexing where applicable.
    self._d_elements = [
        # l3 ast.
        d_func,
        d_args,

        # Rest
        d_func_marker,
        d_selector,
        ]
    self._update_refs()

    self._l3tree = l3tree

    # Highlighting.
    self._outline = None

    # Item cross-referencing.
    self._marker = None
    self._container = None

    # Event handling.
    d_func_marker.connect("event", lambda *a: self.insert_event(*a))

    # Borders etc.
    self.init_header(rentab)
    self.init_deco(rentab)

    pass
l3Call.__init__ = __init__

def _update_refs(self):
    (  self._d_func,
       self._d_args,

       self._d_func_marker,
       self._d_selector,
     ) = self._d_elements
l3Call._update_refs = _update_refs

#
#**        misc.
def _func_shift(self):
    # Shifts needed when FUNC size changes
    if self._d_func:
        _, _, fr, fb = self._d_func.get_bounds()
    else:
        _, _, fr, fb = self._d_func_marker.get_bounds()
    #   FUNC
    _, at, _, _ = self._d_args.get_bounds()
    dy = fb - at                # Shift between OLD d_args and NEW d_func.
    self._d_args.move(0, dy)
    #   SELECTOR
    sl, _, _, _ = self._d_selector.get_bounds()
    self._d_selector.move(fr - sl, 0)
l3Call._func_shift = _func_shift


def _cond_shift(self):
    # Shifts needed when COND size changes
    if self._d_cond:
        _, _, _, y = self._d_cond.get_bounds()
    else:
        _, _, _, y = self._d_cond_marker.get_bounds()
    _, v, _, _ = self._d_yes.get_bounds()
    dy = y - v                # Shift between OLD d_yes and NEW d_cond.
    self._d_yes.move(0, dy)
    self._d_else.move(0, dy)
    self._d_no.move(0, dy)
l3If._cond_shift = _cond_shift

def _cond_shift(self):
    # Shifts needed when COND size changes
    if self._d_cond:
        _, _, _, y = self._d_cond.get_bounds()
    else:
        _, _, _, y = self._d_cond_marker.get_bounds()
    _, v, _, _ = self._d_body.get_bounds()
    dy = y - v                # Shift between OLD d_yes and NEW d_cond.
    self._d_body.move(0, dy)
l3IfWhile._cond_shift = _cond_shift


def _align_to_lhs(self):
    #
    # Align.
    #
    # +------A---------A------+
    # | lhs  |  equal  |  rhs |
    # +------+---------+------+
    flush_events()
    #
    #       LHS marker
    x, y, _, _ = self._d_lhs.get_bounds()
    u, _, _, v = self._d_lhs_marker.get_bounds()
    self._d_lhs_marker.move(x - u + self.w_.cp_.exp_marker_hoff,
                            y + self._canvas.font_ascent_units() +
                            self._canvas.bbox_offset_units() - v )

    #       EQUAL
    _, y, x, _ = self._d_lhs.get_bounds()
    u, v, _, _ = self._d_equal.get_bounds()
    self._d_equal.move(x - u, y - v)

    #       RHS
    if self._d_rhs != None:
        _, y, x, _ = self._d_equal.get_bounds()
        u, v, _, _ = self._d_rhs.get_bounds()
        self._d_rhs.move(x - u, y - v)

    #       RHS marker
    # also see l3Set.insert
    x, y, _, _ = self._d_rhs.get_bounds()
    u, _, _, v = self._d_rhs_marker.get_bounds()
    ### self._canvas.bbox_wait(self._d_rhs_marker, self._d_rhs)
    self._d_rhs_marker.move(x - u + self.w_.cp_.exp_marker_hoff,
                            y - v + self._canvas.font_ascent_units() +
                            self._canvas.bbox_offset_units()
                            )
l3Set._align_to_lhs = _align_to_lhs

#
#**        Events



#*    class l3IfFor members
#
class l3IfFor(l3Nested):
    # Layout:
    #   for V in SEQ
    #       B
    #
    pass


#*    l3Function
class l3Function(l3Nested):
    pass

#
#**        init
def __init__(self, w_, cnvs, tree_id, rentab):
    #
    # The blandest layout:
    #
    #   +-----------+
    #   | function  |
    #   +-----------+
    #   ----+------+
    #       | args |
    #       +------+
    #   ----+------+
    #       | body |
    #       +------+
    #
    # l3 tree.
    l3tree = w_.state_.storage.load(tree_id)
    assert isinstance(l3tree, ast.Function)
    l3Nested.__init__(self, w_, cnvs, l3tree, rentab)

    # Bindings to keep.
    self.w_      = w_
    self._canvas = cnvs

    #
    # Display elements.
    #
    #       function
    d_function = Label(w_, cnvs, self._root_group, "Function")

    #       ARGS
    d_args = l3aList(w_, cnvs,
                     l3tree[0],
                     rentab,
                     root_group = self._root_group,
                     draggable = False,
                     parent = self,
                     )
    ### incorporate into cnvs.add_l3tree(l3tree[1]) ??

    #       BODY
    d_body = l3aList(w_, cnvs,
                     l3tree[1],
                     rentab,
                     root_group = self._root_group,
                     draggable = False,
                     parent = self,
                     )
    ### incorporate into cnvs.add_l3tree(l3tree[1]) ??

    #
    # Marker(s).
    #

    #
    # Inform obj of marker.
    #

    #
    # Align with respect to FUNCTION.
    #
    flush_events()

    #   ARGS
    x, _, _, y = d_function.get_bounds()
    u, v, _, _ = d_args.get_bounds()
    d_args.move(x - u, y - v)

    #   BODY
    x, _, _, y = d_args.get_bounds()
    u, v, _, _ = d_body.get_bounds()
    d_body.move(x - u, y - v)

    # Bindings to keep.
    self._d_function = d_function
    self._d_args     = d_args
    self._d_body     = d_body
    self._l3tree     = l3tree

    # Highlighting.
    self._outline = None

    # Item cross-referencing.
    self._marker = None
    self._container = None

    # Event handling.

    # Borders etc.
    self.init_header(rentab)
    self.init_deco(rentab)

    pass
l3Function.__init__ = __init__


#
#**        events
#
#***            no insertion

#
#**        no insert

#
#**        no detach

#
#**        no draw marker


#*    l3Inline
class l3Inline(l3Nested):
    pass

def __init__(self, w_, cnvs, tree_id, rentab):
    # layout:
    #   inline STR

    # Also see l3Program / l3If.

    # l3 tree.
    l3tree = w_.state_.storage.load(tree_id)
    assert isinstance(l3tree, ast.Inline)
    l3Nested.__init__(self, w_, cnvs, l3tree, rentab)

    # Bindings to keep.
    self.w_ = w_
    self._canvas = cnvs
    self._l3tree = l3tree
    self._marker_points = {}            # marker -> point list

    # Display elements.
    #       Label.
    head = Label(w_, cnvs, self._root_group, "Inline")

    #       Python string.
    pystr = cnvs.add_l3tree(l3tree[0], rentab)
    pystr.reparent(self)

    # Marker(s).
    pystr_marker = self.draw_marker()
    pystr_marker.lower_to_bottom()

    # Bindings to keep.
    self._pystr = pystr
    self._pystr_marker = pystr_marker
    self._head = head

    # Inform obj of marker.
    pystr.setup_marker(pystr_marker, self)

    # Align flush left.
    #   pystr
    flush_events()
    u, _, _, v = head.get_bounds()
    x, y, _, _ = pystr.get_bounds()
    head.move(x - u, y - v)
    #   pystr_marker
    sl, st, _, _ = pystr.get_bounds()
    ml, mt, _, _ = pystr_marker.get_bounds()
    pystr_marker.move(sl - ml + w_.cp_.exp_marker_hoff,
                      st - mt + w_.cp_.exp_marker_voff)

    # Highlighting.
    self._outline = None

    # Item cross-referencing.
    self._marker = None
    self._container = None

    # Event handling.
    ### pystr_marker.connect("event", lambda *a: self.insert_event(*a))

    # Popup menu additions

    # Borders etc.
    self.init_header(rentab)
    self.init_deco(rentab)
    pass
l3Inline.__init__ = __init__


def rebuild_tree(self, text):
    # Form new tree.
    print 'inline text update finished'

    ### trap parse errors here
    tree = reader.parse("'''\n%s\n'''" % text).body()
    tree.setup(self._l3tree, self.w_.state_.def_env, self.w_.state_.storage)
    tree.set_outl_edges(self.w_, None)

    # Replace original raw String().
    self._l3tree[0].deep_replace(tree, self.w_.state_.storage)
l3Inline.rebuild_tree = rebuild_tree

#*    l3Native
class l3Native(l3Nested):
    pass

def __init__(self, w_, cnvs, tree_id, rentab):
    # layout:
    #   header
    #   value
    #
    # Also see l3Inline / l3Program / l3If.

    # l3 tree.
    l3tree = w_.state_.storage.load(tree_id)
    assert isinstance(l3tree, ast.Native)
    l3Nested.__init__(self, w_, cnvs, l3tree, rentab)

    # Bindings to keep.
    self.w_ = w_
    self._canvas = cnvs
    self._l3tree = l3tree

    value = l3tree.value()

    # Display elements.
    #       Label.
    val_gen = w_.state_.storage.generator_of(id(value))
    if 0:
        if val_gen is None:
            head = Label(w_, cnvs, self._root_group, "External value")
        else:
            head = Label(w_, cnvs, self._root_group,
                         "Value from %d" %
                         w_.state_.storage.generator_of(id(value))._id
                         )

    head = uBlank(w_, cnvs, self._root_group)

    # todo: actively link the label to the source expression.
    # This way, the native display can provide other functions itself,
    # and can be a full widget with independent event handling if
    # desired.

    #       Python value.
    #   Note: canvas operations will destroy these value containers
    #   occasionally, so values that are gtk widgets can be displayed
    #   ONLY ONCE.
    # 
    # Avoid 'import EMAN2' dependency
    # Display emdata as image.
    if (value.__class__.__name__ == 'EMData'):
        # For now, put an image handler here.  Add a general handler
        # later.
        pystr = Image(w_, cnvs, self._root_group, misc.emdata2pixbuf(value),
                      parent = self)

    # Display .png file as image ( display Native(String("foo.png")) )
    elif isinstance(value, ast.String) and value.isfile() and \
         value.endswith(".png"):
        pystr = Image(w_, cnvs, self._root_group,
                      misc.png2pixbuf(value.py_string()), parent = self)

    # Display matplotlib figures via in widgets.
    elif (value.__class__.__name__ == 'Figure'):
        from matplotlib.backends.backend_gtk import FigureCanvasGTK \
             as FigureCanvas 
        pystr = Widget(w_, cnvs, self._root_group,
                       FigureCanvas(value), parent = self)

    # Avoid numpy dependency.
    elif (value.__class__.__name__ == 'ndarray'):
        if len(value.shape) == 2:
            pystr = Image(w_, cnvs, self._root_group,
                          misc.arr2pix(value), parent = self)
        else:
            pystr = Label(w_, cnvs, self._root_group,
                          misc.escape_markup(repr(value)))
    # Generic display.
    else:
        pystr = Label(w_, cnvs, self._root_group,
                      misc.escape_markup(str(value)))

    # Marker(s).

    # Bindings to keep.
    self._pystr = pystr
    self._head = head

    # Inform obj of marker.

    # Align flush left.
    #   pystr
    flush_events()
    l1, _, _, b1 = head.get_bounds()
    l2, t2, _, _ = pystr.get_bounds()
    pystr.move(l1 - l2, b1 - t2)
    #   pystr_marker

    # Highlighting.
    self._outline = None

    # Item cross-referencing.
    self._marker = None
    self._container = None

    # Event handling.

    # Popup menu additions
    if isinstance(pystr, Image):
        map(self._pup.prepend, [
            [gtk.SeparatorMenuItem(), None],

            [ gtk.MenuItem('set image size as default'),
              ( 'activate', lambda *args: pystr.set_default_size())],

            [ gtk.MenuItem('shrink image 40%'),
              ('activate', lambda *args: pystr.scale(0.6))],

            [ gtk.MenuItem('enlarge image 40%'),
              ('activate', lambda *args: pystr.scale(1.4))],

            ])

    # Borders etc.
    self.init_header(rentab)
    self.init_deco(rentab)
    pass
l3Native.__init__ = __init__


#*    l3Program
class l3Program(l3Nested):
    pass

def __init__(self, w_, cnvs, tree_id, rentab):
    # l3 tree.
    l3tree = w_.state_.storage.load(tree_id)
    if isinstance(self, l3Program):
        assert isinstance(l3tree, ast.Program)
    else:
        assert isinstance(l3tree, (ast.Map, ast.cls_viewList))
    l3Nested.__init__(self, w_, cnvs, l3tree, rentab)

    #
    #*** Choose display elements.
    #       alist.
    if l3tree._outl_type == 'subtree':
        # Full display (the l3List default).
        alist = l3aList(w_, cnvs,
                        l3tree[0],
                        rentab,
                        root_group = self._root_group,
                        draggable = False,
                        parent = self,
                        )
    elif l3tree._outl_type == 'flat':
        # This heading only; no body additions allowed.
        setup_alist = l3tree[0].setup_alist(w_, l3tree[0])
        alist = l3aList(w_, cnvs,
                        setup_alist,
                        rentab,
                        root_group = self._root_group,
                        draggable = False,
                        parent = self,
                        insertion_on_markers = False,
                        hide_markers = True,
                        )
    elif l3tree._outl_type == 'nested':
        # heading + subheadings only; no body additions/insertions
        # allowed.  Removal is ok.
        alist = l3aList(w_, cnvs,
                        l3tree._outl_children,
                        rentab,
                        root_group = self._root_group,
                        draggable = False,
                        parent = self,
                        insertion_on_markers = False,
                        hide_markers = True,
                        )
    else:
        raise Exception("Invalid _outl_type.  Internal error.")
    #
    #*** Form the label.
    level, index = l3tree.heading_index()
    # Use numbers for even levels, letters for odd.
    if (level % 2) == 0:
        outline_symb = string.ascii_letters[26 + index % 26]
    else:
        outline_symb = str(index + 1) # count from 1

    ttext = l3tree.deco_title_text()
    # Strip quotes.
    if ttext.startswith("'''"):
        ttext = ttext[3:-3]
    if ttext.startswith("'") or ttext.startswith('"'):
        ttext = ttext[1:-1]

    head_text = ("<b>" +
                 outline_symb +
                 ". " +
                 ttext +
                 "</b>")
    head = Label(w_, cnvs, self._root_group, head_text,
                 font = w_.cp_.viewlist_head_font)
    #
    #*** Expander
    #
    # The actions cycle to the next view state.
    expander_fn, expander_action = {
        'flat': (uInvisibleSymbol,
                 lambda *args: self.view_as('nested')),
        'nested': (uPartiallyVisible,
                   lambda *args: self.view_as('subtree')),
        'subtree': (uVisibleSymbol,
                 lambda *args: self.view_as('flat')),
        }[ l3tree.get_outline_type() ]

    expander = expander_fn(w_, cnvs, self._root_group,
                           action = expander_action)

    # Align.
    #    Expander Head
    #             Alist
    flush_events()                      # Update bounding boxes
    LE, TE, RE, BE = expander.get_bounds()
    LH, TH, RH, BH = head.get_bounds()
    head.move(RE - LH, TE - TH)

    LA, TA, _, _ = alist.get_bounds()
    alist.move(LH - LA, BH - TA)

    # Events.
    head.connect("event", lambda *a: self.head_event(*a))

    # Bindings to keep.
    self.w_ = w_
    self._canvas = cnvs
    self._alist = alist
    self._head = head
    self._expander = expander
    self._l3tree = l3tree

    # Highlighting.
    self._outline = None

    # Item cross-referencing.
    self._marker = None
    self._container = None

    # Popup menu additions
    if isinstance(self, l3Program):
        map(self._pup.prepend, [
            # Program specials.
            [gtk.SeparatorMenuItem(), None],
            [ gtk.MenuItem("run code"),
              ( "activate", lambda *args: self.run_code())],
            ])

    map(self._pup.prepend, [
        # Outline specials.
        [ gtk.SeparatorMenuItem(), None],
        [ gtk.MenuItem('view full contents'),
          ( 'activate', lambda *args: self.view_as('subtree'))],
        [ gtk.MenuItem('view nested outlines'),
          ( 'activate', lambda *args: self.view_as('nested'))],
        [ gtk.MenuItem('view this level only'),
          ( 'activate', lambda *args: self.view_as('flat'))],
        [ gtk.SeparatorMenuItem(), None],
        [ gtk.MenuItem('show values written'),
          ( 'activate', lambda *args: self.show_written_names())],
        [ gtk.MenuItem('show values read'),
          ( 'activate', lambda *args: self.show_read_names())],

        ])

    # Borders etc.
    self.init_header(rentab)
    self.init_deco(rentab)
    pass
l3Program.__init__ = __init__
l3ViewList.__init__ = __init__


def view_as(self, type):
    def body():
        self._l3tree.set_outline(type)
        tw = ast.TreeWork(self.w_.state_.storage)
        root = tw.find_root(self._l3tree)
        if root:
            root.set_outl_edges(self.w_, None)
        else:
            self._l3tree.set_outl_edges(self.w_, None)
        self.start_replace_visible()
    self.w_.with_fluids(body,
                        position_tree = False)
l3Program.view_as = view_as
l3ViewList.view_as = view_as


def show_written_names(self):
    from l3gui.l3canvas import RenderTable
    import re

    ignored_syms = re.compile(r'(IDX|ITEMS|LEN|LOOP)[0-9]+')
        
    ma = ast.Matcher()
    view, view_id = ast.viewList(aList([])).setup(
        empty_parent(),
        self._l3tree.eval_env() or self.w_.state_.def_env,
        self.w_.state_.storage)

    for nd in self._l3tree.top_down_truncate([ast.Function, ast.Call]):
        # Several choices exist concerning display of nested
        # bindings.  These are not visible outside their scope in L3,
        # but can be accessed via the interface, and conceivably be
        # exported by the language in some way.
        # For simplicity, only show those names directly accessible
        # here.
        if ma.match(nd, Set(Marker('lhs'), Marker('rhs')) ):
            if isinstance(ma['lhs'], ast.Symbol):
                # Filter out internal names from FOR, WHILE, etc.
                if ignored_syms.match(ma['lhs'].py_string()) != None:
                    continue

            node, nid = misc.node_copy(self.w_, ma['lhs'])
            view.append_child(node)

    view.set_outl_edges(self.w_, None)
    view.set_outline('subtree')

    # Add label.
    view.set_label('Values written by -->')

    # Display the structure.
    pic = self._canvas.add_l3tree(view, RenderTable())

    # Position to the left of expression.
    lp, tp, rp, bp = pic.get_bounds_world()
    lo, to, ro, bo = self.get_bounds_world()
    pic.move(lo - rp, to - tp)
    pass
l3Program.show_written_names = show_written_names
l3ViewList.show_written_names = show_written_names


def show_read_names(self):
    from l3gui.l3canvas import RenderTable
    import re

    ignored_syms = re.compile(r'(IDX|ITEMS|LEN|LOOP)[0-9]+')
    valid_ident = re.compile(r'[a-zA-Z0-9_]+')
    ignored_lhs = []                    # Symbols to ignore.
    shown = []                          # Already included.
    ma = ast.Matcher()

    # Prepare display list.
    view, view_id = ast.viewList(aList([])).setup(
        empty_parent(),
        self._l3tree.eval_env() or self.w_.state_.def_env,
        self.w_.state_.storage)

    for nd in self._l3tree.top_down_truncate([]):
        if ma.match(nd, Set(Marker('lhs'), Marker('rhs')) ):
            if isinstance(ma['lhs'], ast.Symbol):
                # Ignore names assigned to.
                ignored_lhs.append(ma['lhs'])
                # TODO: ignore names bound in Function argument list.
                
                # Filter out internal names from FOR, WHILE, etc.
                if ignored_syms.match(ma['lhs'].py_string()) != None:
                    ignored_lhs.append(ma['lhs'])

        if isinstance(nd, ast.Symbol) and \
               (nd not in ignored_lhs) and \
               (nd not in shown) and \
               valid_ident.match(nd.py_string()): # Ignore operators.
            node, nid = misc.node_copy(self.w_, nd)
            view.append_child(node)
            shown.append(nd)

    view.set_outl_edges(self.w_, None)
    view.set_outline('subtree')

    # Add label.
    view.set_label('Values read by -->')

    # Display the structure.
    pic = self._canvas.add_l3tree(view, RenderTable())

    # Position to the left of expression.
    lp, tp, rp, bp = pic.get_bounds_world()
    lo, to, ro, bo = self.get_bounds_world()
    pic.move(lo - rp, to - tp)
    pass
l3Program.show_read_names = show_read_names
l3ViewList.show_read_names = show_read_names

def run_code(self):
    # Attach progress indicator to visible nodes.
    for nd in self.iter_visibles():
        nd._l3tree._pre_interp_hook = misc.DisplayInterp(self.w_, nd)

    # Execute the program, redirecting stdout/err to the message handler.
    w_ = self.w_
    sys.stdout.flush()
    sys.stderr.flush()

    w_.stdinouterr.push()
    if not w_.opts.gui.native_console:
        w_.message.std_to_widget()
    ## sys.settrace (w_.message.idle)      # MAJOR slowdown!
    try:
        pprint(self._l3tree.interpret(w_.state_.def_env, w_.state_.storage))
    except:
        if w_.opts.gui.native_console:
            raise                       # allow debugging via pdb.pm()
        try:
            tb = sys.exc_traceback
            if tb:
                tb = tb.tb_next
            traceback.print_exception(sys.exc_type, sys.exc_value, tb)
        except:
            w_.message.std_to_default()
            traceback.print_exc()
    ## sys.settrace(None)
    # Restore stdin/out/err
    w_.stdinouterr.pop()
    # Remove progress indicators (assuming no editing happened during
    # execution...)  
    for nd in self.iter_visibles():
        nd._l3tree._pre_interp_hook = None
l3Program.run_code = run_code

#*    visit visible nodes
# Visit visible nodes that have l3trees.
# Taken from `def hide(...)` and adjusted.  These need testing.
def iter_visibles(self):
    yield self
    for ii in self._d_elements:
        for prop_iter in ii.iter_visibles():
            yield prop_iter
l3Loop.iter_visibles = iter_visibles

def iter_visibles(self):
    return []
Label.iter_visibles = iter_visibles
Image.iter_visibles = iter_visibles
Widget.iter_visibles = iter_visibles
uWidget.iter_visibles = iter_visibles
Placeholder.iter_visibles = iter_visibles

def iter_visibles(self):
    raise DisplayError("Interface only: " + str(self.__class__))
l3Base.iter_visibles = iter_visibles

def iter_visibles(self):
    # aLists are list subclasses and have no custom __deepcopy__, so
    # attaching anything to them causes copy problems during .interpret()
    ## yield self
    for ch in self._dlist:
        for prop_iter in ch.iter_visibles():
            yield prop_iter
l3aList.iter_visibles = iter_visibles

def iter_visibles(self):
    yield self
    for sub in self._subtrees:
        for prop_iter in sub.iter_visibles():
            yield prop_iter
l3Rawtext.iter_visibles = iter_visibles

def iter_visibles(self):
    yield self
    for prop_iter in self._alist.iter_visibles():
        yield prop_iter
    for prop_iter in self._head.iter_visibles():
        yield prop_iter
l3List.iter_visibles = iter_visibles
l3Map.iter_visibles = iter_visibles

def iter_visibles(self):
    yield self
    if self._l3tree._outl_type in ['subtree', 'nested']:
        for prop_iter in self._alist.iter_visibles():
            yield prop_iter
        for prop_iter in self._head.iter_visibles():
            yield prop_iter
    elif self._l3tree._outl_type == 'flat':
        pass
    else:
        raise Exception("Invalid _outl_type.  Internal error.")
l3ViewList.iter_visibles = iter_visibles

def iter_visibles(self):
    yield self
    for prop_iter in self._d_lhs.iter_visibles():
        yield prop_iter
    for prop_iter in self._d_equal.iter_visibles():
        yield prop_iter
    for prop_iter in self._d_rhs.iter_visibles():
        yield prop_iter
l3Set.iter_visibles = iter_visibles

def iter_visibles(self):
    yield self
    for prop_iter in self._d_label.iter_visibles():
        yield prop_iter
l3IfLoopBreak.iter_visibles = iter_visibles
l3IfLoopContinue.iter_visibles = iter_visibles

def iter_visibles(self):
    yield self
    for prop_iter in self._d_loop.iter_visibles():
        yield prop_iter
    #
    for prop_iter in self._d_body.iter_visibles():
        yield prop_iter
l3IfLoop.iter_visibles = iter_visibles


def iter_visibles(self):
    yield self
    for prop_iter in self._d_cond_V.iter_visibles():
        yield prop_iter
    #
    for prop_iter in self._d_cond_SEQ.iter_visibles():
        yield prop_iter
    #
    for prop_iter in self._d_body.iter_visibles():
        yield prop_iter
l3IfFor.iter_visibles = iter_visibles

def iter_visibles(self):
    yield self
    for prop_iter in self._d_loop.iter_visibles():
        yield prop_iter
    #
    for prop_iter in self._d_cond.iter_visibles():
        yield prop_iter
    #
    for prop_iter in self._d_body.iter_visibles():
        yield prop_iter
l3IfWhile.iter_visibles = iter_visibles

def iter_visibles(self):
    yield self
    for prop_iter in self._d_if.iter_visibles():
        yield prop_iter
    #
    for prop_iter in self._d_cond.iter_visibles():
        yield prop_iter
    #
    for prop_iter in self._d_yes.iter_visibles():
        yield prop_iter
    #
    for prop_iter in self._d_else.iter_visibles():
        yield prop_iter
    #
    for prop_iter in self._d_no.iter_visibles():
        yield prop_iter
l3If.iter_visibles = iter_visibles

def iter_visibles(self):
    yield self
    for prop_iter in self._d_func.iter_visibles():
        yield prop_iter
    #
    for prop_iter in self._d_args.iter_visibles():
        yield prop_iter
l3Call.iter_visibles = iter_visibles

def iter_visibles(self):
    yield self
    for prop_iter in self._d_function.iter_visibles():
        yield prop_iter
    #
    for prop_iter in self._d_args.iter_visibles():
        yield prop_iter
    #
    for prop_iter in self._d_body.iter_visibles():
        yield prop_iter
l3Function.iter_visibles = iter_visibles

def iter_visibles(self):
    yield self
    for prop_iter in self._alist.iter_visibles():
        yield prop_iter
    #
    for prop_iter in self._head.iter_visibles():
        yield prop_iter
l3Program.iter_visibles = iter_visibles

def iter_visibles(self):
    yield self
    for prop_iter in self._pystr.iter_visibles():
        yield prop_iter
    #
    for prop_iter in self._head.iter_visibles():
        yield prop_iter
l3Inline.iter_visibles = iter_visibles
l3Native.iter_visibles = iter_visibles


#*    hiding
def hide(self):
    self._root_group.hide()
    for ii in self._d_elements:
        ii.hide()
l3Loop.hide = hide

def hide(self):
    self._root_group.hide()
Label.hide = hide
Image.hide = hide
Widget.hide = hide
uWidget.hide = hide

def hide(self):
    raise DisplayError("Interface only: " + str(self.__class__))
l3Base.hide = hide

def hide(self):
    self._root_group.hide()
    for ch in self._dlist:
        ch.hide()
l3aList.hide = hide

def hide(self):
    self._root_group.hide()
    self._ltext.hide()
l3Rawtext.hide = hide

def hide(self):
    self._root_group.hide()
    self._alist.hide()
    self._head.hide()
l3List.hide = hide
l3Map.hide = hide

def hide(self):
    self._root_group.hide()
    self._d_lhs.hide()
    self._d_equal.hide()
    self._d_rhs.hide()
l3Set.hide = hide

def hide(self):
    self._root_group.hide()
    self._d_label.hide()
l3IfLoopBreak.hide = hide
l3IfLoopContinue.hide = hide

def hide(self):
    self._root_group.hide()
    self._d_loop.hide()
    self._d_body.hide()
l3IfLoop.hide = hide

def hide(self):
    self._root_group.hide()
    self._d_loop.hide()
    self._d_cond.hide()
    self._d_body.hide()
l3IfWhile.hide = hide

def hide(self):
    self._root_group.hide()
    self._d_if.hide()
    self._d_cond.hide()
    self._d_yes.hide()
    self._d_else.hide()
    self._d_no.hide()
l3If.hide = hide

def hide(self):
    self._root_group.hide()
    self._d_func.hide()
    self._d_args.hide()
l3Call.hide = hide

def hide(self):
    self._root_group.hide()
    self._d_function.hide()
    self._d_args.hide()
    self._d_body.hide()
l3Function.hide = hide

def hide(self):
    self._root_group.hide()
    self._alist.hide()
    self._head.hide()
l3Program.hide = hide

def hide(self):
    self._root_group.hide()
    self._pystr.hide()
    self._head.hide()
l3Inline.hide = hide
l3Native.hide = hide


#*    insert subtree
#**        event handler
def insert_event_shared(self, item, event):
    if event.type == gtk.gdk.BUTTON_PRESS:
        if event.button == 2:
            if not self.w_.selector.have_selection():
                self.w_.ten_second_message("No node selected.")
                return False
            else:
                self.insert(item, self.w_.selector.get_selection())
                return False
l3Call.insert_event = insert_event_shared
l3Set.insert_event = insert_event_shared
l3If.insert_event = insert_event_shared
l3IfWhile.insert_event = insert_event_shared

#
#**        inserter
def append(self, newobj):
    self.insert(len(self._l3tree), newobj)
l3aList.append = append

def insert(self, index, newobj):
    ### Remove any existing references to newobj in the CALLER --
    ### distinguish between MOVE and COPY.

    assert isinstance(newobj, l3Base)

    # Avoid special cases:
    if newobj in self._dlist:
        self.w_.ten_second_message("Disconnect element before inserting "
                                   "into the same list.")
        return

    if newobj.contains_recursive(self):
        self.w_.ten_second_message("Cannot insert list into itself.")
        return

    if newobj._canvas != self._canvas:
        self.w_.ten_second_message("Cannot move objects across displays.  "
                                   "Copy instead.")
        return

    # Reparent object.
    newobj.reparent(self)

    # Draw new marker over insertion marker.
    ox1, oy1 = marker_get_anchor(self.w_, self._marker_lst[index])

    if self._l3tree.getthe('layout') == 'horizontal':
        ox1 -= self.w_.cp_.marker_padding
        ## oy1 += self.w_.cp_.marker_padding
    else:
        oy1 -= self.w_.cp_.marker_padding

    if self._hide_markers:
        newmarker = self.draw_marker(ox1, oy1, hidden_color = 'white')
    else:
        newmarker = self.draw_marker(ox1, oy1)

    if self._insertion_on_markers:
        self.bind_marker_events(newmarker)

    # Adjust positions.
    if self._l3tree.getthe('layout') == 'horizontal':
        self._adj_h_insert(index, newobj, newmarker)
    else:
        self._adj_v_insert(index, newobj, newmarker)

    # Move following items in parent.
    if self._parent:
        self._parent.new_size_for(self)

    # Update data and marker lists.
    if self.w_.fluid_ref(insert_l3_tree = True):
        self._l3tree.insert_child_rec(index,
                                      newobj._l3tree,
                                      self.w_.state_.storage)
        if self.w_.fluid_ref(trace_gui_events = False):
            print "insertion tree"
    self._dlist.insert(index, newobj)
    self._marker_lst.insert(index, newmarker)

    self._mv_spacer()

    # inform obj of marker
    newobj.setup_marker(newmarker, self)
    pass
l3aList.insert = insert

def _adj_v_insert(self, index, newobj, newmarker):
    # Put object under marker.
    sl, st, sr, sb = marker_get_bounds(self.w_, newmarker).pad_tb()
    ol, ot, _, ob = newobj.get_bounds()
    newobj.move(sl - ol, sb - ot)

    # Move following markers, labels and objects.
    shift_y = (sb - st) + (ob - ot) ## + self.w_.cp_.marker_padding

    for itm in self._dlist[index : None]:
        itm.move(0, shift_y)

    for itm in self._marker_lst[index : None]:
        itm.move(0, shift_y)
l3aList._adj_v_insert = _adj_v_insert

def _adj_h_insert(self, index, newobj, newmarker):
    # Put object under marker.
    sl, st, sr, sb = marker_get_bounds(self.w_, newmarker).pad_lr()
    ol, ot, or_, ob = newobj.get_bounds()
    newobj.move(sr - ol, st - ot)

    # Move following markers, labels and objects.
    shift_x = (sr - sl) + (or_ - ol) ## + self.w_.cp_.marker_padding

    for itm in self._dlist[index : None]:
        itm.move(shift_x, 0)

    for itm in self._marker_lst[index : None]:
        itm.move(shift_x, 0)
l3aList._adj_h_insert = _adj_h_insert


def insert(self, marker, newobj):
    assert isinstance(newobj, l3Base)

    if marker != self._d_cond_marker:
        raise DisplayError("Insertion from wrong marker.")

    # Avoid special cases:
    if newobj.contains_recursive(self):
        self.w_.ten_second_message("Cannot insert if into itself.")
        return

    if newobj._canvas != self._canvas:
        self.w_.ten_second_message("Cannot move objects across displays.  "
                                   "Copy instead.")
        return

    # Validate slot.
    old_entry = self._l3tree.deref(0)
    slot_available(self.w_, old_entry)

    # Update reference.
    self._d_cond = d_cond = newobj

    # Reparent object.
    newobj.reparent(self)

    # Position COND.
    flush_events()
    _, y, x, _ = self._d_if.get_bounds()
    u, v, _, _ = self._d_cond.get_bounds()
    d_cond.move(x + self.w_.cp_.loop_cond_sep - u, y - v)

    # Reposition rest.
    self._cond_shift()

    # Refresh decoration.
    self.refresh_deco()

    # Move following items in parent.
    if self._parent:
        self._parent.new_size_for(self)

    # Update tree data.
    if self.w_.fluid_ref(insert_l3_tree = True):
        self._l3tree.replace_child(old_entry._id, newobj._l3tree)

    # Inform obj of marker.
    newobj.setup_marker(self._d_cond_marker, self)
    pass
l3If.insert = insert


def insert(self, marker, newobj):
    '''
    Insert new condition.
    '''
    assert isinstance(newobj, l3Base)

    if marker != self._d_cond_marker:
        raise DisplayError("Insertion from wrong marker.")

    # Avoid special cases:
    if newobj.contains_recursive(self):
        self.w_.ten_second_message("Cannot insert if into itself.")
        return

    if newobj._canvas != self._canvas:
        self.w_.ten_second_message("Cannot move objects across displays.  "
                                   "Copy instead.")
        return

    # Validate slot.
    if len(self._matcher['COND-PARENT']) != 0:
        raise DisplayError("Insertion in occupied slot.")

    # Update reference.
    self._d_cond = d_cond = newobj

    # Reparent object.
    newobj.reparent(self)

    # Position COND.
    flush_events()
    _, y, x, _ = self._d_loop.get_bounds()
    u, v, _, _ = self._d_cond.get_bounds()
    d_cond.move(x + self.w_.cp_.loop_cond_sep - u, y - v)

    # Reposition rest.
    self._cond_shift()

    # Refresh decoration.
    self.refresh_deco()

    # Move following items in parent.
    if self._parent:
        self._parent.new_size_for(self)

    # Update tree data.
    if self.w_.fluid_ref(insert_l3_tree = True):
        self._matcher['COND-PARENT'].insert_child(
            self._matcher['COND-IDX'], newobj._l3tree)
        ## from: self._l3tree.replace_child(old_entry._id, newobj._l3tree)

    # Inform obj of marker.
    newobj.setup_marker(self._d_cond_marker, self)
    pass
l3IfWhile.insert = insert


def insert(self, marker, newobj):
    assert isinstance(newobj, l3Base)

    if marker != self._d_func_marker:
        raise DisplayError("Insertion from wrong marker.")

    # Avoid special cases:
    if newobj.contains_recursive(self):
        self.w_.ten_second_message("Cannot insert if into itself.")
        return

    if newobj._canvas != self._canvas:
        self.w_.ten_second_message("Cannot move objects across displays.  "
                                   "Copy instead.")
        return

    # Validate slot.
    old_entry = self._l3tree.deref(0)
    slot_available(self.w_, old_entry)

    # Update reference.
    self._d_func = d_func = newobj

    # Reparent object.
    newobj.reparent(self)

    # Position FUNC.
    flush_events()
    x, y, _, _ = self._d_func_marker.get_bounds()
    u, v, _, _ = self._d_func.get_bounds()
    d_func.move(x - u - self.w_.cp_.exp_marker_hoff,
                y - v - self.w_.cp_.exp_marker_voff)

    # Reposition rest.
    self._func_shift()

    # Update tree data.
    if self.w_.fluid_ref(insert_l3_tree = True):
        self._l3tree.replace_child(old_entry._id, newobj._l3tree)

    # Refresh decoration.
    self.refresh_deco()

    # Move following items in parent.
    if self._parent:
        self._parent.new_size_for(self)

    # Inform obj of marker.
    newobj.setup_marker(self._d_func_marker, self)
    pass
l3Call.insert = insert


def insert(self, marker, newobj):
    assert isinstance(newobj, l3Base)

    # Avoid special cases:
    if newobj.contains_recursive(self):
        self.w_.ten_second_message("Cannot insert if into itself.")
        return

    if newobj._canvas != self._canvas:
        self.w_.ten_second_message("Cannot move objects across displays.  "
                                   "Copy instead.")
        return

    if marker == self._d_lhs_marker:
        # Validate slot.
        old_entry = self._l3tree.deref(0)
        slot_available(self.w_, old_entry)

        # Check and update reference.
        self._d_lhs = newobj

        # Reparent object.
        newobj.reparent(self)

        # Position lhs w.r.t. marker
        flush_events()
        x, y, _, _ = self._d_lhs_marker.get_bounds()
        u, v, _, _ = newobj.get_bounds()
        newobj.move(x - u - self.w_.cp_.exp_marker_hoff,
                    y - v - self.w_.cp_.exp_marker_voff)

        # Reposition.
        self._align_to_lhs()

        # Update tree data.
        if self.w_.fluid_ref(insert_l3_tree = True):
            self._l3tree.replace_child(old_entry._id, newobj._l3tree)

        # Hide markers.
        self._d_lhs_marker.hide()

    elif marker ==  self._d_rhs_marker:
        # Validate slot.
        old_entry = self._l3tree.deref(1)
        slot_available(self.w_, old_entry)

        # Check and update reference.
        self._d_rhs = newobj

        # Reparent object.
        newobj.reparent(self)

        # Position rhs.
        # see also l3Set._align_to_lhs
        flush_events()
        x, _, _, y = self._d_rhs_marker.get_bounds()
        u, v, _, _ = newobj.get_bounds()
        newobj.move(x - u - self.w_.cp_.exp_marker_hoff,
                    y - v - self._canvas.font_ascent_units() -
                    self._canvas.bbox_offset_units()
                    )

        # Reposition.

        # Update tree data.
        if self.w_.fluid_ref(insert_l3_tree = True):
            self._l3tree.replace_child(old_entry._id, newobj._l3tree)

        # Hide markers.
        self._d_rhs_marker.hide()

    else:
        raise DisplayError("Insertion from invalid marker.")

    # Refresh decoration.
    self.refresh_deco()

    # Move following items in parent.
    if self._parent:
        self._parent.new_size_for(self)

    # Inform obj of marker.
    newobj.setup_marker(marker, self)

    pass
l3Set.insert = insert



#*    movement
def move(self, dx, dy):
    self._root_group.move(dx, dy)
Label.move = move
Image.move = move
Widget.move = move
uWidget.move = move

def move(self, dx, dy):
    self._root_group.move(dx, dy)
    if self._outline:
        self._outline.move(dx, dy)
l3Base.move = move

def move(self, dx, dy):
    self._root_group.move(dx, dy)
l3aList.move = move


#*    bounding boxes
#**        world
def get_bounds_world(self):
    i2w = self._root_group.get_property("parent").i2w
    x1, y1, x2, y2 = self.get_bounds()
    u1, v1 = i2w(x1, y1)
    u2, v2 = i2w(x2, y2)
    return u1, v1, u2, v2
l3Base.get_bounds_world = get_bounds_world
Label.get_bounds_world = get_bounds_world
Image.get_bounds_world = get_bounds_world
Widget.get_bounds_world = get_bounds_world
uWidget.get_bounds_world = get_bounds_world

#
#**        local
def get_bounds(self):
    raise DisplayError("interface only")
l3Base.get_bounds = get_bounds

def get_bounds(self):
    ### magic bound offset adjustment hack
    x, y, u, v = self._root_group.get_bounds()

    # No adjustment when path is absent (like inserting or removing)
    if self._deco == None:
        # Good for "a = 2" (CanvasText at extremes), too much
        # for "<ph> = 2" (CanvasRect on left)
        return x + 0.5, y + 0.5, u - 0.5, v - 0.5
        # return x + 0.5, y + 0.5, u, v
    else:
        # With surrounding path decoration.
        cp_ = self.w_.cp_
        adj = (cp_.deco.outline_width / 2.0) * self._canvas._pixpu
        return x + adj, y + adj, u - adj, v - adj
    #  with header
    # return x + 0.5, y + 0.5, u, v
    # return x + 0.5, y + 0.5, u - 0.5, v - 0.5
    # return x, y, u, v
l3Nested.get_bounds = get_bounds

def get_bounds(self):
    (ll, tt, rr, bb) = l3Nested.get_bounds(self)
    pad = self.w_.cp_.marker_padding
    return (ll, tt, rr + pad, bb + pad)
l3Native.get_bounds = get_bounds

# def get_bounds(self):
#     return self._root_group.get_bounds()
# Placeholder.get_bounds = get_bounds

def get_bounds(self):
    ### magic bound offset adjustment hack
    le, to, ri, bo = self._root_group.get_bounds()
    return le, to, ri, bo
    # return x + 0.5, y + 0.5, u - 0.5, v - 0.5
l3aList.get_bounds = get_bounds

def get_bounds(self):
    x, y, u, v = self._root_group.get_bounds()
    # No adjustment when path is absent.
    if self._deco == None:
        ### magic bound offset adjustment hack
        return x + 0.5, y + 0.5, u - 0.5, v - 0.5
    else:
        # With surrounding path decoration.
        cp_ = self.w_.cp_
        adj = (cp_.deco.outline_width / 2.0) * self._canvas._pixpu
        return x + adj, y + adj, u - adj, v - adj
    # with header
l3Rawtext.get_bounds = get_bounds

def get_bounds(self):
    ### magic bound offset adjustment hack
    x, y, u, v = self._root_group.get_bounds()
    return x + 0.5, y + 0.5, u - 0.5, v - 0.5
Label.get_bounds = get_bounds
uWidget.get_bounds = get_bounds

#
# libart's bounding boxes are a serious problem.
#
# A rotated rectangle changes bounding box size, which is not
# desirable here.  So use an ellipse as background.
# This didn't help, because the triangle is a path, for which the
# bounding box is wrong anyway.
#
# For this purpose, using symbol glyphs is a better approach anyway,
# but fonts are not handled very well either...
#
# Time to switch drawing/canvas engine.
#
def get_bounds(self):
    ### magic bound offset adjustment hack
    x, y, u, v = self._root_group.get_bounds()
    return x + 0.5, y + 0.5, u - 0.5, v - 0.5
uVisibleSymbol.get_bounds = get_bounds
uPartiallyVisible.get_bounds = get_bounds
uInvisibleSymbol.get_bounds = get_bounds

def get_bounds(self):
    x, y, u, v = self._root_group.get_bounds()
    return x, y, u, v
Widget.get_bounds = get_bounds
Image.get_bounds = get_bounds


def get_bounds(self):
    return self._root_group.get_bounds()
l3IfLoopContinue.get_bounds = get_bounds
l3IfLoopBreak.get_bounds = get_bounds


#*    Draw insertion markers
def draw_marker_shared(self):
    cp = self.w_.cp_
    points = [0                   , 0,
              cp.exp_marker_width , 0,
              cp.exp_marker_width , cp.exp_marker_height,
              0                   , cp.exp_marker_height,
              0                   , 0,
              ]
    _marker = self._root_group.add(
        canvas.CanvasPolygon,
        fill_color  = "black",
        points      = points,
        width_units = 0.2,
        cap_style   = gtk.gdk.CAP_ROUND,
        join_style  = gtk.gdk.JOIN_ROUND,
        )
    self._marker_points[_marker] = points
    return _marker
l3Set.draw_marker = draw_marker_shared
l3If.draw_marker = draw_marker_shared
l3IfWhile.draw_marker = draw_marker_shared
l3Call.draw_marker = draw_marker_shared
l3Inline.draw_marker = draw_marker_shared

#*    Comments & headers
#
# Fully movable text, but without any effect on execution.
# These need most of the l3Rawtext functionality; a Label is grossly
# insufficient.
#
class l3Comment(l3Rawtext):
    pass

# # def __init__(self, *args, **kwds):
# #     l3Rawtext.__init__(self, *args, **kwds)
# # l3Comment.__init__ = __init__

def init_params(self):
    self._font_desc = self.w_.cp_.comment_font
    self._font_size = self.w_.cp_.font_size_comments * 1.0 * pango.SCALE
    self._fill_color = self.w_.cp_.label_fill_color
    self._outline_color = self.w_.cp_.label_outline_color
l3Comment.init_params = init_params


#*    Text editing support
# Functions are in calling order.
#
#**        l3Rawtext
def rebuild_tree(self, text):
    # Form new tree.
    ## print 'text_finished: parent: ', self._l3tree._parent
    if self._l3tree._parent:
        parent = self.w_.state_.storage.load( self._l3tree._parent )
    else:
        parent = empty_parent()

    ### trap parse errors here
    tree = reader.parse(text).body()
    tree.setup(parent, self.w_.state_.def_env, self.w_.state_.storage)
    tree.set_outl_edges(self.w_, None)

    # Reparent.
    if self._l3tree._parent:
        self._l3tree.deep_replace(tree, self.w_.state_.storage)

    self._l3tree = tree
l3Rawtext.rebuild_tree = rebuild_tree

def ed_new_text(self, text):
    # Reset text.
    self._text_orig = text
    text_and_outline(self)

    # Resize from root node.

    # Adjust highlighting.
    if self._outline:
        # Avoid repositioning mess.
        self.plain_view()
        self.highlight()
    self._under_destruction = False
l3Rawtext.ed_new_text = ed_new_text

def ed_restore_text(self):
    self.ed_new_text(self._text_orig)
    self._under_destruction = False
l3Rawtext.ed_restore_text = ed_restore_text

def finish_edit(self):
    # Replace the display text with one that includes selectable
    # subtrees. 
    self.start_replace_visible()

    # Remove decoration.
    pass

    # Refresh decoration.
    self.refresh_deco()
    # Move following items in parent.
    if self._parent:
        self._parent.new_size_for(self)
l3Rawtext.finish_edit = finish_edit

#
#**        l3Comment
def rebuild_tree(self, text):
    # Form new tree.
    tree = ast.Comment(text)

    # Attach (logically) to node.
    if self._parent:
        self.w_.deco_table_[ self._parent._l3tree._id ] = tree

    tree.setup(empty_parent(), self.w_.state_.def_env, self.w_.state_.storage)
    tree.set_outl_edges(self.w_, None)

    self._l3tree = tree
l3Comment.rebuild_tree = rebuild_tree


def ed_new_text(self, text):
    # Reset text.
    self._text_orig = text
    # # text_and_outline(self)

    # Resize from root node.

    # Adjust highlighting.
    if self._outline:
        # Avoid repositioning mess.
        self.plain_view()
        self.highlight()
l3Comment.ed_new_text = ed_new_text


def finish_edit(self):
    # Refresh decoration.
    self.refresh_deco()

    # Update geometry.
    if self._parent:
        p1 = self._parent
        p1.refresh_header()
        p1.refresh_deco()
        if p1._parent:
            p1._parent.new_size_for(p1)
l3Comment.finish_edit = finish_edit

#
#**        l3ViewList / l3Program
def ed_restore_text(self):
    pass
l3ViewList.ed_restore_text = ed_restore_text
l3Program.ed_restore_text = ed_restore_text


def rebuild_tree(self, text):
    def body():
        self._l3tree.set_label(text)
        self.start_replace_visible()
    self.w_.with_fluids(body,
                        position_tree = False)
l3ViewList.rebuild_tree = rebuild_tree
l3Program.rebuild_tree = rebuild_tree


def ed_new_text(self, text):
    # Reset text.
    self._text_orig = text

    # Adjust highlighting.
    if self._outline:
        # Avoid repositioning mess.
        self.plain_view()
        self.highlight()
l3ViewList.ed_new_text = ed_new_text
l3Program.ed_new_text = ed_new_text


def finish_edit(self):
    # Refresh decoration.
    self.refresh_deco()

    # Update geometry.
    if self._parent:
        self._parent.new_size_for(self)

l3ViewList.finish_edit = finish_edit
l3Program.finish_edit = finish_edit

#
#**        misc.
def prep_deco_text(self):
    deco_t = self.w_.deco_table_

    # Clear up prior temporaries.
    if deco_t.has_key( self._l3tree._id ):
        if hasattr(deco_t[ self._l3tree._id ], '_placeholder_temporary'):
            del deco_t[ self._l3tree._id ]

    # Avoid empty display.
    #   1. No default text
    deco_text = ""

    #   2. Override with explicit deco text.
    if deco_t.has_key(self._l3tree._id):
        deco_text = deco_t[self._l3tree._id]

    #   3. Get automatic deco text for placeholder use, when no "good"
    #   text is available.
    if deco_text == "" and isinstance(self, Placeholder):
        deco_text = self._l3tree.deco_title_text()
        if deco_text == "":
            deco_text = "unlabeled code"
        # Update deco info.
        #   Set as default for this node.
        comm = deco_t[self._l3tree._id] = ast.Comment(deco_text)
        comm.setup(ast.empty_parent(),
                   self.w_.state_.def_env,
                   self.w_.state_.storage)
        # Mark as temporary.
        comm._placeholder_temporary = 1

    return deco_text
l3Nested.prep_deco_text = prep_deco_text
l3Rawtext.prep_deco_text = prep_deco_text


# Draw the header before the current contents of the _root_group.
# Additions are made to the same _root_group.
#
# Used after subclass __init__, before init_deco().
#
def init_header(self, rentab):
    deco_text = self.prep_deco_text()
    # Maybe add header.
    if deco_text != "":
        # Create header and align H[eader] with S[elf].
        self._header = self._canvas.add_header_of(self._l3tree, rentab)
        self._header.reparent(self)  # Only reparent the canvas group.
        flush_events()
        ls, ts, rs, bs = self.padded_bounds_world()
        lh, th, rh, bh = self._header.get_bounds_world()
        #
        # Vertical alignment:
        if isinstance(self, l3List):
            # Header on top of label (hide the label)
            ll, tl, rl, bl = self._head.get_bounds_world()
            self._header.move(ll - lh, bl - bh)
        else:
            #   H[eader]
            #   S[elf]
            self._header.move(ls - lh, ts - bh)
    else:
        # No header.
        self._header = None

    pass
l3Nested.init_header = init_header
l3Rawtext.init_header = init_header


def init_header(self, rentab):
    deco_text = self.prep_deco_text()
    if deco_text == "":
        self._header = None
        return

    vl_type = self._l3tree.get_outline_type()

    if vl_type == 'flat':
        # Display only _head (label), ignore _header (comment).
        self._header = None
        return

    # Create header and align H[eader] with S[elf].
    self._header = self._canvas.add_header_of(self._l3tree, rentab)
    self._header.reparent(self)  # Only reparent the canvas group.
    flush_events()
    # # ls, ts, rs, bs = self.padded_bounds_world()
    lh, th, rh, bh = self._header.get_bounds()
    le, te, re, be = self._expander.get_bounds()
    ll, tl, rl, bl = self._head.get_bounds()
    la, ta, ra, ba = self._alist.get_bounds()
    #
    # Vertical alignment:
    if vl_type in ['nested', 'subtree']:
        # Layout:
        #   E L
        #     H
        #     A
        #
        # where:
        #         _head     (label,     l)
        #         _header   (comment,   h)
        #         body      (alist,     a)
        #
        # Keep existing horizontal positioning for E, L, A; only H is
        # new.
        self._header.move(la - lh, ta - bh)         # H-A
        self._head.move(0,                          # L-A
                        (ta - (bh - th)) - bl)
        self._expander.move(0,                      # E-A
                            (ta - (bh - th)) - be)

    else:
        raise Exception("Invalid outline type.  Internal error.")
l3ViewList.init_header = init_header
l3Program.init_header = init_header


def refresh_header(self):
    from l3gui.l3canvas import RenderTable
    # Delete header visuals (and reinsert with current settings).
    _safed(self._header)
    self._header = None

    # todo: preserve rentab info?
    self.init_header(RenderTable())
    flush_events()
l3Nested.refresh_header = refresh_header
l3Rawtext.refresh_header = refresh_header


#*    event handling
#
#**        drag / selection
def drag_event(self, item, event):
    if event.type == gtk.gdk.BUTTON_PRESS:
        if event.button == 1:
            if event.state & gtk.gdk.CONTROL_MASK:
                self.w_.selector.toggle_selection(self)
            else:
                self.w_.selector.set_selection(self)

            self._canvas.grab_focus()   # Ensure keystrokes go here.

            # sm: move group start
            self.remember_x = event.x
            self.remember_y = event.y
            return True

    elif event.type == gtk.gdk.ENTER_NOTIFY:
        # Alt + Shift + motion adds self to selection.
        if ((event.state & gtk.gdk.SHIFT_MASK) and
            (event.state & gtk.gdk.MOD1_MASK)):
            self.w_.selector.add_to_selection(self)
            return True

    elif event.type == gtk.gdk.LEAVE_NOTIFY:
        self.w_.cp_.drag_state = (None, None)

    elif event.type == gtk.gdk.MOTION_NOTIFY:
        if event.state & gtk.gdk.BUTTON1_MASK:
            # Do not drag nested text, or the text of Inline.
            if isinstance(self._parent, (l3Rawtext, l3Inline)):
                (d_state, _) = self.w_.cp_.drag_state
                if d_state == "ds_dragging":
                    return False
                else:
                    self.w_.selector.remove(self)
                    self.w_.cp_.drag_state = ("ds_start", (event.x, event.y))
                    return False
            #
            # Get the new position and move by the difference.
            #
            # sm: move group
            # The button_press event above may not have occurred, so
            # ensure a valid prior position.
            if self.remember_x is None:
                self.remember_x = event.x
                self.remember_y = event.y

            new_x = event.x
            new_y = event.y
            dx = new_x - self.remember_x
            dy = new_y - self.remember_y
            self.move(dx, dy)
            self.remember_x = new_x
            self.remember_y = new_y

            # sm: detach item if needed.
            if self._marker:
                x, y = self.get_anchor()
                u, v = marker_get_anchor(self.w_, self._marker)
                if self.w_.fluid_ref(trace_gui_events = False):
                    print "distance: ", abs(u - x) + abs(v - y)
                    sys.stdout.flush()
                if (abs(u - x) + abs(v - y) >
                    self.w_.cp_.item_detach_distance):
                    self._marker = None
                    self._container.detach(self )
                    self._container = None

            return True
    return False
l3Rawtext.drag_event = drag_event

def drag_event(self, item, event):
    return self._parent.drag_event(item, event)
l3Comment.drag_event = drag_event

def drag_event(self, item, event):
    if event.type == gtk.gdk.BUTTON_PRESS:
        if event.button == 1:
            if event.state & gtk.gdk.CONTROL_MASK:
                self.w_.selector.toggle_selection(self)
            else:
                self.w_.selector.set_selection(self)

            self._canvas.grab_focus()   # Ensure keystrokes go here.

            # sm: move group start
            self.remember_x = event.x
            self.remember_y = event.y
            return True

    elif event.type == gtk.gdk.ENTER_NOTIFY:
        # Alt + Shift + motion adds self to selection.
        if ((event.state & gtk.gdk.SHIFT_MASK) and
            (event.state & gtk.gdk.MOD1_MASK)):
            self.w_.selector.add_to_selection(self)
            return True

    elif event.type == gtk.gdk.MOTION_NOTIFY:
        if event.state & gtk.gdk.BUTTON1_MASK:
            #
            # Get the new position and move by the difference.
            #
            # sm: move group
            # The button_press event above may not have occurred, so
            # ensure a valid prior position.
            (d_state, d_val) = self.w_.cp_.drag_state
            if d_state == "ds_start":
                (self.remember_x, self.remember_y) = d_val
                self.w_.cp_.drag_state = ("ds_dragging", d_val)

            if self.remember_x is None:
                self.remember_x = event.x
                self.remember_y = event.y

            new_x = event.x
            new_y = event.y
            dx = new_x - self.remember_x
            dy = new_y - self.remember_y
            self.move(dx, dy)
            self.remember_x = new_x
            self.remember_y = new_y

            if self._marker:
                # Detach from parent once self is far enough from marker.
                x, y = self.get_anchor()
                u, v = marker_get_anchor(self.w_, self._marker)
                if self.w_.fluid_ref(trace_gui_events = False):
                    print "distance: ", abs(u - x) + abs(v - y)
                    sys.stdout.flush()
                if (abs(u - x) + abs(v - y)) > self.w_.cp_.item_detach_distance:
                    def _do():
                        self._container.detach(self)
                        self._marker = None
                        self._container = None
                    misc.single_event(_do)
            return True

    return False
l3Nested.drag_event = drag_event


def drag_event(self, item, event):
    if event.type == gtk.gdk.BUTTON_PRESS:
        if event.button == 1:
            if event.state & gtk.gdk.CONTROL_MASK:
                self.w_.selector.toggle_selection(self)
            else:
                self.w_.selector.set_selection(self)

            self._canvas.grab_focus()   # Ensure keystrokes go here.

            # sm: move group start
            self.remember_x = event.x
            self.remember_y = event.y
            return True

    elif event.type == gtk.gdk.MOTION_NOTIFY:
        if event.state & gtk.gdk.BUTTON1_MASK:
            #
            # Get the new position and move by the difference.
            #
            # sm: move group
            new_x = event.x
            new_y = event.y
            dx = new_x - self.remember_x
            dy = new_y - self.remember_y
            self.move(dx, dy)
            self.remember_x = new_x
            self.remember_y = new_y
            return True

    return False
l3aList.drag_event = drag_event

#
#**        deletion
def destroy_event(self, item, event):
    if event.type == gtk.gdk._2BUTTON_PRESS:
        if event.button == 3:
            self._canvas.remove_node_edge_all(self)
            self.destroy()
    return False
l3Rawtext.destroy_event = destroy_event

#
#**        edit start
def head_event(self, item, event):
    # doubleclick-button-1 to edit.
    if event.type == gtk.gdk._2BUTTON_PRESS:
        if event.button == 1:
            self._editor = TextEdit(self.w_,
                                    self,
                                    self._l3tree.deco_title_text(),
                                    self._canvas._common_tag_table,
                                    self._canvas,
                                    )
            self._editor._view.show()
            self._editor._view.grab_focus()
            return True
    return False
l3ViewList.head_event = head_event
l3Program.head_event = head_event



#*    Decoration
#
#**        l3Deco:  Decoration around contents.
# This includes the outlines, buttons (if
# any) and the titlebar (if present)
class l3Deco:
    pass

def __init__(self, node):
    # For full initialization example, see add_deco_to.
    #
    # # not practical: self._node_id = node._l3tree._id
    # # cyclic: self._node = node
    self.w_ = node.w_
    #
    self._deco_path = None              # art_path
    self._deco_path_item = None         # canvas.CanvasBpath (holds _deco_path)
    self._draw_queue = []               # See add_item()
    #
l3Deco.__init__ = __init__

def add_item(self, cnvsitem):
    assert hasattr(cnvsitem, "destroy")
    assert hasattr(cnvsitem, "refresh")
    self._draw_queue.append(cnvsitem)
l3Deco.add_item = add_item

def destroy(self):
    _safed(self._deco_path_item)
    for ii in self._draw_queue:
        _safed(ii)

    self._draw_queue = []
    self._deco_path = None
    self._deco_path_item = None
l3Deco.destroy = destroy


def magnify_event(self, item, event):
    if event.type == gtk.gdk.ENTER_NOTIFY:
        if self.w_.fluid_ref(trace_gui_events = False):
            print "deco_magnify_event"
        item.set_property("width_units",
                          self.w_.cp_.deco.outline_width *
                          self.w_.cp_.hover_magnification)
        return False

    elif event.type == gtk.gdk.LEAVE_NOTIFY:
        if self.w_.fluid_ref(trace_gui_events = False):
            print "deco_magnify_event"
        item.set_property("width_units", self.w_.cp_.deco.outline_width)
        return False
l3Deco.magnify_event = magnify_event

#
#**        attach (add) deco to node
def add_deco_to(node, callbacks = [], emphasis_for = None):
    """
    Produce and return the l3Deco for `node`.
    This routine puts an outline around `node`.

    `node` interface requirements:
          .get_bounds_world()
          .w_
          ._root_group

    Every ballback cb must  (1) be callable
                            (2) return a canvasitem or l3Nested instance OB
    The returned Ob must satisfy the interface specified in add_item().

    """
    the_deco = l3Deco(node)
    # Self-drawing items.
    for cb in callbacks:
        the_deco.add_item(cb())

    # Bounds for the node body.
    ll1, tt1, rr1, bb1 = node.get_bounds_world()
    path_width = node.w_.cp_.deco.outline_width
    if isinstance(node, (l3Nested,  Label)):
        padding = 2 * path_width
        # Pad the content.
        ll1 -= padding
        tt1 -= padding
        rr1 += padding
        bb1 += padding
    else:
        padding = 0

    # Check for content, and inflate bounds to get a valid vector path.
    if rr1 - ll1 < 1:  rr1 = ll1 + 1.0
    if bb1 - tt1 < 1:  bb1 = tt1 + 1.0

    #
    # Draw boundary path.
    #
    the_deco._deco_path = path_rectangle_rounded(ll1, rr1, tt1, bb1,
                                                 0.5, 0.5, ### params
                                                 0.3, 0.3,
                                                 )
    #   Draw path.
    bdry_path = canvas.path_def_new(the_deco._deco_path)
    if emphasis_for:
        the_deco._deco_path_item = bdry_item = node._root_group.add(
            canvas.CanvasBpath,
            width_units = path_width,
            outline_color_rgba = node.w_.cp_.deco.emph_color[emphasis_for],
            fill_color = "green",
            cap_style = gtk.gdk.CAP_ROUND,
            join_style = gtk.gdk.JOIN_ROUND,
            )
    else:
        the_deco._deco_path_item = bdry_item = node._root_group.add(
            canvas.CanvasBpath,
            width_units = path_width,
            outline_color_rgba = node.w_.cp_.deco.outline_color,
            fill_color = "green",
            cap_style = gtk.gdk.CAP_ROUND,
            join_style = gtk.gdk.JOIN_ROUND,
            )
    bdry_item.set_bpath(bdry_path)
    #     Adjust coordinate offsets.
    l_bi, t_bi, r_bi, b_bi_ = canvas_item_get_bounds_world(bdry_item)
    bdry_item.move(ll1 - l_bi - path_width, tt1 - t_bi - path_width)

    # Background coloring.
    the_deco._background = None
    if emphasis_for:
        # Draw background.
        bck = the_deco._background = node._root_group.add(
            canvas.CanvasRect,
            x1 = ll1, y1 = tt1,
            x2 = rr1, y2 = bb1,
            # fill_color_rgba = node.w_.cp_.deco.outline_color,
            fill_color = 'white',
            width_pixels = node.w_.cp_.outline_width_normal,
            )
        # Move background to bottom.
        bck.lower_to_bottom()

    #
    # Event handling.
    #
    the_deco._deco_path_item.connect(
        "event", lambda *a: the_deco.magnify_event(*a))

    return the_deco


#
#**        init_deco
def padded_bounds_world(self):
    #     Bounds for the node body.
    ll1, tt1, rr1, bb1 = self.get_bounds_world()
    path_width = self.w_.cp_.deco.outline_width
    if isinstance(self, l3Nested):
        padding = 2 * path_width
        # Pad the content.
        ll1 -= padding
        tt1 -= padding
        rr1 += padding
        bb1 += padding
    else:
        padding = 0

    # Check for content, and inflate bounds to get a valid vector path.
    if rr1 - ll1 < 1:  rr1 = ll1 + 1.0
    if bb1 - tt1 < 1:  bb1 = tt1 + 1.0

    # Headerless state.
    nrr1 = rr1
    nbb1 = bb1

    return ll1, tt1, rr1, bb1
l3Nested.padded_bounds_world = padded_bounds_world
l3Rawtext.padded_bounds_world = padded_bounds_world


#
# Draw the border and other decorations around the current
# contents of the _root_group.
# Additions are made to the same _root_group.
#
# Used *after* init_header().
#

def init_deco(self, rentab):
    # Decorate body.
    self._deco = add_deco_to(self)
l3Function.init_deco = init_deco

def init_deco(self, rentab):
    # Decorate body.
    ## self._deco = add_deco_to(self, callbacks = self._deco_cb_buffer)
    self._deco = None
l3IfLoop.init_deco = init_deco
l3IfWhile.init_deco = init_deco


def init_deco(self, rentab):
    # Decorate body.
    self._deco = None
    # Move the placeholder to the right of the header string, into
    # the header path.
    if self._header != None:
        l1, t1, r1, b1 = self._header.get_bounds()
        l2, t2, r2, b2 = self._blank.get_bounds()
        self._blank.move(l1 - r2 - self.w_.cp_.placeholder.padding,
                         t1 - t2)
    else:
        # init_header must always provide a header for Placeholder.
        raise Exception("Invalid state in l3Nested.init_deco.  "
                        "No header found.  Internal error")
    # Emphasize body / add outline.
    emph = self._l3tree.get_emphasis()
    if emph:
        self._deco = add_deco_to(self, emphasis_for = emph)
    else:
        self._deco = None

Placeholder.init_deco = init_deco


def init_deco(self, rentab):
    # Emphasize body / add outline.
    emph = self._l3tree.get_emphasis()
    if emph:
        self._deco = add_deco_to(self, emphasis_for = emph)
    else:
        self._deco = None
l3Rawtext.init_deco = init_deco
l3Nested.init_deco = init_deco


#
#**        refresh
def refresh_deco(self):
    from l3gui.l3canvas import RenderTable
    # Remove decoration.
    _safed(self._deco)

    # Bodyless state
    self._deco = None

    # todo: preserve rentab info?
    self.init_deco(RenderTable())
    flush_events()
l3Nested.refresh_deco = refresh_deco
l3Rawtext.refresh_deco = refresh_deco

#
#**        destroy
def destroy_deco(self):
    _safed(self._deco)
    self._deco = None
    _safed(self._header)
    self._header = None
    # ???
    _safed(self._root_group)
    del self._root_group

    return True
l3Nested.destroy_deco = destroy_deco

#
#**        interactive modification
def deco_change_label(self, label = None):
    # The following also works in "evaluate locally"
    if not label:
        print "enter new label:"
        sys.stdout.flush()
        label = sys.stdin.readline().rstrip()

    assert isinstance(label, StringType), "Label must be a string."

    # Update deco info.
    comm = self.w_.deco_table_[self._l3tree._id] = ast.Comment(label)
    comm.setup(ast.empty_parent(),
               self.w_.state_.def_env,
               self.w_.state_.storage)

    # Refresh decoration.
    self.refresh_header()
    self.refresh_deco()

    # Propagate.
    if self._parent:
        self._parent.new_size_for(self)
l3Base.deco_change_label = deco_change_label

def deco_add_comment(self):
    # Update deco info.

    comm = self.w_.deco_table_[self._l3tree._id] = \
           ast.Comment("double-click to edit")
    comm.setup(ast.empty_parent(),
               self.w_.state_.def_env,
               self.w_.state_.storage)

    # Refresh decoration.
    self.refresh_header()
    self.refresh_deco()

    # Propagate.
    if self._parent:
        self._parent.new_size_for(self)
l3Base.deco_add_comment = deco_add_comment


#*    Resizing / new_size_for
#**        alist
def new_size_for(self, child):
    assert isinstance(child, (l3Base, l3aList))
    try:
        index = self._dlist.index(child)
    except ValueError:
        raise DisplayError("nonexistent child.")

    # Reposition child.
    if self._l3tree.getthe('layout') == 'horizontal':
        ml, mt, mr, mb = marker_get_bounds(
            self.w_, self._marker_lst[index]).pad_lr()
        cl, ct, cr, cb = child.get_bounds()

        # Horizontal alignment:
        #   M C
        child.move(mr - cl, mt - ct)
        # Move following markers, labels, and objects
        #       Find shift.
        ml, mt, mr, mb  = marker_get_bounds(
            self.w_, self._marker_lst[index + 1]).pad_lr()
        cl, ct, cr, cb  = child.get_bounds()
        shift_x = cr - ml
        for itm in self._dlist[index + 1 : None]:
            itm.move(shift_x, 0)
        for itm in self._marker_lst[index + 1 : None]:
            itm.move(shift_x, 0)

    else:
        ml, mt, mr, mb = marker_get_bounds(
            self.w_, self._marker_lst[index]).pad_tb()
        cl, ct, cr, cb = child.get_bounds()


        # Vertical alignment:
        #   M
        #   C
        child.move(ml - cl, mb - ct)
        # Move following markers, labels, and objects
        # Find shift.
        x, y, _, _ = marker_get_bounds(
            self.w_, self._marker_lst[index + 1]).pad_tb()
        _, _, u, v = child.get_bounds()
        shift_y = v - y
        for itm in self._dlist[index + 1 : None]:
            itm.move(0, shift_y)
        for itm in self._marker_lst[index + 1 : None]:
            itm.move(0, shift_y)

    # Propagate.
    if self._parent:
        self._parent.new_size_for(self)
    return
l3aList.new_size_for = new_size_for

#
#**        nested / native
def new_size_for(self, child):
    # Contained alist is updated; only forward request.
    if self._parent:
        self._parent.new_size_for(self)
l3Nested.new_size_for = new_size_for

def new_size_for(self, child):
    # Contained value updated; only forward request.
    if self._parent:
        self._parent.new_size_for(self)
l3Native.new_size_for = new_size_for

#
#***            if / set / call / function
def new_size_for(self, child):
    #
    # Shift contents vertically as needed.
    #
    assert isinstance(child, (l3Base, l3aList))

    # Find and apply shift.
    if   child == self._d_cond:
        _, _, _, y = self._d_cond.get_bounds()
        _, v, _, _ = self._d_yes.get_bounds()
        dy = y - v
        self._d_yes.move(0, dy)
        self._d_else.move(0, dy)
        self._d_no.move(0, dy)

    elif child == self._d_yes:
        _, _, _, y = self._d_yes.get_bounds()
        _, v, _, _ = self._d_else.get_bounds()
        dy = y - v
        self._d_else.move(0, dy)
        self._d_no.move(0, dy)

    elif child == self._d_no:
        pass

    else:
        raise DisplayError("nonexistent child.")

    # Refresh decoration.
    self.refresh_deco()

    # Propagate.
    if self._parent:
        self._parent.new_size_for(self)
    return
l3If.new_size_for = new_size_for


def new_size_for(self, child):
    #
    # Shift contents vertically as needed.
    #
    assert isinstance(child, (l3Base, l3aList))

    # Find and apply shift.
    if child is self._d_cond:
        _, _, _, bco = self._d_cond.get_bounds()
        _, tbo, _, _ = self._d_body.get_bounds()
        self._d_body.move(0, bco - tbo)

    # Refresh decoration.
    self.refresh_deco()

    # Propagate.
    if self._parent:
        self._parent.new_size_for(self)
    return
l3IfWhile.new_size_for = new_size_for


def new_size_for(self, child):
    #
    # Shift contents vertically as needed.
    #
    assert isinstance(child, (l3Base, l3aList))

    # Find and apply shift.

    # Refresh decoration.
    self.refresh_deco()

    # Propagate.
    if self._parent:
        self._parent.new_size_for(self)
    return
l3IfLoop.new_size_for = new_size_for
l3IfLoopBreak.new_size_for = new_size_for
l3IfLoopContinue.new_size_for = new_size_for


def new_size_for(self, child):
    #
    # Shift contents as needed.
    #
    assert isinstance(child, (l3Base, l3aList))

    # Find and apply shift.
    if   child == self._d_lhs:
        self._align_to_lhs()

    elif child == self._d_rhs:
        pass

    else:
        raise DisplayError("nonexistent child.")

    # Refresh decoration.
    self.refresh_deco()

    # Propagate.
    if self._parent:
        self._parent.new_size_for(self)
l3Set.new_size_for = new_size_for


def new_size_for(self, child):
    #
    # Shift contents as needed.
    #
    assert isinstance(child, (l3Base, l3aList))

    # Find and apply shift.
    if   child == self._d_func:
        self._func_shift()

    elif child == self._d_args:
        pass

    else:
        raise DisplayError("nonexistent child.")

    # Refresh decoration.
    self.refresh_deco()

    # Propagate.
    if self._parent:
        self._parent.new_size_for(self)
l3Call.new_size_for = new_size_for


def new_size_for(self, child):
    #
    # Shift contents vertically as needed.
    #
    assert isinstance(child, (l3Base, l3aList))

    # Find and apply shift.
    if   child == self._d_args:
        _, _, _, y = self._d_args.get_bounds()
        _, v, _, _ = self._d_body.get_bounds()
        dy = y - v
        self._d_body.move(0, dy)

    elif child == self._d_body:
        pass

    else:
        raise DisplayError("nonexistent child.")

    # Refresh decoration.
    self.refresh_deco()

    # Propagate.
    if self._parent:
        self._parent.new_size_for(self)
    return
l3Function.new_size_for = new_size_for

#
#***            list / map / program
def new_size_for(self, child):
    #
    # Shift contents vertically as needed.
    #
    assert isinstance(child, (l3aList,))

    # Find and apply shift.

    # Refresh decoration.
    self.refresh_deco()

    # Propagate.
    if self._parent:
        self._parent.new_size_for(self)
    return
l3Map.new_size_for = new_size_for
l3List.new_size_for = new_size_for
l3Program.new_size_for = new_size_for


#*    Introspection
#**        internal functions 
def marker_eval_local(self, args):
    # args: (menuitem, index)
    # For console use:
    if 0:
        from code import InteractiveConsole
        ic_dict = locals()
        ic = InteractiveConsole(ic_dict)
        utils.print_("==== marker index is %d ====" % args[1] )
        ic.interact("==== local console; exit with end-of-file (ctrl-d) ====")
        utils.print_( "==== local console exit ====")
l3aList.marker_eval_local = marker_eval_local

def eval_local(self, args):
    # args: (menuitem, index)
    from l3gui.cruft.pylab import Console
    name_s = "Local Python Console\nfor %d" % self._l3tree._id
    console = Console(
        self.w_, locals(),
        quit_handler =
        lambda *args: (self.w_.notebk_consoles.destroy_page(name_s),
                       self.w_.stdinouterr.pop()
                       ))
    self.w_.notebk_consoles.add_page(console, name_s)
    self.w_.stdinouterr.push()
    console.std_to_widget()
    console.write("==== Local console; exit with end-of-file (ctrl-d) ====\n"
                  "==== The name `self` is the instance. ====\n",
                  style='error')
    console.banner()
    misc.show_consoles(self.w_)
l3Base.selection_eval_local = eval_local
l3Base.eval_local = eval_local
l3aList.eval_local = eval_local


def eval_local_console(self, args):
    # args: (menuitem)
    # For use from calling console; this version stops the gui event
    # loop.
    from code import InteractiveConsole
    ic_dict = locals()
    ic = InteractiveConsole(ic_dict)

    # Simple combination of TxtConsole._raw_input and  new_termconsole(). 
    # This is a good overview of the use of those parts.
    def _raw_input(prompt=""):
        import thread 
        from threading import Lock
        import time
        lck = Lock()
        value = []
        def _do():
            try:
                value.append(raw_input(prompt))
            except EOFError:
                value.append(EOFError())
            lck.release()
        lck.acquire()
        thread.start_new_thread(_do, ())
        # Manual main loop until input is ready.
        while not lck.acquire(False):
            flush_events()
            time.sleep(0.01)
        if isinstance(value[-1], Exception):
            raise value[-1]
        return value[-1]

    ic.raw_input = _raw_input
    ic.interact("==== local console; exit with end-of-file (ctrl-d) ====")
    utils.print_( "==== local console exit ====")
l3Base.eval_local_console = eval_local_console


# 
#**        User functions.
def get_values_list(self):
    ''' return ((id, value) list)'''
    return self._l3tree.get_values_list(self.w_)
l3Base.get_values_list = get_values_list
l3aList.get_values_list = get_values_list

# # def dump_values(self):
# #     st = self.w_.state_.storage
# #     tw = ast.TreeWork(self.w_.state_.storage)

# #     # Toplevel id (static).
# #     c_id = self._l3tree._id
# #     G.logger.info("value of %d is %s" % (
# #         c_id, st.get_attribute(c_id, 'interp_result')))

# #     # Dynamic id(s).
# #     clone_l = st.get_attribute(c_id, "interp_clone")
# #     if clone_l:
# #         G.logger.info("%d has %d clones.\nValues:" % (c_id, len(clone_l)))

# #         for id in clone_l:
# #             G.logger.info("%d %s\n" %
# #                           (id, st.get_attribute(id, 'interp_result')))
# #     else:
# #         G.logger.info("No clones of %d" % c_id)

# # def dump_values(self):
# #     ind = "    "
# #     st = self.w_.state_.storage
# #     tw = ast.TreeWork(self.w_.state_.storage)

# #     # Toplevel id (static).
# #     c_id = self._l3tree._id
# #     print "----------------------"

# #     # Dynamic id(s).
# #     def _dynamic(c_id, lev):
# #         clone_l = st.get_attribute(c_id, "interp_clone")
# #         if clone_l:
# #             print ind*lev, "%d has %d clone(s):" % (c_id, len(clone_l))

# #             for id in clone_l:
# #                 _dynamic(id, lev+1)
# #         else:
# #             print ind*lev, c_id, "has no clones;",\
# #                   "value: ", st.get_attribute(c_id, 'interp_result')
# #     _dynamic(c_id, 0)

# 
def dump_values(self):
    """Print a (potentially) nested tree of values for self and all clones.
    """
    ind = "    "
    st = self.w_.state_.storage
    tw = ast.TreeWork(self.w_.state_.storage)

    # Toplevel id (static).
    c_id = self._l3tree._id
    print "----------------------"

    # Dynamic id(s).
    def _dynamic(c_id, lev):
        clone_l = st.get_attribute(c_id, "interp_clone")
        if clone_l:
            print ind*lev, "%d repeats %d time(s):" % (c_id, len(clone_l))

            for id in clone_l:
                _dynamic(id, lev+1)
        else:
            print ind*lev, c_id, "does not repeat;",\
                  "value: ", st.get_attribute(c_id, 'interp_result')
    _dynamic(c_id, 0)
l3Base.dump_values = dump_values
l3aList.dump_values = dump_values

def get_value(self, id):
    """Return the value corresponding to the numeric `id`.
    """
    st = self.w_.state_.storage
    return st.get_attribute(id, 'interp_result')
l3Base.get_value = get_value
l3aList.get_value = get_value

# 
def string_export(self):
    ind = "    "
    st = self.w_.state_.storage
    tw = ast.TreeWork(self.w_.state_.storage)

    # Toplevel id (static).
    c_id = self._l3tree._id
    print '---------------------------------'

    # Dynamic id(s).
    def _dynamic(c_id, lev):
        clone_l = st.get_attribute(c_id, "interp_clone")
        if clone_l:
            # print ind*lev, "%d repeats %d time(s):" % (c_id, len(clone_l))
            for id in clone_l:
                _dynamic(id, lev+1)
        else:
            # value in str() form 
            val = st.get_attribute(c_id, 'interp_result')
            if val != None:
                print val
    _dynamic(c_id, 0)
    print '---------------------------------'
l3Base.string_export = string_export
l3aList.string_export = string_export


#*    Subtree visibility
#
#**        marking
def mark_as(self, visibility):
    # Mark in table.
    self._canvas._vis_tab[self._l3tree._id] = visibility

    # Background visibility indicator.
    def _draw(color):
        if self._vis_indic:
            self._vis_indic.destroy()
            self._vis_indic = None

        flush_events()
        x1, y1, x2, y2 = self.get_bounds()
        pad = self.w_.cp_.highlight_padding
        x1 -= pad
        y1 -= pad
        x2 += pad
        y2 += pad

        # What depth to use?
        #     Subtrees are in their own group; within that, the box is
        #     always at the bottom.
        #
        #         Tree
        #             subtree1
        #                 subtree2
        #                 box2
        #             box1
        #         Box
        #
        # The parent's bounding box should not change.
        if self._parent:
            add = self._parent._root_group.add
        else:
            add = self._canvas.root().add
        self._vis_indic = add(
            canvas.CanvasRect,
            x1 = x1, y1 = y1,
            x2 = x2, y2 = y2,
            fill_color = color,
            outline_color = 'black',
            width_pixels = self.w_.cp_.outline_width_normal,
            )
        self._vis_indic.lower_to_bottom()

    if visibility == "vis_hide":
        _draw("grey80")

    elif visibility == "vis_show":
        _draw("white")

    elif visibility == "vis_neutral":
        if self._vis_indic:
            self._vis_indic.destroy()
            self._vis_indic = None

    else:
        raise DisplayError("Invalid visibility flag.")
l3Base.mark_as = mark_as

#
#**        subtree swapping
def start_replace_visible(self):
    def body():
        if self._parent:
            self._parent.replace_visible(self)
        else:
            self._canvas.replace_visible(self)
    self.w_.with_fluids(body,
                        detach_l3_tree = False,
                        insert_l3_tree = False,
                        )
l3Base.start_replace_visible = start_replace_visible

def replace_visible(self, child):
    # Collect info.
    marker = child._marker
    l3tree = child._l3tree

    # Remove existing display.
    child.destroy()

    # Draw (visible) subtree.
    tree_disp = self._canvas.start_add_l3tree(l3tree)

    # Attach / move / reparent
    flush_events()
    self.insert(marker, tree_disp)

    # Resize parent.
    pass
l3Nested.replace_visible = replace_visible

def replace_visible(self, child):
    # Collect info.
    index = self._dlist.index(child)
    l3tree = child._l3tree
    # #     if isinstance(l3tree, ast.viewList):
    # #         l3tree = l3tree._real_tree
    # #         assert l3tree

    # Remove existing display.
    child.destroy()

    # Draw (visible) subtree.
    ### self._canvas._vis_tab.get_render_table(l3tree)
    tree_disp = self._canvas.start_add_l3tree(l3tree)

    # Attach it.
    flush_events()
    self.insert(index, tree_disp)

    # Resize parent.
    if self._parent:
        self._parent.new_size_for(self)
l3aList.replace_visible = replace_visible

#
#**        Convenience combinations.
def show_nested_lhs(self):
    # Combination.
    vista = self._canvas._vis_tab

    # Clear existing markings
    vista.clear_tree( self._l3tree )

    # Mark subtree.
    self.mark_as("vis_show")

    ma = ast.Matcher()
    N = 3                               # maximum visible sublevel
    for nd, dent in self._l3tree.top_down_indented():
        if dent > N:
            vista[nd._id] = "vis_hide"
        else:
            if ma.match(nd, Set(Marker('lhs'), Marker('rhs')) ):
                vista[ma['lhs']._id] = "vis_show"
                vista[ma['rhs']._id] = "vis_hide"

    # Redisplay.
    self.start_replace_visible()
l3Base.show_nested_lhs = show_nested_lhs

def show_one_subtree(self):
    # Combination.
    self.mark_as("vis_show")
    self._canvas._vis_tab.hide_at(self._l3tree, 2) ### choose level
    self.start_replace_visible()
l3Base.show_one_subtree = show_one_subtree

def show_subtree(self):
    # Combination.
    def body():
        self.mark_as("vis_show")
        self.start_replace_visible()
    self.w_.with_fluids(body, position_tree = False)
l3Base.show_subtree = show_subtree

def show_full_subtree(self):
    self._canvas._vis_tab.clear_tree( self._l3tree )
    # # self.mark_as("vis_show")
    self.start_replace_visible()
l3Base.show_full_subtree = show_full_subtree

def hide_subtree(self):
    # Combination.
    self.mark_as("vis_hide")
    self.start_replace_visible()
l3Base.hide_subtree = hide_subtree


def hide_subtree(self):
    # Redirect the hide request to the tree this comment is attached to.
    # Find key matching value -- deco_table_ maps (tree id -> comment)
    for tree_id, comment in self.w_.deco_table_.iteritems():
        if comment == self._l3tree: break
    l3tree = self._canvas._nodes[tree_id]
    l3tree.mark_as("vis_hide")
    l3tree.start_replace_visible()
l3Comment.hide_subtree = hide_subtree


#*    detach header
def detach_header(self, header):
    # Allow for multiple headers in the future.
    if header != self._header:
        raise Exception("detaching unknown header")

    # Reparent graphical parts.
    header.reparent_to_root()
    self._header = None

    # Update table data.
    # #     if self.w_.deco_table_.has_key(self._l3tree._id):
    # #         del self.w_.deco_table_[self._l3tree._id]
    # #     else:
    # #         print "WARNING: disconnected header was not known." \
    # #               "  Internal inconsistency."

    # Decoration and header must be separate when destroying
    # placeholder constructs (and others?).
    ## self.refresh_deco()

    # Move following items in parent.
    if self._parent:
        self._parent.new_size_for(self)
l3Base.detach_header = detach_header


#*    detach subtree
#**        hooks
def add_reparent_to_root_hook(self, func):
    self._reparent_to_root_hook.append(func)
l3Base.add_reparent_to_root_hook = add_reparent_to_root_hook
l3aList.add_reparent_to_root_hook = add_reparent_to_root_hook
Label.add_reparent_to_root_hook = add_reparent_to_root_hook
Widget.add_reparent_to_root_hook = add_reparent_to_root_hook
Image.add_reparent_to_root_hook = add_reparent_to_root_hook
uWidget.add_reparent_to_root_hook = add_reparent_to_root_hook

def _run_reparent_to_root_hook(self):
    for hook in self._reparent_to_root_hook:
        hook()
    self._reparent_to_root_hook = []
l3Base._run_reparent_to_root_hook = _run_reparent_to_root_hook
l3aList._run_reparent_to_root_hook = _run_reparent_to_root_hook
Label._run_reparent_to_root_hook = _run_reparent_to_root_hook
Widget._run_reparent_to_root_hook = _run_reparent_to_root_hook
Image._run_reparent_to_root_hook = _run_reparent_to_root_hook
uWidget._run_reparent_to_root_hook = _run_reparent_to_root_hook

# 
#**        editing and deletion
#***            l3aList
def detach(self, child):
    # When drag distance from marker to item exceeds item_detach_distance,
    # detach the item.  This criterion is implemented in
    # l3Rawtext.drag_event.

    index = self._dlist.index(child)
    marker = self._marker_lst[index]

    # Reparent graphical parts.
    child.reparent_to_root()

    # Shift following marker over to-be-detached marker.  Also shift
    # following objects.
    sl, st, _, _ = marker_get_bounds(self.w_, marker)
    ol, ot, _, _ = marker_get_bounds(self.w_, self._marker_lst[index + 1])
    ##   child.get_bounds() is off after the child was dragged.
    ##   shift_y = (sy2 - st) + (oy2 - ot)
    if self._l3tree.getthe('layout') == 'horizontal':
        shift_x = - (ol - sl)
        for itm in self._dlist[index + 1 : None]:
            itm.move(shift_x, 0)
        for itm in self._marker_lst[index + 1 : None]:
            itm.move(shift_x, 0)
        del itm

    else:
        shift_y = - (ot - st)
        for itm in self._dlist[index + 1 : None]:
            itm.move(0, shift_y)
        for itm in self._marker_lst[index + 1 : None]:
            itm.move(0, shift_y)
        del itm

    # Move following items in parent.
    if self._parent:
        self._parent.new_size_for(self)

    # Update data and marker lists.
    if self.w_.fluid_ref(detach_l3_tree = True):
        self._l3tree[index].detach_from_parent_rec(self.w_.state_.storage)
    del self._dlist[index]
    del self._marker_lst[index]

    # Delete detached marker.
    destroy_if_last(marker)
    pass
l3aList.detach = detach


#
#***            l3Set / l3If / l3Call
def detach(self, child):
    assert(child is self._d_body)

    # Reparent graphical parts.
    child.reparent_to_root()

    # Update reference.
    self._d_body = None

    # Shift body parts.

    # Refresh decoration.
    self.refresh_deco()

    # Move following items in parent.
    if self._parent:
        self._parent.new_size_for(self)

    # Update tree data.
    if self.w_.fluid_ref(detach_l3_tree = True):
        fail()
        # _matcher['LBODY'] is fixed at match time -- manual editing
        # invalidates it.
        self._matcher['LBODY'].detach_from_parent_rec(self.w_.state_.storage)

    pass
l3IfLoop.detach = detach

def detach(self, child):
    # When drag distance from marker to item exceeds item_detach_distance,
    # detach the item.  This criterion is implemented in
    # l3Rawtext.drag_event.

    if child != self._d_cond:
        self.detach_only(child)
        return

    # Reparent graphical parts.
    child.reparent_to_root()

    # Update reference.
    self._d_cond = None

    # Shift body parts.
    self._cond_shift()

    # Refresh decoration.
    self.refresh_deco()

    # Move following items in parent.
    if self._parent:
        self._parent.new_size_for(self)

    # Update tree data.
    if self.w_.fluid_ref(detach_l3_tree = True):
        self._l3tree[0].detach_from_parent_rec(self.w_.state_.storage)

    pass
l3If.detach = detach


def detach(self, child):
    '''
    Detach the condition.
    '''
    # When drag distance from marker to item exceeds item_detach_distance,
    # detach the item.  This criterion is implemented in
    # l3Rawtext.drag_event.

    if child != self._d_cond:
        self.detach_only(child)
        return

    # Reparent graphical parts.
    child.reparent_to_root()

    # Update reference.
    self._d_cond = None

    # Shift body parts.
    self._cond_shift()

    # Refresh decoration.
    self.refresh_deco()

    # Move following items in parent.
    if self._parent:
        self._parent.new_size_for(self)

    # Update tree data.
    if self.w_.fluid_ref(detach_l3_tree = True):
        self._matcher.get_cond().detach_from_parent_rec(self.w_.state_.storage)
    pass
l3IfWhile.detach = detach


def detach(self, child):
    assert(child == self._pystr)

    # Reparent graphical parts.
    child.reparent_to_root()

    # Update reference.
    self._pystr = None

    # Shift body parts.

    # Refresh decoration.
    self.refresh_deco()

    # Move following items in parent.
    if self._parent:
        self._parent.new_size_for(self)

    # Update tree data.
    if self.w_.fluid_ref(detach_l3_tree = True):
        self._l3tree[0].detach_from_parent_rec(self.w_.state_.storage)

    pass
l3Inline.detach = detach

def detach(self, child):
    if child == self._d_func:
        ### print "detach: ", self

        # Reparent graphical parts.
        child.reparent_to_root()

        # Update reference.
        self._d_func = None

        # Shift body parts.
        self._func_shift()

        # Refresh decoration.
        self.refresh_deco()

        # Move following items in parent.
        if self._parent:
            self._parent.new_size_for(self)

        # Update tree data.
        if self.w_.fluid_ref(detach_l3_tree = True):
            self._l3tree[0].detach_from_parent_rec(self.w_.state_.storage)

    elif child == self._d_args:
        # aList children are only detached on deletion -- no shifting
        # or resizing

        # Reparent graphical parts.
        child.reparent_to_root()

        # Update reference.
        self._d_args = None
        self._update_refs()

        # Shift body parts.
        pass

        # Refresh decoration.
        pass

        # Move following items in parent.

        # Update tree data.
        if self.w_.fluid_ref(detach_l3_tree = True):
            self._l3tree[1].detach_from_parent_rec(self.w_.state_.storage)

    pass
l3Call.detach = detach

def detach(self, child):
    if child == self._d_lhs:
        # Reparent graphical parts.
        child.reparent_to_root()

        # Update reference.
        self._d_lhs = None

        # Shift body parts.

        # Update tree data.
        if self.w_.fluid_ref(detach_l3_tree = True):
            self._l3tree[0].detach_from_parent_rec(self.w_.state_.storage)

        # Show markers.
        self._d_lhs_marker.show()

    elif child == self._d_rhs:
        # Reparent graphical parts.
        child.reparent_to_root()

        # Update reference.
        self._d_rhs = None

        # Shift body parts.

        # Update tree data.
        if self.w_.fluid_ref(detach_l3_tree = True):
            self._l3tree[1].detach_from_parent_rec(self.w_.state_.storage)

        # Show markers.
        self._d_rhs_marker.show()

    else:
        raise DisplayError("Detaching invalid child.")

    ### print "detach: ", self

    # Refresh decoration.
    self.refresh_deco()

    # Move following items in parent.
    if self._parent:
        self._parent.new_size_for(self)
    pass
l3Set.detach = detach
#
#**        internal use only
#
# These types' children are never detached in editing; currently,
# these functions are only called for object destruction.
#
#***            l3List
def detach(self, child):
    # This function is for internal use, not editing -- yet.
    #
    if child != self._alist:
        raise DisplayError("Detaching invalid child.")

    ### print "detach: ", self

    # Reparent graphical parts.
    child.reparent_to_root()

    # Update reference.
    self._alist = None

    # Shift body parts.

    # Refresh decoration.
    pass

    # Move following items in parent.
    if self._parent:
        self._parent.new_size_for(self)

    # Update tree data.
    if self.w_.fluid_ref(detach_l3_tree = True):
        self._l3tree[0].detach_from_parent_rec(self.w_.state_.storage)

    pass
l3List.detach = detach
l3Map.detach = detach
l3Program.detach = detach

def detach(self, child):
    raise Exception("Native types cannot be detached.  Internal error.")
l3Native.detach = detach


#
#***            l3If
def detach_only(self, child):
    # Detach any child from self, without shifting, resizing, or
    # updating insertion markers.
    try:
        idx = self._d_elements.index(child)
    except ValueError:
        raise DisplayError("Detaching invalid child.")

    ### print "detach: ", self

    # Reparent graphical parts.
    child.reparent_to_root()

    # Update reference.
    self._d_elements[idx] = None
    self._update_refs()

    # Shift body parts.

    # Refresh decoration.
    self.refresh_deco()

    # Move following items in parent.

    # Update tree data.
    if self.w_.fluid_ref(detach_l3_tree = True):
        self._l3tree[idx].detach_from_parent_rec(self.w_.state_.storage)
l3If.detach_only = detach_only
l3IfWhile.detach_only = detach_only


#
#***            l3Function
def detach(self, child):
    if child == self._d_args:
        # Reparent graphical parts.
        child.reparent_to_root()

        # Update reference.
        self._d_args = None

        # Shift body parts.

        # Update tree data.
        if self.w_.fluid_ref(detach_l3_tree = True):
            self._l3tree[0].detach_from_parent_rec(self.w_.state_.storage)

    elif child == self._d_body:
        # Reparent graphical parts.
        child.reparent_to_root()

        # Update reference.
        self._d_body = None

        # Shift body parts.

        # Update tree data.
        if self.w_.fluid_ref(detach_l3_tree = True):
            self._l3tree[1].detach_from_parent_rec(self.w_.state_.storage)

    else:
        raise DisplayError("Detaching invalid child.")

    ### print "detach: ", self

    # Refresh decoration.
    pass

    # Move following items in parent.
    if self._parent:
        self._parent.new_size_for(self)
    pass
l3Function.detach = detach

#
#***            l3Loop
def detach(self, child):
    # Skeleton detach method, for compatibility only.
    # Because Loop is a macro, none of the toplevel children can be
    # edited; but detach is also needed for destruction.
    #
    # If detach is called for any child, it should be called for
    # all children.

    # Reparent graphical parts.
    child.reparent_to_root()

    # Update reference.

    # Shift body parts.

    # Refresh decoration.

    # Move following items in parent.

    # Update tree data.
    pass
l3Loop.detach = detach



#*    destroy etc: l3 "widget" destruction functions.
from copy import deepcopy, copy
from l3gui.misc import _safed
#
#**        destroy subtree and display
# .destroy() is recursive
#***            common hooks
def add_destroy_hook(self, func):
    self._destroy_hook.append(func)
l3Base.add_destroy_hook = add_destroy_hook
l3aList.add_destroy_hook = add_destroy_hook
Label.add_destroy_hook = add_destroy_hook
Widget.add_destroy_hook = add_destroy_hook
Image.add_destroy_hook = add_destroy_hook
uWidget.add_destroy_hook = add_destroy_hook

def _run_destroy_hook(self):
    for hook in self._destroy_hook:
        hook()
    del self._destroy_hook
l3Base._run_destroy_hook = _run_destroy_hook
l3aList._run_destroy_hook = _run_destroy_hook
Label._run_destroy_hook = _run_destroy_hook
Widget._run_destroy_hook = _run_destroy_hook
Image._run_destroy_hook = _run_destroy_hook
uWidget._run_destroy_hook = _run_destroy_hook

#
#***            Base
def destroy(self):
    self._root_group.hide()             # Much faster w/o display update.
    if self._vis_indic:
        self._vis_indic.destroy()
        self._vis_indic = None

    self._run_destroy_hook()
l3Base.destroy = destroy

#
#****                Nested
def destroy(self):
    l3Base.destroy(self)

    if self._container != self._parent:
        raise DisplayError("internal _container / _parent inconsistency.")

    if self._parent:
        self._parent.detach(self)
        self._parent = None

    # Headerless state.
    if self._header:
        _safed(self._header)
        self._header = None

    if self.w_.selector.get_selection() == self:
        self.w_.selector.unselect()

    self._marker = None
    self._container = None

    return True
l3Nested.destroy = destroy


#
#*****                    derived
def destroy(self):
    l3Nested.destroy(self)
    _safed(self._d_loop)
    _safed(self._d_body)
    l3Nested.destroy_deco(self)
l3IfLoop.destroy = destroy

def destroy(self):
    l3Nested.destroy(self)
    _safed(self._d_label)
    l3Nested.destroy_deco(self)
l3IfLoopBreak.destroy = destroy
l3IfLoopContinue.destroy = destroy

def destroy(self):
    l3Nested.destroy(self)

    _safed(self._d_if)
    _safed(self._d_cond)
    _safed(self._d_yes)
    _safed(self._d_else)
    _safed(self._d_no)
    ### self._l3tree.delete(self.w_.state_.storage)
    l3Nested.destroy_deco(self)
l3If.destroy = destroy

def destroy(self):
    l3Nested.destroy(self)
    _safed(self._d_loop)
    _safed(self._d_cond)
    _safed(self._d_body)
    l3Nested.destroy_deco(self)
l3IfWhile.destroy = destroy

def destroy(self):
    l3Nested.destroy(self)

    _safed(self._blank)
    l3Nested.destroy_deco(self)
Placeholder.destroy = destroy

def destroy(self):
    l3Nested.destroy(self)

    _safed(self._d_lhs)
    _safed(self._d_equal)
    _safed(self._d_rhs)
    ### self._l3tree.delete(self.w_.state_.storage)
    l3Nested.destroy_deco(self)
l3Set.destroy = destroy

def destroy(self):
    l3Nested.destroy(self)

    _safed(self._d_func)
    _safed(self._d_args)
    ### self._l3tree.delete(self.w_.state_.storage)
    l3Nested.destroy_deco(self)
l3Call.destroy = destroy

def destroy(self):
    l3Nested.destroy(self)

    _safed(self._d_function)
    _safed(self._d_args)
    _safed(self._d_body)

    ### self._l3tree.delete(self.w_.state_.storage)

    l3Nested.destroy_deco(self)
l3Function.destroy = destroy

def destroy(self):
    l3Nested.destroy(self)
    _safed(self._head)
    _safed(self._alist)
    ### self._l3tree.delete(self.w_.state_.storage)
    l3Nested.destroy_deco(self)
l3List.destroy = destroy
l3Map.destroy = destroy
l3Program.destroy = destroy

def destroy(self):
    l3Nested.destroy(self)
    _safed(self._head)
    _safed(self._pystr)
    l3Nested.destroy_deco(self)
l3Inline.destroy = destroy
l3Native.destroy = destroy

#
#***            independent
def destroy(self):
    self._root_group.hide()             # Much faster w/o display update.
    if self._parent:
        self._parent.detach(self)
        self._parent = None
        self._marker = None
        self._container = None

    if self.w_.selector.get_selection() == self:
        self.w_.selector.unselect()

    # Remove other's _marker references first.
    for entry in copy(self._dlist):    # _dlist changes during deletion
        _safed(entry)

    for mark in copy(self._marker_lst):
        _safed(mark)

    ### self._l3tree.delete(self.w_.state_.storage)

    _safed(self._root_group)
    del self._root_group

    self._run_destroy_hook()
    pass
l3aList.destroy = destroy


def destroy(self):
    # .destroy order checks.
    self._under_destruction = True

    self._canvas.remove_zoom_text(self)

    l3Base.destroy(self)

    # For structures, _marker and _container are set.
    # For nested text fragments (only editable via text editing),
    # _container and _marker are not set.
    inside_text = isinstance(self._parent, l3Rawtext)
    if not inside_text:
        if self._container != self._parent:
            raise DisplayError("internal _parent / _marker inconsistency.")

    if self._parent:
        if not inside_text:
            self._parent.detach(self)
        self._parent = None
        self._marker = None
        self._container = None

    # Headerless state.
    if self._header:
        _safed(self._header)
        self._header = None

    # if self._marker:
    #     self._container.detach(self)
    #     self._marker = None
    #     self._container = None

    if self.w_.selector.get_selection() == self:
        self.w_.selector.unselect()

    # Destroy children first.
    for ch in copy(self._subtrees):
        _safed(ch)

    ### self._l3tree.delete(self.w_.state_.storage)

    # Destroy the display elements.
    _safed(self._ltext)
    _safed(self._loutline)
    _safed(self._root_group)
    del self._root_group
    del self._ltext
    del self._loutline
l3Rawtext.destroy = destroy

def destroy(self):
    # .destroy order checks.
    self._under_destruction = True

    self._canvas.remove_zoom_text(self)

    l3Base.destroy(self)

    # Diff.
    if self._container != None:
        raise DisplayError("internal marker inconsistency.")

    # Diff.
    if self._parent:
        self._parent.detach_header(self)
        self._parent = None
        self._marker = None
        self._container = None

    if self.w_.selector.get_selection() == self:
        self.w_.selector.unselect()

    _safed(self._ltext)
    _safed(self._loutline)
    _safed(self._root_group)
    del self._root_group
    del self._ltext
    del self._loutline
l3Comment.destroy = destroy

def destroy(self):
    self._canvas.remove_zoom_text(self)
    _safed(self._loutline)
    _safed(self._ltext)
    _safed(self._root_group)

    self._run_destroy_hook()
Label.destroy = destroy

def destroy(self):
    _safed(self._ltext)
    _safed(self._root_group)
    self._run_destroy_hook()
Widget.destroy = destroy
Image.destroy = destroy
uWidget.destroy = destroy


#*    Saving and loading of state.
# The main entry points are `save_state` and `load_state`.
from types import IntType, LongType

from l3gui.misc import _get_props, _set_props

#
#**        saving
# 	The Canvas* instances are not extensible; either subclass them here for
# 	smooth persistence -- or use a dispatch table.
#
# 	Some of the more useful properties (width-units, char* version of
# 	fill-color) are write-only.  Subclasses could intercept and store
# 	them.
# 	Generally, mirroring properties in a derived class (or elsewhere)
# 	is not practical; many properties are changed via commands other
# 	than .set() and .set_property(), and those changes need to be
# 	observed.
#
# 	So, rather than mirroring much of the gtk object tree, use only
# 	attributes that are Read/Write, AND can be pickled if possible.
#
#   When absolutely necessary, provide special access functions for
#   specific properties.

#
#***            gtk widgets
def st_TextBuffer(item, prop_list):
    ## item.get_text(*item.get_bounds())
    return _get_props(
        item, prop_list,
        ## "tag-table",  # GtkTextTagTable : Read / Write / Construct Only
        # Not in pygtk
        ## "text",                          # gchararray : Read / Write
        )

def st_TextTag(item, prop_list):
    return _get_props(
        item, prop_list,
        ## "background",                    # gchararray : Write
        "background-full-height-set",    # gboolean : Read / Write
        "background-full-height",        # gboolean : Read / Write
        ## "background-gdk",                # GdkColor : Read / Write
        ## "background-set",                # gboolean : Read / Write
        ## "background-stipple",            # GdkPixmap : Read / Write
        ## "background-stipple-set",        # gboolean : Read / Write
        ## "direction",                  # GtkTextDirection : Read / Write
        "editable-set",                  # gboolean : Read / Write
        "editable",                      # gboolean : Read / Write
        "family-set",                    # gboolean : Read / Write
        "family",                        # gchararray : Read / Write
        "font",                          # gchararray : Read / Write
        ## "font-desc",              # PangoFontDescription : Read / Write
        ## "foreground",                    # gchararray : Write
        ## "foreground-gdk",                # GdkColor : Read / Write
        ## "foreground-set",                # gboolean : Read / Write
        ## "foreground-stipple",            # GdkPixmap : Read / Write
        ## "foreground-stipple-set",        # gboolean : Read / Write
        ## "indent",                        # gint : Read / Write
        ## "indent-set",                    # gboolean : Read / Write
        ## "invisible",                     # gboolean : Read / Write
        ## "invisible-set",                 # gboolean : Read / Write
        ## "justification",              # GtkJustification : Read / Write
        ## "justification-set",             # gboolean : Read / Write
        ## "language",                      # gchararray : Read / Write
        ## "language-set",                  # gboolean : Read / Write
        ## "left-margin",                   # gint : Read / Write
        ## "left-margin-set",               # gboolean : Read / Write
        ## "name",            # gchararray : Read / Write / Construct Only
        ## "paragraph-background",          # gchararray : Write
        ## "paragraph-background-gdk",      # GdkColor : Read / Write
        ## "paragraph-background-set",      # gboolean : Read / Write
        ## "pixels-above-lines",            # gint : Read / Write
        ## "pixels-above-lines-set",        # gboolean : Read / Write
        ## "pixels-below-lines",            # gint : Read / Write
        ## "pixels-below-lines-set",        # gboolean : Read / Write
        ## "pixels-inside-wrap",            # gint : Read / Write
        ## "pixels-inside-wrap-set",        # gboolean : Read / Write
        ## "right-margin",                  # gint : Read / Write
        ## "right-margin-set",              # gboolean : Read / Write
        ## "rise",                          # gint : Read / Write
        ## "rise-set",                      # gboolean : Read / Write
        ## "scale",                         # gdouble : Read / Write
        ## "scale-set",                     # gboolean : Read / Write
        ## "size",                          # gint : Read / Write
        "size-set",                     # gboolean : Read / Write
        "size-points",                  # gdouble : Read / Write
        ## "stretch",                       # PangoStretch : Read / Write
        ## "stretch-set",                   # gboolean : Read / Write
        ## "strikethrough",                 # gboolean : Read / Write
        ## "strikethrough-set",             # gboolean : Read / Write
        ## "style",                         # PangoStyle : Read / Write
        ## "style-set",                     # gboolean : Read / Write
        ## "tabs",                          # PangoTabArray : Read / Write
        ## "tabs-set",                      # gboolean : Read / Write
        ## "underline",                    # PangoUnderline : Read / Write
        ## "underline-set",                 # gboolean : Read / Write
        ## "variant",                       # PangoVariant : Read / Write
        ## "variant-set",                   # gboolean : Read / Write
        ## "weight",                        # gint : Read / Write
        ## "weight-set",                    # gboolean : Read / Write
        ## "wrap-mode",                     # GtkWrapMode : Read / Write
        ## "wrap-mode-set",                 # gboolean : Read / Write
        )

def st_TextView(item, prop_list):
    return _get_props(
        item, prop_list,
        "accepts-tab",                   # gboolean : Read / Write
        ## "buffer",                        # GtkTextBuffer : Read / Write
        "cursor-visible",                # gboolean : Read / Write
        "editable",                      # gboolean : Read / Write
        "indent",                        # gint : Read / Write
        ## "justification",              # GtkJustification : Read / Write
        "left-margin",                   # gint : Read / Write
        "overwrite",                     # gboolean : Read / Write
        "pixels-above-lines",            # gint : Read / Write
        "pixels-below-lines",            # gint : Read / Write
        "pixels-inside-wrap",            # gint : Read / Write
        "right-margin",                  # gint : Read / Write
        ## "tabs",                          # PangoTabArray : Read / Write
        ## "wrap-mode",                     # GtkWrapMode : Read / Write
        )

#
#***            canvas items
# List of readable & writeable display properties taken from the
# documentation.
#

#
#****                CanvasShape
def st_CanvasShape(item, prop_list):
    # The   fill-color-gdk is a C struct;
    #       fill-color-rgba is a (platform-dependent?) value
    # The width-units property is needed for scaling.
    return _get_props(
        item, prop_list,
        "cap-style",      # GdkCapStyle          : Read / Write
        "dash",           # gpointer             : Read / Write

        ## "fill-color",  # gchararray           : Write
        ## "fill-color-gdk", # GdkColor             : Read / Write
        "fill-color-rgba", # guint            : Read / Write

        "fill-stipple",   # GdkDrawable          : Read / Write
        "join-style",     # GdkJoinStyle         : Read / Write
        "miterlimit",     # gdouble              : Read / Write

        ## "outline-color", # gchararray         : Write
        ## "outline-color-gdk", # GdkColor          : Read / Write
        "outline-color-rgba", # guint         : Read / Write

        "outline-stipple", # GdkDrawable         : Read / Write
        "width-pixels",   # guint                : Read / Write
        # Do not use scaled units.
        ## "width-units",           # gdouble              : Write
        "wind",           # guint                : Read / Write
        )

#
#****                CanvasRE
def st_CanvasRE(item, prop_list):
    return _get_props(item, prop_list,
               "x1",             # gdouble              : Read / Write
               "x2",             # gdouble              : Read / Write
               "y1",             # gdouble              : Read / Write
               "y2",             # gdouble              : Read / Write
               )
    # # Ignore these renderer-specific properties.
    # # return st_CanvasShape(item, prop_list)

#
#****                CanvasPolygon
def st_CanvasPolygon(w_, item, prop_list, creator = None):
    if creator:
        prop_list.append( ("points", creator._marker_points[item]) )
    else:
        ### This fails because of a bug in
        ###     gnome_canvas_polygon_get_property().
        raise DisplayError("st_CanvasPolygon(internal): "
                           "Missing creator arg.")
        _get_props(item, prop_list,
                   "points",            # GnomeCanvasPoints : Read / Write
                   )

    if w_.fluid_ref(dump_ps = False):
        pts = creator._marker_points[item]
        i2w = item.i2w

        print "% st_CanvasPolygon"
        if len(pts) >=2 :
            print "    %s %s gcmoveto" % i2w(pts[0], pts[1])
            for pt1, pt2 in zip(pts[2::2], pts[3::2]):
                print "    %s %s gclineto" % i2w(pt1, pt2)
            print "    stroke"

    return st_CanvasShape(item, prop_list)

#
#****                CanvasRect
def st_CanvasRect(w_, item, prop_list):
    return st_CanvasRE(item, prop_list)

#
#****                CanvasGroup
def st_CanvasGroup(item, prop_list):
    return _get_props(item, prop_list,
                      "x",      # gdouble              : Read / Write
                      "y",      # gdouble              : Read / Write
                       )
#
#****                CanvasText
def st_CanvasText(w_, item, prop_list):
    if w_.fluid_ref(dump_ps = False):
        get = item.get_property
        i2w = item.i2w
        print "% st_CanvasText"
        x1, y1, _, _ = item.get_bounds()
        print "    %s %s gcmoveto" % i2w(x1, y1)
        print '    (%s)  gcshow' % (get("text"))

    return _get_props(
        item, prop_list,
        "anchor",                # GtkAnchorType        : Read / Write
        ## None fails to restore.
        ## "attributes",            # PangoAttrList        : Read / Write
        "clip",                  # gboolean             : Read / Write
        "clip-height",           # gdouble              : Read / Write
        "clip-width",            # gdouble              : Read / Write
        "family-set",            # gboolean             : Read / Write
        "family",                # gchararray           : Read / Write
        "fill-color",            # gchararray           : Read / Write
        ## "fill-color-gdk",        # GdkColor             : Read / Write
        "fill-color-rgba",       # guint                : Read / Write
        "fill-stipple",          # GdkDrawable          : Read / Write
        "font",                  # gchararray           : Read / Write
        ## "font-desc",             # PangoFontDescription : Read / Write
        "justification",         # GtkJustification     : Read / Write
        ## "markup",                       # gchararray           : Write
        "rise-set",              # gboolean             : Read / Write
        "rise",                  # gint                 : Read / Write
        "scale-set",             # gboolean             : Read / Write
        "scale",                 # gdouble              : Read / Write
        "size",                  # gint                 : Read / Write
        "size-set",              # gboolean             : Read / Write
        "size-points",           # gdouble              : Read / Write
        "stretch-set",           # gboolean             : Read / Write
        "stretch",               # PangoStretch         : Read / Write
        "strikethrough-set",     # gboolean             : Read / Write
        "strikethrough",         # gboolean             : Read / Write
        "style-set",             # gboolean             : Read / Write
        "style",                 # PangoStyle           : Read / Write
        "text",                  # gchararray           : Read / Write
        #
        # property text-height is not writable text-height 2.28
        # property text-width is not writable text-width 1.68
        ## "text-height",           # gdouble              : Read / Write
        ## "text-width",            # gdouble              : Read / Write
        "underline-set",         # gboolean             : Read / Write
        "underline",             # PangoUnderline       : Read / Write
        "variant-set",           # gboolean             : Read / Write
        "variant",               # PangoVariant         : Read / Write
        "weight-set",            # gboolean             : Read / Write
        "weight",                # gint                 : Read / Write
        "x",                     # gdouble              : Read / Write
        "x-offset",              # gdouble              : Read / Write
        "y",                     # gdouble              : Read / Write
        "y-offset",              # gdouble              : Read / Write
        )

#
#****                CanvasLine
def st_CanvasLine(w_, item, prop_list):
    if w_.fluid_ref(dump_ps = False):
        pts = item.get_property("points")
        i2w = item.i2w

        print "% st_CanvasLine"
        if len(pts) >=2 :
            print "    %s %s gcmoveto" % i2w(pts[0], pts[1])
            for pt1, pt2 in zip(pts[2::2], pts[3::2]):
                print "    %s %s gclineto" % i2w(pt1, pt2)
            print "    stroke"

    return _get_props(
        item, prop_list,
        "arrow-shape-a",         # gdouble              : Read / Write
        "arrow-shape-b",         # gdouble              : Read / Write
        "arrow-shape-c",         # gdouble              : Read / Write
        "cap-style",             # GdkCapStyle          : Read / Write
        "fill-color",            # gchararray           : Read / Write
        ## "fill-color-gdk",        # GdkColor             : Read / Write
        "fill-color-rgba",       # guint                : Read / Write
        "fill-stipple",          # GdkDrawable          : Read / Write
        "first-arrowhead",       # gboolean             : Read / Write
        "join-style",            # GdkJoinStyle         : Read / Write
        "last-arrowhead",        # gboolean             : Read / Write
        "line-style",            # GdkLineStyle         : Read / Write
        "points",                # GnomeCanvasPoints    : Read / Write
        "smooth",                # gboolean             : Read / Write
        "spline-steps",          # guint                : Read / Write
        ## "width-pixels",          # guint                : Read / Write
        "width-units",           # gdouble              : Read / Write
        )

#
#****                CanvasWidget
def st_CanvasWidget(item, prop_list):
    return _get_props(
        item, prop_list,
        ## "anchor",                       # GtkAnchorType : Read / Write
        "height",                       # gdouble : Read / Write
        "size-pixels",                  # gboolean : Read / Write
        ## "widget",                       # GtkWidget : Read / Write
        "width",                        # gdouble : Read / Write
        "x",                            # gdouble : Read / Write
        "y",                            # gdouble : Read / Write
        )

#
#***            local classes
#
# Originally, the full display state was saved via recursion of the
# display tree.  This turned out to be a bad idea for file size,
# reliabilty, and made using old state files impossible (just like a
# well-known word processor).
#
# Now, only the position information of the topmost object is
# collected.
#
# The traversal used and the data structuring may be
# useful in other contexts (e.g., export to PDF), so the code is
# retained here.  Some original notes:
#
#     Pickling highly intertwined (graph-structured) state is tricky.  The
#     get_state() members are arranged to produce a spanning tree traversal of
#     all data and avoid circularities.
#
#     For trees, starting pickling in a subtree and (randomly) getting the
#     parents is bad for pickling and reconstruction.
#
#     Also, there is some special state, the global
#         w_ == w_
#     and multiple upward references like
#         self._canvas  (circular -- parent)
#
#     This special state is saved only once, and the links restored in a
#     separate pass.
#
def get_state(self):
    #
    st = utils.Shared(
        _root_group = st_CanvasGroup(self._root_group, []),
        )
    return st
l3aList.get_state = get_state
Label.get_state = get_state
Widget.get_state = get_state
Image.get_state = get_state
uWidget.get_state = get_state

def get_state(self):
    return utils.Shared(
        # # _l3tree     = self._l3tree,
        _root_group = st_CanvasGroup(self._root_group, []),
        )
l3Base.get_state = get_state

def get_state(self):
    return utils.Shared(
        l3base         = l3Base.get_state(self),
        # # _root_group    = st_CanvasGroup(self._root_group, []),
        )
l3Rawtext.get_state = get_state

def get_state(self):
    return utils.Shared(
        l3base = l3Base.get_state(self),
        )
l3Nested.get_state = get_state


def get_state(self):
    return None
CommonPopup.get_state = get_state
#
#***            table formation
#
def start_set_tables(l3tree):
    '''
    Fill tables with values stored as attributes of an astType tree
    `l3tree`.  Return a table of tables.
    '''
    from l3gui.l3canvas import RenderTable
    tables = utils.Shared(rentab = RenderTable())
    l3tree.set_tables(tables)
    return tables
#
# These functions are intended to fully traverse the (display) widget
# tree, and may be split into traversal iterators / main functions
# later.
#
def set_tables(self, tables):
    l3Base.set_tables(self, tables)
    tables.rentab.set_state(self._l3tree, self._render_mode)
l3Nested.set_tables = set_tables


def set_tables(self, tables):
    l3Base.set_tables(self, tables)
    tables.rentab.set_state(self._l3tree, self._render_mode)
    for st in self._subtrees:
        st.set_tables(tables)
l3Rawtext.set_tables = set_tables

def set_tables(self, tables):
    l3Rawtext.set_tables(self, tables)
l3Comment.set_tables = set_tables

def set_tables(self, tables):
    l3List.set_tables(self, tables)
    self._alist.set_tables(tables)
l3ViewList.set_tables = set_tables


def set_tables(self, tables):
    l3Nested.set_tables(self, tables)
    self._alist.set_tables(tables)
l3List.set_tables = set_tables
l3Map.set_tables = set_tables


def set_tables(self, tables):
    l3Nested.set_tables(self, tables)
    (_d_cond,
     _d_yes ,
     _d_no  ,
     _,
     _,
     _,
     ) = self._d_elements
    _d_cond.set_tables(tables)
    _d_yes .set_tables(tables)
    _d_no  .set_tables(tables)
l3If.set_tables = set_tables


def set_tables(self, tables):
    l3Nested.set_tables(self, tables)
    ( d_loop,
      d_body, ) = self._d_elements
    d_loop.set_tables(tables)
    d_body.set_tables(tables)
l3IfOutline.set_tables = set_tables
l3IfLoop.set_tables = set_tables


def set_tables(self, tables):
    l3Nested.set_tables(self, tables)
    (d_cond,
     d_body,
     _,
     _,
     ) = self._d_elements
    d_cond.set_tables(tables)
    d_body.set_tables(tables)
l3IfWhile.set_tables = set_tables


def set_tables(self, tables):
    l3Nested.set_tables(self, tables)
    (  _d_func,
       _d_args,
       _, 
       _,
       ) = self._d_elements
    _d_func.set_tables(tables)
    _d_args.set_tables(tables)
l3Call.set_tables = set_tables


def set_tables(self, tables):
    l3Nested.set_tables(self, tables)
    self._alist.set_tables(tables)
l3Program.set_tables = set_tables


def set_tables(self, tables):
    l3Nested.set_tables(self, tables)
    self._pystr.set_tables(tables)
l3Inline.set_tables = set_tables


def set_tables(self, tables):
    l3Nested.set_tables(self, tables)
    self._d_function.set_tables(tables)
    self._d_args.set_tables(tables)
    self._d_body.set_tables(tables)
l3Function.set_tables = set_tables


def set_tables(self, tables):
    l3Nested.set_tables(self, tables)
    self._d_lhs.set_tables(tables)
    self._d_rhs.set_tables(tables)
l3Set.set_tables = set_tables


def set_tables(self, tables):
    l3Nested.set_tables(self, tables)
Placeholder.set_tables = set_tables
l3IfLoopBreak.set_tables = set_tables
l3IfLoopContinue.set_tables = set_tables
l3Native.set_tables = set_tables


def set_tables(self, tables):
    pass
Label.set_tables = set_tables
Widget.set_tables = set_tables
Image.set_tables = set_tables
uWidget.set_tables = set_tables
l3Base.set_tables = set_tables
l3Deco.set_tables = set_tables


def set_tables(self, tables):
    for itm in self._dlist:
        itm.set_tables(tables)
l3aList.set_tables = set_tables


#
#***            find_root
def find_root(self):
    if self._parent != None:
        return self._parent.find_root()
    else:
        return self, self._l3tree._id
l3Base.find_root = find_root
l3aList.find_root = find_root
#
#**        loading
# The reconstruction of the data structures is done in passes:
#   - form the spanning tree using loaded data
#   - reconnect graph edges.

#
#***            l3aList
def set_state(self, state):
    _set_props(self._root_group, state._root_group)
l3Base.set_state = set_state
l3aList.set_state = set_state
Label.set_state = set_state
Widget.set_state = set_state
Image.set_state = set_state
uWidget.set_state = set_state

def set_state(self, state):
    l3Base.set_state(self, state.l3base)
    # # _set_props(self._root_group, state._root_group)
l3Rawtext.set_state = set_state

def set_state(self, state):
    l3Base.set_state(self, state.l3base)
l3Nested.set_state = set_state

#
#***            CommonPopup
def set_state(self, state):
    pass
CommonPopup.set_state = set_state


#*    l3IfFor members
# An experiment of grouping the members with the class definition,
# instead of functional grouping.
# As expected, this class grouping makes function-based additions much
# more difficult.

def continue_stop_pt(self):
    # todo.
    lc, tc, rc, bc = self._d_loop.get_bounds_world()
    return lc + (rc - lc)/10, bc        ## parameters
l3IfFor.continue_stop_pt = continue_stop_pt

def break_stop_pt(self):
    # todo.
    lc, tc, rc, bc = self.get_bounds_world()
    return rc + 4, bc                   ## parameters
l3IfFor.break_stop_pt = break_stop_pt


def __init__(self, w_, cnvs, tree_id, rentab, matcher):
    # l3 tree.
    l3tree = w_.state_.storage.load(tree_id)
    assert isinstance(l3tree, ast.If)
    l3Nested.__init__(self, w_, cnvs, l3tree, rentab)

    # Bindings to keep.
    self.w_      = w_
    self._canvas = cnvs
    self._marker_points = {}            # marker -> point list
    self._matcher = ma = matcher
    self._l3tree = l3tree
    self._deco_cb_buffer = []           # callback list for add_item()

    #
    # Display elements.
    #   -- for
    d_loop_for = Label(w_, cnvs, self._root_group, "for")

    #   -- V
    d_cond_V = cnvs.add_l3tree(ma.get_current_V(), rentab)
    d_cond_V.reparent(self)

    #   -- in
    d_loop_in = Label(w_, cnvs, self._root_group, "in")

    #   -- SEQ
    d_cond_SEQ = cnvs.add_l3tree(ma.get_current_SEQ(), rentab)
    d_cond_SEQ.reparent(self)

    #   -- B
    d_body = l3aList(w_, cnvs,
                     ma.get_B(),
                     rentab,
                     root_group = self._root_group,
                     draggable = False,
                     parent = self,
                     )

    # Marker(s).
    d_cond_V_marker = self.draw_marker()
    d_cond_V_marker.lower_to_bottom()

    d_cond_SEQ_marker = self.draw_marker()
    d_cond_SEQ_marker.lower_to_bottom()

    # Inform obj of marker.
    d_cond_V.setup_marker(d_cond_V_marker, self)
    d_cond_SEQ.setup_marker(d_cond_SEQ_marker, self)

    # Bindings to keep.
    # Indexed access.  Match l3tree indexing where applicable.
    self._d_elements = [
        # l3 ast.
        d_cond_V,
        d_cond_SEQ,
        d_body,

        # Rest
        d_loop_for,
        d_loop_in,
        d_cond_V_marker,
        d_cond_SEQ_marker,
        ]
    self._update_refs()

    #
    # Align display elements, starting from d_loop_for
    #
    self._align_display()

    # Highlighting.
    self._outline = None

    # Item cross-referencing.
    self._marker = None
    self._container = None

    # Event handling.
    d_cond_V_marker.connect("event", lambda *a: self.insert_event(*a))
    d_cond_SEQ_marker.connect("event", lambda *a: self.insert_event(*a))

    # Borders etc.
    self.init_header(rentab)
    self.init_deco(rentab)

    pass
l3IfFor.__init__ = __init__

def _update_refs(self):
    (
        self._d_cond_V,
        self._d_cond_SEQ,
        self._d_body,

        self._d_loop_for,
        self._d_loop_in,
        self._d_cond_V_marker,
        self._d_cond_SEQ_marker,
        ) = self._d_elements
l3IfFor._update_refs = _update_refs


def _align_display(self):
    flush_events()
    #
    #  Align w.r.t. 'for'
    #
    (
        d_cond_V,
        d_cond_SEQ,
        d_body,

        d_loop_for,
        d_loop_in,
        d_cond_V_marker,
        d_cond_SEQ_marker,
        ) = self._d_elements

    w_ = self.w_
    #   -- V
    l_for, t_for, r_for, b_for = d_loop_for.get_bounds()
    l_v, t_v, r_v, b_v = d_cond_V.get_bounds()
    d_cond_V.move(r_for + w_.cp_.loop_cond_sep - l_v,
                  t_for - t_v)
    #   marker for child replacement.
    l_v, t_v, r_v, b_v = d_cond_V.get_bounds()
    lma, tma,   _,   _ = d_cond_V_marker.get_bounds()
    d_cond_V_marker.move(l_v - lma + w_.cp_.exp_marker_hoff,
                         t_v - tma + w_.cp_.exp_marker_voff)

    #   -- in
    l_v, t_v, r_v, b_v = d_cond_V.get_bounds()
    l_in, t_in, r_in, b_in = d_loop_in.get_bounds()
    d_loop_in.move(r_v + w_.cp_.loop_cond_sep - l_in,
                   t_v - t_in)

    #   -- SEQ
    l_seq, t_seq, r_seq, b_seq = d_cond_SEQ.get_bounds()
    l_in, t_in, r_in, b_in = d_loop_in.get_bounds()
    d_cond_SEQ.move(r_in + w_.cp_.loop_cond_sep - l_seq,
                    t_in - t_seq)
    # marker
    l_seq, t_seq, r_seq, b_seq = d_cond_SEQ.get_bounds()
    lma, tma,   _,   _ = d_cond_SEQ_marker.get_bounds()
    d_cond_SEQ_marker.move(l_seq - lma + w_.cp_.exp_marker_hoff,
                           t_seq - tma + w_.cp_.exp_marker_voff)

    #   -- B
    l_v, t_v, r_v, b_v = d_cond_V.get_bounds()
    l_seq, t_seq, r_seq, b_seq = d_cond_SEQ.get_bounds()
    l_b, t_b, r_b, b_b = d_body.get_bounds()
    d_body.move(l_for - l_b,
                max(b_v, b_seq) - t_b)
l3IfFor._align_display = _align_display


def hide(self):
    self._root_group.hide()
    self._d_loop_for.hide()
    self._d_loop_in.hide()
    self._d_cond_V.hide()
    self._d_cond_SEQ.hide()
    self._d_body.hide()
l3IfFor.hide = hide


def insert(self, marker, newobj):
    #
    # Insert new value or sequence
    #
    assert isinstance(newobj, l3Base)

    # Avoid special cases:
    if newobj.contains_recursive(self):
        self.w_.ten_second_message("Cannot insert if into itself.")
        return

    if newobj._canvas != self._canvas:
        self.w_.ten_second_message("Cannot move objects across displays.  "
                                   "Copy instead.")
        return

    # Which marker?
    if marker == self._d_cond_V_marker:
        # Validate slot.
        if not self._matcher.get_current_V().eql(ast.aNone()):
            raise DisplayError("Insertion in occupied slot.")

        # Update reference.
        self._d_cond_V = d_cond_V = newobj

        # Reparent object.
        newobj.reparent(self)

        # Position COND.
        flush_events()

        # Reposition rest.
        self._align_display()

        # Refresh decoration.
        self.refresh_deco()

        # Move following items in parent.
        if self._parent:
            self._parent.new_size_for(self)

        # Update tree data.
        if self.w_.fluid_ref(insert_l3_tree = True):
            self._matcher.set_V(newobj._l3tree)

        # Inform obj of marker.
        newobj.setup_marker(self._d_cond_V_marker, self)

    elif marker == self._d_cond_SEQ_marker:
        # Validate slot.
        if not self._matcher.get_current_SEQ().eql(ast.aNone()):
            raise DisplayError("Insertion in occupied slot.")

        # Update reference.
        self._d_cond_SEQ = d_cond_SEQ = newobj

        # Reparent object.
        newobj.reparent(self)

        # Position COND.
        flush_events()

        # Reposition rest.
        self._align_display()

        # Refresh decoration.
        self.refresh_deco()

        # Move following items in parent.
        if self._parent:
            self._parent.new_size_for(self)

        # Update tree data.
        if self.w_.fluid_ref(insert_l3_tree = True):
            self._matcher.set_SEQ(newobj._l3tree)

        # Inform obj of marker.
        newobj.setup_marker(self._d_cond_SEQ_marker, self)

    else:
        raise DisplayError("Insertion from wrong marker.")
l3IfFor.insert = insert


def init_deco(self, rentab):
    # Decorate body.
    self._deco = None ## add_deco_to(self, callbacks = self._deco_cb_buffer)
l3IfFor.init_deco = init_deco


def new_size_for(self, child):
    #
    # Shift contents vertically as needed.
    #
    assert isinstance(child, (l3Base, l3aList))

    # Find and apply shift.
    if child in [self._d_cond_V, self._d_cond_SEQ]:
        self._align_display()

    # Refresh decoration.
    self.refresh_deco()

    # Propagate.
    if self._parent:
        self._parent.new_size_for(self)
    return
l3IfFor.new_size_for = new_size_for


def detach(self, child):
    # When drag distance from marker to item exceeds item_detach_distance,
    # detach the item.  This criterion is implemented in
    # l3Rawtext.drag_event.

    if child == self._d_cond_V:
        # Update reference.
        self._d_cond_V = None
        the_child = self._matcher.get_current_V()

    elif child == self._d_cond_SEQ:
        # Update reference.
        self._d_cond_SEQ = None
        the_child = self._matcher.get_current_SEQ()

    else:
        self.detach_only(child)
        return

    # Reparent graphical parts.
    child.reparent_to_root()

    # Shift body parts.
    # self._align_display()

    # Refresh decoration.
    self.refresh_deco()

    # Move following items in parent.
    if self._parent:
        self._parent.new_size_for(self)

    # Update tree data.
    if self.w_.fluid_ref(detach_l3_tree = True):
        the_child.detach_from_parent_rec(self.w_.state_.storage)
    pass
l3IfFor.detach = detach


def destroy(self):
    l3Nested.destroy(self)
    _safed(self._d_loop_for)
    _safed(self._d_loop_in)
    _safed(self._d_cond_SEQ)
    _safed(self._d_cond_V)
    _safed(self._d_body)
    l3Nested.destroy_deco(self)
l3IfFor.destroy = destroy


def set_tables(self, tables):
    l3Nested.set_tables(self, tables)
    (
        d_cond_V,
        d_cond_SEQ,
        d_body,
        _,
        _,
        _,
        _,
        ) = self._d_elements
    d_cond_V.set_tables(tables)
    d_cond_SEQ.set_tables(tables)
    d_body.set_tables(tables)
l3IfFor.set_tables = set_tables


l3IfFor.detach_only = detach_only
l3IfFor.draw_marker = draw_marker_shared
l3IfFor.insert_event = insert_event_shared
l3IfFor.detach_only = detach_only

