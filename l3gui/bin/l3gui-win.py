#!/usr/bin/env python 
# -*- python -*-
# 
# Start the l3 gui from the command line.
# 
import pdb, os, sys
from optparse import OptionParser


def main():
    usage = "%prog [options]"
    parser = OptionParser(usage, version = "0.4")

    parser.add_option("-f", "--file", dest = 'file', type = "string",
                      help = "Name of l3 script to load.")

    parser.add_option("-s", "--statefile", type = "string",
                      help = "Name of l3gui state file to load.")

    parser.add_option("-i", "--interactive", action="store_true",
                      help = "Go to Python console on exit.  This "
                      "allows moving between Python and the gui.  To "
                      "restart the gui, use gtk.main()")

    (options, args) = parser.parse_args()
    
    if len(args) != 0:
        parser.print_help()
        sys.exit(1)
    
    from l3gui import main, misc
    import gtk

    if options.file:
        def do():
            print "Loading %s" % options.file
            misc.load_script(main.w_, options.file)
            return False
        gobject.timeout_add(2000, do)

    if options.statefile:
        def do():
            print "Loading %s" % options.statefile
            misc.load_state(main.w_, options.statefile)
            return False
        gobject.timeout_add(2000, do)

    misc.flush_events()
    main.gtk.main()

    if options.interactive:
        from code import InteractiveConsole
        import readline
        from pdb import pm
        ic_dict = locals()
        ic = InteractiveConsole(ic_dict)
        ic.interact("==== Python console                  ====\n"
                    "==== exit with end-of-file (ctrl-d)  ====\n"
                    "==== restart gui via main.gtk.main() ====")

if __name__ == "__main__":
    main()
